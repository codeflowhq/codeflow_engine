/**
 * Check if an object is a stream.
 *
 * @TODO - find a better alternative.
 */
module.exports.isStream = function(stream) {
  return stream !== null &&
    typeof stream === 'object' &&
    typeof stream.pipe === 'function';
}

module.exports.isWritableStream = function(stream) {
  return module.exports.isStream(stream) &&
    stream.writable !== false &&
    typeof stream._write === 'function' &&
    typeof stream._writableState === 'object';
}

module.exports.isReadableStream = function(stream) {
  return module.exports.isStream(stream) &&
    stream.readable !== false &&
    typeof stream._read === 'function' &&
    typeof stream._readableState === 'object';
}

module.exports.isDuplexStream = function(stream) {
  return module.exports.isWritableStream(stream) &&
    module.exports.isStream.isReadableStream(stream);
}

module.exports.isTransformStream = function(stream) {
  return module.exports.isDuplexStream(stream) &&
    typeof stream._transform === 'function' &&
    typeof stream._transformState === 'object'
}
