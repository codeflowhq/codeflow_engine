/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var _ = require('underscore')

const fileStore = require('session-file-store')
const memoryStore = require('memorystore')

var makeError = function() {
    return new Error("vars.__httpServer not found. getSession module should be placed under the same instance or a child instance under trigger module core/httpServer")
}

class Session {

    /**
     * Middleware for fileStore
     * @param {*} input
     * @param {*} callback
     */
    fileStore(input, callback) {
        let {expressSession, config} = input || {}
        let FileStore = fileStore(expressSession)
        try {
            let store = new FileStore(config)
            callback(null, store)
        } catch(e) {
            callback(e)
        }

    }
/**
     * Middleware for memoryStore
     * @param {*} input
     * @param {*} callback
     */
    memoryStore(input, callback) {
        let {expressSession, config} = input || {}
        let MemoryStore = memoryStore(expressSession)
        try {
            let store = new MemoryStore(config)
            callback(null, store)
        } catch(e) {
            callback(e)
        }
    }

    getSession(input, callback, options) {
        var server = options.vars.__httpServer
        if (!server) {
            return callback(makeError())
        }
        var session = server.request.session || {}
        if(!session) {
            return callback('Session not initialised')
        }
        callback(null, session[input.key])
    }

    setSession(input, callback, options) {
        var server = options.vars.__httpServer
        if (!server) {
            return callback(makeError())
        }
        var session = server.request.session
        if(!session) {
            return callback('Session not initialised')
        }
        _.each(input.data, entry=> {
            session[entry.key] = entry.value
            if (input.expires) {
                session.expires = input.expires
            }
        })
        callback()
    }

    removeSession(input, callback, options) {
        var server = options.vars.__httpServer
        if (!server) {
            return callback(makeError())
        }
        var session = server.request.session
        if(!session) {
            return callback('Session not initialised')
        }
        session[input.key] = null
        callback()
    }

}
module.exports = new Session()