{
  "source": "../http.js",
  "method": "renderInline",
  "input-schema": {
    "type": "object",
    "additionalProperties": false,
    "properties": {
      "template": {
        "type": "string",
        "description": "An EJS template string.",
        "format": "multiline"
      },
      "data": {
        "label": "Data",
        "type": "array",
        "items": {
          "type": "object",
          "title": "data",
          "properties": {
            "key": {
              "label": "Key",
              "type": "string"
            },
            "value": {
              "label": "Value",
              "type": "any"
            }
          }
        }
      },
      "options": {
        "type": "object",
        "displayOptions": {
          "showCollapsed": true
        },
        "properties": {
          "root": {
            "type": "string",
            "format": "filechooser",
            "subformat": "folder",
            "description": "The path to the project root. When this is set, absolute paths for includes (/filename.ejs) will be relative to the project root"
          },
          "filename": {
            "type": "string",
            "description": "The filename of the template. Required for inclusion and caching"
          },
          "cache": {
            "type": "boolean",
            "description": "Whether or not to enable caching of template functions. Requires `filename` to be set."
          },
          "delimiter": {
            "type": "string",
            "description": "Character to use with angle brackets for open/close"
          },
          "openDelimiter": {
            "type": "string",
            "description": "The opening delimiter for all statements. This allows you to clearly delinate the difference between template code and existing delimiters. (It is recommended to synchronize this with the closeDelimiter property.)"
          },
          "closeDelimiter": {
            "type": "string",
            "description": "The closing delimiter for all statements."
          },
          "views": {
            "type": "array",
            "description": "An array of paths to use when resolving includes with relative paths",
            "items": {
              "type": "string",
              "format": "filechooser",
              "title": "viewPath",
              "subformat": "folder"
            }
          },
          "debug": {
            "type": "boolean",
            "description": "Log the generated JavaScript source for the EJS template to the console."
          },
          "rmWhitespace": {
            "type": "boolean",
            "description": "Remove all safe-to-remove whitespace, including leading and trailing whitespace"
          }
        }
      }
    }
  },
  "output-schema": {
    "type": "object",
    "properties": {
      "html": {
        "type": "string"
      }
    }
  },
  "meta": {
    "name": "renderEJS",
    "description": "Returns an HTML by rendering an EJS template string"
  }
}