/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

const http = require('http')
const https = require('https')
const path = require('path')
const fs = require('fs')
const util = require('util')
const cluster = require('cluster')
const os = require('os')
const express = require('express')
const expressSession = require('express-session')
const ejs = require('ejs')
const compression = require('compression')
const favicon = require('serve-favicon')
const httpUtil = require('./util')
const { Stream } = require('stream')

module.exports.server = function(input, callback, options) {
    input = input || {}
    if (input.advanced && input.advanced.clustering && cluster.isMaster) {
        //initServer(input, callback, options)
        initCluster(input)
    } else {
        new HttpServer(input, options, (err, data, data2)=>[
            callback(err, data, data2)
        ]).initServer().then(() => {
        }, error => {
            console.error(error)
            callback(error)
        })
    }
}

function initCluster(input) {
    console.log(`Cluster with master pid: ${process.pid} is running`)

    const onWorkerMessage = (message, worker) => {
        //console.log(`Master ${process.pid} recevies message '${JSON.stringify(message)}' from worker ${worker.process.pid}`)
    }

    const numCPUs = input && input.numWorkers || 4 //os.cpus().length
    for (let i = 0; i < numCPUs; i++) {
        let worker = cluster.fork({ ENV_CLUSTER_WORKER: true })
        worker.on('message', message => {
            onWorkerMessage(message, worker)
        })
    }

    process.on('message', message => {
        if (message && message === 'disconnect-cluster' && cluster) {
            cluster.disconnect()
        }
    })

    //listen for worker process exists
    cluster.on('exit', (worker, code, signal) => {
        //don't create new worker process if the signal is empty(when the master process disconnects)
        if (!signal || signal === 'SIGKILL') {
            return
        }

        console.log('Starting a new worker')
        let _worker = cluster.fork({ ENV_CLUSTER_WORKER: true })
        _worker.on('message', message => {
            onWorkerMessage(message, _worker)
        })
    })
}

module.exports.sendResponse = function(input, callback, options) {
    input = input || {}
    const advanced = input.advanced || {}
    let app = input.httpConnection || options.vars.__httpServer
    let content = input.content || ""
    let response = app.response
    response.status(input.status || 200)
    if (advanced.charset) {
        response.charset = advanced.charset
    }
    input.headers && input.headers.forEach(header => {
        response.setHeader(header.key, header.value)
    })
    switch (input.type) {
        case 'html':
            if (!response.getHeader('Content-Type')) {
                response.setHeader("Content-Type", "text/html")
            }
            response.send(content)
            break
        case 'plain':
            if (!response.getHeader('Content-Type')) {
                response.setHeader("Content-Type", "text/plain")
            }
            response.send(content)
            break
        case 'json':
            response.json(content)
            break
        case 'jsonp':
            response.jsonp(content)
            break
        default:
            if (input.contentType) {
                response.setHeader('Content-Type', input.contentType)
            }
            if (httpUtil.isStream(content)) {
                //pipe content(stream) to response
                return Stream.pipeline(content, response, (err) => {
                    callback(err, { success: true })
                })
            }
            response.send(content)
            break
    }
    return callback(null, {
        success: true
    })
}

module.exports.redirect = function(input, callback, options) {
    input = input || {}
    let app = input.httpConnection || options.vars.__httpServer
    let res = app.response
    const statusCode = input.statusCode || 302

    const redirect = () => {
        res.redirect(statusCode, input.url)
        callback(null, {success: true})
    }

    if(input.saveSession) {
        if(app.request.session && app.request.session.save) {
            app.request.session.save()
        }
        setTimeout(redirect, 200)
    } else {
        redirect()
    }
}

module.exports.renderView = function(input, callback, options) {
    input = input || {}
    let app = input.httpConnection || options.vars.__httpServer
    let res = app.response
    input.headers && input.headers.forEach(header => {
        res.setHeader(header.key, header.value)
    })

    let templateData = {}
    input.data && input.data.forEach(item => {
        templateData[item.key] = item.value
    })
    if (input.template == "ejs") {
        res.render(path.resolve(ROOT_DIR, input.path), templateData)
    } else {
        res.status(200).sendFile(path.resolve(ROOT_DIR, input.path))
    }
    callback(null, {
        success: true
    })
}

module.exports.renderHtml = function(_input, callback, options) {
    //return this.renderView(input, callback, options)
    const input = _input || {}
    let templateData = {}
    input.data && input.data.forEach(item => {
        templateData[item.key] = item.value
    })
    fs.readFile(path.resolve(ROOT_DIR, input.path), (err, template) => {
        if (err) {
            return callback(err)
        }
        let html = template.toString()
        try {
            html = ejs.render(html, templateData, {
                filename: path.resolve(ROOT_DIR, input.path),
                ...input.options
            })
        } catch (e) {
            return callback(e.message)
        }
        callback(null, { html })
    })
}

module.exports.renderInline = function(_input, callback, options) {
    //return this.renderView(input, callback, options)
    const input = _input || {}
    const template = input.template
    let html = ""
    let templateData = {}
    input.data && input.data.forEach(item => {
        templateData[item.key] = item.value
    })
    try {
        html = ejs.render(template, templateData, input.options)
    } catch (e) {
        return callback(e.message)
    }
    callback(null, { html })
}

module.exports.pipeToFile = function(input, callback, options) {
    let httpRequest = (input.http_connection || options.vars.__httpServer).request

    httpRequest.pipe(fs.createWriteStream(path.resolve(ROOT_DIR, input.file)))
    httpRequest.on('end', function() {
        callback(null, input.file)
    })
}

module.exports.applyInputFilter = async (_input, callback, options) => {
    const input = _input || {}
    let inputSchema = { ...options.step['input-schema'] }
    let outputSchema = { ...options.step['output-schema'] }
    inputSchema.properties.contentType._hidden = true
    if (!input.type || (input.type.replace(/^"|"$/g, '') === 'custom')) {
        delete inputSchema.properties.contentType._hidden
    }
    return callback(null, { inputSchema, outputSchema })
}

const ROOT_DIR = global.ROOT_DIR || process.cwd()

class HttpServer {

    constructor(input = {}, options = {}, onRequest = null) {
        this.config = input
        this.advanced = input.advanced || {}

        this.options = options
        this.options.global.HTTP_CACHE = options.global.HTTP_CACHE || {}
        this.options.global.HTTPS_CACHE = options.global.HTTPS_CACHE || {}

        this.port = typeof _CF_HTTP_PORT != 'undefined' ? _CF_HTTP_PORT : (input.port || 3002)
        this.requestLimit = this.advanced.maxRequestSize || '100kb'
        this.uploadSize = (this.advanced.maxUploadSize || 2) + 'mb'

        this.APP = null
        this.onRequest = onRequest
    }

    initServer = async () => {

        if (this.options.global.HTTP_CACHE[this.port]) {
            this.APP = this.options.global.HTTP_CACHE[this.port].app
            this.setupStaticRouting()
            this.setupCompression()
            await this.setupRoutes()
            if (this.advanced.ssl) {
                this.setupSSLServer()
            }
            return null
        }

        this.initializeApp()

        let sessMiddleware = await this.setupSession()
        await this.setupMiddleware(this.APP, this.config.middlewares)

        await this.setupRoutes()
        this.setupServer()
        this.setupSSLServer()

        await this.setupWebsocket(sessMiddleware, this.onRequest)

        return null
    }

    initializeApp() {
        this.APP = express()
        this.APP.disable('x-powered-by')
        //set the codeflow server header
        this.APP.use((req, res, next) => {
            res.setHeader('X-Powered-By', 'Codeflow')
            next()
        })
        this.setupFavIcon()
        this.setupStaticRouting()
        this.setupCompression()
    }

    setupFavIcon() {
        let faviconPath = path.resolve(ROOT_DIR, this.advanced.favicon ? this.advanced.favicon : 'favicon.ico')
        if (fs.existsSync(faviconPath)) {
            this.APP.use(favicon(faviconPath))
        }
    }

    setupStaticRouting() {
        var staticPaths = this.advanced.staticFiles || []
        if (!Array.isArray(staticPaths)) {
            return null
        }
        staticPaths.forEach(staticPath => {
            var sPath = staticPath.location || ''
            var sPrefix = staticPath.prefix || ''
            var maxAge = staticPath.maxAge || 0
            if (sPrefix) {
                if (sPrefix) {
                    sPrefix = sPrefix.substring(0, 1) == '/' ? sPrefix : '/' + sPrefix
                    this.APP.use(sPrefix, express.static(path.resolve(ROOT_DIR, sPath), {maxAge}))
                } else {
                    this.APP.use(express.static(path.resolve(ROOT_DIR, sPath), {maxAge}))
                }
            }
        })
    }

    setupCompression() {
        if (this.advanced.gzipResponse) {
            this.APP.use(compression())
        }
    }

    /**
     * Setup routes by processing config.routes
     */
    async setupRoutes() {
        let routes = this.config.routes && this.config.routes.filter(route => Object.keys(route).length > 0)
        if (!routes || routes.length === 0) {
            routes = [{ pattern: '*', name: "default" }]
        }
        for (let routeId = 0; routeId < routes.length; routeId++) {
            const route = routes[routeId]
            if (!route.pattern) {
                throw new Error("Pattern is empty for route #" + routeId)
            }
            if (!route.name) {
                throw new Error("Label is empty for route #" + routeId)
            }
            console.log("Listening on Route " + route.pattern)
            let methods = route.methods
            if (!methods) {
                methods = ['ALL']
            }
            for (let i = 0; i < methods.length; i++) {
                let method = methods[i].toLowerCase()
                let routeOptions = [
                    route.pattern
                ]
                if (route.middlewares) {
                    await this.setupMiddleware({
                        use: (middleware) => {
                            routeOptions.push(middleware)
                        }
                    }, route.middlewares)
                }
                await this.setupRoute(method, route.name, routeOptions)
            }
        }
    }

    /**
     * Setup a single route for the app,, the route with call the onRequest function on getting a hit
     */
    async setupRoute(method, routeName, routeOptions) {
        routeOptions.push((request, response, next) => {
            let routeJson = {}
            routeJson[routeName] = true
            let params = {}
            for (let k in request.params) {
                params[k] = request.params[k]
            }
            //set an instance var
            this.onRequest(null, {
                routes: routeJson,
                method: request.method,
                params: params,
                headers: request.headers,
                query: request.query,
                post: request.body, //deprecated
                body: request.body,
                files: request.files,
                connection: {
                    request: request,
                    response: response
                }
            },
                { // this is set to instance variable
                    __httpServer: { request: request, response: response, next: next, options: { port: this.port } }
                })
        })
        if (this.APP[method]) {
            this.APP[method].apply(this.APP, routeOptions)
        }
    }

    setupServer() {
        this.APP.engine('htm', ejs.renderFile)
        this.APP.engine('ejs', ejs.renderFile)
        this.APP.engine('html', ejs.renderFile)

        let server = http.createServer(this.APP)
            .setMaxListeners(0)
            .listen(this.port, () => {
                this.afterStartingServer(this.port)
            })

        this.options.global.HTTP_CACHE[this.port] = { app: this.APP, server }
        this.options.engine.emit("httpInit", { express: this.APP, server })
    }

    /**
     * Setup the SSL server with the ssl advanced options
     */
    setupSSLServer() {
        let config = this.advanced.ssl
        if (!config) { return }

        let port = config.port || 443
        //return the cached server
        if (this.options.global.HTTPS_CACHE[port] && this.options.global.HTTPS_CACHE[port].server) {
            return this.options.global.HTTPS_CACHE[port].server
        }

        if (!config.key || !config.certificate) {
            throw new Error("SSL key and certificate should be given")
        }

        let sslOptions = {
            key: fs.readFileSync(path.resolve(ROOT_DIR, config.key)),
            cert: fs.readFileSync(path.resolve(ROOT_DIR, config.certificate))
        }
        if (config.passphrase) {
            sslOptions.passphrase = config.passphrase
        }
        let server = https.createServer(sslOptions, this.APP)
            .setMaxListeners(0)
            .listen(port, () => {
                this.afterStartingServer(port)
            })
        this.options.global.HTTPS_CACHE[port] = { server }
        return server
    }

    afterStartingServer(port) {
        port = port || this.port
        if (typeof _CF_HTTP_HOST != 'undefined') {
            console.log("HTTP Server running at http://" + _CF_HTTP_HOST + "/")
        } else {
            var portLabel = port == 80 ? "http://localhost/" : "http://localhost:" + port + "/"
            console.log("HTTP Server running at " + portLabel)
        }
    }

    /**
     * Execute a flow with the file path with its input
     * @param {*} flow
     * @param {*} input
     */
    executeFlow(flow, input) {
        return new Promise((resolve, reject) => {
            this.options.engine.execute({
                file: flow,
                name: flow,
                input,
                options: this.options,
                callback: (err, output) => {
                    if (err) {
                        reject(err)
                    }
                    return resolve(output)
                }
            })
        })
    }

    /**
     * Execute a module with its ref and input
     * @param {*} ref
     * @param {*} input
     */
    async executeModule(ref, input) {
        return await util.promisify(this.options.engine.executeStep).call(this.options.engine, { ref, input })
    }

    setupWebsocket = async (sessionMiddleware, callback) => {
        if (!this.config || !this.config.websocket || !this.config.websocket.file) {
            return null
        }
        let input = this.config.websocket
        let config = input.config || {}

        let {server} = this.options.global.HTTP_CACHE[this.port]

        let websocket = null
        try {
            if (path.extname(input.file) === '.flw') {
                websocket = await this.executeFlow(input.file, { server, callback, config, sessionMiddleware })
            } else {
                websocket = await this.executeModule(input.file, { server, callback, config, sessionMiddleware })
            }
            console.log(`Initialized socket listener ${input.file}`)
        } catch (e) {
            console.log(e)
            console.error(`Error loading the socket listener ${input.file}`)
        }
    }

    setupSession = async () => {
        if (!this.config || !this.config.session || !this.config.session.file) {
            return null
        }
        let input = this.config.session
        let config = input.config || {}
        if (!config.secret) {
            throw new Error("Specify a session secret string")
        }
        if (typeof config.name === 'undefined') {
            config.resave = 'connect.sid'
        }
        if (typeof config.resave === 'undefined') {
            config.resave = true
        }
        if (typeof config.saveUninitialized === 'undefined') {
            config.saveUninitialized = true
        }
        if (typeof config.unset === 'undefined') {
            config.unset = 'keep'
        }
        if (this.APP.get('env') === 'production') {
            this.APP.set('trust proxy', 1) // trust first proxy
            config.cookie = config.cookie || {}
            config.cookie.secure = true // serve secure cookies
        }
        let store = null
        try {
            if (path.extname(input.file) === '.flw') {
                store = await this.executeFlow(input.file, { expressSession, config: config.store })
            } else {
                store = await this.executeModule(input.file, { expressSession, config: config.store })
            }
            if (typeof store === 'object' && store instanceof expressSession.Store) {
                store.on('error', console.error)
                let middleware = expressSession({ ...config, store })
                this.APP.use(middleware)
                return middleware
                console.error(`Initialized session middleware ${input.file}`)
            } else {
                console.error(`Could't initialize the session middleware, invalid-store-type ${input.file}`)
            }
        } catch (e) {
            console.error(`Error loading the serssion store ${input.file}`)
        }
    }

    setupMiddleware(app, middlewareList) {
        if (!middlewareList) {
            return null
        }
        return Promise.all(middlewareList.map((middleware) => this.processMiddleWare(app, middleware)))
    }

    processMiddleWare = async (app, middleware, input = {}) => {
        if (!middleware || (typeof middleware === 'object' && !middleware.file)) {
            return null
        }
        let module = middleware
        if (typeof module === 'object' && middleware.file) { //consider as a module flow
            module = middleware.file
            input = middleware.input
        }

        if (path.extname(module) === '.flw') {
            return await this.processMiddlewareFlow(app, module, input)
        } else {
            return await this.processMiddlewareModule(app, module, input)
        }
    }

    processMiddlewareFlow = async (app, flowFile, input = {}) => {
        const ENGINE = this.options.engine
        const useFlowMiddleware = file => {
            app.use(async (req, res, next) => {
                try {
                    let shouldContinue = await this.executeFlow(file, { req, res, config: input })
                    if (typeof shouldContinue !== 'boolean' || shouldContinue) {
                        next()
                    }
                } catch (e) {
                    console.error(e)
                }
            })
        }
        let flowData = await util.promisify(ENGINE.loadFlow).call(ENGINE, { file: flowFile })
        if (!flowData) {
            return null
        }
        //if the flow inherits middleware schema, then direcly use it
        if (flowData.meta.inherits === 'core/http/middleware/interface') {
            console.log(`Initialized middleware ${flowFile}`)
            return useFlowMiddleware(flowFile)
        }
        //execute the flow and based on the output apply the middleware into the app instance,
        //if it returns a method user as it is else use the fow-middleware mode
        try {
            let handle = await this.executeFlow(flowFile, input)
            if (!handle) {
                return console.log(`error loading middleware ${flowFile}`)
            }
            if (typeof handle === 'function') {
                app.use((req, res, next) => {
                    handle(req, res, next)
                })
            } else if (typeof handle === 'string') {
                useFlowMiddleware(handle)
            } else {
                return null
            }
            return console.log(`Initialized middleware wrapper ${flowFile}`)
        } catch (e) {
            console.log(`error loading middleware ${flowFile}`)
            throw e
        }
    }

    processMiddlewareModule = async (app, module, input) => {
        try {
            let handle = await this.executeModule(module, input)
            if (typeof handle === 'function') {
                app.use(handle)
            } else {
                throw new Error(`invalid handle`)
            }
            return console.log(`Initialized middleware ${module}`)
        } catch (e) {
            console.log(`error loading middleware ${module}`)
            throw e
        }
    }

}
