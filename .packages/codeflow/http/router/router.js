const path = require('path')
const subflow = require('../../core/subflow')
const loader = require('../../../../src/loader.js')
const fsUtil = require('../../../../src/fsUtil.js')
const parseUrl = require('parseurl')

const INTERFACE_ID = "core/http/router/routeinterface"

const STATUS_CODES = {
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    ERROR: 500
}

class Router {

    async applyInputFilter(input = {}, callback, options={}) {
        input = input || {}
        let inputSchema = { ...options.step['input-schema'] }
        let outputSchema = { ...options.step['output-schema'] }
        input.options = input.options || {}
        if(input.options && input.options.bypassOutput) {

            inputSchema.properties.options.properties.outputSchema = {
                type: 'string',
                description: "Specify a schema to define the output structure.",
                format: "schemachooser"
            }
            if(input.options.outputSchema) {
                outputSchema = {
                    "$ref": "file://" + input.options.outputSchema
                }
            }
        }
        return callback(null, { inputSchema, outputSchema })
    }


    async route(input, cb, engineOptions) {
        let stepId = engineOptions.stepId
        let httpServer = engineOptions.vars.__httpServer
        if (!httpServer) {
            return cb(new Error("The router should be placed under an instance of httpServer(core/http/server)"))
        }
        let _isHttpResponseSent = engineOptions._isHttpResponseSent
        if (_isHttpResponseSent) {
            return cb(null, { ignored: true, handled: false })
        }
        input = input || {}
        if (!input.routesFolder) {
            return cb(new Error("routesFolder should be set"))
        }

        try {
            await this.init(input)

            let routeHandler = new RouteHandler(httpServer, input, this.routes, engineOptions,  cb)
            routeHandler.serveRoutes(this.routes)
        } catch(e) {
            cb(e.message)
        }
    }

    async init(input) {
        if (this.routes) {
            return true
        }
        try {
            await this.loadRoutes(input.routesFolder)
        } catch(e) {
            console.error(e)
            throw new Error("Error loading routes: " + e.message)
        }
    }

    async loadRoutes(folder) {
        this.routes = {}
        let rootFolder = path.join(ROOT_DIR, folder)
        try {
            let {files, tree} = await fsUtil.listFileTree(rootFolder)
            for(let idx = 0; idx < files.length; idx++) {
                let file = files[idx]
                let parts = path.parse(file)
                if (parts.ext !== '.flw') {
                    console.error(`Not a valid flow file ${file}`)
                    continue
                }

                let postfix = `/${parts.name}`
                if (parts.name == '_all') {
                    postfix = `/*`
                } else if (parts.name == `index`) {
                    postfix = ''
                }
                let pattern = this.makePatternFromPath(parts.dir.slice(rootFolder.length) + postfix)
                let handler = {
                    flow: file.slice(ROOT_DIR.length + 1),
                    isWild: postfix === `*`
                }

                try {
                    let flow = await this.loadFlow(file)
                    let meta = flow.meta || {}
                    if (meta.inherits !== INTERFACE_ID) {
                        console.error(`Flow file ${file} is not inheriting ${INTERFACE_ID}, ignoring.`)
                        continue
                    }
                    let annotation = meta.annotation || {}
                    if (annotation.disabled === "true" || annotation.disabled === true) {
                        console.error(`Handler file ${filename} is disabled, ignoring.`)
                        continue
                    }
                    handler.tag = stripQuotes(annotation.tag)
                    handler.onBeforeExecute = stripQuotes(annotation.onBeforeExecute)
                    handler.bypassOutput = annotation.bypassOutput ===  "true" || annotation.bypassOutput ===  true
                    handler.inputSchema = flow.startStep.inputSchema
                    handler.outputSchema = flow.endStep.outputSchema
                    if (!this.routes[pattern]) {
                        this.routes[pattern] = {
                            methods: {}
                        }
                    }
                    this.routes[pattern] = handler
                } catch (e) {
                    console.error(`Error loading flow file: ${e.message}`)
                    continue
                }
            }
            return this.routes
        } catch(e) {
            throw e
        }
    }


    /**
     * Make an HTTP route pattern from a flow file's path
     *
     * @param {*} pathPattern
     */
    makePatternFromPath(pathPattern) {
        // if (pathPattern[0] !== '/') {
        //     pathPattern = '/' + pathPattern
        // }
        let parts = pathPattern.split('/')
        let newParts = []
        parts.forEach(function(part) {
            if (part[0] === '{' && part[part.length - 1] === '}') {
                part = ':' + part.slice(1, part.length - 1)
            }
            newParts.push(part)
        })
        return newParts.join('/')
    }

    loadFlow(file) {
        return new Promise((resolve, reject)=>{
            loader.loadFlow({file}, (err, flow)=>{
                if(err) {
                    return reject(err)
                }
                resolve(flow)
            })
        })
    }
}


class RouteHandler {

    constructor(httpServer, input, routes, engineOptions, callback) {
        this.request = httpServer.request
        this.response = httpServer.response
        this.input = input
        this.config = this.input.options || {}
        this.routes = routes
        this.routeKeys = Object.keys(routes)
        this.engineOptions = engineOptions
        this.callback = callback

        this.handled = false
    }

    /**
     * Serve a request by matching the cached route handlers against the given HTTP request's path.
     */
    async serveRoutes() {
        let basePath = this.config.basePath || "/"
        let matches = []
        let url = parseUrl(this.request)

        for (let i = 0; i < this.routeKeys.length; i++) {
            let pathPattern = this.routeKeys[i]
            let handler = this.routes[pathPattern]
            let match = matchRoute(path.posix.join(basePath, pathPattern), url.pathname)
            if (match && match.params) {
                handler.hasParams = Object.keys(match.params).length > 0
                matches.push({
                    params: match.params,
                    handler: handler
                })
            }
        }

        let inputParameters = {
            headers: this.request.headers,
            urlParameters: this.request.query,
            body: this.request.body,
            files: this.request.files,
            method: this.request.method.toUpperCase(),
            basePath: basePath,
            requestUrl: this.request.url
        }

        if (matches.length === 0) {
            return await this.execute404(url.pathname, inputParameters)
        }

        let match = matches.length === 1 ? matches[0] : this.getBestMatch(matches)
        inputParameters.pathParameters = match.params

        let handler = match.handler

        //execute router level global hook
        if (this.config.onBeforeRoute) {
            try {
                let data = await this.executeHook(this.config.onBeforeRoute, {...inputParameters, tag: handler.tag })
                inputParameters.hook = data || {}
            } catch(e) { }
        }

        //execute for route level hook
        if (handler.onBeforeExecute) {
            try {
                let data = await this.executeHook(handler.onBeforeExecute, {...inputParameters, tag: handler.tag })
                data = data || {}
                inputParameters.hook = inputParameters.hook ? {...inputParameters.hook, ...data} : data
            } catch(e) {}
        }
        return await this.executeRoute(handler, inputParameters)
    }

    /**
     * Handle page not found response
     * @returns
     */
    async execute404(pathname, inputParameters) {
        let error = `No handlers for ${pathname}`
        if (this.config.onNotFound) {
            try {
                await this.executeRoute({ flow: this.config.onNotFound }, inputParameters)
            } catch(e) {
                error = e.message
            }
        }
        if(!this.handled) {
            await this.handleResponse({}, {code: STATUS_CODES.NOT_FOUND, message: error})
        }
    }

    /**
     * Execute a hook.
     *
     * @param {*} hook
     * @param {*} parameters
     * @param {*} handler
     */
    async executeHook(hook, parameters) {
        try {
            if(this.handled) { return }
            let data = await this.executeFlow(hook, parameters)
            data = data || {}
            if (data.continue !== false) {
                return data
            }
            await this.handleResponse(data, {code: STATUS_CODES.ERROR, message: `Invalid hook. Hook didn't set any response.`})
            return false
        } catch (e) {
            return this.handleError(e, parameters)
        }
    }

    /**
     * Execute the route and handle the response
     */
    async executeRoute(handler, inputParameters) {
        try {
            if(this.handled) { return }
            let data = await this.executeFlow(handler.flow, inputParameters)
            if((this.config.bypassOutput || handler.bypassOutput) && !this.handled) {
                this.handled = true
                return this.callback(null, data.response.responseData)
            }
            await this.handleResponse(data, {code: STATUS_CODES.ERROR, message: `The handler didn't set any response.`})
            //console.error(`${handler.flow} didn't set any response.`)
        } catch (e) {
            this.handleError(e, inputParameters)
        }
    }

    /**
     * Handle error during handler flow/hook flow.
     *
     * @param {*} error
     */
    async handleError(error, params) {
        //@TODO - check error template / dev mode to custom render errors.
        if (this.config.onError) {
            await this.executeRoute({ flow: this.config.onError }, params)
        }
        this.finishResponse(STATUS_CODES.ERROR, error)
    }

    /**
     * Handle a response returned by a hook or the route handler.
     *
     * @param {*} data
     */
    async handleResponse(data={}, error) {
        if (data.isRendered) {
        } else if (data.redirect) {
            await this.redirect(data.redirect, 0)
        } else if (data.response) {
            //render the response if given
            if (data.response.statusCode) {
                this.response.statusCode = data.response.statusCode
            }
            let isBinary = false
            let contentType = data.response.contentType
            if (contentType) {
                if (contentType == 'binary') {
                    isBinary = true
                }
                let headerMap = {
                    html: 'text/html',
                    text: 'text/plain',
                    json: 'application/json',
                    binary: 'application/binary'
                }
                if (headerMap[contentType]) {
                    this.response.setHeader("Content-Type", headerMap[contentType])
                }
            }
            if (data.response.headers) {
                for(let key in data.response.headers) {
                    this.response.setHeader(key, data.response.headers[key])
                }
            }
            if (isBinary) {
                this.response.write(data.response.responseData, 'binary')
                this.response.end(null, 'binary')
            } else {
                this.response.send(data.response.responseData)
            }
        } else {
            //finish the response with the error
            this.response.status(error.code).send(error.message)
        }
        return this.finishResponse()
    }

    redirect(url, delay=500) {
        console.log(`redirecting to ${url} in ${delay}ms`)
        return new Promise(resolve=>{
            //save the session before redirecting the request. FileStore had a delay in saving the session file which causes incinsistency in the session data
            if(this.request.session && this.request.session.save) {
                this.request.session.save()
            }
            setTimeout(() => {
                this.response.redirect(url)
                resolve()
            }, delay)
        })
    }

    finishResponse() {
        if(this.handled) {
            return
        }
        this.handled = true
        return this.callback(null, {}, { _isHttpResponseSent: true })
    }

    executeFlow(file, input) {
        return new Promise((resolve, reject)=>{
            subflow.execute({flow: {file,input}}, (e, data)=>{
                if(e) {
                    return reject(e)
                }
                resolve(data)
            }, this.engineOptions)
        })
    }

    /**
     * Find a best matching route.
     */
    getBestMatch(arrHandlers) {
        let match, wild, params
        for (let i = 0; i < arrHandlers.length; i++) {
            let handler = arrHandlers[i].handler
            if (handler.isWild) {
                wild = arrHandlers[i]
            } else if(handler.hasParams) {
                params = arrHandlers[i]
            } else {
                match = arrHandlers[i]
            }
        }
        return match || params || wild
    }


}
//Derived from express.js
const matchRoute = function(pattern, path) {
    let params = {}
    let cached = pathRegexp(pattern)
    let keys = cached.keys
    let m = cached.regexp.exec(path)
    if (!m) {
        return null
    }
    let val
    for (let i = 1, len = m.length; i < len; ++i) {
        try {
            val = 'string' == typeof m[i] ? decodeURIComponent(m[i]) : m[i]
        } catch (e) {
            let err = new Error(`Failed to decode param '${m[i]}'`)
            err.status = STATUS_CODES.BAD_REQUEST
            return { error: err }
        }
        let key = keys[i - 1]
        if (key) {
            params[key.name] = val
        } else {
            //params.push(val)
        }
    }
    return { params: params }
}

//cache regexp patterns
let patternCache = {}

//Derived from express.js
const pathRegexp = function(path, sensitive, strict) {
    if (patternCache[path]) {
        return patternCache[path]
    }
    let keys = []
    let originalPath = path
    path = path
        .concat(strict ? '' : '/?')
        .replace(/\/\(/g, '(?:/')
        .replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?(\*)?/g, function(_, slash, format, key, capture, optional, star) {
            keys.push({ name: key, optional: !!optional })
            slash = slash || ''
            return ''
                + (optional ? '' : slash)
                + '(?:'
                + (optional ? slash : '')
                + (format || '') + (capture || (format && '([^/.]+?)' || '([^/]+?)')) + ')'
                + (optional || '')
                + (star ? '(/*)?' : '')
        })
        .replace(/([\/.])/g, '\\$1')
        .replace(/\*/g, '(.*)');

    let regexp = new RegExp('^' + path + '$', sensitive ? '' : 'i')
    patternCache[originalPath] = { regexp: regexp, keys: keys }
    return patternCache[originalPath]
}

/**
 * Utility function to strip quotes from a Codeflow expression string.
 *
 * @param {*} val
 */
function stripQuotes(val) {
    if (val && val.indexOf('"') == '0' && val.lastIndexOf('"') == val.length - 1) {
        return val.substring(1, val.length - 1)
    }
    return val
}

module.exports = new Router()