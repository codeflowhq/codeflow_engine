{
  "source": "router.js",
  "method": "route",
  "input-schema": {
    "type": "object",
    "dynamicInputFilter": true,
    "additionalProperties": false,
    "properties": {
      "routesFolder": {
        "type": "string",
        "format": "filechooser",
        "formatOptions": {
          "type": "folder"
        },
        "required": true,
        "description": "A folder containing the route handlers. All the route handlers should extend the interface core/http/router/routeinterface. The route handlers should be placed under a folder structure matching the desired URL paths. For e.g., A request for  http(s)://<yourhost>/users/{user-id}/comments, where {user-id} is a dynamic path parameter, will be directed to a flow under filesystem path : <routesFolder>/users/{user-id}/comments.flw, And a request for http(s)://<yourhost>/users/list/?page=2 will be directed to a handler under <routesFolder>/users/list.flw"
      },
      "options": {
        "type": "object",
        "additionalProperties": false,
        "properties": {
          "basePath": {
            "type": "string",
            "description": "If mentioned, the route handlers will be matched relative to this prefix. E.g. adding /v2. in the prefix will match the route http(s)://<yourhost>/v2/users/1/comments to <routesFolder>/users/{user-id}/comments.flw. (No change to the routesFolder). This is a useful setting for changing the routes without changing the routesFolder structure."
          },
          "onBeforeRoute": {
            "description": "If specified, the router will invoke this hook before executing a matching route handler. The matched route handler will be executed only if the hook returns the continue flag as true. This is useful for performing advanced, dynamic filtering based on request parameters and tag value of the route handler.",
            "type": "string",
            "format": "filechooser",
            "formatOptions": {
                "inheritsId": "core/http/router/hookinterface",
                "dialogType": "flow",
                "ext": "flw"
            }
          },
          "onNotFound": {
            "type": "string",
            "description": "If specified, will invoke this flow when there are no matching route handlers. This flow can render or return a custom 404 view.",
            "format": "filechooser",
            "formatOptions": {
                "inheritsId": "core/http/router/routeinterface",
                "dialogType": "flow",
                "ext": "flw"
            }
          },
          "onError": {
            "type": "string",
            "description": "If specified, will invoke this flow when there are errors while executing a matched route handler flows. This flow can return or render a custom error view.",
            "format": "filechooser",
            "formatOptions": {
                "inheritsId": "core/http/router/routeinterface",
                "dialogType": "flow",
                "ext": "flw"
            }
          },
          "bypassOutput": {
              "type": "boolean",
              "format": "plain",
              "description": "Flag to bypass individual route's responseData as output. If set the router will pass the output from the executed route inseated of sending response"
          }
        }
      }
    }
  },
  "output-schema": {
    "type": "object"
  },
  "meta": {
    "name": "flowRouter",
    "description": "HTTP router package for mapping http requests to flow files. Should be placed under a trigger flow of core/http/server"
  }
}