/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

const Http = require('follow-redirects').http
const Https = require('follow-redirects').https
const Url = require('url')
const querystring = require("querystring")
const FormData = require('form-data')
const stream = require("stream")
const { isStream } = require('../util')
const fs = require('fs')
const Path = require('path')

module.exports.applyInputFilter = async (_input, callback, options) => {
    const input = _input || {}
    let uri = Url.parse(input.url || "", true, true)
    let inputSchema = { ...options.step['input-schema'] }
    let outputSchema = { ...options.step['output-schema'] }
    //set url description based on computed query params
    let url = "${url}"
    if (input.parameters && typeof input.parameters.forEach == 'function') {
        input.parameters.forEach((parameter, idx) => {
            if (parameter.key) {
                url += (idx > 0 || (uri.search && uri.search.length > 1) ? '&' : '?')
                url += parameter.key + "=" + encodeURIComponent(parameter.value ? parameter.value.toString() : '')
            }
        })
    }
    // inputSchema.properties.url.description = url

    //show body only for non-get
    const bodySchema = inputSchema.properties.body
    input.method = input.method || 'GET'
    if (input.method && input.method === 'GET') {
        bodySchema._hidden = true
    } else {
        delete bodySchema._hidden
        let showFormData = false
        let showContentType = false
        let showAttachments = false
        let showData = false
        let showFileUpload = false
        const body = input.body || {}
        if (body.type === "raw") {
            showContentType = true
            showData = true
        } else if (body.type === "file") {
            showFileUpload = true
        } else if (body.type == "form-data") {
            showAttachments = true
            showFormData = true
        } else if (body.type == "x-www-form-urlencoded") {
            showFormData = true
        }
        if (showFormData) {
            delete bodySchema.properties.formData._hidden
            if (showAttachments) {
                delete bodySchema.properties.formData.properties.files._hidden
            } else {
                bodySchema.properties.formData.properties.files._hidden = true
            }
        } else {
            bodySchema.properties.formData._hidden = true
        }
        if (showContentType) {
            delete bodySchema.properties.contentType._hidden
        } else {
            bodySchema.properties.contentType._hidden = true
        }
        if (showData) {
            delete bodySchema.properties.data._hidden
        } else {
            bodySchema.properties.data._hidden = true
        }
        if (showFileUpload) {
            delete bodySchema.properties.binaryData._hidden
        } else {
            bodySchema.properties.binaryData._hidden = true
        }
    }
    const advanced = input.advanced || {}
    let responseType = { type: "object" }
    inputSchema.properties.advanced.properties.responseSchema._hidden = true
    if (advanced.responseProcessing === "json") {
        delete inputSchema.properties.advanced.properties.responseSchema._hidden
        if (advanced.responseSchema) {
            responseType = { $ref: "file://" + advanced.responseSchema, description: "Response processed as an object." }
        } else {
            responseType = { type: "any", description: "Response processed as an object." }
        }
    } else if (advanced.responseProcessing === "binary") {
        responseType = { type: "any", description: "A binary buffer instance." }
    } else if (advanced.responseProcessing === "raw") {
        responseType = { type: "any", description: "Response is unprocessed and available as a readable stream. Can be piped to writable streams." }
    } else {
        responseType = { type: "string", description: "Response processed as a string." }
    }
    inputSchema.properties.advanced.properties.encoding._hidden = true
    if (advanced.responseProcessing !== "binary" && advanced.responseProcessing !== "raw") {
        delete inputSchema.properties.advanced.properties.encoding._hidden
    }
    outputSchema.properties.responseData = responseType
    return callback(null, { inputSchema, outputSchema })
}

module.exports.makeRequest = function(_input, _callback) {
    let callbackHandled = false
    const callback = (err, resp) => {
        if (callbackHandled) {
            if (err) {
                console.error(err)
            }
            return
        }
        callbackHandled = true
        _callback(err, resp)
    }
    const input = _input || {}
    if (!input.url || input.url === '') {
        return callback(new Error("url must be specified in the input."))
    }
    const uri = Url.parse(input.url, true, true)
    let path = uri.path
    const headers = {}

    input.headers && input.headers.forEach(header => {
        if (header.key) {
            headers[header.key] = header.value
        }
    })

    if (input.parameters) {
        let isFirst = true, value
        if (uri.search && uri.search.length > 0) {
            isFirst = false
        }
        input.parameters && input.parameters.forEach(parameter => {
            if (parameter.key) {
                value = parameter.value ? parameter.value.toString() : ''
                path += (isFirst ? '?' : '&')
                path += parameter.key + "=" + encodeURIComponent(value)
                isFirst = false
            }
        })
    }

    let data = ""
    if (input.method !== 'GET') {
        const body = input.body || {}
        const requestType = body.type || "raw"
        switch (requestType) {
            case "form-data":
                const form = new FormData()
                if (body.formData && body.formData.fields) {
                    body.formData.fields.forEach(function(item) {
                        form.append(item.field, item.value)
                    })
                }
                if (body.formData && body.formData.files) {
                    body.formData.files.forEach(function(item) {
                        form.append(item.field, item.content, item.options)
                    })
                }
                data = form
                const formHeaders = form.getHeaders()
                for (const key in formHeaders) {
                    if (formHeaders.hasOwnProperty(key)) {
                        headers[key] = formHeaders[key]
                    }
                }
                break;
            case "x-www-form-urlencoded":
                const formData = {}
                if (body.formData && body.formData.fields) {
                    body.formData.fields.forEach(item => {
                        if (item.field) {
                            formData[item.field] = item.value
                        }
                    })
                }
                data = querystring.stringify(formData)
                headers['Content-Type'] = 'application/x-www-form-urlencoded'
                break;
            //raw body
            case "raw":
                data = body.data || ""
                if (typeof data != 'string' && !Buffer.isBuffer(data) && !(isStream(data))) {
                    if (body.contentType == "application/json") {
                        try {
                            data = JSON.stringify(data)
                        } catch (e) {
                            console.error(e)
                            return callback(new Error("Error parsing body.data: " + e.message))
                        }
                    } else {
                        return callback(new Error("body.data should be a string, stream or Buffer."))
                    }
                }
                if (body.contentType) {
                    headers['Content-Type'] = body.contentType
                }
                break;
            case "file":

                break;
        }

        //set content length
        if (!headers['Content-Length']) {
            let contentLength
            if (typeof data === "string") {
                contentLength = data.length
            } else if (Buffer.isBuffer(data)) {
                contentLength = Buffer.byteLength(data)
            }
            if (typeof contentLength != "undefined") {
                headers['Content-Length'] = contentLength
            }
        }
    }

    if (input.authorization) {
        if (input.authorization.basic) {
            headers['Authorization'] = 'Basic ' + Buffer.from(input.authorization.basic.username + ':' + input.authorization.basic.password).toString('base64')
        }
    }
    const advancedOptions = input.advanced || {}
    const timeout = (typeof advancedOptions.timeout == 'undefined') ? 10000 : advancedOptions.timeout
    const options = {
        host: uri.hostname,
        port: uri.port,
        path: path,
        method: input.method || 'GET',
        headers: headers,
        followRedirects: advancedOptions.followRedirects,
        maxRedirects: advancedOptions.maxRedirects,
        maxBodyLength: advancedOptions.maxBodyLength || Infinity,
        insecure: true
    }
    if (timeout > 0) {
        options.timeout = timeout
    }
    if (advancedOptions.ssl) {
        options.agent = false
        options.rejectUnauthorized = false

        if (advancedOptions.ssl.key) {
            options.key = fs.readFileSync(Path.join(ROOT_DIR, advancedOptions.ssl.key))
        }
        if (advancedOptions.ssl.cert) {
            options.cert = fs.readFileSync(Path.join(ROOT_DIR, advancedOptions.ssl.cert))
        }
        if (advancedOptions.ssl.passphrase) {
            options.passphrase = advancedOptions.ssl.passphrase
        }
    }
    if (advancedOptions.encoding) {
        options.encoding = advancedOptions.encoding
    }
    const responseProcessing = advancedOptions.responseProcessing
    const requester = uri.protocol === 'https:' ? Https : Http
    const req = requester.request(options, (resp) => {
        if (advancedOptions.responseProcessing === "raw") {
            //return raw stream
            return callback(null, {
                headers: resp.headers,
                statusCode: resp.statusCode,
                responseData: resp
            })
        }
        let responseData = undefined
        let chunks = []
        resp.on('data', data => chunks.push(data))
        resp.on('end', () => {
            let responseBuffer = Buffer.concat(chunks)
            if (responseProcessing === "json" || responseProcessing === "text") {
                try {
                    responseData = responseBuffer.toString(options.encoding)
                } catch (err) {
                    err.message = `Unexpected error while converting buffer to string with encoding ${options.encoding}: ${err.message}`
                    return callback(err)
                }
                if (responseProcessing === "json") {
                    try {
                        responseData = responseData ? JSON.parse(responseData) : undefined
                    } catch (err) {
                        err.message = `Error while parsing response as JSON: ${err.message}`
                        return callback(err)
                    }
                }
            } else if (responseProcessing === "binary") {
                responseData = responseBuffer
            } else {
                return callback(new Error("Unsupported responseProcessing."))
            }
            return callback(null, {
                headers: resp.headers,
                statusCode: resp.statusCode,
                responseData: responseData
            })
        })
    })
    if (input.method === 'GET') {
        req.end()
    } else {
        if (data instanceof stream.Readable || isStream(data)) {
            stream.pipeline(data, req, (error) => {
                if (error) {
                    return callback(error)
                }
            })
        } else {
            req.write(data)
            req.end()
        }
    }
    req.on('error', (e) => {
        delete e.stack
        return callback(e)
    })
    req.on('timeout', () => {
        if (typeof req.destroy === 'function') {
            req.destroy()
        } else {
            req.abort()
        }
        let e = new Error("Response not received within specified timeout " + (timeout) + " milliseconds, aborted.")
        delete e.stack
        return callback(e)
    })
}
