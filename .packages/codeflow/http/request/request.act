{
  "source": "request.js",
  "hidden": false,
  "method": "makeRequest",
  "input-schema": {
    "dynamicInputFilter": true,
    "type": "object",
    "additionalProperties": false,
    "properties": {
      "url": {
        "type": "string",
        "required": true
      },
      "method": {
        "type": "string",
        "format": "choice",
        "choice": [
          {"label": "", "value": ""},
          {"label": "GET", "value": "\"GET\""},
          {"label": "POST", "value": "\"POST\""},
          {"label": "PUT", "value": "\"PUT\""},
          {"label": "DELETE", "value": "\"DELETE\""},
          {"label": "PATCH", "value": "\"PATCH\""},
          {"label": "OPTIONS", "value": "\"OPTIONS\""},
          {"label": "HEAD", "value": "\"HEAD\""},
          {"label": "CONNECT", "value": "\"CONNECT\""},
          {"label": "TRACE", "value": "\"TRACE\""}
        ],
        "required": true
      },
      "parameters": {
        "title": "queryParameters",
        "description": "Query parameters that will be appended to the URL.",
        "type": "array",
        "items": {
          "title": "query",
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "key": {
              "type": "string"
            },
            "value": {
              "type": "any"
            }
          }
        }
      },
      "headers": {
        "description": "Custom headers to be sent.",
        "type": "array",
        "items": {
          "type": "object",
          "title": "header",
          "additionalProperties": false,
          "properties": {
            "key": {
              "type": "string",
              "enum": [
                "Accept",
                "Accept-Encoding",
                "Accept-Langauge",
                "Accept-Patch",
                "Accept-Ranges",
                "Age",
                "Allow",
                "Alt-Svc",
                "Authorization",
                "Cache-Control",
                "Connection",
                "Content-Disposition",
                "Content-Encoding",
                "Content-Language",
                "Content-Length",
                "Content-Location",
                "Content-Range",
                "Content-Type",
                "Date",
                "Delta-Base",
                "ETag",
                "Expires",
                "From",
                "IM",
                "If-Modified-Since",
                "Last-Modified",
                "Link",
                "Location",
                "Pragma",
                "Proxy-Authenticate",
                "Public-Key-Pins",
                "Retry-After",
                "Referer",
                "Server",
                "Set-Cookie",
                "Strict-Transport-Security",
                "Trailer",
                "Transfer-Encoding",
                "Tk",
                "Upgrade",
                "User-Agent",
                "Vary",
                "Via",
                "Warning",
                "WWW-Authenticate",
                "Content-Security-Policy",
                "Refresh",
                "X-Powered-By",
                "X-Request-ID",
                "X-UA-Compatible",
                "X-XSS-Protection"
              ],
              "description": "Parameter name"
            },
            "value": {
              "type": "any",
              "description": "Parameter value"
            }
          }
        }
      },
      "body": {
        "type": "object",
        "additionalProperties": false,
        "properties": {
          "type": {
            "type": "string",
            "format": "choice",
            "choice" : [
              {"label": "x-www-form-urlencoded", "value": "\"x-www-form-urlencoded\""},
              {"label": "multipart/form-data", "value": "\"form-data\""},
              {"label": "raw", "value": "\"raw\""}
            ],
            "description" : "Set the type of the request body."
          },
          "formData": {
            "title": "data",
            "type": "object",
            "description": "Values set here will be sent as multipart/form-data or x-wwww-form-urlencoded.",
            "additionalProperties": false,
            "properties": {
              "fields": {
                "type": "array",
                "items": {
                  "type": "object",
                  "title": "field",
                  "additionalProperties": false,
                  "properties": {
                    "field": {
                      "type": "string",
                      "description": "Name of the form field."
                    },
                    "value": {
                      "type": "string",
                      "description": "Value of the form field."
                    }
                  }
                }
              },
              "files": {
                "type": "array",
                "items": {
                  "type": "object",
                  "title": "file",
                  "additionalProperties": false,
                  "properties": {
                    "field": {
                      "type": "string",
                      "description": "Name of the form field."
                    },
                    "content": {
                      "type": "any",
                      "description": "Content of the file. Can be binary, text or stream."
                    },
                    "options": {
                      "type": "object",
                      "properties": {
                        "filename": {
                          "type": "string",
                          "description": "Name of the file, including extension, that the recipient should receive."
                        },
                        "filepath": {
                          "type": "string",
                          "description": "The filepath property overrides filename and may contain a relative path."
                        },
                        "contentType": {
                          "type": "string",
                          "description": "Content-Type of the file. Will be auto-filled if empty."
                        },
                        "knownLength": {
                          "type": "integer",
                          "description": "Size of the file in bytes. Will be auto-filled if empty."
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "data": {
            "type": "any",
            "description": "Set the data to send. Can be string, buffer or stream."
          },
          "binaryData": {
            "title": "data",
            "type": "object",
            "properties": {
              "file": {
                "title": "file",
                "type": "string",
                "format": "filechooser",
                "description": "Select a file to send. Content-Type will be guessed from extension, unless specified."
              },
              "contentType": {
                "type": "string"
              }
            },
            "additionalProperties": false
          },
          "contentType": {
            "description": "Select a value for Content-Type or type a custom value. The value if specified will override the Content-Type header even if set.",
            "type": "string",
            "enum": [
              "text/html",
              "text/plain",
              "text/xml",
              "application/json",
              "application/xml",
              "application/javascript",
              "image/bmp",
              "image/gif",
              "image/jpeg",
              "image/png",
              "application/epub+zip",
              "application/gzip",
              "application/java-archive",
              "application/ld+json",
              "application/msword",
              "application/octet-stream",
              "application/ogg",
              "application/pdf",
              "application/rtf",
              "application/vnd.amazon.ebook",
              "application/vnd.apple.installer+xml",
              "application/vnd.mozilla.xul+xml",
              "application/vnd.ms-excel",
              "application/vnd.ms-fontobject",
              "application/vnd.ms-powerpoint",
              "application/vnd.oasis.opendocument.presentation",
              "application/vnd.oasis.opendocument.spreadsheet",
              "application/vnd.oasis.opendocument.text",
              "application/vnd.openxmlformats-officedocument.presentationml.presentation",
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
              "application/vnd.rar",
              "application/vnd.visio",
              "application/x-7z-compressed",
              "application/x-abiword",
              "application/x-bzip",
              "application/x-bzip2",
              "application/x-csh",
              "application/x-freearc",
              "application/x-httpd-php",
              "application/x-sh",
              "application/x-shockwave-flash",
              "application/x-tar",
              "application/xhtml+xml",
              "application/zip",
              "audio/3gpp",
              "audio/3gpp2",
              "audio/aac",
              "audio/midi audio/x-midi",
              "audio/mpeg",
              "audio/ogg",
              "audio/opus",
              "audio/wav",
              "audio/webm",
              "font/otf",
              "font/ttf",
              "font/woff",
              "font/woff2",
              "image/svg+xml",
              "image/tiff",
              "image/vnd.microsoft.icon",
              "image/webp",
              "text/calendar",
              "text/css",
              "text/csv",
              "text/javascript",
              "video/3gpp",
              "video/3gpp2",
              "video/mp2t",
              "video/mpeg",
              "video/ogg",
              "video/webm",
              "video/x-msvideo"
            ]
          }
        }
      },
      "authorization": {
        "type": "object",
        "displayOptions": {
          "showCollapsed": "true"
        },
        "properties": {
          "basic": {
            "type": "object",
            "properties": {
              "username": {
                "type": "string"
              },
              "password": {
                "type": "string"
              }
            }
          },
          "oAuth": {
            "type": "object",
            "_hidden": true,
            "properties": {
              "consumer_key": {
                "type": "string"
              },
              "consumer_secret": {
                "type": "string"
              },
              "token": {
                "type": "string"
              },
              "token_secret": {
                "type": "string"
              }
            }
          }
        }
      },
      "advanced": {
        "type": "object",
        "displayOptions": {
          "showCollapsed": "true"
        },
        "additionalProperties": false,
        "properties": {
          "responseProcessing": {
            "type": "string",
            "description": "How to process the response. Defaults to text. Select json to parse the output as a JSON object with an option to attach a schema. Binary will process the response as a binary Buffer. Raw mode will pass the response stream unprocessed and is useful for piping to other stream sources.",
            "format": "choice",
            "choice": [
              {"label": "text", "value": "\"text\""},
              {"label": "json", "value": "\"json\""},
              {"label": "binary (buffer)", "value": "\"binary\""},
              {"label": "raw (stream/unprocessed)", "value": "\"raw\""}
            ]
          },
          "encoding": {
            "type": "string",
            "enum": ["utf8", "utf-8", "ascii", "binary", "latin1", "hex", "base64", "base64url", "ucs2", "ucs-2", "utf16le", "utf-16le"],
            "description": "Encoding for response data, e.g. utf8, latin1 etc."
          },
          "responseSchema": {
            "type": "string",
            "format": "schemachooser",
            "description": "Optionally bind a schema for the JSON response."
          },
          "timeout": {
            "type": "integer",
            "default": 10000
          },
          "followRedirects": {
            "type": "boolean",
            "description": "Whether redirects should be followed. Defaults to true."
          },
          "maxRedirects": {
            "type": "integer",
            "description": "The maximum number of allowed redirects. Defaults to 21."
          },
          "maxBodyLength": {
            "type": "any",
            "description": "The maximum size of the request body. Defaults to Infinity."
          },
          "ssl": {
            "additionalProperties": false,
            "type": "object",
            "displayOptions": {
              "showCollapsed": "true"
            },
            "properties": {
              "certificate": {
                "type": "string",
                "format": "filechooser",
                "formatOptions": {
                  "ext": "pem"
                }
              },
              "key": {
                "type": "string",
                "format": "filechooser",
                "formatOptions": {
                  "ext": "pem"
                }
              },
              "passphrase": {
                "type": "string",
                "format": "password"
              }
            }
          }
        }
      }
    }
  },
  "output-schema": {
    "type": "object",
    "properties": {
      "headers": {
        "type": "object",
        "additionalProperties": {
          "type": "string"
        }
      },
      "statusCode": {
        "type": "integer"
      },
      "responseData": {
        "type": "any"
      }
    }
  },
  "meta": {
    "name": "sendRequest",
    "description": "Send an HTTP(s) request"
  }
}