const express = require('express')
const connectMultiparty = require('connect-multiparty')

module.exports.text = function(_input, callback) {
  const input = _input || {}
  return callback(null, express.text(input))
}

module.exports.json = function(_input, callback) {
  const input = _input || {}
  return callback(null, express.json(input))
}

module.exports.raw = function(_input, callback) {
  const input = _input || {}
  return callback(null, express.raw(input))
}

module.exports.urlencoded = function(_input, callback) {
  const input = _input || {}
  if (typeof input.extended == 'undefined') {
    input.extended = true
  }
  return callback(null, express.urlencoded(input))
}

module.exports.multipart = function(_input, callback) {
  const input = _input || {}
  return callback(null, connectMultiparty(input))
}
