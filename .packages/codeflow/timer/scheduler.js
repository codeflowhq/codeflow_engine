/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var crypto = require('crypto')


var DEFAULT_INTERVAL = 1000
var DEFAULT_TIMEOUT = 0

var TIMER_RUNNING = 1
var TIMER_PAUSED = 2
var TIMER_STOPPED = 3

//global map of timers
var timers = {}

//unique label
var hash = function(str) {
    var hashLib = crypto.createHash('md5')
    return hashLib.update(str).digest('hex')    
}

/**
 * For delay module. Inserts a time delay using setTimeout.
 */
module.exports.delay = function(input, callback) {
    var timeout = typeof input.timeout === 'number' ?  input.timeout : DEFAULT_TIMEOUT
    setTimeout(callback, timeout)
}

/**
 * For timer module. Starts a timer using setInterval.
 */
var timerFn = module.exports.timer = function(input, callback, options) {
    input = input || {}
    var label = input.label
    if (label && timers[label]) {
        return new callback(new Error("Duplicate label specified. This label " + label + " is already used by the flow " + timers[label].options.filename))
    }
    if (!label) {
        label = hash(options.filename)
    }    
    var interval = typeof input.interval == 'number' ?  input.interval : DEFAULT_INTERVAL
    var id = setInterval(function() {
        callback(null, {label: label, seq: timers[label].seq++, interval: interval})
    }, interval)

    //store the data to global map
    timers[label] = {
        state: TIMER_RUNNING,
        id: id,
        seq: input._seq || 0,
        interval: interval,
        callback: callback
    }
    //trigger an instance immediately if needed
    if (input.triggerImmediately) {
        setTimeout(function() {
            callback(null, {label: label, seq: timers[label].seq++, interval: interval})
        }, 0)
    }    
}

/**
 * Update the timer. If paused, update the interval and if running, stop and restart the timer with updated interval.
 */
module.exports.updateTimer = function(input, callback, options) {
    var timer = validateInput(input, callback)
    if (!timer) {
        return
    }
    var interval = typeof input.interval === 'number' ?  input.interval : DEFAULT_INTERVAL        
    switch (timer.state) {
        case TIMER_PAUSED:
            //update the interval in the global
            timer.interval = interval
            return callback(null, {success: true})
        break
        case TIMER_STOPPED:
            return callback(null, {success: false})
        break
        case TIMER_RUNNING:
            clearInterval(timer.id)
            //restart timer with new value
            deleteTimer(input.label)
            timerFn({label: input.label, interval: interval, _seq: timer.seq}, timer.callback)
            return callback(null, {success: true})
        break
        default:
            return callback(null, {success: false})
    }   
}

module.exports.pauseTimer = function (input, callback, options) {
    var timer = validateInput(input, callback)
    if (!timer) {
        return
    }
    if (timer.state === TIMER_STOPPED)  {
        return callback(new Error("The timer is in stopped state. Cannot pause a stopped timer."))
    }
    if (timer.state !== TIMER_RUNNING)  {
        return callback(null, {success: false})
    }
    timer.state = TIMER_PAUSED
    clearInterval(timer.id)
    return callback(null, {success: true})
}

module.exports.resumeTimer = function (input, callback, options) {
    var timer = validateInput(input, callback)
    if (!timer) {
        return
    }
    if (timer.state === TIMER_STOPPED)  {
        return callback(new Error("The timer is in stopped state. Cannot resume a stopped timer."))
    }
    if (timer.state !== TIMER_PAUSED) {
        return callback(null, {success: false})        
    }
    deleteTimer(input.label)
    timerFn({label: input.label, interval: timer.interval, _seq: timer.seq, triggerImmediately: input.triggerImmediately}, timer.callback)
    return callback(null, {success: true})
}

module.exports.stopTimer = function (input, callback, options) {
    var timer = validateInput(input, callback)
    if (!timer) {
        return
    }
    if (timer.state === TIMER_STOPPED) {
        return callback(null, {success: false})        
    }
    timer.state = TIMER_STOPPED
    clearInterval(timer.id)
    return callback(null, {success: true})
}

function deleteTimer(label) {
    delete timers[label]
}

function validateInput(input, callback) {
    if (!input) {
        callback(new Error("input is missing and is required"))
        return false
    }
    var label = input.label
    if (!label) {
        callback(new Error("label is missing and is required"))
        return false
    }
    var timer = timers[label]
    if (!timer) {
        callback(new Error("No timer found with the given label. Specify the exact value as returned by the timer module."))
        return false
    }
    return timer
}