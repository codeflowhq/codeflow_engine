'use strict'
const stream = require("stream")

module.exports.pipe = function(_input, callback) {
  const input = _input || {}
  const options = input.options || {}
  if (!input.source instanceof stream.Readable) {
    return callback(new Error("Source is not a Readable stream."))
  }
  if (!input.destination instanceof stream.Writable) {
    return callback(new Error("Destination is not a Writable stream."))
  }
  if (!input.source.readable) {
    return callback(new Error("Source is not in a readable state."))
  }
  const resStream = input.source.pipe(input.destination, {
    end: typeof options.autoEndWriter == 'boolean' ? options.autoEndWriter : true
  })
  const streamCleanup = stream.finished(input.source, (error) => {
    streamCleanup()
    callback(error, resStream)
  })
}

module.exports.pipeline = function(_input, callback) {
  const input = _input || {}
  if (!input.source instanceof stream.Readable) {
    return callback(new Error("Source is not a Readable stream."))
  }
  if (!input.destination instanceof stream.Writable) {
    return callback(new Error("Destination is not a Writable stream."))
  }
  stream.pipeline(input.source, ...input.transformations || [], input.destination, callback)
}

module.exports.checkStream = function(_input, callback) {
  const input = _input || {}
  if (!input.stream) {
    return callback(new Error("input.stream must be set."))
  }
  return callback(null, {
    isStream: input.stream instanceof stream.Stream,
    isReadStream: input.stream instanceof stream.Readable,
    isWriteStream: input.stream instanceof stream.Writable,
    isDuplexStream: input.stream instanceof stream.Duplex,
    isTransformStream: input.stream instanceof stream.Transform,
    isReadReady: input.stream.readable || false,
    isWriteReady: input.stream.writable || false
  })
}