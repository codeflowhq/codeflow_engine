/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

module.exports.log = function(input, callback) {
    if (input) {
        console.log(input.text)
    }
    callback()
}

module.exports.logMulti = function(input, callback) {
    if (input && typeof input == 'object' && typeof input.length == 'number') {
        for (var i = 0; i < input.length; i++)
            console.log(input[i])
    }
    callback()
}

module.exports.error = function(input, callback) {
    if (input) {
        console.error(input.message)
    }
    callback()
}

module.exports.params = function(input, callback) {
    if (input && input.message) {
        console.log(input.message)
    }
    callback()
}
