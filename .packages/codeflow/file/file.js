/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

//stat, create, delete, rename, append, watch - files and directories

const fs = require('fs')
const path = require('path')
const fse = require('fs-extra')
const _ = require('underscore')
const async = require('async')

/**
 * @TODO Temporarily added to append ROOT_DIR to non-absolute & non-relative urls
 */
function normalizePath(uri) {
    if (typeof uri != 'string') {
        return null
    }
    if (!isAbsolute(uri)) {
        return path.join(ROOT_DIR, uri)
    }
    return uri
}

/**
 * Read a file
 */
module.exports.read = function(input, callback, options) {
    if (!input.path) {
        return callback('Path is missing')
    }
    let filePath = normalizePath(input.path)
    let readOptions = {}
    //setting encoding not working for binary files. so it was better to not set encoding.
    if (input.encoding && input.encoding !== 'binary') {
        readOptions.encoding = input.encoding
    }
    if (input.json) {
        fse.readJson(filePath, { encoding: input.encoding }, callback)
    } else {
        fs.readFile(filePath, readOptions, function(err, data) {
            if (err) {
                return callback(err)
            }
            //convert from buffer to string, except for binary data, which is kept as buffer itself.
            let contents = input.encoding == 'binary' ? data : data.toString()
            return callback(null, contents)
        })
    }
}
/**
 * Write a file
 */
module.exports.write = function(input, callback) {
    let filePath = normalizePath(input.path)
    let contents = input.data || input.contents
    if (input.data) {
        console.log("Warning: [file/write_file]. use input.contents. input.data is deprecated.")
    }
    if (input.json) {
        fse.outputJson(filePath, contents, { encoding: input.encoding }, callback)
    } else {
        try {
            fs.writeFile(filePath, contents, { encoding: input.encoding }, callback)
        } catch (e) {
            return callback(e)
        }
    }
}

/**
 * Create File stream writer
 */
module.exports.createWriter = function(_input, callback) {
    const input = _input || {}
    if (!input.path) {
        return callback(new Error("Please provide a value for path parameter."))
    }
    const writeStream = fs.createWriteStream(input.path, input.options)
    return callback(null, { writeStream })
}

/**
 * Create File stream reader
 */
 module.exports.createReader = function(_input, callback) {
    const input = _input || {}
    if (!input.path) {
        return callback(new Error("Please provide a value for path parameter."))
    }
    const readStream = fs.createReadStream(input.path, input.options)
    return callback(null, { readStream })
}

/**
 * Append data to a file
 */
module.exports.append = function(input, callback) {
    let filePath = normalizePath(input.path)
    fs.appendFile(filePath, input.data, {
        encoding: input.encoding
    }, callback)
}

/**
* Get file info
*/
module.exports.stat = function(input, callback, options) {
    if (typeof input.path != 'string') {
        return callback(new Error("Invalid file path. Path should be a string."))
    }
    let filePath = normalizePath(input.path)
    let fn = input.dontFollowSymlink ? fs.lstat : fs.stat
    fn(filePath, function(error, stats) {
        if (error) {
            return callback(error)
        }
        let parts = path.parse(filePath)
        if (!parts) {
            parts = {}
        }
        return callback(null, {
            exists: !error,
            isFile: stats.isFile(),
            isDirectory: stats.isDirectory(),
            isBlockDevice: stats.isBlockDevice(),
            isCharacterDevice: stats.isCharacterDevice(),
            isSymbolicLink: stats.isSymbolicLink(),
            isFIFO: stats.isFIFO(),
            isSocket: stats.isSocket(),
            info: {
                size: stats.size,
                mode: stats.mode,
                uid: stats.uid,
                gid: stats.gid,
                changeTime: stats.ctime.getTime(),
                accesstime: stats.atime.getTime(),
                modifiedtime: stats.mtime.getTime(),
                createdTime: stats.birthtime.getTime()
            },
            pathInfo: {
                name: parts.name,
                basename: parts.base,
                dir: parts.dir,
                ext: parts.ext,
                isAbsolute: path.isAbsolute(filePath)
            }
        })
    })
}

/**
 * Unlink a file
 */
module.exports.unlink = function(input, callback) {
    fs.unlink(normalizePath(input.path), callback)
}
/**
 * Remove a file or directory
 */
module.exports.remove = function(input, callback) {
    fse.remove(normalizePath(input.path), callback)
}
/**
 * Truncate
 */
module.exports.truncate = function(input, callback) {
    fs.truncate(normalizePath(input.path), callback)
}

/**
 * Check if a file exists
 */
module.exports.exists = function(input, callback) {
    fs.stat(normalizePath(input.path), function(err, response) {
        if (!err && response && (response.isFile() || response.isDirectory())) {
            callback(null, {
                exists: true
            })
        } else {
            callback(null, {
                exists: false
            })
        }
    })
}

/**
 * Rename a file.
 */
module.exports.rename = function(input, callback) {
    fs.rename(normalizePath(input.currentPath), normalizePath(input.newPath), callback)
}

/**
 * Make a new directory
 */
module.exports.mkdir = function(input, callback) {
    fse.mkdirp(normalizePath(input.path), callback)
}


/**
 * Empty directory
 */
module.exports.empty_dir = function(input, callback) {
    fse.emptyDir(normalizePath(input.path), callback)
}
/**
 * Copy a file or directory even across devices
 */
module.exports.copy = function(input, callback) {
    fse.copy(normalizePath(input.source), normalizePath(input.destination), callback)
}
/**
 * Move a file or directory even across devices
 */
module.exports.move = function(input, callback) {
    fse.move(normalizePath(input.source), normalizePath(input.destination), { overwrite: input.overwrite }, callback)
}
/**
 * Simple list files
 */
module.exports.list = function(input, callback) {
    if (input.recursive) {
        return list_files(input, callback)
    }
    let fPath = normalizePath(input.path)
    fs.readdir(fPath, function(error, files) {
        if (error) {
            return callback(error)
        }
        let result = files.map(function(fileName) {
            return {
                name: fileName,
                path: path.join(fPath, fileName),
                type: fs.statSync(path.join(fPath, fileName)).isDirectory() ? "folder" : "file",
            }
        })
        return callback(null, result)
    })
}

/**
 * List files under a path recursively.
 */
const list_files = module.exports.list_files = function(input, callback) {
    if (!input || typeof input.path === 'undefined') {
        return callback(new Error("input.path is missing and is required."))
    }
    let fPath = normalizePath(input.path)
    let files = []
    let self = this
    let depth = input._depth || 0
    let maxDepth = input.maxDepth || 100
    let excludedPaths = []
    //read the directory files
    fs.readdir(fPath, function(err, fileList) {
        if (err || !fileList) {
            return callback(err)
        }
        //sort the files into files and directories
        async.each(fileList, function(file, next) {
            if (!file || excludedPaths.indexOf(file) > -1) {
                return next()
            }
            let filePath = path.join(fPath, file)
            fs.stat(filePath, function(err, stat) {
                if (err) {
                    return callback(err)
                }
                files.push({
                    name: file,
                    type: stat && stat.isDirectory() ? "folder" : "file",
                    path: filePath,
                    depth: depth
                })
                if (stat && stat.isDirectory()) {
                    //if a depth is specified in the options, dig only that deeper
                    if (maxDepth >= 0 && depth >= maxDepth) {
                        return next()
                    }
                    //get the files inside the directory
                    list_files({ path: filePath, maxDepth: input.maxDepth, _depth: depth + 1 }, function(err, dirFiles) {
                        files = files.concat(dirFiles)
                        //go to next file
                        next()
                    })
                } else {
                    next()
                }
            })
        }, function() {
            return callback(null, files)
        })
    })
}

/*
* Fallback for isAbsolute
*/
function isAbsolute(path) {
    if (process.platform === 'win32') {
        // https://github.com/joyent/node/blob/b3fcc245fb25539909ef1d5eaa01dbf92e168633/lib/path.js#L56
        let splitDeviceRe = /^([a-zA-Z]:|[\\\/]{2}[^\\\/]+[\\\/]+[^\\\/]+)?([\\\/])?([\s\S]*?)$/
        let result = splitDeviceRe.exec(path)
        let device = result[1] || ''
        let isUnc = !!device && device.charAt(1) !== ':'

        // UNC paths are always absolute
        return !!result[2] || isUnc
    } else {
        return path.charAt(0) === '/'
    }
}
