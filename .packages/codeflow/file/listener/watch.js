/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var fs = require('fs')

var listeners = {}

module.exports.watch = function(input, callback, options) {
    let id = input.id || Math.random().toString(36).substring(10)
    if (listeners[id]) {
        return callback(new Error("Label " + id + " already used by " + listeners[id].listener + "."))
    }
    listeners[id] = {
        path: input.path,
        callback: callback,
        options: {recursive: input.recursive}
    }
    listeners[id].watcher = fs.watch(listeners[id].path, listeners[id].options, function(event, filename) {
        callback(null, {
            listener_id: id,
            event: event,
            filename: filename
        })
    })
}

module.exports.pauseWatch = function(input, callback, options) {
    let id = input.id
    if (!listeners[id]) {
        return callback(new Error("Listener not found with label " + id))
    }
    listeners[id].watcher.close()
    callback()
}

module.exports.resumeWatch = function(input, callback, options) {
    let id = input.id
    if (!listeners[id]) {
        return callback(new Error("Listner not found with label " + id))
    }
    listeners[id].watcher = fs.watch(listeners[id].path, listeners[id].options, function(event, filename) {
        listeners[id].callback(null, {
            listener_id: id,
            event: event,
            filename: filename
        })
    })
    callback()
}
