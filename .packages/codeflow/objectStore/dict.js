/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var globals = {}

module.exports.get = function(input, callback) {
    var name = input.name
    var key = input.key
    if (typeof globals[name] != 'undefined') {
        callback(null, globals[name][key])
    } else {
        callback()
    }
}

module.exports.list = function(input, callback) {
    var name = input.name
    if (typeof globals[name] != 'undefined') {
        callback(null, shallowCopy(globals[name]))
    } else {
        callback()
    }
}

module.exports.set = function(input, callback) {
    var name = input.name
    var key = input.key
    var value = input.value
    if (typeof globals[name] == 'undefined') {
        globals[name] = {}
    }
    globals[name][key] = value
    callback()
}

module.exports.rm = function(input, callback) {
    var name = input.name
    var key = input.key
    if (typeof globals[name] != 'undefined') {
        delete globals[name][key]
        callback(null, true)
    } else {
        callback(null, false)
    }
}

module.exports.rmAll = function(input, callback) {
    var name = input.name
    if (typeof globals[name] != 'undefined') {
        globals[name] = {}
        callback(null, true)
    } else {
        callback(null, false)
    }
}

function shallowCopy(obj) {
    var clone = {}
    var keys = Object.getOwnPropertyNames(obj)
    for (var i = 0; i < keys.length; i++) {
        clone[keys[i]] = obj[keys[i]]
    }
    return clone
}
