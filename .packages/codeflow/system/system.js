/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

exports.stderr = function(input, callback) {
    return callback(null, process.stderr)
}

exports.stdin = function(input, callback) {
    return callback(null, process.stdin)
}

exports.stdout = function(input, callback) {
    return callback(null, process.stdout)
}

exports.resourceUsage = function(input, callback) {
    return callback(null, process.resourceUsage())
}

exports.memoryUsage = function(input, callback) {
    return callback(null, process.memoryUsage())
}

exports.cpuUsage = function(input, callback) {
    return callback(null, process.cpuUsage())
}

exports.send = function(_input, callback) {
    const input = _input || {}
    if (process.send) {
        process.send(input.message, callback)
    }
    return callback(null, undefined)
}

exports.execute = function(input, callback) {
    const exec = require('child_process').exec
    const args = input.args || []
    const command = input.command + ' ' + args.join(' ')
    const options = input.options
    exec(command, options, function(error, stdout, stderr) {
        const output = {
            stdout: stdout,
            stderror: stderr
        }
        if (error) {
            output.error = {
                exitCode: error.code,
                signal: error.signal || ''
            }
        }
        callback(null, output)
    })
}

exports.exit = function(input, callback) {
    const inputData = input || {}
    const code = inputData.code
    process.exit(code)
}

exports.getPID = function(input, callback) {
    callback(null, { pid: process.pid })
}

exports.kill = function(input, callback) {
    const inputData = input || {}
    const pid = inputData.pid
    const signal = inputData.signal
    process.kill(pid, signal)
    return callback()
}

exports.getCwd = function(input, callback) {
    return callback(null, { directory: process.cwd() })
}

exports.setCwd = function(input, callback) {
    const inputData = input || {}
    if (!inputData.directory) {
        return callback(new Error("Missing directory."))
    }
    try {
        process.chdir(inputData.directory)
        return callback(null)
    } catch (err) {
        return callback(err)
    }
}

exports.setExitCode = function(input, callback) {
    const inputData = input || {}
    process.exitCode = inputData.code
    return callback()
}

exports.onExit = function(input, callback) {
    process.on('exit', function(code) {
        callback(null, code)
    })
}

exports.onEvent = function(input, callback) {
    const inputData = input || []
    inputData.forEach(function(event) {
        process.on(event, function() {
            callback(null, { event: event, data: arguments })
        })
    })
}