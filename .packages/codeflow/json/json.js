/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

module.exports.jsonToString = function(input, callback) {
  var obj
  if (input.formatSpaces) {
    obj = JSON.stringify(input.json, null, input.formatSpaces)
  } else {
    obj = JSON.stringify(input.json)
  }
  callback(null, obj)
}

module.exports.stringToJson = function(input, callback) {
  //console.log("json.parse", input)
  var obj = JSON.parse(input.json)
  if (input.validate) {
    validateJson({json: obj, schema: input.schema}, function(err, data) {
      //data.report -- what to do ?
      if (data.report.errors) {        
        callback(new Error(JSON.stringify(data.report.errors)))
      } else {
        callback(null, data.json)
      }
    })
  } else {
    callback(null, obj)
  }
}

var validateJson = module.exports.validateJson = function(input, callback) {
  var json = input.json
  var schema = input.schema
  var SchemaValidator = require("json-schema")
  callback(null, {json: json, report: SchemaValidator.validate(json, schema)})
}