/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var vm = require('vm')
var path = require('path')
var _ = require('underscore')
var Module = require('module')
var Crypto = require('crypto')
var moduleCache = {}
var scriptCache = {}

module.exports.execute = function(input={}, callback, options) {
    //fallback to default script in case input is empty
    input.script = input.script || 'console.log("hello ", input.name); callback(null, "Any data")'
    input.input = input.input || {name: 'codeflow'}

    //var cacheKey = options.filename + ':' + options.stepname
    var cacheKey = Crypto.createHash('md5').update(input.script).digest('hex');
    var scriptFn = scriptCache[cacheKey]
    if (!scriptFn) {
        var pre = "(function(require, input, callback, global, CONFIG, __filename, __dirname, _options) {"
        var post = "});"
        scriptFn = vm.runInThisContext(pre + input.script + post)
        scriptCache[cacheKey] = scriptFn
    }
    var filepath = path.join(global.ROOT_DIR, options.filename)
    var cwd = path.dirname(filepath)
    var parent = moduleCache[options.filename]
    if (!parent) {
        parent = new Module('codeflow/module', null)
        parent.paths = Module._nodeModulePaths(cwd)
        parent.filename = filepath
        moduleCache[options.filename] = parent
    }
    /**
     * Because calling require inside a custom script will take into
     * consideration the caller's context, we need to wrap it.
     */
    var proxyRequire = function(request) {
        return Module._load(request, parent)
    }
    var inputData = input.input
    try {
        var thing = scriptFn(proxyRequire, inputData, callback, global, global.CONFIG, filepath, cwd, options)
        //promise support
        if (thing && typeof thing == 'object' && typeof thing.then == 'function') {
            thing.then(function(result) {
                return callback(null, result)
            }).catch(function(err) {
                return callback(err)
            })
        }
    } catch(e) {
        callback(e)
    }
}
