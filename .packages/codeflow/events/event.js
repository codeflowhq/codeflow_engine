/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var _ = require('underscore')
var EventEmitter = require("events").EventEmitter
var eventEmitter = new EventEmitter()

module.exports.listener = function(input, callback, options) {
    var events = input || []
    for (var i = 0, length = events.length; i < length; i++) {
        (function(event) {
            eventEmitter.on(event, function(data) {
                setImmediate(function() {
                    callback(null, {
                        event: event,
                        data: data
                    })
                })
            })
        })(events[i])
    }
}

module.exports.emit = function(input, callback, options) {
    eventEmitter.emit(input.event, input.data || null)
    callback()
}

module.exports.once = function(input, callback, options) {
    if (input && input.event) {
        eventEmitter.once(input.event, function(data) {
            callback(null, { data: data })
        })
    } else {
        setImmediate(callback)
    }
}