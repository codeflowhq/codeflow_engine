/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var globals = {}

module.exports.set = function(input, callback, options) {
    if (typeof input.key !== 'undefined') {
        globals[input.key] = input.value
    }
    callback(null, input.value)
}

module.exports.get = function(input, callback, options) {
    var data
    if (typeof input.key !== 'undefined') {
        data = globals[input.key]
    } 
    callback(null, data)
}

module.exports.setInstanceVar = function(input, callback, options) {
    var vars = options.vars //setting variable in instance specific field
    if (typeof input.key !== 'undefined') {
        var data = vars[options.root + '_' + input.key]
        //merge the data (only for objects)
        if(typeof data == 'object' && typeof data === typeof input.value && input.merge) {
            var _ = require('underscore')
            _.extend(data, input.value)
        } else {
            vars[options.root + '_' + input.key] = input.value
        }
    }
    callback(null, input.value)
}

module.exports.getInstanceVar = function(input, callback, options) {
    var vars = options.vars
    var data
    if (typeof input.key !== 'undefined') {
        data = vars[options.root + '_' + input.key]
    }
    callback(null, data)
}