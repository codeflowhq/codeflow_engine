/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var async = require('async')
var _ = require('underscore')

/**
* Util function to determine if a value is empty.
* numbers and booleans are not considered empty even if they are
* evaluated to 0 or false.
*/
function isEmpty(data) {
    switch(typeof data) {
        case 'object':
        case 'string':
            return _.isEmpty(data)
        case 'number':
        case 'boolean':
            return false
        case 'undefined':
            return true
        default:
            return _.isEmpty(data)
    }
}

module.exports.iterate = function(input, callback, options) {
    var isObj = false
    input = input || {}
    var collection = input.collection
    if (!collection) {
        return callback()
    }
    if (typeof collection == 'object' && !_.isArray(collection)) {
        collection = Object.keys(collection)
        isObj = true
    }
    var config = input.options || {}
    var iteratorFn = config.parallel ? async.each : async.eachSeries
    if (config.parallel && config.limit) {
        iteratorFn = function(arr, iterator, callback) {
            async.eachLimit(arr, config.limit, iterator, callback)
        }
    }

    var index = 0
    var output = []
    var errors = []
    iteratorFn(collection, function(elem, next) {
        var key = isObj ? elem: index++
        var value = isObj ? input.collection[elem] : elem
        var itemData = {
            value: value,
            index: key
        }
        options.execute(itemData, function(error, data) {
            if (error && !config.ignoreError) {
                errors.push(error)
            } else {
                if (!(config.excludeEmpty && isEmpty(data))) {
                    output.push(data)
                }
            }
            next()
        })
    }, function(err) {
        var error = null
        if (errors.length > 0) {
            error = new Error("Error occurred during iterator")
            error.code = "ITERATOR_ERROR"
            error.data = {errors: _.map(errors, function(error) { return error.message})}
        }
        callback(error, output)
    })
}

module.exports.iterateUntil = function(input, callback, options) {
    input = input || {}
    var data = input.data || undefined
    var itemData = {
        index: 0,
        data: data
    }
    var next = function(err, result) {
        if (err) {
            return callback(err)
        }
        if (result && result.continue) {
            itemData = {
                index: itemData.index + 1,
                data: result.data
            }
            options.execute(itemData, next)
        } else {
            return callback(null, {data: result.data, index: itemData.index})
        }
    }
    options.execute(itemData, next)
}

module.exports.reduceGroup = function(input, callback, options) {
    var output = []
    var errors = []
    var index = 0
    var isObj = false
    var collection = input.collection
    if (typeof collection == 'object') {
        if (!_.isArray(collection)) {
            collection = Object.keys(collection)
            isObj = true
        }
    }
    var item = {}
    var lastOutput
    var initialVal = input.initial
    var iteratorKey = input.key

    var isInitial = true
    async.eachSeries(collection, function(elem, next)  {
        var value = isObj ? input.collection[elem] : elem
        if (isInitial && typeof initialVal === 'undefined') {
            isInitial = false
            lastOutput = value
            return next()
        }
        item[iteratorKey] = {
            current: value,
            previous: isInitial ? initialVal : lastOutput
        }
        isInitial = false
        options.execute(item, function(err, data) {
            lastOutput = data
        })
    }, function(err) {
        var error = null
        if (errors.length > 0) {
            //@TODO - format properly
            error = new Error("Error occurred during reduce operation")
            error.data = errors
        }
        callback(error, lastOutput)
    })
}
module.exports.group = function(input, callback, options) {
    options.execute(input, function(err, data) {
        callback(err, data)
    })
}