/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'
var async = require('async')

module.exports.group = function(data, callback) {
  var tasks = data.stepJson
  var grpType = data.config.type
  var engine = new Engine()
  var context = {}

  switch(grpType) {
    case 'iterate':
      var list = data.config.list
      for (var key in list) {
        if (list.hasOwnProperty(key)) {
          context.item = list[key]
          engine.execute(tasks, context, function(error, data) {
          })
        }
      }
      callback(data)
    break;
    case 'for':
      var aggregated = []
      for (var i = 0; i < data.config.loop_count; i++) {
        context.id = i
        engine.execute(tasks, context, function(error, data) {
          aggregated.push(data)
        })
      }
      callback(data)
    break
    case 'plain':
      engine.execute(tasks, function(error, data) {
        callback(data)
      })
    break
  }
}