/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var _ = require('underscore')
var async = require('async')
var isAbsolute = require('path-is-absolute')
var subflow = require('./subflow')
//this should be changed to require('codeflow')
var EE = require('../../../src/eval.js')

/**
* Util function to determine if a value is empty.
* numbers and booleans are not considered empty even if they are
* evaluated to 0 or false.
*/
function isEmpty(data) {
    switch(typeof data) {
        case 'object':
        case 'array':
        case 'string':
            return _.isEmpty(data)
            break
        case 'number':
        case 'boolean':
            return false
            break
        case 'undefined':
            return true
            break
        default:
            return _.isEmpty(data)
    }
}

module.exports.reduce = function(input, callback, options) {
    var output = []
    var errors = []
    var scriptPath = options.step.path + '@script'
    var index = 0
    var accumulator = input.accumulator
    var isObj = false
    var collection = input.collection
    if (typeof collection == 'object') {
        if (!_.isArray(collection)) {
            collection = Object.keys(collection)
            isObj = true
        }
    }

    var newStack = {}
    var lastOutput
    var initialVal = input.initial
    //make a shallow copy of stack to newStack
    //this is because new key will be added to newStack
    //and we do not want to pollute the parent's context
    var curKeys = Object.keys(options.stack)
    for (var i = 0; i < curKeys.length; i++) {
        newStack[curKeys[i]] = options.stack[curKeys[i]]
    }
    var iteratorKey = input.key
    var file = accumulator.file
    file = !isAbsolute(file) ? ROOT_DIR + '/' + file : file
    //loading here because if it's a parallel loop, it might choke the
    //loading with multiple parallel load requests
    options.engine.loadFlow({name: accumulator.file, file: file}, function(error) {
        if (error) {
            return callback(error)
        }
        var isInitial = true
        async.eachSeries(collection, function(elem, next)  {
            var value = isObj ? input.collection[elem] : elem
            if (isInitial && typeof initialVal === 'undefined') {
                isInitial = false
                lastOutput = value
                return next()
            }
            newStack[iteratorKey] = {
                current: value,
                previous: isInitial ? initialVal : lastOutput
            }
            isInitial = false
            var subFlowInput = EE.evalObject(accumulator.__input, newStack, scriptPath)
            subflow.execute({
                flow: {
                    file: accumulator.file,
                    input: subFlowInput
                }
            }, function(error, data) {
                if (error) {
                    return next(error)
                }
                lastOutput = data
                next()
            }, options)
        }, function(err) {
            var error = null
            if (errors.length > 0) {
                //@TODO - format properly
                error = new Error("Error occurred during reduce operation")
                error.data = errors
            }
            callback(error, lastOutput)
        })
    })
}