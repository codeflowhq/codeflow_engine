/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var isAbsolute = require('path-is-absolute')
var version = 0

try {
    version = parseFloat(process.versions['node'])
} catch (e) { }

//process.nextTick is faster in later versions, but can starve event loop
var trampolineFn =  process.nextTick

module.exports.execute = function(input, callback, options) {
    input = input || {}
    var name = input.flow ? input.flow.file : false
    if (!name) {
        return callback(new Error("An empty flow path was passed."))
    }
    var engine = options.engine
    var ROOT_DIR = global.ROOT_DIR || process.cwd()
    if (options.packageName) {
        name = '.packages' + '/' + options.packageName + '/' + name
    }
    //@TODO - cache this inside engine.execute -
    var file = isAbsolute(name) ? name : ROOT_DIR + '/' + name
    var sync = true
    engine.execute({
        file: file,
        name: name,
        input: input.flow.input,
        options: options,
        callback: function(error, data) {
            trampolineFn(function() {
                callback(error, data)
            })
        }
    })
    sync = false
}