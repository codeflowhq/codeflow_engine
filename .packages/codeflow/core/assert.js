/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

module.exports.evalAssertions = function(input, callback) {
    var okAssertions = input.assertions
    var output = true
    if (okAssertions) {
        for (var i = 0; i < okAssertions.length; i++) {
            if (!okAssertions[i]) {
                output = false
            }
        }
    }
    callback(null, output)
}