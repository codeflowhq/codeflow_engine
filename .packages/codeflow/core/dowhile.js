
var Path = require('path')
var _ = require('underscore')
var async = require('async')
var isAbsolute = require('path-is-absolute')

module.exports.dowhile = function(input, callback, options) {
    input = input || {}
    var name = input.flow ? input.flow.file : false

    if (!name) {
        return callback(new Error("An empty flow path was passed."))
    }

    var engine = options.engine
    var ROOT_DIR = global.ROOT_DIR || process.cwd()
    if (options.packageName) {
        name = '.packages' + '/' + options.packageName + '/' + name
    }
    //@TODO - cache this inside engine.execute -
    var file = isAbsolute(name) ? name :  ROOT_DIR + '/' + name

    var inpOptions = input.options || {}
    var cnt = 0
    var results = []
    async.doWhilst(function(next) {
        engine.execute({
            file: file,
            name: name,
            input: input.flow.input,
            options: options,
            callback: function(error, data) {
                cnt++
                if (inpOptions.ignoreError) {
                    error = null
                }
                if (inpOptions.maxCount && cnt >= inpOptions.maxCount) {
                    return next(error, false)
                }

                if (inpOptions.delay) {
                    setTimeout(function() {
                        next(error, data)
                    }, inpOptions.delay)
                } else {
                    next(error, data)
                }
            }
        })
    },function(result) {
        results.push(result)
        return result
    }, function(err, data) {
        callback(err, results)
    })
}