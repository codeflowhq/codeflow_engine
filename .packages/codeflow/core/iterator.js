/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var Path = require('path')
var _ = require('underscore')
var async = require('async')
var isAbsolute = require('path-is-absolute')
var subflow = require('./subflow')
//this should be changed to require('codeflow')
var EE = require('../../../src/eval.js')

/**
* Util function to determine if a value is empty.
* numbers and booleans are not considered empty even if they are
* evaluated to 0 or false.
*/
function isEmpty(data) {
    switch(typeof data) {
        case 'object':
        case 'array':
        case 'string':
            return _.isEmpty(data)
            break
        case 'number':
        case 'boolean':
            return false
            break
        case 'undefined':
            return true
            break
        default:
            return _.isEmpty(data)
    }
}

module.exports.iterate = function(input, callback, options) {
    var output = []
    var errors = []
    var scriptPath = options.step.path + '@script'
    var index = 0
    var fn = async.eachSeries
    var config = input.options || {}
    if (config.parallel) {
        if (config.limit) {
            fn = function(arr, iterator, cb) {
                async.eachLimit(arr, config.limit, iterator, cb)
            }
        } else {
            fn = async.each
        }
    }

    var iteratee = input.iteratee
    var isObj = false
    var collection = input.collection
    if (typeof collection == 'object') {
        if (!_.isArray(collection)) {
            collection = Object.keys(collection)
            isObj = true
        }
    }

    if (typeof collection == 'undefined') {
        collection = []
    }
    var newStack = {}

    //make a shallow copy of stack to newStack
    //this is because new key will be added to newStack
    //and we do not want to pollute the parent's context
    var curKeys = Object.keys(options.stack)
    for (var i = 0; i < curKeys.length; i++) {
        newStack[curKeys[i]] = options.stack[curKeys[i]]
    }
    var iteratorKey = input.key
    var file = iteratee.file
    file = !isAbsolute(file) ? ROOT_DIR + '/' + file : file
    //loading here because if it's a parallel loop, it might choke the
    //loading with multiple parallel load requests

    options.engine.loadFlow({name: iteratee.file, file: file}, function(error) {
        if (error) {
            return callback(error)
        }
        fn(collection, function(elem, cb)  {
            var key = isObj ? elem: index++
            var value = isObj ? input.collection[elem] : elem
            newStack[iteratorKey] = {
                value: value,
                index: key
            }
            var subFlowInput = EE.evalObject(iteratee.__input, newStack, scriptPath)
            subflow.execute({
                flow: {
                    file: iteratee.file,
                    input: subFlowInput
                }
            }, function(error, data) {
                if (error && !config.ignoreError) {
                    errors.push({
                        message: error.message,
                        path: error.path,
                        code: error.code,
                        data: error.data
                    })
                } else {
                    if (!(config.excludeEmpty && isEmpty(data))) {
                        output.push(data)
                    }
                }
                cb()
            }, options)

        }, function(err) {
            var error = null
            if (errors.length > 0) {
                //@TODO - format properly
                error = new Error("Error occurred during iterator")
                error.data = errors
            }
            callback(error, output)
        })
    })
}
