{
  "source": "dowhile.js",
  "method": "dowhile",
  "input-schema": {
    "type": "object",
    "additionalProperties": false,
    "properties": {
        "flow": {
            "title": "flow",
            "description": "A flow file that will be executed repeatedly until it returns false.",
            "type": "flow",
            "required": true
        },
        "options": {
            "type": "object",
            "description": "Optional properties",
            "depends": "{input.flowfile}",
            "additionalProperties": false,
            "properties": {
                "maxCount": {
                    "type": "integer",
                    "description": "Maximum number of times the loop should run. Defaults to infinite. Useful to prevent infinite loops."
                },
                "delay": {
                    "type": "integer",
                    "description": "Optional delay to be inserted for each loop. Value in milliseconds. Defaults to zero."
                }
            }
        }
    }
  },
  "output-schema": {
    "type": "array",
    "dynamic": {
      "target": "flow"
    }
  },
  "meta": {
    "name": "doWhile",
    "sort_index": 6,
    "description": "Do while loop. Invokes a sub flow repeatedly as long as it returns a non-empty value.",
    "icon": {
      "type": "fa",
      "name": "repeat",
      "color":"#747CA3"
    }
  }
}