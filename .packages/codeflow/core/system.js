/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

module.exports.bootup = function(input, callback, options) { 
    options.engine.listen('booted', function(data) {
        callback(null, {
            files: data
        })
    })
}