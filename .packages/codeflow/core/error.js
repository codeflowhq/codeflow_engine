/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var _Error = require('../../../src/error.js')

module.exports.error = function(input, callback) { 
	input = input || {}
    var error = new Error(input.message)
    error.code = input.code
    error.data = input.data
	callback(error)
}

module.exports.rethrow = function(input, callback, options) {
    input = input || {}
    var error = input.error
    if (!error || !error instanceof _Error) {
        error = new _Error("Invalid attempt to rethrow. Error field should be an instance of another error.")
    }
    callback(input.error)
}