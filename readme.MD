# Codeflow

Codeflow is a visual programming platform built on top of Node.js™ that makes it easy for developers to build high performance, scalable back ends. Codeflow programs are inherently asynchronous and look like a graph, so it's easy to write complex, asynchronous code without any callback hell.

**View full documentation on [codeflow.co/docs](http://codeflow.co/).**

## Setup

Codeflow programs are designed and tested using Codeflow designer. To download and setup the designer, please visit the [Codeflow](http://codeflow.co) home.

Codeflow engine is the runtime component of Codeflow. Even though the designer comes with a latest version of the engine embedded to test and debug Codeflow programs, the engine can be also be installed directly and invoked from a command line, which is the typical way for running Codeflow applications in production. 

## Installing the engine
```sh
$ sudo npm install -g codeflow
```

## Running engine

After installing the engine, Codeflow commands can be executed from a command line terminal as below:
```
codeflow [command] <options> <command params>
```

### Running flows

The run command is used to run a flow or entire project:

```bash
codeflow run <options> [path]
```
Where path can be a path to an individual flow, or the root folder of a Codeflow project. When the folder is passed, the project is started as whole, where all the trigger files inside the project are started by the engine automatically during startup. For more documentation visit the [deployment guide](http://codeflow.co/docs/language-guide/deployment/).

Examples:

To run a single flow:
```
codeflow run hello.flw
```

Run a flow with an input JSON:  
```
codeflow run -i '{"id": 1, "name": "Jack Sparrow"}' update.flw
```

Run a flow with an input JSON and print the output 
```
codeflow run -i '{"age": 30}' calculate.flw  -o
```

Start as project from the current directory
```
codeflow run .
```

Start as project from a specific path 
```
codeflow run -w /path-to-project .
```

### Installing packages

The install command is used to install a package

```
codeflow install <namespace/name@version>,<namespace/name@version>...
```

Example:

To install all the dependent packages, simply run install without any parameters at the project's root directory:
```
cd <project directory>
codeflow install
```

To install a specific package, run:
```
codeflow install codeflow/handlebars 
```

To install a specific version of the package, append `@<version>`:
```
codeflow install codeflow/handlebars@0.1.1
```

### Uninstalling packages
The uninstall command can be used to remove an unused package from the current project.

```
codeflow uninstall <namespace/name>,<namespace/name>...
```

**For more details, visit [http://codeflow.co/](http://codeflow.co/)**

## Releasing

```bash
cd <engine directory>
grunt
cd build && npm publish
```

## Building and pushing to Docker

### Updating Dockerfile

Open Dockerfile and update engine version to latest in below line:

```bash
RUN npm i codeflow@<latest version> -g
```

### Building docker image

```bash
cd <engine directory>
rm -rf build
docker build -t codeflowlang/codeflow --no-cache .
```

### Tag docker image with latest version

```bash
docker tag codeflowlang/codeflow:latest codeflowlang/codeflow:<version here>
```

### Push latest image to Docker hub

```bash
docker push codeflowlang/codeflow
docker push codeflowlang/codeflow:<version here>
```
