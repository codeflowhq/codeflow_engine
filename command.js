/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'
var nodeVersion = parseFloat(process.version.substring(1))
if (nodeVersion < 16) {
    console.error(`Node.js version ${nodeVersion} detected.\nCodeflow requires Node.js version 16.0 or above.`)
    process.exit(1)
}

var {Command} = require('commander')
var packageJson = require('./package.json')
var runner = require('./src/runner')
var logger = require('./src/logger')
var Constants = require('./src/constants')
var installer = require('./src/package/install')
var fs = require('fs')

var program = new Command()
var isCommandHandled = false
program.version(packageJson.version)

var showStartExamples = function() {
    logger.log('  ')
    logger.log('  Examples:')
    logger.log('')
    logger.log('    $ codeflow run . \t\t\t start a project from current folder')
    logger.log('    $ " run /path \t\t\t start a project under \'/path\'')
    logger.log('    $ " run ./hello.flw \t\t run a flow from current working directory \'./hello.flw\'')
    logger.log('    $ " run hello.flw  -o \t\t run and print the output returned')
    logger.log('    $ " run pi.flw -i \'{"cnt": 21}\' \t run with the given json as the input, or specify a json file path relative to the running file path')
    logger.log('    $ " run ../calc.flw -w /path \t run with \'/path\' as the working directory ')
    logger.log('')
}

var fileStats = function(path) {
    var stats = null
    try {
        stats = fs.lstatSync(path)
    } catch(e) {}
    return stats
}

program
    .command('run <path>')
    .description("start a flow or a project")
    .option('-i, --input <json>', 'pass a json as the input data')
    .option('-w, --working-dir <folder>', 'set the current working directory')
    .option('-o, --output', 'print the output returned.')
    .option('-e, --env <env>', 'set the environment.', /^(dev|qa|prod)$/i)
    .option('-d, --debug [ipc|tcp]', 'set the engine in debug mode')
    .option('-c, --optimize', 'run optimized')
    .option('-p, --debug-port <port>', 'change the default debugger port from '+ Constants.DEFAULT_DEBUGGER_PORT, parseInt)
    .option('--o-host <host>', 'set \'host\' as a global named `_CF_HTTP_HOST`')
    .option('--o-port <port>', 'set \'port\' as a global named `_CF_HTTP_PORT`', parseInt)
    .option('--runStep <runStep>', 'pass a module ref to run')
    .action(function(path, options) {
        /*var stats = fileStats(path)
        if (!stats) {
            logger.error("The path '" + path + "' is not a directory or a flow file. Not starting.")
            process.exit(1)
        } else {
            logger.log("Starting " + path)
        }*/
        isCommandHandled = true
        if (options.workingDir) {
            var stats = fileStats(options.workingDir)
            if (!stats) {
                logger.error("Working directory '" +  options.workingDir + "' couldn't be accessed. Does it exist? ")
                process.exit(1)
            } else if (!stats.isDirectory()) {
                logger.error("Working directory '" +  options.workingDir + "' is not a valid directory.")
                process.exit(1)
            }
        }

        runner.run(path, options, function(err, output) {
            if (err) {
                logger.error(err)
                process.exitCode = 1
                // return process.exit(1)
            }
            if (options.output) {
                logger.log(output)
            }
            //process.exit()
        })
    }).on('--help', function() {
          showStartExamples()
    })

program
    .command('install [packages]')
    .option('-p <path>', ' use <path> as the project directory')
    .description('install one or more package(s)')
    .action(function(packageName, cmd) {
        isCommandHandled = true
        var projectDir = cmd.projectDir || process.cwd()

        var packageNames = packageName ? packageName.split(',') : []

        if (packageNames && packageNames.length) {
            if (packageNames.length > 1) {
                console.log("Installing package(s)", packageNames.join(','))
            }
            installer.install(packageNames, null, projectDir, function(err) {
                if (err) {
                    logger.log("Installation failed: ", err.message)
                } else {
                    console.log("Installation completed")
                }
            })
        } else {
            console.log("Installing all the dependencies.")
            installer.installAll(projectDir, function(err, packages) {
                if (err) {
                    logger.log("Installation failed: ", err.message)
                } else if (packages && packages.length) {
                    console.log("Installation completed.")
                } else {
                    console.log("Nothing to install.")
                }
            })
        }
    }).on('--help', function() {
          console.log('  Examples:')
          console.log('')
          console.log('    $ codeflow i \t\t install all the dependent packages')
          console.log('    $ codeflow i <pckg> \t install the latest version of the package')
          console.log('    $ codeflow i <pckg>@<ver> \t install the specified version')
          console.log('')
    })

program
    .command('uninstall <package>')
    .description('uninstall the specified package')
    .option('-p, --project-dir <path>', 'use the given path as working directory.')
    .action(function(packageName, cmd) {
        isCommandHandled = true

        var projectDir = cmd.projectDir || process.cwd()
        installer.uninstall(packageName, projectDir, function() {
            logger.log("Uninstalled package:", packageName)
        })
    })

program
    .command('list')
    .description('print a list of installed packages ')
    .option('-p, --project-dir <path>', 'use the given path as working directory.')
    .action(function(cmd) {
        isCommandHandled = true
        var workingDir = cmd.projectDir || process.cwd()
        logger.log("Listing packages in :", workingDir)
    })

/*
 *
program
    .command('deploy [project]')
    .option('-h, --host <host>', 'Specify the host to deploy to.')
    .description('Coming soon. Deploy the specified project to a public domain.')
    .action(function(project) {
        isCommandHandled = true
        logger.log("This feature is coming soon!")
    })

program
    .command('edit <project>')
    .description('Open codeflow editor with the given project.')

program
    .command('search <package>')aq
    .description('Search for a package in codeflow package repository.')

program
    .command('list <package>')
    .description('list installed packages and their versions.')
*/

program.unknownOption = function() {}

var printExamples = function() {
  console.log('  For more help on each command, type:')
  console.log('')
  console.log('    $ codeflow [command] --help \t print more help on the command')
  console.log('')
}

program.on('--help', function() {
    printExamples()
})

program.parse(process.argv)

//no arguments, try executing current folder
if (!isCommandHandled) {
    console.log('')
    if (!program.args.length) {
        program.help()
    } else {
        console.log('  Unrecognized command "%s".', program.args.join(', '))
        showStartExamples()
    }
}
