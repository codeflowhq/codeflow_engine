/**
 * Migrate earlier, unversioned (0.0) format flows into the newer (0.1) format. 
 */

 var migrate = function() {
 	//@TBD
 }

 module.exports = {
 	from: '0.0',
 	to: '0.1',
 	migrate: migrate
 }