/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var Path = require('path')
var Package = require('./package')
var Util = require('./util')
var Constants = require('./constants')

function Step(id, properties, file) {
    this.id = id
    this.incoming = []
    this.outgoing = []
    this.incomingCount = 0
    this.outgoingCount = 0
    this.errorHandlerCount = 0
    this.isLoaded = false
    this.path = this.id + ' (' + file + ')'
    this.ref = properties.ref
    this.meta = properties.meta
    this.inputBinding = properties['input-binding']
    //Trigger & group type will be duplicated in the flow source as well
    //and not just in module definition. This will speedup loading
    //as package definition need to be looked up only during running
    if (properties.type == Constants.GROUP_TYPE) {
        this.isGroup = true
    }
    if (properties.type === Constants.TRIGGER_TYPE) {
        this.isTrigger = true
    } else if (this.ref === Constants.START_REF) {
        this.isStart = true
    } else if (this.ref === Constants.END_REF) {
        this.isEnd = true
    } else if (this.ref === Constants.ERROR_REF) {
        this.isError = true
    }

    this.file = file
    this.inputSchema = properties['input-schema'] || {}
    this.outputSchema = properties['output-schema'] || {}
    if (properties.data) {
        this.data = properties.data
    }

    return this
}

//load step's schema from module definition and load the code
//in memory
Step.prototype.load = function(cb) {
    if (this.isLoaded) {
        return cb()
    }
    var error = null
    var file = this.file
    var self = this
    var path = Path.join(ROOT_DIR, file)
    Package.getModuleDefinition(this.ref, path, function(err, definition) {
        if (err || !definition) {
            return cb(new Error("Could not find definition for: " + self.ref))
        }
        self.source = definition.source
        self.sourcePath = definition.path
        self.packageName = definition.packageType == 'installed' ? definition.package : ''

        if (Util.getFileExt(definition.source) !== 'flw') {
            self.method = definition.method
            if (definition['input-schema']) {
                //if exists in definition, and in properties, merge
                if (self.inputSchema) {
                    self.inputSchema = Util.extendSchema( Util.deepClone(definition['input-schema']), self.inputSchema)
                } else {
                    self.inputSchema = definition['input-schema']
                }
            }
            if (definition['output-schema']) {
                //if exists in definition, and in properties, merge
                if (self.outputSchema) {
                    self.outputSchema = Util.extendSchema(Util.deepClone(definition['output-schema']), self.outputSchema)
                } else {
                    self.outputSchema = definition['output-schema']
                }
            }
            self.loadCode(definition).then(response=>{
                if (response.error) {
                    error = response.error
                    return cb(error)
                } else {
                    self.code = response.code
                    self.isLoaded = true
                }
                return cb()
            }, error=>{
                return cb(error)
            })

        } else {
            self.flowfile = Path.join(definition.path, self.source)
            self.isLoaded = true
            return cb()
        }
    })
}

/**
 * Load code for native code modules.
 */
Step.prototype.loadCode = async function(definition, cb) {
    var error, code
    var self = this
    var codePath = Path.join(definition.path, this.source)

    try {
        code = await Util.loadModuleSource(codePath)

        if (!code || !code[self.method]) {
            error = new Error("Error loading JavasScript module. No method named " + self.method + " found in " + codePath)
        }
    } catch(e) {
        //node version
        var nodeVersion = parseFloat(process ? process.version.slice(1): 0)
        if (e.name === 'SyntaxError') {
            if (nodeVersion >= 6) {
                //For SyntaxError, line number and column number are available only on node version 6 and above.
                e.message = "Error loading JavaScript module: SyntaxError: " +  e.message + ": " + e.stack.split('\n').slice(0, 2)
            } else {
                //for all other versions, there is no line number information is available.
                e.message = "Error loading JavasScript module: SyntaxError: \"" + e.message + "\": at " + codePath
            }
        }
        error = e
    }
    return {
        error: error,
        code: code
    }
}

Step.prototype.addIncoming = function(transition) {
    this.incoming.push(transition)
    this.incomingCount++
}

Step.prototype.addOutgoing = function(transition) {
    this.outgoing.push(transition)
    this.outgoingCount++
    if (transition.type == 'error' || transition.type == 'always') {
        this.errorHandlerCount++
    }
}

Step.prototype.hasBreakpoint = function() {
    return this.meta && this.meta.breakPointBefore
}

Step.prototype.execute = function(input, options, callback) {
    var self = this
    this.load(function(error) {
        if (error) {
            return callback(error)
        }
        if (self.flowfile) {
            //@TODO we might need to load the file to resolve the actual path
            var cwd = global.ROOT_DIR
            return options.engine.execute({
                file: self.flowfile,
                packageName: self.packageName,
                name: Path.relative(cwd, self.flowfile),
                input: input,
                options: options,
                callback: callback
            })
        } else {
            try {
                return self.code[self.method].call(self.code, input, callback, options)
            } catch (error) {
                return callback(new Error("Error while executing native module. " + error.message + "\n" + error.stack))
            }
        }
    })
}

module.exports = Step