/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 *
 * Copyright 2016-2017 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var fs = require('fs')
var path = require('path')
var _ = require('underscore')
var Engine = require('./engine')
var Util = require('./util')
var Loader = require('./loader')
var Logger = require('./logger')

/**
 * Wont search for executable flow files in below paths/patterns
 */
var GLOBAL_IGNORED_FILE_LIST = [
    'node_modules',
    '.git',
    '.gitignore',
    '.DS_Store',
    '.logs',
    '.packages'
]

var getEngineInstance = function(params) {
    return new Engine(params)
}

/**
 * Execute a project or flow file..
 */
var run = function(projectOrFile, options, cb) {
    var nodeVersion = parseFloat(process.version.substring(1))
    if (nodeVersion < 16) {
        console.error(`Node.js version ${nodeVersion} detected.\nCodeflow requires Node.js version 16.0 or above.`)
        process.exit(1)
    }
    options = options || {}
    var absPath
    if (!projectOrFile) {
        Logger.log("No app or file specified. Proceeding with current working directory.")
        projectOrFile = process.cwd()
    }
    if (options.workingDir) {
        absPath = path.resolve(options.workingDir, projectOrFile)
    } else {
        absPath = projectOrFile
    }
    var resolvedPath = null
    var fsStat = null
    try {
        resolvedPath = fs.realpathSync(absPath)
        fsStat = fs.lstatSync(resolvedPath)
    } catch (err) {
        return cb(new Error("No matching file or directory named '" + absPath + "'"))
    }
    //if the path is not a file or directory, exit!
    if (!fsStat.isDirectory() && !fsStat.isFile()) {
        return cb(new Error("Path " + projectOrFile + " is not a directory or a flow file. Not starting"))
    }

    var rootDir
    if (fsStat.isDirectory()) {
        rootDir = resolvedPath
    } else {
        if (options.workingDir) {
            rootDir = fs.realpathSync(options.workingDir)
        } else {
            rootDir = process.cwd()
        }
    }
    //@TODO - get rid of this global stuff...
    global.ROOT_DIR = rootDir

    //set the engine dir
    global.ENGINE_DIR = __dirname

    //@TODO - why is it done?
    process.chdir(global.ROOT_DIR)

    //if possible, let it remain as evironment variable, instead of setting in global
    if (options.oPort) {
        global._CF_HTTP_PORT = options.oPort
    }
    if (options.oHost) {
        global._CF_HTTP_HOST = options.oHost
    }
    global._CF_DEBUG_ENABLED = !!options.debug

    global.ENVIRONMENT = options.env || "dev"

    var input = null
    loadProperties(global.ROOT_DIR).then(function(configData) {
        global.CONFIG = configData

        var isFile = fsStat.isFile()
        var fileUri = path.relative(ROOT_DIR, resolvedPath)
        if (isFile && Util.getFileExt(fileUri) == 'js') {
            require(path.join(path.resolve(ROOT_DIR), fileUri))
        } else {
            var params = {
                input: options.input,
                debug: options.debug,
                optimize: options.optimize,
                debugPort: options.debugPort
            }
            var engine = getEngineInstance(params)
            if (!params.debug && params.optimize) {
                console.log("Running engine in optimized mode. This is a beta feature, use with caution.")
                engine.execute = engine.executeOptimized
            }
            //pass the flow to the engine instance to execute
            var t1 = Date.now()
            engine.init(function() {
                if (options.runStep) { //run a single module with specified inputs
                    runStep(engine, fileUri, options.runStep, params, cb)
                } else if (isFile) {
                    runFile(engine, fileUri, params, cb)
                } else {
                    runProject(engine, resolvedPath, params)
                }
            })
        }
    }, cb)
}

/**
 * Run a single flow.
 */
function runFile(engine, fileUri, params, cb) {
    var input = null
    if (params.input) {
        try {
            input = JSON.parse(params.input)
        } catch (error) {
            if (!params.input.match(/\.json$/i)) {
                return cb(new Error(`Invalid JSON provided as input. ${error.message} - ${params.input}`))
            }

            let filePath = path.resolve(path.dirname(fileUri), params.input)
            console.log("Loading inputs from file " + filePath)

            if (!fs.existsSync(filePath)) {
                return cb(new Error("File " + filePath + " does not exist"))
            }
            try {
                input = JSON.parse(fs.readFileSync(filePath))
            } catch (error) {
                return cb(new Error("Invalid json input file"))
            }
        }
    }
    var options = {
        file: path.join(ROOT_DIR, fileUri), //file to run
        input: input, //input
        name: fileUri, //name of the file
        callback: function(err, data) {
            cb(err, data)
            engine.sendData(data)
        }
    }
    engine.execute(options, null, null, function(data) {
        engine.emit('booted', [data])
    })
}

function runStep(engine, file, ref, params, cb) {

    let id = null
    let parts = ref.split(':')
    if (parts.length > 1) {
        id = parts[0] !== '' ? parts[0] : id
        ref = parts[1]
    }
    let input = params.input ? JSON.parse(params.input) : null

    if (ref.indexOf('codeflow/') < 0 && ref.indexOf('core/') < 0) {
        return cb('Operation not permitted')
    }

    engine.executeStep({ id, ref, input, file }, null, null, (err, data) => {
        engine.sendData(data)
        cb(err, data)
    })
}

/**
 * Start an entire project. Will list all starter flows and execute them.
 */
function runProject(engine, projectRoot, params) {
    if (params.input) {
        console.log("Running as a project. Ignoring the input data.")
    }
    var executables = []
    listFiles(projectRoot, '', function(err, results) {
        if (err) {
            return Logger.error(err)
        }
        var length = results.length
        if (!length) {
            return Logger.log("No starter flows found under '" + projectRoot + "'")
        }
        Logger.log("Starting project under '" + projectRoot + "'")
        var completed = 0
        for (var i = 0; i < length; i++) {
            startExecution(engine, projectRoot, results[i], function(data) {
                if (data) {
                    executables.push(data)
                }
                if (++completed == length) {
                    engine.emit('booted', executables)
                }
            })
        }
    })
}

/**
 * Load configuration properties in the project.
 */
var loadProperties = async(roothPath)=> {
    var defaultConfig = path.join(roothPath, 'config.json')
    var envConfigPath = path.join(roothPath, 'config.' + (global.ENVIRONMENT || 'dev') + '.json')
    var projectPropsPath = path.join(roothPath, '.codeflow')

    var configData = {}
    if (fs.existsSync(defaultConfig)) {
        configData = JSON.parse(fs.readFileSync(defaultConfig))
        Logger.log("Loaded configuration from config.json")
    }
    if (fs.existsSync(envConfigPath)) {
        Logger.log(`${Object.keys(configData).length > 0 ? 'Merging': 'Loaded'} configuration from config.${global.ENVIRONMENT || 'dev'}.json`)
        var data = JSON.parse(fs.readFileSync(envConfigPath))
        configData = Util.mergeObjects(configData, data)
    }
    if(Object.keys(configData).length === 0) {
        Logger.log("No environment specific config found, using defaults")
    } else {
        global.CONFIG = configData
    }
    //load project properties file (.codeflow)
    if (fs.existsSync(projectPropsPath)) {
        var data = JSON.parse(fs.readFileSync(projectPropsPath))
        let envVars = data && data.envVars ? data.envVars : {}
        if(envVars && Object.keys(envVars)){
            Object.keys(envVars).forEach(k=>{
                process.env[k] = process.env[k] || envVars[k]
            })
        }
    }

    return configData
}

// http://stackoverflow.com/a/5827895/574716
var listFiles = function(dir, parent, done) {
    var results = []
    fs.readdir(dir, function(err, list) {
        if (err) return done(err)
        var pending = list.length
        if (!pending) {
            return done(null, results)
        }
        list.forEach(function(file) {
            if (GLOBAL_IGNORED_FILE_LIST.indexOf(file) > -1) {
                if (!--pending) {
                    return done(null, results)
                }
            } else {
                var fullPath = path.join(dir, file)
                fs.stat(fullPath, function(err, stat) {
                    if (stat && stat.isDirectory()) {
                        listFiles(fullPath, path.join(parent, file), function(err, res) {
                            results = results.concat(res)
                            if (!--pending) {
                                return done(null, results)
                            }
                        })
                    } else {
                        if (Util.getFileExt(file) == 'flw') {
                            results.push(path.join(parent, file))
                        }
                        if (!--pending) {
                            return done(null, results)
                        }
                    }
                })
            }
        })
    })
}

var startExecution = function(engine, projectPath, file, cb) {
    var fpath = path.join(projectPath, file)
    Loader.loadPackages(function() {
        Loader.loadFlow({
            name: file,
            file: fpath
        }, function(error, flow) {
            if (error) {
                return cb(error)
            }
            var start = flow.startStep
            if (start.isTrigger) {
                var params = {
                    input: null,
                    name: file,
                    flow: flow
                }
                Logger.log("Running starter flow '" + file + "'")
                return engine.execute(params, null, null, cb)
            } else {
                return cb()
            }
        })
    })
}

module.exports = {
    run: run,
    loadProperties: loadProperties
}