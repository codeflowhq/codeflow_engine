/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2024 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'


exports.error = function() {
    console.error.apply(console, arguments)
}
exports.warn = function() {
    console.warn.apply(console, arguments)
}
exports.info = function() {
    console.log.apply(console, arguments)
}
exports.log = function() {
    console.log.apply(console, arguments)
}
exports.debug = function() {
    console.log.apply(console, arguments)
}