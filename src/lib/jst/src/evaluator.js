/**
 * Evaluate JST templates
 */ 
(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.JstEvaluator = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    
    /* labels */
    var LBL_ITEM = '#item'
    var LBL_FOREACH = 'forEach'
    var LBL_IF = 'if'
    var LBL_ELSEIF = 'elseif'
    var LBL_ELSE = 'else'
    var LBL_CHOOSE = 'choose'    
    
    var util = require ? require('./util') : exports.JstUtil
    
    function Evaluator(context, methods) {
        this.context = context || {}
        this.methods = methods
    }
    
    /**
     * Evaluate JST
     */
    Evaluator.prototype.evaluate = function(data, result) {
        var ret
        if (util.isArray(data)) {
            ret = result || []
            this.evalArray(data, ret)
        } else if (typeof data == 'object') {
            ret = result || {}
            this.evalObject(data, ret)
        } else {
            ret = this.evalElement(data)
        }
        return ret
    }
    
    /**
     * Evaluate array fragment of a JST
     */
    Evaluator.prototype.evalArray = function(data, result) {
        var item, arItem, keys, ret
        for (var i = 0; i < data.length; i++) { 
            arItem = data[i]
            //because its preprocessed, preprocessor
            //always wraps array item under a #item tag
            //check if its #item
            keys = Object.keys(arItem)
            //var item = this.getArrayItem(arItem), ret
            if (keys[0] == LBL_ITEM) {
                item = arItem[keys[0]]
                ret = this.evaluate(item)
                result.push(ret)
            } else {
                //otherwise it could be a keyword, evaluate
                //the keyword
                item = this.evaluate(arItem, result)
            }
        }
        return result
    }
    

    /**
     * Evaluate object fragment of a JST
     */
    Evaluator.prototype.evalObject = function(data, result) {
        var keys = Object.keys(data)
        for (var k = 0; k < keys.length; k++) {
            var p = keys[k]
            var item = data[p]
            if (util.isKeyword(p)) {
                this.evalKeyword(p, item, result)
            } else if (result && util.isArray(result)) {
                //if result is array, add item to the result
                result.push(this.evaluate(item))
            } else {
                var key
                if (p[0] == '{' && p[p.length - 1] == '}') {
                    p = p.substring(1).substring(0, p.length - 2)
                    key = this.evaluateExpression(p)
                } else {
                    key = p
                }
                var value
                if (key[0] == '_' && key[1] == '_') {
                    value = item
                } else {
                    value = this.evaluate(item)
                }
                if (typeof value != 'undefined') {
                    result[key] = value
                }
            }
            
        }
        return result
    }
    
    /**
     * Evaluate a keyword
     */
    Evaluator.prototype.evalKeyword = function(key, data, result) {   
        var keyword = util.getKeyword(key), i
        //@choose is added by preprocessor for @if blocks, 
        //iterate each block and 
        if (keyword == LBL_CHOOSE) {
            var conditionFound = false, elseBlock
            for (i = 0; i < data.length; i++) {
                var item = data[i], label
                for (var k in item) {
                    if (item.hasOwnProperty(k)) {
                        label = k
                        break
                    }
                }
                var block = item[label]
                if (util.getKeyword(label) == LBL_ELSE) {
                    elseBlock = block
                } else {
                    var condition = util.getKeywordVal(label)
                    if (this.evaluateExpression(condition)) {
                        conditionFound = true
                        this.evaluate(block, result)
                        break
                    }
                }
            }
            if (!conditionFound) {
                this.evaluate(elseBlock, result)
            }
        } else if (keyword == LBL_FOREACH) {
            var val = util.getKeywordVal(key)
            var split = val.lastIndexOf(';')
            var exp = val.substring(0, split).trim()
            var name = val.substring(split + 1, val.length).trim()
            var iterator = this.evaluateExpression(exp)
            var tmp
            if (util.isArray(iterator)) {
                for (i = 0; i < iterator.length; i++) {
                    tmp = this.context[name] 
                    this.context[name] = {
                        index: i,
                        key: i,
                        value: iterator[i]
                    }
                    this.evaluate(data, result)
                    this.context[name] = tmp 
                }
            } else {
                i = 0
                for (var k in iterator) {
                    //save the existing key
                    tmp = this.context[name] 
                    this.context[name] = {
                        index: i++,
                        key: k,
                        value: iterator[k]
                    }
                    this.evaluate(data, result)
                    //and replace
                    this.context[name] = tmp 
                }
            }
        }
        return result
    }
    
    var evalString = new Function(['str'], 'return str')

    /**
     * Evaluate value of element
     */
    Evaluator.prototype.evalElement = function(data) {
        if (!data) {
            return data
        }
        if (typeof data != 'string') {
            return data
        }
        
        //@TODO - A hack added to prevent parsing strings thereby improving performance.
        //Currently " inside a string is not parsed properly
        //also \n\t etc are not parsed properly
        //This may be a non-issue when compilation
        if (data[0] == '"') {
            if (data[data.length - 1] == '"') {
                //Assuming it's just string            
                var str = data.substring(1, data.length - 1)
                return str.replace(/\\"/g, '"')
            }
        } else if (data[0] == 'r' && data[1] == 'e' && data[2] == 's' && data[3] == ':' && data[4] == '/' && data[5] == '/') {
            return data.substring(6)
        }        
        return this.evaluateExpression(data)
    }

    Evaluator.prototype.evaluateExpression = function(expression) {
        var compiled, result
        if (expression) {
            compiled = util.getCompiledExpression(expression)
            if (compiled) {
                result = compiled.call(null, this.methods, this.context)
            }
        }
        return result
    }
    
    /**
     * Exports
     */
    return Evaluator 
})