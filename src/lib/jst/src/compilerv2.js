/**
 * Compiler for JST v2
 */ 
(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.JstCompiler = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    
    /* labels */
    var LBL_ITEM = '#item'
    var LBL_FOREACH = 'forEach'
    var LBL_IF = 'if'
    var LBL_ELSEIF = 'elseif'
    var LBL_ELSE = 'else'
    var LBL_CHOOSE = 'choose'    
    
    //flag to indicate if the props should be in order
    //if the flag is false, it will result in a slightly improved performance but
    //the generated object will not guarantee to have properties in the same 
    //order as defined. It should run fine as the js runtime does not require you
    //to keep object props in the same order. But some programs might rely on it
    var KEEP_PROPS_ORDER = false 
    var RET_TYPE_COMPLEX = 'complex'
    var RET_TYPE_SIMPLE  = 'simple'

    var eeEvaluator = require ? require('../../expressionBuilder/src/evaluator') : exports.eeEvaluator
    var util = require ? require('./util') : exports.JstUtil
    
    var defaultOptions = {
        errorMethod: '_throwError'        
    }

    function Compiler(context, options) {
        this.options = options || defaultOptions    
        this.context = context || {}
    }
    
    Compiler.prototype.compileJst = function(data) {
        var res = this.compile(data)
        return res.result
    }
    
    Compiler.prototype.compile = function(data, varName, dontCall) {       
        var compiled
        if (util.isArray(data)) {
            compiled = this.compileArray(data, varName, dontCall)            
        } else if (typeof data == 'object') {          
            compiled = this.compileObject(data, varName, dontCall)            
        } else {
            compiled = {
                result: this.compileElement(data)
            }
        }
        return compiled
    }
        
    /**
     * Evaluate array fragment of a JST
     */
    Compiler.prototype.compileArray = function(data, pVarName, dontCall) {
        var varName = pVarName || 'arr'
        var output = ''
        var isSimple = true
        var i, item, arItem, ret
        var type = RET_TYPE_COMPLEX
        var isThereComplexKeyword = false
        //first pass to find out if it's a simple array - 
        for (i = 0; i < data.length; i++) {
            item = this.getArrayItem(data[i])
            if (item === null) {
                //isSimple = false
                break
            }            
        }
        var isSingleKeyword = (data.length == 1 && !isSimple)
        if (!pVarName) {
            for (i = 0; i < data.length; i++) {
                arItem = data[i]
                //because its preprocessed, preprocessor
                //always wraps array item under a #item tag
                //check if its item
                item = this.getArrayItem(arItem)
                if (item !== null) {
                    ret = this.compile(item)
                } else {
                    for (var prop in arItem) {
                        if (arItem.hasOwnProperty(prop)) {
                            break
                        }
                    }
                    if (util.isKeyword(prop)) {
                        ret = this.compileKeyword(prop, arItem[prop], varName)                            
                    } else {
                        //shouldnt come here
                    }
                }
                output += ('\n'+ (i > 0 ? ',\n': '') +  ret.result)
            }
            output = '[' + output + ']'
        } else {
            for (i = 0; i < data.length; i++) { 
                arItem = data[i]
                //because its preprocessed, preprocessor
                //always wraps array item under a #item tag
                //check if its item
                item = this.getArrayItem(arItem)
                if (item !== null) {
                    ret = this.compile(item)
                    output += ('\n' + varName + '.push(' + ret.result + ');')
                } else {
                    //otherwise it could be a keyword, evaluate
                    //the keyword
                    for (var prop in arItem) {
                        if (arItem.hasOwnProperty(prop)) {
                            break
                        }
                    }
                    if (util.isKeyword(prop)) {
                        var computed = this.compileKeyword(prop, arItem[prop], varName)
                        if (computed.type == RET_TYPE_COMPLEX) {
                            isThereComplexKeyword = true
                        }
                        output += (output ? ';':'') + computed.result
                    }
                }
            }
        }
        var result
        if (!pVarName && !isSimple && (!isSingleKeyword || isThereComplexKeyword)) {
            result = 'function(){ \nvar ' + varName + '= Array();' + output + '\nreturn ' + varName + ';\n}'
            if (!dontCall) {
                result = '(' + result + ')()'
            }
        } else {
            type = RET_TYPE_SIMPLE
            result = output
        }
        return {
            type: type,
            result: result
        }        
    }
    
    Compiler.prototype.escapeStr = function(str) {
        str =  str.replace(/(?:\r\n|\r|\n)/g, '\\n')
        return str.replace(/"/g, '\\"')
    }

    Compiler.prototype.compileElement = function(expr) {
        if (!expr || typeof expr != 'string') {
            return 'undefined'
        }        
        if (expr[0] == '"' && expr[expr.length - 1] == '"') {
            return '`' + expr.substring(1, expr.length - 1) + '`'
        }
        if (expr.indexOf("res://") === 0) {
            return '"' + expr.substring(6) + '"'
        }
        var compiled = this.compileExpression(expr)
        if (compiled) {
            if (compiled.declarations) {
                return '(function() {' +(compiled.declarations || '') + '; return ' + compiled.statement + '})()'
            } else {
                return compiled.statement
            }
        } else {
            throw new Error("Not a valid expression: " + expr)
        }
    }
    
    Compiler.prototype.compileObject = function(data, pVarName, dontCall) {
        var varName = pVarName || 'obj'
        var result = ''
        var arItem = this.getArrayItem(data)
        var type = RET_TYPE_COMPLEX
        if (varName == 'arr' && arItem !== null) {
            var compiled = this.compile(arItem, null)//, dontCall)
            type = compiled.type
            if (dontCall) {
                if (type == RET_TYPE_SIMPLE) {
                    result = 'return ' + compiled.result
                } else {
                    result = compiled.result
                }
            } else {
                result = varName + '.push(' + compiled.result + ')'
            }
        } else {
            var value
            var output = ''
            var keys = Object.keys(data)
            //add simple properties
            var cnt = 0
            var i, key
            var simpleObj = ''
            var processed = {}
            //first pass - this is to gather simple properties
            //for a direct object assignment
            if (!pVarName) {
                for (i = 0; i < keys.length; i++) {
                    key = keys[i]
                    if (!util.isKeyword(key) && !this.getExpressionProperty(key)) {
                        if (key[0] == '_' && key[1] == '_') {
                            value = JSON.stringify(data[key])
                        } else {
                            value = this.compile(data[key]).result
                        }
                        simpleObj += (cnt++ > 0 ? ',': '') + "'" + key + "':" + value + ''
                        processed[key] = true
                    } else {
                        //if the keep-order flag is set, then we should break the loop
                        //on the first occurrence of second-pass property assignemnt
                        //otherwise the order will mix up.
                        if (KEEP_PROPS_ORDER) {
                            break
                        }
                    }
                }
                if (simpleObj) {
                    simpleObj = '{' + simpleObj + '}'
                }
            }
            for (i = 0; i < keys.length; i++) {
                key = keys[i]
                if (processed[key]) {
                    continue
                }
                var item = data[key]
                if (util.isKeyword(key)) {
                    output += this.compileKeyword(key, item, varName, dontCall).result
                } else {
                    if (key[0] == '_' && key[1] == '_') {
                        value = JSON.stringify(item)
                    } else {
                        value = this.compile(item).result
                        var exprKey = this.getExpressionProperty(key)
                        if (exprKey) {
                            key = this.compileElement(exprKey)
                            output += ('\n' + varName + "[" + key + "] = " + value + ';')
                        } else {
                            output += ('\n' + varName + "['" + key + "'] = " + value + ';')
                        }
                    }
                }
            }
            if (output && !pVarName) {
                result = 'var ' + varName + '=' + (simpleObj || '{}') + ';'
                result += output + '; return ' + varName + ';'
                if (!dontCall) {
                    result = '(function() { ' +  result + '})()'
                }
                type = RET_TYPE_COMPLEX
            } else {
                result += (simpleObj + output) || '{}'
                type = RET_TYPE_SIMPLE
            }
        }
        return {
            type: type,
            result: result
        }
    }
    
    /**
     * A special case of evaluating object keys contained inside curly braces.
     */
    Compiler.prototype.getExpressionProperty = function(key) {
        if (key[0] == '{' && key[key.length - 1] == '}') {
            return key.substring(1).substring(0, key.length - 2)
        }
        return null
    }

    /**
     * Get the object/property wrapped under the #item tag
     */
    Compiler.prototype.getArrayItem = function(data) {
        var ret = null
            for (var p in data) {
                if (data.hasOwnProperty(p)) {
                    if (p == LBL_ITEM) {
                        ret = data[p]
                    }
                    break
                }
            }
        return ret
    }
    
    /**
     * Evaluate a keyword
     */
    Compiler.prototype.compileKeyword = function(key, data, varName, dontCall) {
        var compiled = ''
        var blockCompiled
        var keyword = util.getKeyword(key)
        var i
        var type = RET_TYPE_COMPLEX
        //@choose is added by preprocessor for @if blocks, 
        //iterate each block and 
        if (keyword == LBL_CHOOSE) {
            type = RET_TYPE_SIMPLE
            for (i = 0; i < data.length; i++) {
                var item = data[i], label
                for (var k in item) {
                    if (item.hasOwnProperty(k)) {
                        label = k
                    }
                }
                var block = item[label]
                var condition
                if (util.getKeyword(label) == LBL_ELSEIF) {
                    condition = this.compileElement(util.getKeywordVal(label))
                    compiled += '\nelse if (' + condition + ') {'
                } else if (util.getKeyword(label) == LBL_ELSE) {
                    compiled += '\nelse {'
                } else {
                    condition = this.compileElement(util.getKeywordVal(label))
                    compiled += '\nif (' + condition + ') {'                    
                }
                blockCompiled = this.compile(block, varName, dontCall)
                compiled += blockCompiled.result
                compiled += '\n}'
                if (blockCompiled.type == RET_TYPE_COMPLEX) {
                    type = RET_TYPE_COMPLEX
                }
            }
            
        } else if (keyword == LBL_FOREACH) {
            var val = util.getKeywordVal(key)
            var split = val.lastIndexOf(';')
            var exp = val.substring(0, split).trim()
            var name = val.substring(split + 1, val.length).trim()            
            var iterator = this.compileElement(exp)
            for (var k in data) {
                if (data.hasOwnProperty(k)) {
                    label = k
                    break
                }
            }
            if (label) {
                var isKeyword = util.isKeyword(label)
                blockCompiled = this.compile(data, varName, true)
                if (isKeyword) {
                    compiled = '__methods.mapInternal(' + iterator + ', function('+ name + ') {' + blockCompiled.result + '})'
                } else if (blockCompiled.type == RET_TYPE_SIMPLE) {
                    type = RET_TYPE_SIMPLE
                    compiled = '__methods.mapInternal(' + iterator + ', function('+ name + ') {' + (varName == 'obj' ? '' : 'return ') + blockCompiled.result + '})'
                } else {
                    var operation
                    if (varName == 'obj') {
                        operation = 't(' + name + ')'
                    } else {
                        operation = varName + '.push(t(' + name + '))'
                    }
                    compiled = '(function(){ '+ 
                                    'var t = function(item) {' + 
                                        (varName == 'obj' ? '' : 'return ') + blockCompiled.result + 
                                    '};\n ' + 
                                    'return __methods.mapInternal(' + iterator + ', function('+ name + ') { ' 
                                        + operation 
                                    + '})' +
                                '})()'
                }
            }
        }
        return {
            type: type,
            result: compiled
        }
    }
    
    /**
     * Compile expression
     */
    Compiler.prototype.compileExpression = function(expression) {        
        var compiled = {}
        if (expression) {
            compiled = eeEvaluator(expression, true)
        }
        return compiled
    }

    /**
     * Exports
     */
    return Compiler 
})