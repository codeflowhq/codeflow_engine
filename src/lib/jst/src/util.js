/**
 * JST Utils
 */ 
(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.JstUtil = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    var LBL_ITEM = '#item'
    var LBL_FOREACH = 'forEach'
    var LBL_IF = 'if'
    var LBL_ELSEIF = 'elseif'
    var LBL_ELSE = 'else'
    var LBL_CHOOSE = 'choose'
    
    var eeEvaluator = require ? require('../../expressionBuilder/src/evaluator') : exports.eeEvaluator
    var eeCache = {}
    
    /**
     * Pre-process jst data.
     */
    function preProcess(data, root) {        
        var chooseIdx = 0
        var type = typeof data
        if (isArray(data)) {
            root = preProcessArray(data)
        } else if (type == 'object' && data != null) {
            root = root || (isArray(data) ? [] : {})
            var keys = Object.keys(data)
            for (var i = 0, length = keys.length; i < length; i++) {
                var prop = keys[i]
                var item = data[prop]
                if (isKeyword(prop)) {
                    var keyword = getKeyword(prop)
                    //wrap @if/@elseif and @else inside @choose element
                    if (keyword == LBL_IF) {
                        var chooseData = preProcessIf(prop, item)
                        root['@' + LBL_CHOOSE +' ('+ (chooseIdx++) + ')'] = chooseData
                    } else { 
                        root[prop] = preProcess(item)
                    }
                } else if (isArray(item)) {
                    root[prop] = preProcessArray(item)
                } else if (typeof item == 'object') {
                    root[prop] = preProcess(item)
                } else {
                    root[prop] = item
                }
            }
        } else {
            root = data
        }
        return root
    }

    /**
     * Special processing for if blocks. Currently because json/javascript 
     * objects do not support duplicate keys, else and ifelse blocks are 
     * specified inside the if block itself. Preprocessing logic below moves 
     * them to a parent element (@choose)
     */
    function preProcessIf(statement, value) {
        var wrap = [], 
        token, ifObj = {}, 
        children = {}, 
        allChildren = {}, 
        tmp,
        block, 
        items, 
        label

        ifObj[statement] = {}
        wrap.push(ifObj)                            
        var elseFound = false

        //extract elseif and else
        for (var prop in value) {
            if (value.hasOwnProperty(prop)) {
                if (isKeyword(prop)) {
                    token = getKeyword(prop)
                    if (token == LBL_ELSEIF || token == LBL_ELSE) {
                        tmp = {}
                        tmp[prop] = value[prop]
                        wrap.push(tmp)
                        delete value[prop]
                        if (token == LBL_ELSE) {
                            elseFound = true
                        }                      
                    } else {
                        ifObj[statement][prop] = value[prop]
                    }
                } else {
                    ifObj[statement][prop] = value[prop]
                }
            }
        }

        /** - adding empty else 
        //add empty else block if not present
        if (!elseFound) {
            tmp = {}
            tmp['@'  + LBL_ELSE] = {}
            wrap.push(tmp)
        }
        */
        //iterate through each block and process them
        for (var i = 0; i < wrap.length; i++) {
            block = wrap[i]
            for (var key in block) {
                if (block.hasOwnProperty(key)) {
                    items = block[key]
                    label = key
                }
            }
            children[label] = {}
            block[label] = preProcess(items)
            for (prop in block[label]) {
                if (block[label].hasOwnProperty(prop)) {
                    if (isKeyword(prop)) {
                        //get child elements recursively and store it
                        tmp = getChildElements(prop, block[label][prop]).elements
                        for (var t in tmp) {
                            if (tmp.hasOwnProperty(t)) {
                                allChildren[t] = children[label][t] = tmp[t]
                                //remove duplicate fields, inner ones get 
                                //preference
                                if (block[label][t]) {
                                    delete block[label][t]
                                }
                            }                  
                        }
                    } else {
                        allChildren[prop] = children[label][prop] = block[label][prop]
                    }
                }
            }
        }

        //1iterate through all if/else/elsif blocks and fill missing elements 
        for (i = 0; i < wrap.length; i++) {
            block = wrap[i]
            for (var key in block) {
                if (block.hasOwnProperty(key)) {
                    items = block[key]
                    label = key
                }
            }
            //check for missing properties in sibling blocks and add if not 
            //present in the current block but are inside other blocks
            for (var element in allChildren) {
                if (allChildren.hasOwnProperty(element)) {                    
                    if (typeof children[label][element] == 'undefined') {
                        var val = allChildren[element]
                        if (isArray(val)) {
                            items[element] = []
                        } else if (typeof val == 'object') {
                            items[element] = {}
                        } else {
                            items[element] = ""
                        }
                    }
                }
            }
        }

        //finally return
        return wrap
    }

    /**
     * Process each item in an array. Wrap each item inside a LBL_ITEM tag.
     */
    function preProcessArray(arr) {        
        var result = [], item            
        for (var i = 0, len = arr.length; i < len; i++) {
            item = arr[i]
            //if its an array, recurse
            if (isArray(item)) {
                item = preProcessArray(item)
            } else {
                //otherwise process
                item = preProcess(item)
            }
            //wrap each item under LBL_ITEM if it doesnt exist already.
            if (!checkItemWrapper(item)) {
                var tmp = {}
                tmp[LBL_ITEM] = item
                item = tmp
            }
            result.push(item)
        }
        return result
    }

    /**
     * Recursively check if LBL_ITEM wrap is found.
     */
    function checkItemWrapper(item, flag) {
        var itemTagFound = false
        // if its an array or simple element, wrap is not there
        if (typeof item == 'object' && !isArray(item)) {
            var count = 0, key
            for (var prop in item) {
                if (item.hasOwnProperty(prop)) {
                    count++
                    key = prop
                }
            }
            if (count == 1 && isKeyword(key)) {
                var keyword = getKeyword(key)
                if (keyword == LBL_CHOOSE) {
                    //process each item under choose
                    for (var i = 0; i < item[key].length; i++) {
                        var ifEl = item[key][i], ifKey
                        //get key in object
                        for (var k in ifEl) {
                            if (ifEl.hasOwnProperty(k)) {
                                ifKey = k
                            }
                        }

                        if (ifKey && checkItemWrapper(ifEl[ifKey])) {
                            itemTagFound = true
                        }
                    }
                } else {
                    itemTagFound = checkItemWrapper(item[key])
                }                
            } else {
                if (key == LBL_ITEM) {
                    itemTagFound = true
                }
            }
        }

        return itemTagFound
    }


    /**
     * Recursively iterate and find child elements inside an object or keyword
     * block
     */
    function getChildElements(label, data) {        
        var children = [], 
        item, 
        elements = {},
        wrap = {}

        wrap[label] = {}

        //if its choose keyword, iterate through all child elements
        if (getKeyword(label) == LBL_CHOOSE) {
            for (var i = 0; i < data.length; i++) {
                var props, k
                item = data[i]
                for (var key in item) {
                    if (item.hasOwnProperty(key)) {
                        props = item[key]
                        k = key
                    }
                }
                children.push(getChildElements(k, props))
            }
            wrap[label] = data
        } else if (typeof data == 'object') {
            for (item in data) {
                if (data.hasOwnProperty(item)) {
                    if (isKeyword(item)) {
                        var ret = getChildElements(item, data[item])
                        children.push(ret)
                        wrap[label][item] = ret.obj[item]
                    } else {                     
                        wrap[label][item] = data[item]
                        elements[item] = data[item]
                    }
                }
            }
        }

        for (i = 0; i < children.length; i++) {
            var el = children[i]
            for (var k in el.elements) {
                if (el.elements.hasOwnProperty(k)) {
                    elements[k] = el.elements[k]
                }
            }
        }

        return {
            obj: wrap, 
            elements: elements
        }
    }    

    /**
     * Get the bare keyword from the label. Assumes that keyword start with 
     * @ and end with ( or empty, tolerating white spaces in between
     */
    function getKeyword(keyword) {        
        if (keyword.indexOf('@') === 0) {
            keyword = keyword.split('@')[1]
            return keyword.trim().split('(')[0].trim()
        } else {
            return null
        }
    }


    function getKeywordVal(keyword) {                
        if (keyword.indexOf('(') >= 0 && keyword.indexOf(')') >= 0) {
            return keyword.substring(keyword.indexOf('(') + 1, keyword.lastIndexOf(')'))
        } else {
            return ''
        }
    }

    /**
     * Check if given statement is a keyword or not
     */
    function isKeyword(label) {
        return label.indexOf('@') === 0
    }
    
    /**
     * Returns a unique, css selector path for the given dom, 
     * relative to the container (.jst_builder)
     * //@TODO useful for maintaining selection after
     * rendering
     */
    function getDomPath(el) {
        var path = ''            
        var child = el
        el.parents().each(function(i) {
            var parent = $(this)
            var className = 
                child.hasClass('element') ? '.element' :
                (child.hasClass('item') ? '.item' : 
                 child.hasClass('object') ? '.object': (
                  child.hasClass('keyword') ? '.keyword' : ''
                ))
            var idx = parent.children(className).index(child)
            path = child.get(0).tagName + className + ':eq(' + idx + ')' + (path ? '>' + path : '')
            if (parent.hasClass('jst_builder')) {
                return false
            }
            child = parent
        })
        return path
    }
    
    function addQuotes(val) {
        if (val.indexOf('"') == '0' && val.lastIndexOf('"') == val.length - 1) {
            return val
        }
        return '"' + val + '"'
    }
    
    function stripQuotes(val) {
        if (val.indexOf('"') == '0' && val.lastIndexOf('"') == val.length -1 ) {
            return val.substring(1, val.length - 1)
        }
        return val
    }
    
    function cloneObj(obj) {
        return JSON.parse(JSON.stringify(obj))
    }
    
    var isArray = Array.isArray || function (xs) {
        return Object.prototype.toString.call(xs) === '[object Array]'
    }      
    
    var  getCompiledExpression = function(expression) {
        if (!eeCache[expression]) {
            eeCache[expression] = eeEvaluator(expression)
        }
        return eeCache[expression] 
    }
    
    var clearExpressionCache = function() {
        eeCache = {}
    }
    
    var size = function(obj) {
        var len = 0
        obj = obj || {}
        if (Array.isArray(obj)) {
            len = obj.length
        } else {
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    len++
                }
            }
        }
        return len
    }
    
    return {
        preProcess: preProcess,
        isKeyword: isKeyword,
        getKeyword: getKeyword,
        getKeywordVal: getKeywordVal,
        getChildElements: getChildElements,
        getDomPath: getDomPath,
        addQuotes: addQuotes,
        stripQuotes: stripQuotes,
        cloneObj: cloneObj,
        isArray: isArray,
        size: size,
        getCompiledExpression: getCompiledExpression,
        clearExpressionCache: clearExpressionCache
    }
})