(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsFast = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var _ = require ? require('underscore') : exports._

    return {
        map: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute:
            function(collection, fn) {
                var idx = 0
                return _.map(collection, function(item, key) {
                    return fn(item, key, idx++)
                })
            }
        },
        groupBy: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.groupBy
        },
        find: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.find
        },
        filter: {
            hidden: true,
            description: "Used internally.",
            execute: _.filter
        },
        reduce: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.reduce
        },
        sortBy: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.sortBy
        },
        minOf: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.min
        },
        maxOf: {
            hidden: true,
            description: "Used internally by compiled snippets.",
            execute: _.max
        }
    }
}, this || (typeof window != 'undefined' ? window : null))
