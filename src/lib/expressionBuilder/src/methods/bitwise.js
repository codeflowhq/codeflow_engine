(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsBitwise = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    return {
        bNot: {
            isCore: true,
            description: "Bitwise NOT. Reverse all the bits in the value.",
            arguments: [
                {
                    name: "value",
                    type: "integer",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1) {
                return ~arg1
            }
        },
        bOr: {
            isCore: true,
            description: "Bitwise OR. Performs a boolean OR operation on each bit of the operands.",
            arguments: [
                {
                    name: "left",
                    type: "integer",
                    required: true
                },
                {
                    name: "right",
                    type: "integer",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 | arg2
            }
        },
        bAnd: {
            isCore: true,
            description: "Bitwise AND. Performs a boolean AND operation on each bit of the operands.",
            arguments: [
                {
                    name: "left",
                    type: "integer",
                    required: true
                },
                {
                    name: "right",
                    type: "integer",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 & arg2
            }
        },
        bXor: {
            isCore: true,
            description: "Bitwise XOR. Performs a boolean exclusive OR on each bit of the operands.",
            arguments: [
                {
                    name: "left",
                    type: "integer",
                    required: true
                },
                {
                    name: "right",
                    type: "integer",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 ^ arg2
            }
        },
        bLShift: {
            isCore: true,
            description: "Bitwise left shift. Moves all the bits to the left by number of places specified in the second argument. New bits are filled with zero.",
            arguments: [
                {
                    name: "value",
                    type: "integer",
                    required: true
                },
                {
                    name: "number",
                    type: "integer",
                    description: "Number of spaces to move",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 << arg2
            }
        },
        bRShift: {
            isCore: true,
            description: "Bitwise right shift (Sign-propagating right shift).  Moves all the bits to the right by the number of places specified in the second argument. The sign of value is preserved. New bits are filled with zero or one depending on the sign (Zero for positive and One for negative).",
            arguments: [
                {
                    name: "value",
                    type: "integer",
                    required: true
                },
                {
                    name: "number",
                    type: "integer",
                    description: "Number of spaces to move",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 >> arg2
            }
        },
        bURShift: {
            isCore: true,
            description: "Unsigned bitwise right shift (Zero-fill right shift). Moves all the bits to the right by the number of places specified in the second argument. Sign is not preserved. New bits are filled with zero.",
            arguments: [
                {
                    name: "value",
                    type: "integer",
                    required: true
                },
                {
                    name: "number",
                    type: "integer",
                    description: "Number of spaces to move",
                    required: true
                }
            ],
            returnType: "integer",
            execute: function(arg1, arg2) {
                return arg1 >>> arg2
            }
        },
    }
}, this || (typeof window != 'undefined' ? window : null))
