(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsJson = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    return {
        stringify: {
            description: "Convert an Object to JSON string.",
            arguments: [
            {
                name: "value",
                type: "any",
                required: true,
                description: "The value to convert to a JSON string."
            },
            {
                name: "space",
                type: "integer",
                description: "If specified, output JSON will be pretty printed with the given number of white spaces. The number is capped at 10"
            }
            ],
            returnType: "string",
            execute: function(obj, tabStops) {
                return JSON.stringify(obj, null, typeof tabStops === "number" ? tabStops: null)
            }
        },
        parse: {
            description: "Convert JSON String to Object.",
            arguments: [
            {
                name: "string",
                type: "any",
                required: true,
                description: "JSON string"
            }
            ],
            returnType: "any",
            execute: function(str) {
                if(!str || typeof str === 'undefined') {
                    return null
                }
                return JSON.parse(str)
            }
        },
    }
}, this || (typeof window != 'undefined' ? window : null))