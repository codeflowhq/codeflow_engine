(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsCollection = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var _ = require ? require('underscore') : exports._
    var helper = require ? require('./_helper') : {}

    return {
        filter: {
            isCore: true,
            description: "A method to extract elements from a given collection. Returns a new array by passing each each item in the collection through a filter expression and picking only the ones where the expression evaluated to true.",
            examples: [
                "filter(array(1,2,3), f(gt(item, 1)) //returns [2, 3]"
            ],
            arguments: [
            {
                name: "collection",
                type: "any", //@TODO update JSON schema to support array of types.
                description: "A collection to filter. Can be an array or an object.",
                required: true
            },
            {
                name: "filter",
                type: "function",
                required: true,
                description: "A boolean expression that is applied to each item in the collection. The item is chosen only if the expression evaluates to true.",
                arguments: function(method) {
                    var listSchema = (method.arguments && method.arguments.length > 0) ? method.arguments[0] : {}
                    var isObject = listSchema && listSchema.type === 'object'
                    var isArray = listSchema && listSchema.type === 'array'
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {   //first argument is the array item
                        label: 'item',
                        schema: itemSchema
                    },
                    {   //second argument is the key
                        label: "key",
                        schema: {
                            "type": isArray ? "integer" : (isObject ? 'string' : 'unknown')
                        }
                    },
                    {   //third argument is the index
                        label: "index",
                        schema: {
                            "type": "integer"
                        }
                    }
                    ]
                },
                returnType: "boolean"
            }
            ],
            returnType: function(argsType) {
                return Array.isArray(argsType) ? argsType[0] : {
                    "type": "any"
                }
            },
            execute: function(collection, fn) {
                //@TODO - change to _.find as compiled mode uses that and can cause disparities in results
                var filtered, i
                if (collection !== null && typeof collection === 'object') {
                    var isArray = Array.isArray(collection)
                    var keys = isArray ? collection : Object.keys(collection)
                    filtered = isArray ? [] : {}
                    for(i = 0; i < keys.length; i++) {
                        var item = isArray ? collection[i]: collection[keys[i]]
                        var funContext = {
                            item: item,
                            key: isArray ? i : keys[i],
                            index: i
                        }
                        if (fn.apply(this, [funContext])) {
                            if (isArray) {
                                filtered.push(item)
                            } else {
                                filtered[keys[i]] = item
                            }
                        }
                    }
                }
                return filtered
            }
        },
        find: {
            isCore: true,
            description: "Find a single item in a collection. The filter function is invoked for each item in the collection. If the filter returns true then the corresponding item is returned. Find stops searching on the first match. Use filter if you want to return all matching items.",
            examples: [
                "find(array(1,2,3), f(gt(item, 1)) //returns 2"
            ],
            arguments: [
            {
                name: "collection",
                required: true,
                type: "any", //@TODO update JSON schema to support array of types.
                description: "Collection to filter"
            },
            {
                name: "filter",
                required: true,
                description: "An expression to filter the items in the collection. For each item in the collection, " +
                "the expression is executed. If the expression evaluates to true, "  +
                "that item is returned",
                type: "function",
                arguments: function(method) {
                    var listSchema = (method.arguments && method.arguments.length > 0) ? method.arguments[0] : {}
                    var isObject = listSchema && listSchema.type === 'object'
                    var isArray = listSchema && listSchema.type === 'array'
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                        {   //first argument is the array item
                            label: 'item',
                            schema: itemSchema,
                            description: "Each item in the collection."
                        },
                        {   //second argument is the key
                            label: "key",
                            schema: {
                                type: isArray ? "integer" : (isObject ? 'string' : 'unknown'),
                                description: "Key. Depending on the type of the collection, it's either integer(for arrays) or string(for objects)"
                            },
                            description: "Key. Depending on the type of the collection, it's either integer(for arrays) or string(for objects)"
                        },
                        {   //third argument is the index
                            label: "index",
                            schema: {
                                type: "integer",
                                description: "Counter"
                            },
                            description: "Counter"
                        }
                    ]
                },
                returnType: "boolean"
            }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(collection, fn) {
                var search, i
                if (collection !== null && typeof collection === 'object') {
                    var isArray = Array.isArray(collection)
                    var keys = isArray ? collection :  Object.keys(collection)
                    for(i = 0; i < keys.length; i++) {
                        var item = isArray ? collection[i]: collection[keys[i]]
                        var funContext = {
                            item: item,
                            key: isArray ? i : keys[i],
                            index: i
                        }
                        if (fn.apply(this, [funContext])) {
                            search = item
                            break
                        }
                    }
                }
                return search
            }
        },
        mapInternal: {
            hidden: true,
            description: "Used internally by JST",
            execute: function(collection, fn) {
                var result
                if (typeof collection !== 'undefined') {
                    var i = 0
                    result = _.map(collection, function(value, key) {
                        var funContext = {
                            value: value,
                            key: key,
                            index: i++
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        map: {
            isCore: true,
            description: "Return a new array by mapping each value in the input collection through the mapper function.",
            examples: [
                "map(array(1,2,3), f(add(item, 1)) //returns [2, 3, 4]"
            ],
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Input collection. Can be an array or object. For each item in the collection, the mapper expression will be applied.",
                required: true
            },
            {
                name: "mapperFunction",
                type: "function",
                description: "Called for each item in the collection. Output will be a new array with all the items returned by this expression.",
                required: true,
                arguments: function(method) {
                    var itemSchema
                    var listSchema = (method.arguments && method.arguments.length > 0) ? method.arguments[0] : {}
                    var isObject = listSchema && listSchema.type === 'object'
                    var isArray = listSchema && listSchema.type === 'array'
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {
                        label: 'item',
                        schema: itemSchema
                    },
                    {   //second argument is index
                        label: "key",
                        schema: {
                            "type": isArray ? "integer" : (isObject ? 'string' : 'unknown')
                        }
                    },
                    {
                        label: "index",
                        schema: {
                            "type": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "any"
                }
            }
            ],
            returnType: function(argsType) {
                return {
                    type: "array",
                    items: argsType && argsType.length > 1 ? argsType[1] : {
                        type: "any"
                    }
                }
            },
            execute: function(collection, fn) {
                var result
                if (typeof collection !== 'undefined') {
                    var i = 0
                    result = _.map(collection, function(value, key) {
                        var funContext = {
                            item: value,
                            key: key,
                            index: i++
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        reduce: {
            isCore: true,
            description: "Convert a collection of values into a single value.",
            examples: [
                "reduce(array(1,2,3), f(add(previous, current)) //returns 6 (1+2+3)"
            ],
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Input collection. Can be an array or object.",
                required: true
            },
            {
                name: "callback",
                type: "function",
                description: "Reducer expression called for each item in the list. The output of this expression is fed to the next item's reducer.",
                required: true,
                arguments: function(method) {
                    var listSchema = (method.arguments && method.arguments.length > 0) ? method.arguments[0] : {}
                    var isObject = listSchema && listSchema.type === 'object'
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    var initialValue = (method.arguments && method.arguments.length > 2) ? method.arguments[2] : {}
                    var initSchema = initialValue && initialValue.type ? initialValue : itemSchema
                    return [
                    {
                        label: 'previous',
                        schema: _.extend(initSchema, {description: "Previous value"})
                    },
                    {
                        label: "current",
                        schema: _.extend(itemSchema, {description: "Current value"})
                    },
                    {
                        label: "index",
                        schema: {
                            "type": isObject ? "string": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "any"
                }
            },
            {
                name: "initialValue",
                type: "any",
                description: "Optional. Initial value to be used as previous in the callback."
            }
            ],
            returnType: function(argsType) {
                return argsType && argsType.length > 1 ? argsType[1] : {
                    type: "any"
                }
            },
            execute: function(collection, fn, memo) {
                var result
                if (collection) {
                    var reducer = function(prev, curr, index) {
                        var funContext = {
                            previous: prev === null ? 'undefined' : prev,
                            current: curr,
                            index: index
                        }
                        return fn.apply(this, [funContext])
                    }
                    if (memo !== null) {
                        result = _.reduce(collection, reducer, memo)
                    } else {
                        result = _.reduce(collection, reducer)
                    }
                }
                return result
            }
        },
        group: {
            isCore: true,
            description: "Splits the collection into groups. The key for grouping is determined by the iteratee function",
            examples: [
                'group(array("sam", "john", "aby"), f(size(item))) //returns {"3": ["sam", "aby"], "4": ["john"]}'
            ],
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array or object",
                required: true
            },
            {
                name: "iteratee",
                type: "function",
                description: "Function called for each item in the list. Items are grouped under the key returned by the function.",
                required: true,
                arguments: function(method) {
                    var listSchema = (method.arguments && method.arguments.length > 0) ? method.arguments[0] : {}
                    var isObject = listSchema && listSchema.type === 'object'
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {
                        label: 'item',
                        schema: itemSchema
                    },
                    {
                        label: "index",
                        schema: {
                            "type": isObject ? "string": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "any"
                }
            }
            ],
            returnType: function(argsType) {
                if (argsType && argsType.length > 1) {
                    var schema = argsType[0]
                    var itemSchema = helper.getArrayItemSchema(schema)
                    return {
                        type: "object",
                        additionalProperties: {
                            type: "array",
                            items: itemSchema
                        }
                    }
                } else {
                    return {
                        type: "unknown"
                    }
                }
            },
            execute: function(collection, fn) {
                var result
                if (typeof collection !== 'undefined') {
                    result = _.groupBy(collection, function(item, index) {
                        var funContext = {
                            item: item,
                            index: index
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        minOf: {
            isCore: true,
            description: "Returns the minimum value in the list",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array or object",
                required: true
            },
            {
                name: "iteratee",
                type: "function",
                description: "Called for each item in the list.",
                required: false,
                arguments: function(method) {
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {
                        label: 'item',
                        schema: itemSchema
                    },
                    {
                        label: "index",
                        schema: {
                            "type": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "number"
                }
            }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(collection, fn) {
                var result
                if (typeof collection !== 'undefined') {
                    result = _.min(collection, function(item, index) {
                        var funContext = {
                            item: item,
                            index: index
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        maxOf: {
            isCore: true,
            description: "Return the maximum value in list.",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array or object",
                required: true
            },
            {
                name: "iteratee",
                type: "function",
                description: "Called for each value to find the rank.",
                required: false,
                arguments: function(method) {
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {
                        label: 'item',
                        schema: itemSchema
                    },
                    {
                        label: "index",
                        schema: {
                            "type": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "number"
                }
            }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(collection, fn) {
                var result
                if (typeof collection !== 'undefined') {
                    result = _.max(collection, function(item, index) {
                        var funContext = {
                            item: item,
                            index: index
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        sort: {
            isCore: true,
            description: "Returns a sorted copy of the collection.",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array",
                required: true
            },
            {
                name: "iteratee",
                type: "function",
                description: "Invoked for each item in the list for getting the sort order. The elements are sorted according to the return value (integer). If return value is less than zero (e.g. -1), itemA is less than itemB. If greater than zero, itemB comes first. If equal to zero, itemA is treated equal to itemB.",
                required: true,
                arguments: function(method) {
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                    {
                        label: 'itemA',
                        schema: itemSchema
                    },
                    {
                        label: "itemB",
                        schema: itemSchema
                    },
                    {
                        label: "index",
                        schema: {
                            "type": "integer"
                        }
                    }
                    ]
                },
                returnType: {
                    type: "integer"
                }
            }
            ],
            returnType: function(argsType) {
                return argsType[0]
            },
            execute: function(collection, fn) {
                var result = collection
                if (Array.isArray(collection)) {
                    collection = _.clone(collection)
                    result = collection.sort(function(a, b, index) {
                        var funContext = {
                            itemA: a,
                            itemB: b,
                            index: index
                        }
                        return fn.apply(this, [funContext])
                    })
                }
                return result
            }
        },
        sortBy: {
            isCore: true,
            description: "Sort a collection based on the rank returned by the iteratee.",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array",
                required: true
            },
            {
                name: "iteratee",
                type: "function",
                description: "Invoked for each item in the list for getting the sort order. The elements are ranked according to the return value (integer).",
                required: true,
                arguments: function(method) {
                    var itemSchema = helper.getCollectionItemSchema(method, 0)
                    return [
                        {
                            label: 'item',
                            schema: itemSchema
                        }
                    ]
                },
                returnType: {
                    type: "any"
                }
            }
            ],
            returnType: function(argsType) {
                return argsType[0]
            },
            execute: function(collection, fn) {
                var result = _.sortBy(collection, function(item) {
                    var funContext = {
                        item: item
                    }
                    return fn.apply(this, [funContext])
                })
                return result
            }
        },
        reverse: {
            isCore: true,
            description: "Reverse an array.",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array",
                required: true
            }
            ],
            returnType: function(argsType) {
                return argsType[0]
            },
            execute: function(collection, fn) {
                var result = collection
                if (Array.isArray(collection)) {
                    result = collection.reverse()
                }
                return result
            }
        },
        index: {
            description: "Returns item at the index of an array.",
            arguments: [{
                name: 'array',
                type: 'array',
                required: true
            },{
                name: 'index',
                type: 'integer',
                required: true
            }],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function() {
                if (arguments.length >=2 ) {
                    var index = arguments[1]
                    return arguments[0][index]
                }
                return null
            }
        },
    }
}, this || (typeof window != 'undefined' ? window : null))