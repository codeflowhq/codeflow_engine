(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsString = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    return {
        string: {
            isCore: true,
            description: "Converts other values to string. For converting objects to JSON string, use json:stringify()",
            arguments: [
            {
                name: "value",
                type: "any",
                required: true,
                description: "Any value to that should be converted to a string."
            }
            ],
            returnType: "string",
            execute: function(obj) {
                /*var str = null
                if (obj && typeof obj.toString == 'function')  {
                    str = obj.toString()
                }*/
                if (typeof obj !== 'undefined') {
                    return String(obj)
                } else {
                    return ""
                }
            }
        },
        concat: {
            isCore: true,
            description: "Concatenate the arguments provided, Numbers will be converted to string.",
            arguments: [/*
            {
                name: "destination",
                type: "string",
                required: true,
                description: "Destination string to concat to."
            },
            {
                name: "source",
                type: "string",
                required: true,
                description: "Source string that will be concatinated to the destination."
            } */
            ],
            variableArgument:
            {
                name: "element",
                description: "String that will be concatenated",
                type: "string"
            },
            returnType: "string",
            execute: function(destination, source) {
                var ret = ''
                if (arguments.length > 0) {
                    for (var i = 0; i < arguments.length; i++) {
                        var arg = arguments[i]
                        if (typeof arg === "undefined" || arg === null) {
                            arg = ''
                        } else if (typeof arg !== 'string') {
                            if (typeof arg.toString === 'function') {
                                arg = arg.toString()
                            }
                        }
                        if (typeof arg === 'string') {
                            ret += arguments[i]
                        }
                    }
                }
                return ret
            }
        },
        substring: {
            isCore: true,
            description: "Returns the characters in a string beginning at the specified location through the specified number of characters.",
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "Source string."
            },
            {
                name: "start",
                type: "integer",
                required: true,
                description: "Index at which substring should start."
            },
            {
                name: "length",
                type: "integer",
                required: false,
                description: "Number of characters from start to extract."
            }
            ],
            returnType: "string",
            execute: function(str, index, length) {
                if (str && typeof str.substr === 'function') {
                    if (typeof length === 'number') {
                        return str.substr(index, length)
                    } else {
                        return str.substr(index)
                    }
                }
                return undefined
            }
        },

        indexOf: {
            isCore: true,
            description: "Returns the first index at which a given search string can be found in the source string, or -1 if it is not present.",
            examples: [
                'indexOf("HelloWorld", "Hello") //0',
                'indexOf("HelloWorld", "World") //5',
                'indexOf("HelloWorld", "Utopia") //-1'
            ],
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "Source string."
            },
            {
                name: "search",
                type: "string",
                required: true,
                description: "String to search."
            }
            ],
            returnType: "integer",
            execute: function(str, index, length) {
                if (str && typeof str.indexOf === 'function') {
                    return str.indexOf(index, length)
                }
                return undefined
            }
        },
        split: {
            isCore: true,
            description: "Splits a string into array using the given substring.",
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "Source string."
            },
            {
                name: "start",
                type: "string",
                description: "Substring to split the string with. Defaults to space"
            }
            ],
            returnType: {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            execute: function(str, match) {
                if (str && typeof str.split === 'function') {
                    return str.split(match)
                }
                return undefined
            }
        },
        between: {
            examples: [
                'between("HelloWorld", "He", "ld") //"lloWor"',
                'between("HelloWorld", "he", "ld") //""',
                'between("HelloWorld", "he", "ld", true) //"lloWor" (case is ignored while matching)',
            ],
            description: "Returns the characters in a string between the first occurance of preceding string and the trailing string.",
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "A string"
            },
            {
                name: "preceding",
                type: "string",
                required: true,
                description: "Preceding string to match. If not specified, will return the whole string"
            },
            {
                name: "trailing",
                type: "string",
                required: false,
                description: "Trailing string to match. If omitted, will return till the end."
            }
            ,
            {
                name: "ignoreCase",
                type: "boolean",
                required: false,
                description: 'If set, will ignore case while matching, but return value will have case unchanged. E.g. \'between("Hi There", "hi", "re", false)\' will return empty, but \'between("Hi There", "hi", "re", true)\' will return \'The\''
            }
            ],
            returnType: "string",
            execute: function(str, preceding, trailing, ignoreCase) {
                var ret, origStr = str
                if (str && typeof str === 'string') {
                    if (typeof preceding === 'string') {
                        if (ignoreCase) {
                            str = str.toLowerCase()
                            preceding = preceding.toLowerCase()
                            trailing = trailing ? trailing.toLowerCase() : null
                        }
                        var precedingIdx = str.indexOf(preceding)
                        if (precedingIdx >= 0) {
                            precedingIdx = precedingIdx + preceding.length
                            var trailingIdx = trailing ? str.indexOf(trailing): str.length
                            ret = origStr.substring(precedingIdx, trailingIdx)
                        }
                    } else {
                        ret = str
                    }
                }
                return ret
            }
        },
        toUpper: {
            isCore: true,
            examples: ['toUpper("hello") // "HELLO"'],
            description: "Convert a string to uppercase",
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "A string"
            },
            {
                name: "localeAware",
                type: "boolean",
                description: "should be localeAware?"
            }
            ],
            returnType: "string",
            execute: function(str, localeAware) {
                if (str && typeof str === 'string')  {
                    if (localeAware) {
                        str = str.toLocaleUpperCase()
                    } else {
                        str = str.toUpperCase()
                    }
                }
                return str
            }
        },
        toLower:{
            isCore: true,
            description: "Convert a string to lowercase",
            examples: ['toLower("Hello") // "hello"'],
            arguments: [
            {
                name: "source",
                type: "string",
                required: true,
                description: "A string"
            },
            {
                name: "localeAware",
                type: "boolean",
                description: "should be localeAware?"
            }
            ],
            returnType: "string",
            execute: function(str, localeAware) {
                if (str && typeof str === 'string')  {
                    if (localeAware) {
                        str = str.toLocaleLowerCase()
                    } else {
                        str = str.toLowerCase()
                    }
                }
                return str
            }
        },
        replace: {
            isCore: true,
            description: "Replace a part of the string which matches the pattern with the replacement",
            arguments: [
                {
                    name: "source",
                    type: "string",
                    required: true,
                    description: "A string"
                },
                {
                    name: "pattern",
                    type: "any",
                    description: "String or a regex"
                },
                {
                    name: "replacementString",
                    type: "string",
                    description: "A String"
                }
            ],
            returnType: "string",
            execute: function(str, pattern, replace) {
                if (typeof str === 'string')  {
                    return str.replace(pattern, replace)
                }
                return undefined
            }
        },
        trim: {
            isCore: true,
            description: "Trim whitespace from both sides of string",
            examples: ['trim(" Hello ") // "Hello"'],
            arguments: [
                {
                    name: "source",
                    type: "string",
                    required: true,
                    description: "A string"
                }
            ],
            returnType: "string",
            execute: function(str) {
                if (typeof str === 'string')  {
                    return str.trim()
                }
                return undefined
            }
        },
    }

}, this || (typeof window != 'undefined' ? window : null))