(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsPath = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    class PathObject {

        constructor() {
            try {
                if(typeof nodeRequire == 'undefined' && require) {
                    this.path = require('path')
                } else {
                    //this.path = require('path')
                }
            } catch(e) {

            }
            this.canExec = this.canExec.bind(this)
            this.basename = this.basename.bind(this)
            this.dirname = this.dirname.bind(this)
            this.extname = this.extname.bind(this)
            this.isAbsolute = this.isAbsolute.bind(this)
            this.normalize = this.normalize.bind(this)
            this.relative = this.relative.bind(this)
            this.join = this.join.bind(this)
            this.resolve = this.resolve.bind(this)
        }

        canExec(arg) {
            if(!this.path) { return null }
            if(typeof arg !== 'string') { return null}
            return true
        }
        basename(path, ext) {
            if(!this.canExec(path)) { return null }
            return this.path.basename(path, (ext || ''))
        }
        dirname(path) {
            if(!this.canExec(path)) { return null }
            return this.path.dirname(path)
        }
        extname(path) {
            if(!this.canExec(path)) { return null }
            return this.path.extname(path)
        }
        isAbsolute(path) {
            if(!this.canExec(path)) { return null }
            return this.path.isAbsolute(path)
        }
        normalize(path) {
            if(!this.canExec(path)) { return null }
            return this.path.normalize(path)
        }
        relative(path1, path2) {
            if(!this.canExec(path1) || !this.canExec(path2)) { return null }
            return this.path.relative(path1, path2)
        }
        join(...paths) {
            if(!this.canExec(paths[0]) || !this.canExec(paths[1])) { return null }
            return this.path.join.apply(undefined, paths)
        }
        resolve(...paths) {
            if(!this.canExec(paths[0])) { return null }
            return this.path.resolve.apply(undefined, paths)
        }
    }

    let Path = new PathObject()

    return {
    basename: {
        description: "Get the base name of file/directory from a path",
        examples: [
            "basename('users/load.json')" //load.json
        ],
        arguments: [
            {
                name: "Path",
                type: "string",
                required: true
            },
            {
                name: "Extension",
                type: "string"
            }
        ],
        returnType: "string",
        execute: Path.basename
    },
    dirname: {
        description: "Get the directory name from the path",
        examples: [
            "dirname('users/load.json')" //users
        ],
        arguments: [
            {
                name: "Path",
                type: "string",
                required: true
            }
        ],
        returnType: "string",
        execute: Path.dirname
    },
    extname: {
        description: "Get the extension from the file path. Will be extracted from the last '.' to end of path.",
        examples: [
            "extname('users/load.json')" //.json
        ],
        arguments: [
            {
                name: "Path",
                type: "string",
                required: true
            }
        ],
        returnType: "string",
        execute: Path.extname
    },
    isAbsolute: {
        description: "Check whether the path is an absolute path.",
        examples: [
            "isAbsolute('users/load.json')" //false
        ],
        arguments: [
            {
                name: "Path",
                type: "string",
                required: true
            }
        ],
        returnType: "string",
        execute: Path.isAbsolute
    },
    join: {
        description: "Join all arguments and return a normalized path.",
        examples: [
            "join('users', 'load.json')" //users/load.json
        ],
        arguments: [],
        variableArgument:
        {
            name: "element",
            description: "Path string",
            type: "string"
        },
        returnType: "string",
        execute: Path.join
    },
    normalize: {
        description: "Normalize a path string.",
        examples: [
            "normalize('users//load.json')" //users/load.json
        ],
        arguments: [
            {
                name: "Path",
                type: "string",
                required: true
            }
        ],
        returnType: "string",
        execute: Path.normalize
    },
    relative: {
        description: "olve the relative path from one path to the other.",
        examples: [
            "relative('/users/load.json', '/users')" //load.json
        ],
        arguments: [
            {
                name: "from",
                type: "string",
                required: true
            },
            {
                name: "to",
                type: "string",
                required: true
            }
        ],
        returnType: "string",
        execute: Path.relative
    },
    resolve: {
        description: "Resolve to an absolute path from the arguments.",
        examples: [
            "resolve('/users', '../', 'var/lib')" //load.json
        ],
        arguments: [],
        variableArgument:
        {
            name: "element",
            description: "Path string",
            type: "string"
        },
        returnType: "string",
        execute: Path.resolve
    },
}
}, this || (typeof window != 'undefined' ? window : null))