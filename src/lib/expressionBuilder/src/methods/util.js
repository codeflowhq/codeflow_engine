(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window != 'undefined' ? window : this)
            self.eeMethodsUtil = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var utilLib = require ? require('../util') : exports.eeUtil
    var _ = require ? require('underscore') : exports._
    var helper = require ? require('./_helper') : {}
    const nodeUtilPath = 'util'
    const nodeUtil = typeof nodeRequire == 'undefined' ? (require ? require(nodeUtilPath) : null) : null
    return {
        getNaN: {
            isCore: true,
            description: "Returns NaN (Not a number).",
            examples: ["getNaN() //NaN"],
            returnType : "number",
            execute: function() {
                return NaN
            }
        },
        getInfinity: {
            isCore: true,
            description: "Returns Infinity.",
            examples: ["getInfinity() //Infinity"],
            returnType : "number",
            execute: function() {
                return Infinity
            }
        },
        getUndefined: {
            isCore: true,
            description: "Returns undefined.",
            examples: ["getUndefined() //undefined"],
            returnType : "undefined",
            execute: function() {
                return undefined
            }
        },
        getNull: {
            isCore: true,
            description: 'Returns a null value (alias of nil())',
            examples: ["getNull() //null"],
            returnType : "null",
            execute: function() {
                return null
            }
        },
        nil: {
            isCore: true,
            description: 'Returns a null value (alias of getNull())',
            examples: ["nil() //null"],
            returnType: 'null',
            execute: function() {
                return null
            }
        },
        throwError: {
            name: "throwError",
            namespace: 'util',
            hidden: true,
            arguments: [{
                name: 'msg',
                type: 'string',
                required: true
            }],
            description: 'Throws an error (hidden utility method)',
            execute: function(msg) {
                throw new Error(msg)
            }
        },
        object: {
            isCore: true,
            description: "Returns an object",
            examples: [
                "object(array('moe', 'larry', 'curly'), array(30, 40, 50)) => {moe: 30, larry: 40, curly: 50}"
            ],
            arguments: [],
            variableArgument:
            {
                name: "element",
                description: "Key/value pair to be part of the object",
                type: "array"
            },
            returnType: function(argsType) {
                var schema = {
                    type: "object"
                }
                return schema
            },
            execute: function() {
                try {
                    return _.object.apply(this, arguments)
                } catch (e) {
                    console.error(e)
                    return {}
                }
            }
        },
        array: {
            isCore: true,
            description: "Returns an array with all the arguments passed.",
            arguments: [],
            variableArgument:
            {
                name: "element",
                description: "Value to be part of the array",
                type: "any"
            },
            returnType: function(argsType) {
                var schema = { type: "array" }
                var item = {}
                var typeMap = {}
                for (var i = 0; i < argsType.length; i++) {
                    item = utilLib.normalizeSchema(argsType[i])
                    typeMap[item.type] = item
                }
                // if there is no item or more than one type
                if (_.size(typeMap) === 0) {
                    item = { type: "null" }
                } else if (_.size(typeMap) > 1) {
                    item = { type: "any" }
                }
                if (item) {
                    schema.items = item
                }
                return schema
            },
            execute: function() {
                var len = arguments.length
                var arr = new Array(len)
                for (var i = 0; i < len; i++) {
                    arr[i] = arguments[i]
                }
                return arr
            }
        },
        range: {
            isCore: true,
            description: "Generate an array with integers ranging from Start to End.",
            arguments: [
                {
                    name: "start",
                    type: "integer",
                    description: "Starting number for the sequence (inclusive)",
                    required: true
                },
                {
                    name: "end",
                    type: "integer",
                    description: "End number for the sequence (inclusive)",
                    required: true
                }
            ],
            returnType: {
                "type": "array",
                "items": {
                    "type": "integer"
                }
            },
            execute: function(start, end) {
                var length = end - start
                if (typeof length === "number" && length >= 0) {
                    var a = []
                    for (var i = 0, k = start; k <= end; k++, i++) {
                        a[i] = k
                    }
                }
                return a
            }
        },
        randomInteger: {
            isCore: true,
            description: "Generate a random integer with the given length",
            arguments: [
                {
                    name: "length",
                    type: "integer",
                    description: "Number of digits",
                    default: 3,
                    required: false
                }
            ],
            returnType: {
                "type": "integer"
            },
            execute: function(length=3) {
                length = !length || length < 1 ? 3 : length
                let rand = Math.random()
                let positions = Math.pow(10, length)
                let result = rand * positions

                return Math.round(result)
            }
        },
        randomString: {
            isCore: true,
            description: "Generate a random string with alphanumeric characters of given length.",
            arguments: [
                {
                    name: "length",
                    type: "integer",
                    description: "Number of characters to generate",
                    default: 10,
                    required: true
                },
                {
                    name: "charset",
                    type: "string",
                    description: "Custom charset to choose from. If not specified, defaults to alphanumeric.",
                    required: false
                }
            ],
            returnType: {
                "type": "string"
            },
            execute: function(length, charset) {
                charset = charset || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
                var result = ''
                var chLength = charset.length
                for (var i = length; i > 0; --i) {
                    result += charset[Math.floor(Math.random() * chLength)]
                }
                return result
            }
        },
        size: {
            isCore: true,
            description: "Compute the size of the element provided. If the element is a string or array, it's length is returned. If the element is object, the number of properties in the object is returned.",
            arguments: [
                {
                    name: "element",
                    type: "any",
                    required: true,
                    description: "Element whose size is to be computed."
                }
            ],
            returnType: "integer",
            execute: function(obj) {
                var size = null
                if (typeof obj === 'string') {
                    size = obj.length
                } else if (obj && typeof obj === 'object') {
                    if (+obj.length === obj.length) {
                        size = obj.length
                    } else {
                        size = Object.keys(obj).length

                    }
                }
                return size
            }
        },

        len: {
            isCore: true,
            description: "Compute the size of the array, object or string provided. If the type is a string or array, it's length is returned. If the type is object, the number of properties in the object is returned.",
            arguments: [
                {
                    name: "element",
                    type: "any",
                    required: true,
                    description: "Element whose size is to be computed."
                }
            ],
            returnType: "integer",
            execute: function(obj) {
                let size = null
                if (typeof obj === 'string') {
                    size = obj.length
                } else if (obj && typeof obj === 'object') {
                    if (+obj.length === obj.length) {
                        size = obj.length
                    } else {
                        size = Object.keys(obj).length

                    }
                }
                return size
            }
        },
        type: {
            isCore: true,
            description: "Get the type of the argument,",
            arguments: [
                {
                    name: "variable",
                    type: "any",
                    required: true,
                    description: "Variable whose type is to be identified."
                }
            ],
            returnType: "string",
            execute: function(obj) {
                var type = typeof obj
                if (type === 'object') {
                    if (obj === null) {
                        type = 'null'
                    } else if (helper.isBuffer(obj)) {
                        type = 'buffer'
                    } else if (helper.isStream(obj)) {
                        type = 'stream'
                    } else if (+obj.length === obj.length) {
                        type = 'array'
                    }
                }
                return type
            }
        },
        extend: {
            isCore: true,
            description: "Get new object by extending two",
            arguments: [
                {
                    name: "object1",
                    type: "object",
                    required: true,
                    description: "First Object"
                },
                {
                    name: "object2",
                    type: "object",
                    required: true,
                    description: "Second Object"
                }
            ],
            returnType: "object",
            execute: function(obj1, obj2) {
                if (!obj1) {
                    return obj2
                }
                var dest = {}, arg
                var args = [obj1, obj2]
                for (var aIdx = 0; aIdx < args.length; aIdx++) {
                    arg = args[aIdx]
                    if (arg && typeof arg === 'object') {
                        if (+arg.length === arg.length) { //is array
                            continue
                        }
                        for (var p in arg) {
                            if (arg.hasOwnProperty(p)) {
                                dest[p] = arg[p]
                            }
                        }
                    }
                }
                return dest
            }
        },
        regex: {
            isCore: true,
            description: "Create a regular expression.",
            arguments: [
                {
                    name: "expression",
                    type: "string",
                    required: true,
                    description: "Expression string"
                },
                {
                    name: "flags",
                    type: "string",
                    required: false,
                    description: "Flags (g/i/m)"
                }
            ],
            returnType: "any",
            execute: function(expression, flag) {
                if (!expression) {
                    return false
                }
                if (typeof flag === 'undefined') {
                    return new RegExp(expression)
                } else {
                    return new RegExp(expression, flag)
                }
            }
        },
        nativeJs: {
            hidden: true, //experimental
            description: "Execute any native Javascript method.",
            arguments: [
                {
                    name: "nativeMethod",
                    type: "any",
                    description: "A Javascript native method to be call",
                    required: true
                }
            ],
            variableArguments: true,
            returnType: "any",
            execute: function(js) {
                //return eval(js)
            }
        },
        encodeURI: {
            isCore: true,
            description: "Encodes a URL. Same as ECMAScript's encodeURI.",
            examples: [
                "encodeURI('http://google.com/search=codeflow is awesome') //http://google.com/search=codeflow%20is%awesome"
            ],
            arguments: [
                {
                    name: "url",
                    type: "string",
                    required: true
                }
            ],
            returnType: "string",
            execute: function(url) {
                return encodeURI(url)
            }
        },
        encodeURIComponent: {
            isCore: true,
            description: "Encodes a URL component. Same as ECMAScript's encodeURIComponent. To be used when passing values as query parameters.",
            examples: [
                "encodeURIComponent('http://google.com/search=codeflow is awesome') //http%3A%2F%2Fgoogle.com%2Fsearch%3Dcodeflow%20is%20awesome"
            ],
            returnType: "string",
            arguments: [
                {
                    name: "url component",
                    type: "string",
                    required: true
                }
            ]
        },
        formatString: {
            isCore: true,
            description: "Format a string",
            examples: [
                "formatString('Hello %s', 'world') // Hello world"
            ],
            variableArgument:
            {
                name: "element",
                description: "String and arguments to be formatted",
                type: "string"
            },
            arguments: [],
            returnType: "string",
            execute: function(str, ...args) {
                return nodeUtil ? nodeUtil.format(str, ...args) : str
            }
        },
        encodeBase64: {
            isCore: true,
            description: "Converts text to base64 encoded text",
            examples: [
                "encodeBase64('codeflow') //r^~Z0"
            ],
            arguments: [
                {
                    name: "text",
                    type: "string",
                    required: true
                }
            ],
            returnType: "string",
            execute: function(str) {
                return Buffer.from(str, 'binary').toString('base64')
            }
        },
        decodeBase64: {
            isCore: true,
            description: "Decodes base64 encoded text",
            examples: [
                "decodeBase64('r^~Z0') //codeflow"
            ],
            arguments: [
                {
                    name: "base64text",
                    type: "string",
                    required: true
                }
            ],
            returnType: "string",
            execute: function(bstr) {
                return (bstr instanceof Buffer ? bstr : Buffer.from(bstr.toString(), 'base64')).toString('binary')
            }
        }
    }

}, this || (typeof window != 'undefined' ? window : null))
