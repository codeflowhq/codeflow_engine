(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsDate = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"


        /*
         * Date Format 1.2.3
         * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
         * MIT license
         *
         * Includes enhancements by Scott Trenda <scott.trenda.net>
         * and Kris Kowal <cixar.com/~kris.kowal/>
         *
         * Accepts a date, a mask, or a date and a mask.
         * Returns a formatted version of the given date.
         * The date defaults to the current date/time.
         * The mask defaults to dateFormat.masks.default.
         */
    var dateFormat = function(date, mask, utc) {
        var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g,
            pad = function (val, len) {
                val = String(val)
                len = len || 2
                while (val.length < len) val = "0" + val
                return val
            }

        // common format strings
        var masks = {
            default: "yyyy-mm-dd HH:MM:ss",
            isoDate: "yyyy-mm-dd",
            isoTime: "HH:MM:ss",
            isoDateTime: "yyyy-mm-dd'T'HH:MM:ss"
        }

        // Internationalization strings
        var i18n = {
            dayNames: [
                "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
                "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
            ],
            monthNames: [
                "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
            ]
        }

        // Check the arguments
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date
            date = undefined
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date
        if (isNaN(date)) console.log("invalid date")

        mask = String(masks[mask] || mask || masks["default"])

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4)
            utc = true
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  i18n.dayNames[D],
                dddd: i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  i18n.monthNames[m],
                mmmm: i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            }

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1)
        })
    }

    return {
        date: {
            description: "Creates a Date instance that represents a single moment in time",
            examples: [
                'date() //current date',
                'date(2015, 10, 22) //date object 2015-11-22T00:00:00',
                'date(2015, 5, 30, 18, 11) //date object 2015-06-30T18:11:00'
            ],
            arguments: [],
            variableArgument: {
                name: "value",
                type: "any",
                description: "A date object or a date string or the integer"
            },
            returnType: "object",
            execute: function() {
                var args = Array.prototype.slice.call(arguments)
                //@#ToDo remove the null values from the arguments so that this will accept date string input as the first argument
                //add date as the first among the arguments list
                args.unshift(Date)
                return new (Date.bind.apply(Date, args))
            }
        },
        getTimestamp: {
            description: "Get the timestamp in seconds/milliseconds elapsed since 1 January 1970 00:00:00 UTC",
            examples: [
                'getTimestamp()',
                'getTimestamp("2020-06-15 12:30:00", true)'
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "Date string",
                    required: false
                },
                {
                    name: "seconds",
                    type: "boolean",
                    description: "In Seconds",
                    required: false
                }
            ],
            returnType: "integer",
            execute: function(date, inSecs) {
                let TS = date ? Date.parse(date) : Date.now()
                return inSecs ? Math.ceil(TS/1000) : TS
            }
        },
        format: {
            description: "Convert a date to readable string matching the given format. Opposite of parseDate()",
            examples: [
                "format(date(), 'dd/mm/yyyy')",
                "default: yyyy-mm-dd HH:MM:ss",
                "isoDate: yyyy-mm-dd",
                "isoTime: HH:MM:ss",
                "isoDateTime: yyyy-mm-dd'T'HH:MM:ss"
            ],
            arguments: [
                {
                    name: "date",
                    type: "any",
                    description: "A date string or object",
                    required: true
                },
                {
                    name: "format",
                    type: "string",
                    enum: ["default", "isoDate", "isoTime", "isoDateTime", "shortDate", "mediumDate", "longDate", "fullDate", "shortTime", "mediumTime", "longTime", "isoUtcDateTime"],
                    description: "The format with which to render the date as a string.",
                    required: false
                }
            ],
            returnType: "string",
            execute: function(date, format) {
                return dateFormat(date, format)
            }
        },
        parse: {
            description: "Parse the string representation of a date, and return a detailed information",
            examples: [
                "parse('04 Dec 1995 00:12:00 GMT')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "Date string",
                    required: true
                }
            ],
            returnType: "object",
            execute: function(dtString) {
                let d = new Date(dtString)
                return {
                    year: d.getFullYear(),
                    month: d.getMonth() + 1,
                    day: d.getDate(),
                    hour: d.getHours(),
                    minute: d.getMinutes(),
                    second: d.getSeconds(),
                    milliseconds: d.getMilliseconds(),
                    weekday: d.getDay()
                }
            }
        },
        incr: {
            description: "Increment a date value",
            examples: [
                "incr('12/04/2015', 1",
                "incr('12/04/2015', -1,)",
                "incr('12/04/2015', {day: 2, month: 1}),"
            ],
            arguments: [
                {
                    name: "date",
                    type: "any",
                    description: "A string date",
                    required: true
                },
                {
                    name: "increment",
                    type: "any",
                    description: "increment in number of days or an object with day, month, year, hour, minute, second properties",
                    required: false
                }
            ],
            returnType: "string",
            execute: function(date, incr) {
                let d = new Date(date)
                let options = {}
                incr = incr || 0
                if(typeof incr !== 'object') {
                    options.day = incr
                } else {
                    options = {
                        day: incr
                    }
                }

                const propMap = {
                    day: 'Date',
                    month: 'Month',
                    year: 'FullYear',
                    hour: 'Hours',
                    minute: 'Minutes',
                    second: 'Seconds'
                }
                Object.keys(options).map(key=>{
                    let _incr = 1 * options[key]
                    let _prop = propMap[key] || 'Date'
                    d[`set${_prop}`](d[`get${_prop}`]() + _incr)
                })
                return d
            }
        },
        isEquals: {
            description: "Check if two dates are equal",
            examples: [
                "equals('12/03/2015', '12/04/2015')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A date string",
                    required: true
                },
                {
                    name: "date2",
                    type: "string",
                    description: "A date string",
                    required: true
                }
            ],
            returnType: "boolean",
            execute: function(date, date2) {
                return Date.parse(date) === Date.parse(date2)
            }
        },
        isAfter: {
            description: "Check if a date comes after the given date",
            examples: [
                "isAfter('12/03/2015', '12/04/2015')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A string date",
                    required: true
                },
                {
                    name: "date2",
                    type: "string",
                    description: "A string date to compare with",
                    required: true
                }
            ],
            returnType: "boolean",
            execute: function(date, date2) {
                return Date.parse(date) > Date.parse(date2)
            }
        },
        isBefore: {
            description: "Check if a date comes before the given date",
            examples: [
                "isBefore('12/03/2015', '12/04/2015')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A string date",
                    required: true
                },
                {
                    name: "date2",
                    type: "string",
                    description: "A string date to compare with",
                    required: true
                }
            ],
            returnType: "boolean",
            execute: function(date, date2) {
                return Date.parse(date) < Date.parse(date2)
            }
        },
        isBetween: {
            description: "Check if a date comes between the given two dates",
            examples: [
                "isBetween('12/03/2015', '12/04/2015')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A date string",
                    required: true
                },
                {
                    name: "date1",
                    type: "string",
                    description: "A date string",
                    required: true
                },
                {
                    name: "date2",
                    type: "string",
                    description: "A date string",
                    required: true
                },
                {
                    name: "include boundary",
                    type: "boolean",
                    description: "Include the boundaries",
                    required: false
                }
            ],
            returnType: "boolean",
            execute: function(date, date1, date2, includeBoundary) {
                let d = Date.parse(date)
                let d1 = Date.parse(date1)
                let d2 = Date.parse(date2)
                if(includeBoundary) {
                    return d1 <= d && d <= d2
                }
                return d1 < d && d < d2

            }
        },
        isFuture: {
            description: "Check if a date comes in the future",
            examples: [
                "isFuture('12/03/2020')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A string date",
                    required: true
                }
            ],
            returnType: "boolean",
            execute: function(date) {
                return Date.now() < Date.parse(date)
            }
        },
        isPast: {
            description: "Check if a date is in the past",
            examples: [
                "isPast('12/03/2015')"
            ],
            arguments: [
                {
                    name: "date",
                    type: "string",
                    description: "A date string",
                    required: true
                }
            ],
            returnType: "boolean",
            execute: function(date) {
                return Date.now() > Date.parse(date)
            }
        }
    }
}, this || (typeof window != 'undefined' ? window : null))