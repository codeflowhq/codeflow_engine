(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsArray = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var _ = require ? require('underscore') : exports._
    var helper = require ? require('./_helper') : {}
    var pathLib = "path"
    var Path
    try {
        Path = typeof nodeRequire == 'undefined' ? (require ? require(pathLib) : {}) : {}
    } catch(e) {
        Path = {}
    }

    return {
        reverse:  {
            description: "Reverse an array.",
            arguments: [
            {
                name: "list",
                type: "any",
                description: "Array",
                required: true
            }
            ],
            returnType: function(argsType) {
                return argsType[0]
            },
            execute: function(collection, fn) {
                var result = collection
                if (Array.isArray(collection)) {
                    result = collection.reverse()
                }
                return result
            }
        },
        concat: {
            description: "Returns a new array with first array joined with second array or value.",
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "value",
                    type: "any",
                    required: true,
                    description: "An array or value to concatenate into the returned array."
                }
            ],
            returnType: helper.getArrayItemsSchema,
            execute: function(destination, source) {
                var ret
                if (typeof destination === "undefined" || destination === null) {
                    destination = []
                }
                if (typeof source === "undefined" || source === null) {
                    source = []
                }

                if (!_.isArray(destination)) {
                    ret = null
                } else  {
                    ret = destination.concat(source)
                }
                return ret
            }
        },

        join: {
            description: "Joins all elements of an array into a string",
            examples: [
                'join(array("sam", "john", "aby")) //returns "sam,aby,john"',
                'join(array("sam", "john", "aby"), "/") //returns "sam/aby/john"'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "value",
                    type: "string",
                    required: false,
                    description: "An optional separator."
                }
            ],
            returnType: "string",
            execute: function(arr, sep) {
                return !_.isArray(arr) ? null : arr.join(sep)
            }
        },
        includes: {
            description: "Checks whether an array includes a certain element and returning boolean",
            examples: [
                'includes(array("sam", "john", "aby"), "sam") //returns true',
                'includes(array("sam", "john", "aby"), "john", 2) //returns false'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "search",
                    type: "any",
                    required: false,
                    description: "Search item"
                },
                {
                    name: "index",
                    type: "integer",
                    required: false,
                    description: "An optional from index."
                }
            ],
            returnType: "boolean",

            execute: function(arr, item, fromIndex) {

                if(!_.isArray(arr)) return null;

                if (!Array.prototype.includes) {
                    return arr.indexOf(item, fromIndex) > -1
                } else {
                    return arr.includes(item, fromIndex)
                }
            }
        },
        indexOf: {
            description: "Get the first index at which an element if found. The optional second parameter indicated the from what index should the search start",
            examples: [
                'indexOf(array("sam", "john", "aby"), "sam") //returns 0',
                'indexOf(array("sam", "john", "aby", "john"), "john", 2) //returns 3'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "search",
                    type: "any",
                    required: false,
                    description: "Search item"
                },
                {
                    name: "index",
                    type: "integer",
                    required: false,
                    description: "An optional from index."
                }
            ],
            returnType: "integer",
            execute: function(arr, item, fromIndex) {
                return !_.isArray(arr) ? null : arr.indexOf(item, fromIndex)
            }
        },
        lastIndexOf: {
            description: "Get the last index at which a given element can be found. If a fromIndex is specified, the array is searched backwards, starting at fromIndex.",
            examples: [
                'lastIndexOf(array("sam", "john", "sam"), "sam") //returns 2'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "search",
                    type: "any",
                    required: false,
                    description: "Search item"
                },
                {
                    name: "index",
                    type: "integer",
                    required: false,
                    description: "An optional from index."
                }
            ],
            returnType: "integer",
            execute: function(arr, item, fromIndex) {
                return !_.isArray(arr) ? null : arr.lastIndexOf(item, fromIndex)
            }
        },
        findAllIndices: {
            description: "Get all the indices on a given element in the array",
            examples: [
                'findAllIndices(array("sam", "john", "sam"), "sam") //returns [0,2]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "search",
                    type: "any",
                    required: false,
                    description: "Search item"
                },
                {
                    name: "index",
                    type: "integer",
                    required: false,
                    description: "An optional from index."
                }
            ],
            returnType: "integer",
            execute: function(arr, item, fromIndex) {

                var indices = []
                var idx = arr.indexOf(item, fromIndex)
                while (idx != -1) {
                    indices.push(idx)
                    idx = arr.indexOf(item, idx + 1)
                }

                return indices
            }
        },
        keys: {
            description: "Get an Array contains the keys for each index in the array",
            examples: [
                'keys(array("sam", "john", "aby")) //returns [0,1,2]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                }
            ],
            returnType: "array",
            execute: function(arr) {
                return !_.isArray(arr) ? null : arr.keys()
            }
        },
        compact: {
            description: "Returns a copy of the array with all undefined/false/null/0/''/NaN values removed.",
            examples: [
                'compact(array(0, 1, false, true, null, "", 3)) //returns [1,true,3]',
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: _.compact
        },
        union: {
            description: "Get only the unique items in the arrays",
            examples: [
                'union(array([0, 1], [0, 3], [1, 2])) //returns [0, 1, 3, 2]',
            ],
            arguments: [],
            variableArgument: {
                name: "array1",
                    type: "array",
                    required: true,
                    description: "An array"
            },
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: _.union
        },
        intersection: {
            description: "Get an array of values that are intersection of all the arrays",
            examples: [
                'intersection(array([0, 1, 2], [2, 3, 4], [5, 3, 2])) //returns [2]',
            ],
            arguments: [],
            variableArgument: {
                name: "array1",
                    type: "array",
                    required: true,
                    description: "An array"
            },
            returnType: helper.getArrayItemsSchema,
            execute: _.intersection
        },
        difference: {
            description: "Get the array which contain the elements from the first array which are not present in the second",
            examples: [
                'difference(array([0, 1, 2], [2, 3])) //returns [0, 1]',
            ],
            arguments: [],
            variableArgument: {
                name: "array1",
                type: "array",
                required: true,
                description: "An array"
            },
            returnType: helper.getArrayItemsSchema,
            execute: _.difference
        },
        uniq: {
            description: "Get an array with only the unique values",
            examples: [
                'uniq(array(0, 1, 0, 2, 3)) //returns [0, 1, 2, 3]',
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: _.uniq
        },
        initial: {
            description: "Get everything but the last entry of the array. Specify the optional second parameter to exclude last n elements",
            examples: [
                'initial(array("sam", "john", "jack")) //returns ["sam", "john"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "number",
                    type: "integer",
                    required: true,
                    description: "An optional number"
                }
            ],
            returnType: helper.getArrayItemsSchema,
            execute: function(arr, n) {
                return !_.isArray(arr) ? null : _.initial(arr, n)
            }
        },
        first: {
            description: "Get the first element of array. Specify the optional second parameter to get the first n elements",
            examples: [
                'first(array("sam", "john")) //returns "sam"',
                'first(array("sam", "john", "jack"), 2) //returns ["sam", "john"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "number",
                    type: "integer",
                    required: true,
                    description: "An optional number"
                }
            ],
            returnType: helper.getArrayItemsSchema,
            execute: function(arr, n) {
                return !_.isArray(arr) ? null : _.first(arr, n)
            }
        },
        last: {
            description: "Get the last element of array. Specify the optional second parameter to get last n elements",
            examples: [
                'last(array("sam", "john", "jack")) //returns "jack"',
                'last(array("sam", "john", "jack"), 2) //returns ["john", "jack"]',
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "number",
                    type: "integer",
                    required: true,
                    description: "An optional number"
                }
            ],
            returnType: helper.getArrayItemsSchema,
            execute: function(arr, n) {
                return !_.isArray(arr) ? null : _.last(arr, n)
            }
        },
        pop: {
            description: "Remove the last element from array",
            examples: [
                'pop(array("sam", "john", "jack")) //returns ["sam", "john"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemsSchema(argsType[0])
            },
            execute: function(arr) {
                var array = !_.isArray(arr) ? [] : _.map(arr, _.clone)
                array.pop()
                return array
            }
        },
        push: {
            description: "Add a new item  element as the last element of the array",
            examples: [
                'push(array("sam", "john"), "jack") //returns ["sam", "john", "jack"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "array",
                    type: "any",
                    required: true,
                    description: "An element to be pushed into array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(arr, item) {
                var array = !_.isArray(arr) ? [] : _.map(arr, _.clone)
                array.push(item)
                return array
            }
        },
        shift: {
            description: "Remove the first element from array",
            examples: [
                'shift(array("sam", "john", "jack")) //returns ["john", "jack"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemsSchema(argsType[0])
            },
            execute: function(arr) {
                var array = !_.isArray(arr) ? [] : _.map(arr, _.clone)
                array.shift()
                return array
            }
        },
        unshift: {
            description: "Add an item as the first element of an array",
            examples: [
                'unshift(array("sam", "john"), "neil") //returns ["neil", "sam", "john"]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "item",
                    type: "any",
                    required: true,
                    description: "An item to be added to the array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(arr, item) {
                var array = !_.isArray(arr) ? [] : _.map(arr, _.clone)
                array.unshift(item)
                return array
            }
        },
        slice: {
            description: "Get a portion of an array specified by the start and end indices",
            examples: [
                'slice(array(1, 2, 3, 4, 5), 3) //returns [4,5]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "begin",
                    type: "integer",
                    required: true,
                    description: "Index at which to begin extraction"
                },
                {
                    name: "end",
                    type: "integer",
                    required: true,
                    description: "Index at which to stop extraction"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(arr, begin, end) {
            return !_.isArray(arr) ? null : arr.slice(begin, end)
            }
        },
        splice: {
            description: "Changes the content of an array by removing existing elements and adding new",
            examples: [
                'splice(array(1, 2, 3, 4, 5), 0, 3, 8) //returns [8,4,5]'
            ],
            arguments: [
                {
                    name: "array",
                    type: "array",
                    required: true,
                    description: "An array"
                },
                {
                    name: "start",
                    type: "integer",
                    required: true,
                    description: "Index indicating where to start changing the array. If this is higher than the length of the array, it will be set to the length of the array and if specified a negative value, the change will  begin that many elements from the end"
                },
                {
                    name: "delete_count",
                    type: "integer",
                    required: true,
                    description: "Number of elements to remove. This this is zero no array elements will be removed. If this is higher than count of remaining elements in the array from the start, then all the elements till the end of the array will be removed."
                },
                {
                    name: "item",
                    type: "integer",
                    required: true,
                    description: "Element to be inserted into the array at the start index. If no item is specified this will remove only elements from the array"
                }
            ],
            returnType: function(argsType) {
                return helper.getArrayItemSchema(argsType[0])
            },
            execute: function(arr, start, deleteCount, item) {
                var array = !_.isArray(arr) ? [] : _.map(arr, _.clone)
                array.splice(start, deleteCount, item)
                return array
            }
        }
    }
}, this || (typeof window != 'undefined' ? window : null))
