(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsMath = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    return {
        pi: {
            isCore: true,
            description: "Returns the value of PI.",
            examples: [
            "pi() //3.141592653589793"
            ],
            returnType : "number",
            execute: function() {
                return Math.PI
            }
        },
        tan: {
            isCore: true,
            description: "Returns the tangent of an angle.",
            examples: [
                "tan(1) //1.5574077246549023"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "Number whose tangent to be calculated."
            }
            ],
            returnType : "number",
            execute: function(value) {
                return Math.tan(value)
            }
        },
        atan: {
            isCore: true,
            description: "Returns the arctangent(in radians) of the given number as a numeric value between -PI/2 and PI/2.",
            examples: [
                "atan(1) //0.7853981633974483",
                "atan(0) //0"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "Number whose arctangent to be calculated."
            }
            ],
            returnType : "number",
            execute: function(value) {
                return Math.atan(value)
            }
        },
        atan2: {
            isCore: true,
            description: "Returns the arctangent of the quotient of its argument.",
            examples: [
                "atan2(90, 15) // 1.4056476493802699",
                "atan2(15, 90) // 0.16514867741462683"
            ],
            arguments: [
            {
                name: "number1",
                type: "number",
                required: true,
                description: "First number."
            },
            {
                name: "number2",
                type: "number",
                required: true,
                description: "Second number."
            }
            ],
            returnType : "number",
            execute: function(value1, value2) {
                return Math.atan2(value1, value2)
            }
        },
        sin: {
            isCore: true,
            description: "Returns the sine of a number.",
            examples: [
                "sin(0) //0",
                "sin(90) //0.8939966636005579"
            ] ,
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number (in radians)."
            }
            ],
            returnType : "number",
            execute: function(number) {
                return Math.sin(number)
            }
        },
        cos: {
            isCore: true,
            description: "Returns the cosine of a number.",
            examples: [
                "cos(0) //1",
                "cos(90) //-0.4480736161291702"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number (in radians)."
            }
            ],
            returnType : "number",
            execute: function(number) {
                return Math.cos(number)
            }
        },
        asin: {
            isCore: true,
            description: "Returns the arcsine (in radians) of number.",
            examples: [
                "asin(-1) // -1.5707963267948966",
                "asin(0.5) // 0.5235987755982989"
            ],
            arguments: [
            {
                name: "value",
                type: "number",
                required: true,
                description: "A number."
            }
            ],
            returnType : "number",
            execute: function(number) {
                return Math.asin(number)
            }
        },
        acos: {
            isCore: true,
            description: "Returns the arccosine (in radians).",
            examples: [
                "acos(0) // 1.5707963267948966",
                "acos(0.5) // 1.0471975511965979"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number."
            }
            ],
            returnType : "number",
            execute: function(number) {
                return Math.acos(number)
            }
        },
        log: {
            description: "Returns the natural logarithm (base E) of a number",
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number."
            }
            ],
            returnType : "number",
            execute: function(val) {
                return Math.log(val)
            }
        },
        exp: {
            description: "Returns the exponential (e^x where e is Euler's constant)",
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number."
            }
            ],
            returnType : "number",
            execute: function(val) {
                return Math.exp(val)
            }
        },
        random: {
            description: "Returns a random number between 0 and 1.",
            examples: [
                'random() //0.29112607054412365'
            ],
            arguments: [
            ],
            returnType: "number",
            execute: function() {
                return Math.random()
            }
        },
    }
}, this || (typeof window != 'undefined' ? window : null))