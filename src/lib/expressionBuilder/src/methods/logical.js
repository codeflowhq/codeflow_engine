(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsLogical = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    var helper = require ? require('./_helper') : exports.eeMethodsHelper


    return {
        isEmpty: {
            isCore: true,
            description: "Opposite of isSet. Returns true if the argument is empty. A string is considered empty if it's length is 0. A boolean is considered empty if it's value is false. A number is considered empty if it's value is <= 0. An object is considered empty if there are no properties set. Array is considered empty if it's length is 0. null and undefined are also considered empty.",
            arguments: [
            {
                name: "value",
                type: "any",
                description: "Any value.",
                required: true
            }
            ],
            returnType: "boolean",
            execute: function(obj) {
                return !helper.isSet(obj)
            }
        },
        isSet: {
            isCore: true,
            description: "Opposite of isEmpty. Returns true if the argument is set (not empty). A string is considered set if it's length is > 0. A boolean is considered set if it's value is true. A number is considered set if it's value is > 0. An object is considered set, if there is at least one property in it. Array is considered set if there is at least one item in it. Javascript types, null and undefined are considered empty.",
            arguments: [
            {
                name: "value",
                type: "any",
                description: "Any value.",
                required: true
            }
            ],
            returnType: "boolean",
            execute: function(obj) {
                return helper.isSet(obj)
            }
        },
        ifEmpty: {
            isCore: true,
            description: "If the first argument is not empty return it else return the second",
            arguments: [
            {
                name: "param1",
                type: "any",
                required: true,
                description: "A value, which will be returned if not empty."
            },
            {
                name: "param2",
                type: "any",
                required: true,
                description: "A value which will be returned if the first argument is empty."
            }
            ],
            returnType: function(argsType) {
                var arg1 = argsType.length > 0 ? argsType[0]: null
                var arg2 = argsType.length > 1 ? argsType[1]: null
                var retType = 'null'
                if ((arg1 && arg1.type)) {
                    if (arg2 && arg2.type) {
                        if (arg1.type === arg2.type) {
                            retType = arg2.type
                        } else {
                            retType = 'any'
                        }
                    } else {
                        retType = arg1.type
                    }
                }
                return retType
            },
            execute: function(value1, value2) {
                return helper.isSet(value1) ? value1: value2
            }
        },
        ifThen: {
            isCore: true,
            description: "If the condition is true, take the first value else take the second",
            arguments: [
            {
                name: "condition",
                type: "boolean",
                required: true,
                description: "A condition."
            },
            {
                name: "thenValue",
                type: "any",
                required: true,
                description: "Value that will be taken when the condition is true."
            },
            {
                name: "elseValue",
                type: "any",
                required: false,
                description: "Value that will be taken when the condition is false."
            }
            ],
            returnType: function(argsType) {
                var arg1 = argsType.length > 1 ? argsType[1]: null
                var arg2 = argsType.length > 2 ? argsType[2]: null
                var retType = 'null'
                if ((arg1 && arg1.type)) {
                    if (arg2 && arg2.type) {
                        if (arg1.type === arg2.type) {
                            retType = arg2.type
                        } else {
                            retType = 'any'
                        }
                    } else {
                        retType = arg1.type
                    }
                }
                return retType
            },
            execute: function(test, value1, value2) {
                return helper.isSet(test) ? value1 : value2
            }
        },
        equals: {
            isCore: true,
            description: "Returns true if all the arguments are equal to the first argument",
            arguments: [],
            variableArgument: {
                name: "value",
                type: "any",
                description: "Value"
            },
            returnType: "boolean",
            execute: function() {
                if (arguments.length > 0) {
                    for (var i = 1; i < arguments.length; i++) {
                        if(arguments[i] !== arguments[0])
                            return false;
                    }
                }
                return true
            }
        },
        equalsAny: {
            isCore: true,
            description: "Returns true if any of the arguments are equal to the first argument",
            arguments: [],
            variableArgument: {
                name: "value",
                type: "any",
                description: "Value"
            },
            returnType: "boolean",
            execute: function() {
                if (arguments.length > 0) {
                    for (var i = 1; i < arguments.length; i++) {
                        if(arguments[i] === arguments[0])
                            return true;
                    }
                }
                return false
            }
        },
        notEquals: {
            isCore: true,
            description: "Opposite of equals(). Returns true if all the values are not equal.",
            arguments: [],
            variableArgument: {
                name: "value",
                type: "any",
                description: "Value"
            },
            returnType: "boolean",
            execute: function(a, b) {
                if (arguments.length > 0) {
                    for (var i = 1; i < arguments.length; i++) {
                        if(arguments[i] === arguments[0])
                            return false;
                    }
                }
                return true
            }
        },

        or: {
            isCore: true,
            description: "Returns true if any one of the arguments evaluate to true.",
            arguments: [],
            variableArgument: {
                name: "value",
                type: "boolean",
                description: "Value"
            },
            returnType: "boolean",
            execute: function() {
                if (arguments.length > 0) {
                    for (var i = 0; i < arguments.length; i++) {
                        if(arguments[i]) {
                            return true
                        }
                    }
                }
                return false
            }
        },
        and: {
            isCore: true,
            description: "Returns true if all the arguments evaluate to true.",
            arguments: [],
            variableArgument: {
                name: "value",
                type: "boolean",
                description: "Value"
            },
            returnType: "boolean",
            execute: function() {
                if (arguments.length > 0) {
                    for (var i = 0; i < arguments.length; i++) {
                        if(!arguments[i]) {
                            return false
                        }
                    }
                }
                return true
            }
        },
        gt: {
            isCore: true,
            description: "Greater than(>). Returns true if the first value is greater than the second value.",
            arguments: [
            {
                name: "value1",
                type: "any",
                required: true,
                description: "First value."
            },
            {
                name: "value2",
                type: "any",
                required: true,
                description: "Second value."
            }
            ],
            returnType: "boolean",
            execute: function(a, b) {
                return a > b
            }
        },
        lt: {
            isCore: true,
            description: "Less than (<). Returns true if the first value is lesser than the second value.",
            arguments: [
            {
                name: "value1",
                type: "any",
                required: true,
                description: "First value."
            },
            {
                name: "value2",
                type: "any",
                required: true,
                description: "Second value."
            }
            ],
            returnType: "boolean",
            execute: function(a, b) {
                return a < b
            }
        },
        gte: {
            isCore: true,
            description: "Greater than or equal to(>=). Returns true if the first value is greater than or equal to the second value.",
            arguments: [
            {
                name: "value1",
                type: "any",
                required: true,
                description: "First value."
            },
            {
                name: "value2",
                type: "any",
                required: true,
                description: "Second value."
            }
            ],
            returnType: "boolean",
            execute: function(a, b) {
                return a >= b
            }
        },
        lte: {
            isCore: true,
            description: "Less than or equal to (<=). Returns true if the first value is less than or equal to the second value.",
            arguments: [
            {
                name: "value1",
                type: "any",
                required: true,
                description: "First value."
            },
            {
                name: "value2",
                type: "any",
                required: true,
                description: "Second value."
            }
            ],
            returnType: "boolean",
            execute: function(a, b) {
                return a <= b
            }
        },
        not: {
            isCore: true,
            description: "Returns boolean opposite.",
            arguments: [
            {
                name: "value1",
                type: "boolean",
                required: true,
                description: "A boolean value, oppoosite of which will be returned."
            }
            ],
            returnType: "boolean",
            execute: function(test) {
                return !test
            }
        }
    }
}, this || (typeof window != 'undefined' ? window : null))