(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsNumber = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    return {
        isNaN: {
            isCore: true,
            description: "Determines whether a value is NaN (Not a number).",
            examples: ["isNaN(div(10,0)) //true", "isNaN(23) //false"],
            arguments: [
            {
                name: "value",
                type: "any",
                description: "Number value to check if it's a NaN",
                examples: [
                    "isNaN(10) //false",
                    "isNaN(div(0, 0)) //true"],
                required: true
            }],
            returnType : "boolean",
            execute: function(value) {
                //poly fill for Number.isNaN
                return typeof value === "number" && value !== value
            }
        },
        isFinite: {
            isCore: true,
            description: "Determines whether a value is a finite, legal number",
            arguments: [
            {
                name: "value",
                type: "any",
                required: true
            }],
            returnType : "boolean",
            execute: function(num) {
                //built-in isFinite
                return isFinite(num)
            }
        },
        parseFloat: {
            isCore: true,
            description: "Parse a string to float",
            examples: [
                'parseFloat("5")) //5',
                'parseFloat("10.34")) //10.34'],
            arguments: [
            {
                name: "string",
                type: "any",
                description: "A string value to parse. Values of type other than string will be converted to string before parsing.",
                required: true
            }
            ],
            returnType: "number",
            execute: function(value) {
                return parseFloat(value)
            }
        },
        parseInt: {
            isCore: true,
            description: "Parse a string to integer",
            examples: [
                'parseInt("5")) //5',
                'parseInt("10.34")) //10',
                'parseInt("100", 2) //4'],
            arguments: [
            {
                name: "string",
                type: "any",
                description: "A string value to parse. Values other than string will be converted to string before parsing.",
                required: true
            },
            {
                name: "radix",
                type: "integer",
                description: "An integer between 2 and 36 that represents the radix (the base in mathematical numeral systems) of the first parameter. Defaults to 10.",
                required: false
            }
            ],
            returnType: "integer",
            execute: function(value, radix) {
                return parseInt(value, radix || 10)
            }
        },
        add: {
            isCore: true,
            description: "Returns the sum of the given numbers.",
            examples: [
                "add(1,2) //3",
                "add(10.5,2.2) //12.7"
            ],
            arguments: [
            {
                name: "augend",
                type: "number",
                required: true,
                description: "Number to add."
            },
            {
                name: "addend",
                type: "number",
                required: true,
                description: "Number to add."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 === "number" && typeof val2 === "number") {
                    result = val1 + val2
                }
                return result
            }
        },
        negate: {
            isCore: true,
            description: "Returns the negative of the given number.",
            examples: [
                "negate(2) //-2",
                "negate(-10.52) //10.52"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "Number to negate."
            }
            ],
            returnType : "number",
            execute: function(val) {
                return -val
            }
        },
        incr: {
            isCore: true,
            description: "Increment the given number by one.",
            examples: [
                "incr(-2) //-1",
                "incr(12) //13"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                description: "Increment the given number by one.",
                required: true
            }
            ],
            returnType : "number",
            execute: function(val1) {
                var result = NaN
                if (typeof val1 == "number") {
                    result = val1+1
                }
                return result
            }
        },
        decr: {
            isCore: true,
            examples: [
                "decr(30) //29",
                "decr(-2) //-3"
            ],
            description: "Decrement the given number by one.",
            arguments: [
            {
                name: "number",
                type: "number",
                description: "Decrement the given number by one.",
                required: true
            }
            ],
            returnType : "number",
            execute: function(val1) {
                var result = NaN
                if (typeof val1 === "number") {
                    result = val1-1
                }
                return result
            }
        },
        sub: {
            isCore: true,
            description: "Return the result of subtracting one number from the other.",
            examples: [
                "sub(30, 10) //20"
            ],
            arguments: [
            {
                name: "number1",
                type: "number",
                required: true,
                description: "Number to subtract from"
            },
            {
                name: "number2",
                type: "number",
                required: true,
                description: "Number to subtract"
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 === "number" && typeof val2 === "number") {
                    result = val1 - val2
                }
                return result
            }
        },
        div: {
            isCore: true,
            description: "Returns the result of division of the given numbers.",
            examples: [
                "div(20, 10) //2"
            ],
            arguments: [
            {
                name: "dividend",
                type: "number",
                required: true,
                description: "Number to divide."
            },
            {
                name: "divisor",
                type: "number",
                required: true,
                description: "Number to divide by."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 == "number" && typeof val2 == "number") {
                    result = val1 / val2
                }
                return result
            }
        },
        mul: {
            isCore: true,
            description: "Returns the result of multiplication of the given numbers.",
            examples: [
                "mul(4, 5) //20"
            ],
            arguments: [
            {
                name: "number1",
                type: "number",
                required: true,
                description: "Number to multiply."
            },
            {
                name: "number2",
                type: "number",
                required: true,
                description: "Number to multiply."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 == "number" && typeof val2 == "number") {
                    result = val1 * val2
                }
                return result
            }
        },
        mod: {
            isCore: true,
            description: "Returns the remainder after the division of the given numbers.",
            examples: [
                "mod(20, 5) //0",
                "mod(22, 5) //2"
            ],
            arguments: [
            {
                name: "dividend",
                type: "number",
                required: true,
                description: "Number to divide."
            },
            {
                name: "divisor",
                type: "number",
                required: true,
                description: "Number to divide by."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 == "number" && typeof val2 == "number") {
                    result = val1 % val2
                }
                return result
            }
        },
        max: {
            isCore: true,
            description: "Returns the largest of the given numbers.",
            examples: [
                "max(12, 11) //12",
                "max(-22, 5) //5"
            ],
            arguments: [
            {
                name: "number1",
                type: "number",
                required: true,
                description: "First number."
            },
            {
                name: "number2",
                type: "number",
                required: true,
                description: "Second number."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = NaN
                if (typeof val1 === "number" && typeof val2 === "number") {
                    result = Math.max(val1, val2)
                }
                return result
            }
        },
        min: {
            isCore: true,
            description: "Returns the smallest of the given numbers.",
            examples: [
                "min(12, 11) //11",
                "min(-22, 5) //-22"
            ],
            arguments: [
            {
                name: "number1",
                type: "number",
                required: true,
                description: "First number."
            },
            {
                name: "number2",
                type: "number",
                required: true,
                description: "Second number."
            }
            ],
            returnType : "number",
            execute: function(val1, val2) {
                var result = null
                if (typeof val1 === "number" && typeof val2 === "number") {
                    result = Math.min(val1, val2)
                }
                return result
            }
        },
        abs: {
            isCore: true,
            description: "Returns the absolute value of the given number.",
            examples: [
                "abs(-22) //22",
                "abs(12) //12"
            ],
            arguments: [
            {
                name: "value1",
                type: "number",
                required: true,
                description: "Number whose absolute value to be calculated."
            }
            ],
            returnType : "integer",
            execute: function(value1) {
                return Math.abs(value1)
            }
        },
        round: {
            isCore: true,
            description: "Returns the value of a number rounded to the nearest integer.",
            examples: [
                "round(12.5) //13",
                "round(12.2) //12"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number."
            }
            ],
            returnType : "integer",
            execute: function(val1) {
                return Math.round(val1)
            }
        },
        floor: {
            isCore: true,
            description: "Returns the largest integer less than or equal to a given number.",
            examples: [
                "floor(12.5) //12",
                "floor(12.2) //12"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number"
            }
            ],
            returnType : "integer",
            execute: function(val1) {
                return Math.floor(val1)
            }
        },
        ceil: {
            isCore: true,
            description: "Returns the smallest integer greater than or equal to a given number.",
            examples: ["ceil(2.6) //3",
                "ceil(2.2) //3"],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "A number"
            }
            ],
            returnType : "integer",
            execute: function(val1) {
                return Math.ceil(val1)
            }
        },
        sqrt: {
            isCore: true,
            description: "Returns the square root of the given number.",
            examples: [
                "sqrt(16) //4"
            ],
            arguments: [
            {
                name: "number",
                type: "number",
                required: true,
                description: "Number whose square root to be calculated."
            }
            ],
            returnType : "number",
            execute: function(number) {
                return Math.sqrt(number)
            }
        },
        pow: {
            isCore: true,
            description: "Returns the base to the exponent power. That is, given x and y, return x^y",
            arguments: [
            {
                name: "base",
                type: "number",
                required: true,
                description: "The base number."
            },
            {
                name: "exponent",
                type: "number",
                required: true,
                description: "The exponent used to raise the base."
            }

            ],
            returnType : "number",
            execute: function(val1, val2) {
                return Math.pow(val1 ,val2)
            }
        },
    }
}, this || (typeof window != 'undefined' ? window : null))
