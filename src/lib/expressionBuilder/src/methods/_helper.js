(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeMethodsHelper = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var utilLib = require ? require('../util') : exports.eeUtil
    var _ = require ? require('underscore') : exports._
    var pathLib = "path"
    var Path
    try {
        Path = typeof nodeRequire == 'undefined' ? (require ? require(pathLib) : {}) : {}
    } catch(e) {
        Path = {}
    }

    return {
        isSet: function(obj) {
            var isSet = true
            switch(typeof obj) {
                case 'string':
                    isSet = obj.length > 0
                    break
                case 'boolean':
                    isSet = obj
                    break
                case 'number':
                    isSet = obj > 0
                    break
                case 'object':
                    if (obj === null) {
                        isSet = false
                    }
                    else if (+obj.length === obj.length) { //array
                        isSet = (obj.length > 0)
                    } else { //obj
                        isSet = Object.keys(obj).length > 0
                    }
                    break
                case 'undefined':
                    isSet = false
                    break
                case 'function': // unusual?
                    isSet = true
                    break
                default:
                    isSet = false
            }
            return isSet
        },
        getArrayItemSchema: function(list) {
            var itemSchema
            if (list) {
                if (list.items) {
                    var argSchema = list.items
                    if (Array.isArray(argSchema)) {
                        if (argSchema.length > 0) {
                            itemSchema = argSchema[0]
                        }
                    } else {
                        itemSchema = argSchema
                    }
                } else if (list.type === 'object') {
                    if (list.additionalProperties && typeof list.additionalProperties == 'object') {
                        itemSchema = list.additionalProperties
                    } else {
                        itemSchema = {
                            type: 'any'
                        }
                    }
                }
            }
            return itemSchema
        },
        getArrayItemsSchema: function(argsType) {
            var item = {}
            var typeMap = {}
            for (var i = 0; i < argsType.length; i++) {
                var itemSchema = argsType[i] && argsType[i].items ? argsType[i].items : {type: "any"}
                item = utilLib.normalizeSchema(itemSchema)
                typeMap[item.type] = item
            }
            // if there is no item or more than one type
            if (_.size(typeMap) === 0) {
                item = {type: "null"}
            } else if (_.size(typeMap) > 1){
                item = {type: "any"}
            }
            return {
                type: "array",
                items: item
            }

        },
        //helper method to extract item schema of collection
        //from the method's arguments
        getCollectionItemSchema: function(method, idx) {
            var itemSchema
            var listSchema = (method.arguments && method.arguments.length > idx) ? method.arguments[idx] : {}
            var isCol = listSchema && (listSchema.type === 'array' || listSchema.type === 'object')
            if (isCol) {
                itemSchema = this.getArrayItemSchema(listSchema)
            } else {
                itemSchema = {
                    type: "any"
                }
            }
            return itemSchema
        },
        isBuffer: function(obj) {
            return obj != null && obj.constructor != null &&
                typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
        },
        isStream: function(stream) {
            return stream !== null &&
                typeof stream === 'object' &&
                typeof stream.pipe === 'function';
        }
    }
}, this || (typeof window != 'undefined' ? window : null))
