(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeParser = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var ERROR_CODES = {
        SYNTAX_ERROR: { code: 100, label: "Unexpected syntax"},
        RESERVED_KEYWORD_ERROR: { code: 102, label: "Reserved keyword"},
        UNMATCHED_BRACKETS: { code: 200, label: "Expected , or )"},
        ILLEGAL_OBJECT_ACCESS: {code: 300, label: "Unsupported property access"},
        ILLEGAL_ARRAY_ACCESS: {code: 302, label: "Unsupported array access"}
    }

    //regexps for tokens
    var TOKEN_PATTERNS = {
        STRING: /^"((\\"|[^"])*)"/,
        NUMBER: /^[-+]?\d+(\.\d*)?|^[-+]?\.\d+/,
        //@TODO Currently not supporting exponential notation, NaN, Infinity etc, need to support them in future
        /*NUMBER_NAN_EXP_ETC: /(?:NaN|-?(?:(?:\\d+|\\d*\\.\\d+)(?:[E|e][+|-]?\\d+)?|Infinity))/,
        NUMBER_WITH_EXP: /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/,*/
        INTEGER: /^\d+\b/,
        METHOD_IDENTIFIER: /^[\d|\w|:]+/,
        IDENTIFIER: /^[$A-Z\_0-9a-z]+/,
        RESERVED: /^(undefined|NaN|Infinity|do|if|in|for|let|new|try|var|case|else|enum|eval|false|null|this|true|void|with|break|catch|class|const|super|throw|while|yield|delete|export|import|public|return|static|switch|typeof|default|extends|finally|package|private|continue|debugger|function|arguments|interface|protected|implements|instanceof)$/,
        NULL: /^(null)$/,
        BOOLEAN: /^(true|false)$/
    }

    function ParserError(errorCode, payload) {
        this.code = errorCode.code
        this.label = errorCode.label
        this.payload = payload
    }

    ParserError.prototype.getCode = function() {
        return this.code
    }

    ParserError.prototype.getLabel = function() {
        return this.label
    }

    ParserError.prototype.getPayload = function() {
        return this.payload
    }

    /**
     * A minimal expression parser inspired from Eloquent Javascript
     */
    var Parser = function(expression) {
        function parseMethod(expr, program) {
            program = skipSpace(program)
            if (program[0] != "(") {
                return {
                    expr: expr,
                    rest: program
                }
            }
            program = skipSpace(program.slice(1))
            expr = {
                type: "method",
                operator: expr,
                args: []
            }
            while (program[0] != ")") {
                var arg = parseExpression(program)
                expr.args.push(arg.expr)
                program = skipSpace(arg.rest)
                if (program[0] == ",") {
                    program = skipSpace(program.slice(1))
                }
                else if (program[0] != ")") {
                    throw new ParserError(ERROR_CODES.UNMATCHED_BRACKETS, {
                        expr: expr,
                        rest: program
                    })
                }
            }
            return parseMethod(expr, program.slice(1))
        }

        function parseExpression(program) {
            var token = parseToken(program)
            var expr = token.expr
            program = token.rest
            var method = parseMethod(expr, program)
            method = parseRemainder(method)
            return method
        }

        function parseToken(program, isMember) {
            program = skipSpace(program)

            var match, expr
            if (match = TOKEN_PATTERNS.STRING.exec(program)) {
                expr = {
                    type: "value",
                    format: "string",
                    value: match[1]
                }
            }
            else if (match = TOKEN_PATTERNS.NUMBER.exec(program)) {
                var val = match[0]
                if (val == '+' || val == '-') {
                    val = 0
                }
                var subFormat = match[1] === undefined ? 'integer': 'float'
                expr = {
                    type: "value",
                    format: "number",
                    subFormat: subFormat,
                    value: Number(val)
                }
            }
            /* Integer is moved inside number - see above.
             *else if (match = TOKEN_PATTERNS.INTEGER.exec(program)) {
                expr = {
                    type: "value",
                    format: "number",
                    subFormat: "integer",
                    value: Number(match[0])
                }
            }*/
            else if (match = TOKEN_PATTERNS.IDENTIFIER.exec(program)) {
                var value = match[0]
                if (TOKEN_PATTERNS.BOOLEAN.exec(value)) {
                    expr = {
                        type: "value",
                        format: "boolean",
                        value: value
                    }
                } else if (TOKEN_PATTERNS.NULL.exec(value)) {
                    expr = {
                        type: "value",
                        format: "null",
                        value: value
                    }
                } else if (TOKEN_PATTERNS.RESERVED.exec(value) && !isMember) {
                    throw new ParserError(ERROR_CODES.RESERVED_KEYWORD_ERROR, {
                        rest: program
                    })
                } else if(TOKEN_PATTERNS.METHOD_IDENTIFIER.exec(program)) {
                    match = TOKEN_PATTERNS.METHOD_IDENTIFIER.exec(program)
                    var value = match[0]
                    expr = {
                        type: "keyword",
                        name: value
                    }
                } else {
                    expr = {
                        type: "keyword",
                        name: value
                    }
                }
            }
            else {
                throw new ParserError(ERROR_CODES.SYNTAX_ERROR, {
                    rest: program
                })
            }
            return {
                expr: expr,
                rest: program.slice(match[0].length)
            }
        }

        function parseRemainder(method) {
            method = parseMemberExpression(method)
            method = parseArrayAccess(method)
            return method
        }

        function parseMemberExpression(ast) {
            if (ast.rest
                && ast.rest[0] == '.'
                && (ast.expr.type == 'member' || ast.expr.type == 'method' || ast.expr.type == 'array' || ast.expr.type == 'keyword')) {

                var exp = ast.rest.slice(1)
                if (exp.length > 0) {
                    //var token = parseToken(exp)
                    var expAst = parseToken(exp, true)
                    if (expAst.expr.type == 'keyword'
                        || expAst.expr.type == 'array'
                        || expAst.expr.type == 'member') {
                        var memberExpr= {
                            expr: {},
                            rest: expAst.rest
                        }
                        memberExpr.expr.property = expAst.expr
                        memberExpr.expr.type = "member"
                        memberExpr.expr.object = ast.expr
                        ast = memberExpr
                        if (ast.rest) {
                            return parseRemainder(ast)
                        }
                    } else {
                        throw new ParserError(ERROR_CODES.ILLEGAL_OBJECT_ACCESS, {
                            expr: ast.expr,
                            rest: ast.rest
                        })
                    }
                }
            }
            return ast
        }

        function parseArrayAccess(ast) {
            if (ast.rest && ast.rest[0] == '[' &&
                    (ast.expr.type == 'method'
                    || ast.expr.type == 'member'
                    || ast.expr.type == 'keyword'
                    || ast.expr.type == 'array')) {

                var exp = ast.rest.slice(1)
                if (exp.length > 0) {
                    if (exp[0] == ']') {
                        exp = 'null' + exp
                    }
                    var expAst = parseExpression(exp)
                    if (expAst.expr.type == 'keyword'
                        || expAst.expr.type == 'value'
                        || expAst.expr.type == 'member'
                        || expAst.expr.type == 'array'
                        || expAst.expr.type == 'method') {

                        var memberExpr = {
                            expr: {},
                            rest: expAst.rest
                        }
                        memberExpr.expr.property = expAst.expr
                        memberExpr.expr.type = "array"
                        memberExpr.expr.object = ast.expr
                        ast = memberExpr
                        if (ast.rest) {
                           ast = parseRemainder(ast)
                        }
                    } else {
                        throw new ParserError(ERROR_CODES.ILLEGAL_ARRAY_ACCESS, {
                            expr: ast.expr,
                            rest: ast.rest
                        })
                    }
                }
            } else if (ast.rest && ast.rest[0] == ']') {
                ast.rest = ast.rest.slice(1)
            }
            return ast
        }

        function skipSpace(string) {
            var first = string.search(/\S/)
            if (first == -1) return ""
            return string.slice(first)
        }

        /**
         * Begin parsing
         */
        if (typeof expression == "string" && expression.length > 0) {
            var result = {}
            try {
                result = parseExpression(expression)
                if (skipSpace(result.rest).length > 0) {
                    result = {
                        error: "Unexpected text after program",
                        expr: result.expr,
                        rest: result.rest
                    }
                }
            } catch(e) {
                var eData = e.getPayload() || {}
                result = {
                    error : e.label,
                    expr: eData.expr,
                    rest: eData.rest
                }
            }
            return result
        } else {
            return null
        }
    }

    return Parser
})