(function(fn, window) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window != 'undefined' ? window : this)
            self.eeMethods = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var collection = {
        array: require ? require('./methods/array') : exports.eeMethodsArray,
        bitwise: require ? require('./methods/bitwise') : exports.eeMethodsBitwise,
        collection: require ? require('./methods/collection'): exports.eeMethodsCollection,
        date: require ? require('./methods/date'): exports.eeMethodsDate,
        json: require ? require('./methods/json'): exports.eeMethodsJson,
        logical: require ? require('./methods/logical'): exports.eeMethodsLogical,
        math: require ? require('./methods/math'): exports.eeMethodsMath,
        number: require ? require('./methods/number'): exports.eeMethodsNumber,
        path: require ? require('./methods/path'): exports.eeMethodsPath,
        string: require ? require('./methods/string'): exports.eeMethodsString,
        util: require ? require('./methods/util'): exports.eeMethodsUtil,
        _fast: require ? require('./methods/_fast'): exports.eeMethodsFast
    }

    var sortOrder = [
        'logical',
        'string',
        'number',
        'collection',
        'array',
        'bitwise',
        'math',
        'json',
        'path',
        'date',
        'util',
        '_fast'
    ]

    var builtInMethods = []
    sortOrder.map(ns => {
        Object.keys(collection[ns]).map(key => {
            var method = collection[ns][key]
            if (!method.name) {
                method.name = key
            }
            if (!method.namespace) {
                method.namespace = ns
            }
            builtInMethods.push(method)
        })
    })

    /**
    Methods.prototype.setArg = function(idx, identifier) {
        //find out schema from identifier
        var self = this
        var def = self.getSchemaFromIdentifier(identifier)
        self.onArgChange(idx, def)
    }*/
    /**
     * @TODO iterate methods from js files and fill here
     */

    return {
        getBuiltInMethods: function() {
            return builtInMethods
        }
    }
}, this || (typeof window != 'undefined' ? window : null))
