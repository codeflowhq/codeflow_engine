(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeUtil = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var schemaToTree = function(schema, options) {
        if (!schema) {
            throw new Error("empty schema")
        }
        options = options || {}
        var tree = createBranch(null, '-', '', {})
        if (typeof schema == "string") {
            schema = {
                "type": schema
            }
        }
        buildTree(tree, schema, options, {}, 0)
        return tree
    }

    var buildTree = function(root, schema, options, refCache, depth) {

        var properties = schema ? schema.properties || {} : {}
        var branch, type, ref
        var filter = options.filter || null
        //flag to mark if the subtree if filtered or not
        var filteredOut = true, filtered
        for(var prop in properties) {
            if (properties.hasOwnProperty(prop)) {
                var item = properties[prop]
                if (item) {
                    if (item['_id'] && !refCache[item['_id']]) {
                        //cache references, and to be later used to resolve
                        //self references
                        refCache[item['_id']] = {schema: item, cnt: 0}
                    }
                    if (item['$ref']) {
                        ref = refCache[item['$ref']]
                        //if it's the first occurrence, expand reference
                        if (ref && ref.cnt == 0) {
                            item =  ref.schema
                            ref.cnt++
                        }
                    }
                    if (item.type == 'array' && item.format != 'method') {
                        var childSchema = item.items
                        if (childSchema) {
                            if (+childSchema.length === childSchema.length) {
                                childSchema = childSchema[0]
                            }
                            if (childSchema) {
                                if (childSchema['_id'] && !childSchema[item['_id']]) {
                                    refCache[childSchema['_id']] = {schema: childSchema, cnt: 0}
                                }
                                var itemSchema = {}, isRef = false
                                //if the item is a reference
                                if (childSchema['$ref']) {
                                    ref = refCache[childSchema['$ref']]
                                    if (ref) {
                                        itemSchema = ref.schema
                                        //if it's the first occurance, expand reference
                                        if (ref.cnt == 0) {
                                            childSchema =  ref.schema
                                            ref.cnt++
                                        }
                                    }
                                    isRef = true
                                } else {
                                    itemSchema = childSchema
                                }
                                type = '<small> array [' + (isRef ? '$ref: ' : '') + (itemSchema.type || '') + ']</small>'
                                branch = createBranch(root, prop + ' [ ]' + type, prop + '[null]', item)
                                if (item.description) {
                                    branch.title = item.description
                                }
                                filtered = buildTree(branch, childSchema, options, refCache, depth++)
                                //filter out the branch if the children as well as the
                                //branch is not matched in the search
                                branch.filteredOut = filtered && checkFilter(branch.path, filter) < 0
                                if (!branch.filteredOut) {
                                    filteredOut = false
                                    appendTo(root, branch)
                                    if (filter) {
                                        branch.ui_state = 'open'
                                    }
                                }
                            }
                        }
                    } else if (item.type == 'object'
                               && item.format != 'method') {
                        branch = createBranch(root, prop + ' <small>object</small>', prop, item)
                        if (item.description) {
                            branch.title = item.description
                        }
                        filtered = buildTree(branch, item, options, refCache, depth++)
                        //filter out the branch if the children as well as the
                        //branch is not matched in the search
                        branch.filteredOut = filtered && checkFilter(branch.path, filter)  < 0
                        if (!branch.filteredOut) {
                            filteredOut = false
                            appendTo(root, branch)
                            if (filter) {
                                branch.ui_state = 'open'
                            }
                        }
                    } else {
                        if (checkFilter(root.path + prop, filter) >= 0) {
                            if (item['$ref'] ) {
                                type = '$ref:' + item['$ref']
                            } else {
                                type = item.type
                            }
                            var label = options.appendType ?  prop + '<small>' + type + '</small> ' : prop
                            var leaf = createLeaf(root, label, root.path + prop, item)
                            if (item.description) {
                                leaf.title = item.description
                            }
                            leaf.filteredOut = filteredOut = false
                        }
                    }
                }
            }
        }
        if (schema.type == 'object') {
            var aSchema = schema.additionalProperties
            if (aSchema !== 'false') {
                var PATH_PREFIX = '.'
                var EMPTY_LABEL = '<span style="font-size: 0.9em;">additional property (<font style="font-style:italic; font-size: 0.85em;">Click to add</font>)</span>'
                if (typeof aSchema == 'object' && aSchema.type) {
                    var aLabel = options.appendType ?  EMPTY_LABEL + '<small>' + aSchema.type + '</small> ' : EMPTY_LABEL
                    var el = createLeaf(root, aLabel, root.path + PATH_PREFIX, aSchema)
                    if (aSchema.description) {
                        el.title = aSchema.description
                    }
                    //@TODO why was it set to false?
                    //el.filteredOut = filteredOut = false
                }
            }
        }
        return filteredOut
    }

    var appendTo = function(root, child) {
        if (!root.children) {
            root.children = []
        }
        root.children.push(child)
    }

    var checkFilter = function(path, filter) {
        if (!filter) {
            return 0
        }

        if (path.indexOf('/Data') === 0) {
            path = path.substring(6, path.length)
        }

        if (path.lastIndexOf('/') == path.length-1) {
            path = path.substring(0, path.length - 1)
        }

        path = path.replace(/\//g, '.')

        return path.toLowerCase().indexOf(filter.toLowerCase())
    }

    var createBranch = function(root, label, pathToken, item) {
        var data = item._treeData || {}
        var path = root ? (root.path || '') : ''
        var obj = {
            type: "branch",
            name: label,
            children: [],
            path: path + pathToken + '/'
        }
        for (var p in data) {
            if (data.hasOwnProperty(p)) {
                if (!obj[p])
                    obj[p] = data[p]
            }
        }
        return obj
    }

    var createLeaf = function(root, label, path, item) {
        var data = item._treeData || {}
        var obj = {
            type: "leaf",
            name: label,
            path: path,
            data: data.data
        }
        if (!root.children) {
            root.children = []
        }
        for (var p in data) {
            if (data.hasOwnProperty(p)) {
                if (!obj[p])
                    obj[p] = data[p]
            }
        }
        root.children.push(obj)
        return obj
    }

    var arrayChildSchema = function() {

    }

    var methodsToSchema = function(methods, isOpen) {
        var schema = {
            type: "object",
            properties: {

            },
            _treeData: {
                ui_state: 'open',
                cssClass: 'method_header'
            }
        }

        var namespaceDescription = {
            'util': 'Utility methods',
            'logical': 'Logical operations',
            'math': 'Mathematical operations',
            'collection': 'Utility methods for collection'
        }

        for (var i = 0; i < methods.length; i++) {
            var method = methods[i]
            if (method.hidden == true) {
                continue
            }
            var namespace = method.namespace || 'other'
            if (!schema.properties[namespace]) {
                schema.properties[namespace] = {
                    "type": "object",
                    properties: {

                    },
                    _treeData: {
                        ui_state: isOpen ? 'open': 'close',
                        allowSelect: false,
                        cssClass: "method_namespace"
                    },
                    description: namespaceDescription[namespace]
                }
            }
            var type = method.returnType
            if (typeof type == 'function') {
                type = 'dynamic'
            } else if (typeof type == 'object' && type.type) {
                type = type.type
                if (type == 'array') {

                }
            }
            schema.properties[namespace].properties[method.name + '()'] =  {
                type: type,
                path: '/' + method.name,
                description: method.name + ': ' + method.description,
                format: 'method',
                _treeData: {
                    cssClass: "method",
                    data: method
                }
            }
        }
        return schema
    }

    /**
     * Converts ast to string form without adding undefined checks
     */
    var astToStr = function(ast, skeletonOnly) {
        var exp, property, obj
        if (ast.type == "method") {
            var args = ast.args
            exp = ast.operator.name + "("
            if (args) {
                for (var i = 0; i < args.length; i++) {
                    exp += astToStr(args[i], skeletonOnly)
                    if (i != args.length - 1) {
                        exp += ','
                    }
                }
            }
            exp += ")"
        } else if (ast.type == "member") {
            obj = astToStr(ast.object, skeletonOnly)
            property = astToStr(ast.property, skeletonOnly)
            exp = obj + '.' + property
        } else if (ast.type == "keyword") {
            exp = ast.name
        } else if (ast.type == "array") {
            obj = astToStr(ast.object, skeletonOnly)
            if (typeof skeletonOnly != 'undefined' && skeletonOnly) {
                property = ''
            } else {
                property = astToStr(ast.property, skeletonOnly  )
            }
            exp = obj + '[' + property + ']'
        } else if (ast.type == "value") {
            exp = ast.value
        }
        return exp
    }

    var dataToSchema = function(data) {
        var schemaType = 'null', schema = {}
        var type = typeof data
        if (Array.isArray(data)) {
            var items = []
            schemaType = 'array'
            var itemSchema = {}
            for (var i = 0; i < data.length; i++) {
                var tmpSchema = dataToSchema(data[i])
                itemSchema = mergeIfCompatible(itemSchema, tmpSchema)
            }
            //@TODO - currently only supporting single child type
            items.push(itemSchema)
            schema.items = items
        } else if (type == 'object') {
            schemaType = 'object'
            var props = {}
            for (var p in data) {
                if (data.hasOwnProperty(p)) {
                    props[p] = dataToSchema(data[p])
                }
            }
            schema.properties = props
        } else {
            schemaType = type
        }
        schema.type = schemaType
        return schema
    }

    function mergeIfCompatible(dest, source) {
        if (!dest || !dest.type) {
            return source
        }
        if (dest.type == 'object') {
            if (source.type == 'object' && source.properties) {
                dest.properties = dest.properties || {}
                for (var p in source.properties) {
                    if (source.properties.hasOwnProperty(p)) {
                        if (dest.properties[p]) {
                            dest.properties[p] = mergeIfCompatible(dest.properties[p], source.properties[p])
                        }
                    }
                }
            }
            else {

            }
        } else if (dest.type == 'array') {
            if (source.type == 'array') {
                if (!dest.items || dest.items.length == 0) {
                    if (source.items) {
                        dest.items = source.items
                    }
                } else if (source.items && source.items.length > 0){
                    var destItem = dest.items[0]
                    var sourceItem = source.items[0]
                    destItem = mergeIfCompatible(destItem, sourceItem)
                }
            } else {

            }
        } else if (dest.type == 'any') {
            if (source.type != 'null') {
                return source
            }
        }
        return dest
    }

    /**
     * @TODO - temporary way to extract single item from item schema - until
     * mixed type is supported
     */
    var extractItemSchema = function(items) {
        var retSchema
        if (items) {
            if (Array.isArray(items)) {
                if (items.length > 0) {
                    retSchema = items[0]
                }
            } else {
                retSchema = items
            }
        }
        return retSchema
    }


    var isArray = Array.isArray || function (xs) {
        return Object.prototype.toString.call(xs) === '[object Array]'
    }

    var deepClone = function(obj) {
        var ret
        try {
            ret = JSON.parse(JSON.stringify(obj))
        } catch(e) {
            //
        }
        return ret
    }

    var normalizeSchema = function(schema) {
        var retSchema = {}
        switch (typeof schema) {
            case 'string':
                retSchema.type = schema
            break
            case 'object':
                if (schema == null) {
                    retSchema.type = 'null'
                } else {
                    retSchema = schema
                }
            break;
            case 'undefined':
                //TODO - unknown is not a standard json schema type
                retSchema.type = 'unknown'
            break;
            default:
                 //TODO - unknown is not a standard json schema type
                retSchema.type = 'unknown'
        }
        return retSchema
    }

    var size = function(obj) {
        var len = 0
        obj = obj || {}
        if (Array.isArray(obj)) {
            len = obj.length
        } else {
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    len++
                }
            }
        }
        return len
    }

    var isStringExp = function(expression) {
        return expression && expression[0] == '"' && expression[expression.length - 1] == '"'
    }

    /**
     * get an expression like object.field-name and convert it to object["field-name"]
     * It is assumed that the starting entity will not have hyphen
     */
    var hyphensToIndex = function(expression) {
        var portions = expression.split(".")
        var expression = portions.length > 0 ? portions[0] : ""
        for (var i = 1; i < portions.length; i++) {
            var portion = portions[i]
            if (portion.indexOf('-') > -1) {
                expression += ('["' + portion + '"]')
            } else {
                expression += ('.' + portion)
            }
        }
        return expression
    }

    return {
        astToStr: astToStr,
        dataToSchema: dataToSchema,
        schemaToTree: schemaToTree,
        methodsToSchema: methodsToSchema,
        size: size,
        extractItemSchema: extractItemSchema,
        isArray: isArray,
        isStringExp: isStringExp,
        deepClone: deepClone,
        normalizeSchema: normalizeSchema,
        hyphensToIndex: hyphensToIndex
    }
})