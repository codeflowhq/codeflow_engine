(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeEvaluator = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"
    var util = require ? require('./util') : exports.eeUtil
    var parser = require ? require('./parser') : exports.eeParser
    var _ = require ? require('underscore') : exports._

    var DEFAULT_PREFIX = 'data'

    var Evaluator = function(expString, returnRaw, options) {
        var ast = {}
        options = options || {}
        if (!options.prefix) {
            options.prefix = DEFAULT_PREFIX
        }
        var expression = parser(expString)
        if (!expression.error) {
            ast = expression.expr
            //experimental - append <<PREFIX>> to each root. This is to avoid
            //using with() (which was causing performance issues)
            //or dynamic function args..
            preProcess(ast, false, [], options.prefix)
            var compiled = evalExpr(ast, options)
            if (returnRaw) {
                return compiled
            }
            /**
             * Return a function with compiled code called inside the context.
             */
            var executable = new Function("__methods", options.prefix, compiled.declarations + ' return ' + compiled.statement)
            return executable
        }
        return undefined
    }

    //insideF flags that we are inside a method.
    //appending PREFIX should be done carefully here
    function preProcess(ast, insideF, closureVars, prefix) {
        if (ast.type == "member") {
            preProcess(ast.object, insideF, closureVars, prefix)
        } else if (ast.type == "array") {
            preProcess(ast.object, insideF, closureVars, prefix)
            preProcess(ast.property, insideF, closureVars, prefix)
        } else if (ast.type == "method") {
            //carry over
            var isInsideF = insideF
            if (ast.operator.name == "f") {
                isInsideF = true
            }
            if (ast.args) {
                for (var arg in ast.args) {
                    preProcess(ast.args[arg], isInsideF, closureVars, prefix)
                }
            }
        } else if (ast.type == "keyword") {
            //do not put prefix if inside a function passed as argument to a
            //higher order function
            if (!insideF && _.indexOf(closureVars, ast.name) == -1) {
                ast.type = "member"
                ast.object = {type: "keyword", name: prefix}
                ast.property = {type: "keyword", name: ast.name}
                delete ast.name
            }
        }
    }

    /**
     * Generate string form of expression with checks for undefined
     */
    function evalExpr(ast, options) {

        var processClosure = function(ast, closureVars) {
            var optionsClone = _.clone(options)
            var vars = _.clone(options._closureVars || [])
            optionsClone._closureVars = _.uniq(vars.concat(closureVars))
            preProcess(ast, false, optionsClone._closureVars, optionsClone.prefix)
            return evalExpr(ast, optionsClone)
        }
        /**
         * Custom snippets for bypassing function call for basic methods.
         */
        var compiledSnippets = {
            'mul': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('*') + ')'
            },
            'add': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('+') + ')'
            },
            'sub': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('-') + ')'
            },
            'div': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('/') + ')'
            },
            'negate': function(args) {
                if (args.length > 0) {
                    return '(-' + processAst(args[0]) + ')'
                }
                return undefined
            },
            'mod': function(args) {
                if (args.length > 1) {
                    return '(' + processAst(args[0]) + '%' + processAst(args[1]) + ')'
                }
                return args.length && processAst(args[0])
            },
            'gt': function(args) {
                if (args.length > 1) {
                    return '(' + processAst(args[0]) + '>' + processAst(args[1]) + ')'
                }
                return args.length && processAst(args[0])
            },
            'gte': function(args) {
                if (args.length > 1) {
                    return '(' + processAst(args[0]) + '>=' + processAst(args[1]) + ')'
                }
                return args.length &&  processAst(args[0])
            },
            'lt': function(args) {
                if (args.length > 1) {
                    return '(' + processAst(args[0]) + '<' + processAst(args[1]) + ')'
                }
                return args.length && processAst(args[0])
            },
            'lte': function(args) {
                if (args.length > 1) {
                    return '(' + processAst(args[0]) + '<=' + processAst(args[1]) + ')'
                }
                return args.length && processAst(args[0])
            },
            'or': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('||') + ')'
            },
            'and': function(args) {
                if (args.length <= 1) {
                    return args.length && processAst(args[0])
                }
                return '(' + args.map(processAst).join('&&') + ')'
            },
            'map': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'key', 'index'])
                    return '__methods[\'_fast:map\'](' + a + ', function(item, key, index) {' + b.declarations + ' return ' + b.statement + '})'
                }
                return args.length && processAst(args[0])
            },
            'filter': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'key', 'index'])
                    return 'function() {var index=-1; return __methods[\'_fast:filter\'](' + a + ', function(item, key) {index = index + 1; ' + b.declarations + ' return ' + b.statement + '})}()'
                }
                return args.length && processAst(args[0])
            },
            'find': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'key', 'index'])
                    return 'function() {var index=-1; return __methods[\'_fast:find\'](' + a + ', function(item, key) {index = index + 1;' + b.declarations + ' return ' + b.statement + '})}()'
                }
                return args.length && processAst(args[0])
            },
            'reduce': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['previous', 'current', 'index'])
                    var c = undefined
                    if (args.length > 2) {
                        if (args[2].value !== 'null') {
                            c = processAst(args[2])
                        }
                    }
                    var compiled = '__methods[\'_fast:reduce\'](' + a + ', function(previous, current, index) {' + b.declarations + ' return ' + b.statement + '}'
                    compiled += (typeof c !== "undefined" ? ', ' + c + ')' : ')')
                    return compiled
                }
                return undefined
            },
            'sortBy': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item'])
                    return '__methods[\'_fast:sortBy\'](' + a + ', function(item) {' + b.declarations + ' return ' + b.statement + '})'
                }
                return args.length && processAst(args[0])
            },
            '_sort': function(args) {

            },
            'minOf': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'index'])
                    return '__methods[\'_fast:minOf\'](' + a + ', function(item, index) {' + b.declarations + ' return ' + b.statement + '})'
                }
                return args.length && processAst(args[0])
            },
            'maxOf': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'index'])
                    return '__methods[\'_fast:maxOf\'](' + a + ', function(item, index) {' + b.declarations + ' return ' + b.statement + '})'
                }
                return args.length && processAst(args[0])
            },
            'group': function(args) {
                if (args.length > 1) {
                    var a = processAst(args[0])
                    var b = processClosure(args[1].args[0], ['item', 'index'])
                    return '__methods[\'_fast:groupBy\'](' + a + ', function(item, index) {' + b.declarations + ' return ' + b.statement + '})'
                }
                return args.length && processAst(args[0])
            },
            'array': function(args) {
                var stmt = '['
                for (var i = 0; i < args.length; i++) {
                    stmt += processAst(args[i])
                    if (i != args.length - 1) {
                        stmt +=','
                    }
                }
                return stmt + ']'
            }
        }

        //closure for storing variable counter and declaration prefix var
        var varIdx = 0, declarations = ""

        function processAst(ast) {
            var exp
            if (ast.type == "method") {
                exp = processMethod(ast, options)
            } else if (ast.type == "member") {
                exp = processMember(ast)
            } else if (ast.type == "keyword") { //identifier
                exp = processKeyword(ast.name)
            } else if (ast.type == "array") {
                exp = processArray(ast)
            } else if (ast.type == "value") {
                exp = processValue(ast)
            }
            return exp
        }

        function processValue(exp) {
            if (exp.format == "string") {
                return '"' + exp.value + '"'
            }
            return exp.value
        }

        function processKeyword(exp) {
            return '(typeof ' + exp + '!= "undefined" && ' + exp + '!== null?' + exp +  ': undefined)'
        }

        function processKeywordRecursive(exp, prev) {
            var prefix = prev ? prev + '.' : ''
            var dotIx = exp.indexOf('.')
            if (dotIx >= 0) {
                var parent = prefix + exp.substring(0, dotIx)
                var remain = exp.substring(dotIx+1)
                var nxt = processKeywordRecursive(remain, parent)
                return '(typeof ' + parent + '!= "undefined" && ' + parent + '!== null? ' + nxt + ': undefined)'
            } else {
                if (prev) {
                    return prefix.slice(0, -1) + '["' + exp + '"]'
                } else {
                    //TODO - will it reach this branch ever?
                    return '(typeof ' + exp + '!= "undefined" && ' + exp + '!== null ?' + exp +  ': undefined)'
                }
            }
        }

        function processMethod(ast, options) {
            var exp, args = ast.args
            if (ast.operator.name == 'f') {
                var funcArg = 'null'
                if (args.length > 0) {
                    var argExpCompiled =  evalExpr(args[0], options)
                    funcArg = argExpCompiled.declarations + ' return ' + argExpCompiled.statement
                }
                //in case of special methods, we cannot append prefix without going through the
                //methods argument signature. To avoid that we use with() {}, even though it's
                //slightly behind in performance
                //the flag alreadyInsideWith ensures that we do not wrap it over and over again
                exp = '(function(context){\n with('+ options.prefix + '){ with(context) \n{ \n' + funcArg +  '\n}\n} } )'
            } else {
                if (compiledSnippets[ast.operator.name]) {
                    exp = compiledSnippets[ast.operator.name](args)
                } else {
                    exp = '__methods[\'' + ast.operator.name + '\']?' + '__methods[\'' + ast.operator.name +  '\']('
                    if (args) {
                        for (var i = 0; i < args.length; i++) {
                            exp += processAst(args[i])
                            if (i != args.length - 1) {
                                exp += ','
                            }
                        }
                    }
                    var errorMethod = options && options.errorMethod ? options.errorMethod : '__methods[\'util:throwError\']'
                    exp += "): " + errorMethod + "('function "+ ast.operator.name + " not found')"
                }
            }
            var obj = setVar(exp)
            return obj
        }

        function processArray(ast) {
            var object = processAst(ast.object), arr
            if (ast.object.type == 'keyword') {
                arr = ast.object.name
            } else {
                arr = object
            }
            var property = processAst(ast.property)
            var exp = "(typeof " + object + "!= 'undefined' && " + object + "!== null?" + arr + '[' +  property + "]: undefined)"
            return exp
        }

        function processMember(ast) {
            //set a temporary variable with left side operand
            var obj = processAst(ast.object)
            if (ast.object.type != "method" && ast.object.type != "keyword") {
                obj = setVar(obj)
            }
            //now recreate string expression with new variable and evaluate once
            //again. e.g states[counter].cities[1].name will become t1.t2.name
            //where t1 = states[counter] and t2 = t1.cities[1]
            var property = util.astToStr(ast.property)
            var exp
            if (ast.property.type == "keyword")
                exp = processKeywordRecursive(obj + '.' + property)
            else {
                var expProperty = processAst(ast.property)
                expProperty = setVar(expProperty)
                exp = processKeywordRecursive(obj + '.' + expProperty)
            }
            return exp
        }

        function setVar(exp) {
            var vT = '__t' + varIdx
            declarations += '\nvar ' + vT + '=' + exp +';\n'
            varIdx++
            return vT
        }

        var exp = processAst(ast)
        return {declarations: declarations, statement: exp}
    }

    return Evaluator
})