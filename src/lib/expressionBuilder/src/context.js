(function(fn) {
    "use strict"
    if (typeof define != 'undefined') {
        define(fn)
    } else {
        if (typeof module != 'undefined') {
            module.exports = fn(require, exports, module)
        } else {
            var self = (typeof window!= 'undefined' ? window: this)
            self.eeContext = fn(null, self)
        }
    }
})(function(require, exports, module) {
    "use strict"

    var methods, util, parser

    if (require) {
        methods = require('./methods')
        util = require('./util')
        parser = require('./parser')
    } else {
        util = exports.eeUtil
        methods = exports.eeMethods
        parser = exports.eeParser
    }

    /**
     *
     */
    function ExpressionContext(schema, methods) {
        this.setDataSchema(schema || {})
        this.scopeSchema = null
        this.methodContext = {}
        //this.cached = computeTree(schema)
        this.cachedRefs = {}
        this.init()
    }

    ExpressionContext.prototype.init = function() {
        var defMethods = methods.getBuiltInMethods()
        this.methods = defMethods
        for (var i = 0; i < defMethods.length; i++) {
            var method = defMethods[i]
            if (method.execute) {
                if(method.isCore) { //core methods can be used without namespace
                    this.methodContext[method.name] = method.execute
                } else {
                    this.methodContext[method.namespace +':'+ method.name] = method.execute //.bind(this) -- commenting out to improve performance
                }

            }
        }
    }

    ExpressionContext.prototype.getMethodContext = function() {
        return this.methodContext
    }

    ExpressionContext.prototype.getExecutionContext = function(data) {
        return data
    }

    /**
     * Caching schema lookup.
     * //@TODo - currently unused
     */
    var computeTree = function(schema) {
        var tree = {}
        if (!schema) {
            return tree
        }
        var addToTree = function(key, schema) {
            tree[key] =
                schema
        }
        var addChildren = function(prefix, schema) {
            var type = typeof schema == 'object' ? schema.type : typeof schema
            if (type == 'object' && schema.properties) {
                prefix = prefix ? prefix + '.' : ''
                var props = schema.properties
                for (var k in props) {
                    var obj = props[k]
                    if (obj.type == 'object') {
                        addToTree(prefix + k, obj)
                        addChildren(prefix + k, obj)
                    } else if (obj.type == 'array') {
                        addToTree(prefix + k, obj)
                        addChildren(prefix + k, obj)
                    } else {
                        addToTree(prefix + k, obj)
                    }
                }
            } else if (type  == 'array' && schema.items) {
                prefix = prefix ? prefix + '[]' : ''
                var items = schema.items
                var item = Array.isArray(items) ? items[0] : items
                addToTree(prefix, item)
                addChildren(prefix, item)
            } else {
                if (prefix) {
                    addToTree(prefix, obj)
                    //tree[prefix] = schema
                }
            }
        }
        addChildren(null, schema)
        return tree
    }

    /**
     * @TODO - remove. Replaced by findFieldRecursive
     */
    ExpressionContext.prototype.findFieldCached = function(field, contextSchema) {
        var contextTree, result = null
        if (contextSchema) {
            //@TODO use realtime parsing instead of computing tree
            contextTree = computeTree(contextSchema)
            var obj = contextTree[field]
            if (obj) {
                result = obj
            }
        } else {
            if (!this.scopeCached) {
                this.scopeCached = computeTree(this.scopeSchema)
            }
            result = this.scopeCached[field]
            if (!result) {
                if (!this.cached) {
                    this.cached = computeTree(this.schema)
                }
                result = this.cached[field]
            }
        }
        return result
    }

    ExpressionContext.prototype.findMethod = function(name) {
        var methods = this.methods || [], result = null
        var ns = null
        if(name.indexOf('.') > -1 )  {
            var parts = name.split('.')
            ns = parts.shift()
            name = parts.join('.')
        } else if(name.indexOf(':') > -1 )  {
            var parts = name.split(':')
            ns = parts.shift()
            name = parts.join(':')
        }
        for (var i = 0; i < methods.length; i++) {
            var method = methods[i]
            if(method.name !== name) {
                continue
            }
            if ((!ns && method.isCore) || method.namespace == ns) {
                result = method
            }
        }
        return result
    }

    ExpressionContext.prototype.getExpressionSchema = function(str) {
        var as = parser(str)
        if (as && as.expr) {
            return this.getAstSchema(as.expr)
        }
        return null
    }

    ExpressionContext.prototype.getContext = function() {
        if (this.scopeSchema) {
            var schema = {
                type: "object",
                additionalProperties: this.schema.additionalProperties,
                properties: {

                }
            }
            //add other things here - like CONFIG

            var props = this.schema.properties
            for (var p in props) {
                if (props.hasOwnProperty(p)) {
                    schema.properties[p] = props[p]
                }
            }
            schema._treeData = this.schema._treeData

            var scopeProps = this.scopeSchema.properties
            for (var k in scopeProps) {
                if (scopeProps.hasOwnProperty(k)) {
                    schema.properties[k] = scopeProps[k]
                }
            }

            return schema
        } else {
            return this.schema
        }
    }

    ExpressionContext.prototype.getMethodArguments = function(expression, signature, context) {
        var argTypes = []
        for (var i = 0; i < expression.args.length; i++) {
            var arg = expression.args[i]
            var argSchema = this.getAstSchema(arg, context)
            argTypes.push(argSchema)
        }
        for (i = 0; i < expression.args.length; i++) {
            arg = expression.args[i]
            if (arg && arg.type == 'method') {
                if (arg.operator.name == 'f') {
                    if (signature && signature.arguments) {
                        var argSign = signature.arguments[i]
                        if (argSign && typeof argSign.arguments == 'function') {//to get the context
                            var contextSchema = argSign.arguments({
                                arguments: argTypes,
                                signature: signature
                            })
                            context = util.deepClone(context)
                            if (!context.properties) {
                                context.properties = {}
                            }
                            for (var k = 0; k < contextSchema.length; k++) {
                                context.properties[contextSchema[k].label] = contextSchema[k].schema
                            }
                            argSchema = this.getAstSchema(arg, context)
                            argTypes[i] = argSchema
                        }
                    }
                }
            }
        }
        return argTypes
    }
    /**
     * Find the schema of the expression
     */
    ExpressionContext.prototype.getAstSchema = function(expression, context) {
        var retSchema, type = expression.type
        context = context || this.getContext()
        if (type == "method") {
            var method = expression.operator.name
            if (method != 'f') {
                var signature = this.findMethod(method)
                var argTypes = this.getMethodArguments(expression, signature, context)
                if (signature && signature.returnType) {
                    if (typeof signature.returnType == 'function') {
                        retSchema = signature.returnType(argTypes)
                    } else {
                        retSchema = signature.returnType
                    }
                } else {
                    ///null
                }
            } else {
                if (expression.args && expression.args.length > 0) {
                    retSchema = this.getAstSchema(expression.args[0], context)
                } else {
                    retSchema = 'null'
                }
            }
        } else if (type == 'member') {
            if (expression.object.type == 'keyword') {
                 retSchema = this.getUrlSchema(expression, context)
            } else {
                var oSchema = this.getAstSchema(expression.object, context)
                retSchema = this.getUrlSchema(expression.property, oSchema)
            }
        } else if (type == 'array') {
            var outSchema
            //if (expression.object.type == 'keyword') {
            if (0) { //this is faster method, but bypassing it to detect schema better
                retSchema = this.getUrlSchema(expression, context)
            } else {
                outSchema = this.getAstSchema(expression.object, context)
                //array index can be on array as well as object (or string ??)
                if (outSchema) {
                    if (outSchema.items) {
                        if (Array.isArray(outSchema.items)) {
                            if (outSchema.items.length > 0)
                                retSchema = outSchema.items[0]
                        } else {
                            retSchema = outSchema.items
                        }
                    } else if (outSchema.type == 'object') {
                        var matchedSchema = null, valueIdx = false
                        if (outSchema.properties) {
                            if (expression.property.type == 'value') {
                                valueIdx = true
                                var idx = "" + expression.property.value
                                matchedSchema = outSchema.properties[idx]
                            }
                        }
                        //if there is no match for properties,
                        //check if additional properties is defined
                        //if so, return it
                        if (!matchedSchema) {
                            var additionalSchema = outSchema.additionalProperties
                            if (typeof additionalSchema != 'undefined') {
                                if (typeof additionalSchema == 'boolean') {
                                    //if there is no additionalProperties and
                                    //(if the index is a constant value and there is no match detected
                                    //or properties in object is empty then the schema is null)
                                    if (!additionalSchema && (valueIdx || util.size(outSchema.properties) == 0)) {
                                        matchedSchema = 'null'
                                    } else {
                                        matchedSchema = 'any'
                                    }
                                } else {
                                    matchedSchema = additionalSchema
                                }
                            } else {
                                matchedSchema = 'any'
                            }
                        }
                        retSchema = matchedSchema
                    } else if (outSchema.type == 'string') {
                        //accessing string with [] might need to be restricted..
                        retSchema = {
                            type: 'string'
                        }
                    } else if (outSchema.type == 'any') {
                        //index of any can be any
                        retSchema = {
                            type: 'any'
                        }
                    }
                }
            }
        } else if (type == 'value') {
            var schemaType = expression.format
            if (expression.format == 'number') {
                schemaType = expression.subFormat == 'integer' ? 'integer' : 'number'
            }
            retSchema = {
                type: schemaType
            }
        }
        else {
            retSchema = this.getUrlSchema(expression, context)
        }
        if (typeof retSchema == 'string') {
            retSchema = {
                type: retSchema
            }
        }
        if (retSchema && retSchema['_id']) {
            var refId = retSchema['_id']
            if (!this.cachedRefs[refId]) {
                this.cachedRefs[retSchema['_id']] = retSchema
            }
        }
        return retSchema
    }

    ExpressionContext.prototype.findFieldRecursive = function(expression, schema) {
        var retSchema = null
        if (schema && schema['$ref']) {
            var ref = schema['$ref']
            schema = this.cachedRefs[ref] || null
        }
        if (schema && expression) {
            var parser = function(str) {
                var token, rest, isArray = false
                var i = str.indexOf('.')
                if (i > 0) {
                    token = str.substring(0, i)
                    rest = str.substring(i+1)
                } else {
                    token = str
                }
                if ((i = token.indexOf('[')) >= 0) {
                    isArray = true
                    token = token.substring(0, i)
                    rest = str.substring(i + 2)
                    if (rest.indexOf('.') == 0) {
                        rest = rest.substring(1)
                    }
                }
                return {
                    token: token,
                    rest: rest,
                    isArray: isArray
                }
            }
            if (typeof expression == 'string') {
                var expr = parser(expression)
                var additionalProps = typeof schema.additionalProperties == 'undefined' ? true : schema.additionalProperties
                if (schema.type == 'object') {
                     // if schema has sub fields defined, detect it
                    if (schema.properties && typeof schema.properties[expr.token] !== 'undefined') {
                        var type = schema.properties[expr.token]
                        //if the token is array expression, get the item schema
                        if (expr.isArray) {
                            if (type.type == 'array') {
                                retSchema = this.getItemSchema(type)
                            } else if (type.type == 'any') { // if its any, children are any
                                retSchema = getAnyType()
                            }
                        } else {
                            retSchema = type
                        }
                        //recursively dive for sub fields
                        if (expr.rest) {
                            retSchema =  this.findFieldRecursive(expr.rest, retSchema)
                        }
                    } else { // object has no definition or found no match in the properties
                        if (additionalProps !== false) {
                            //in case of additionalProperties set to false,
                            //schema is unknown
                            if (additionalProps != null && typeof additionalProps == 'object') {
                                retSchema = additionalProps
                            } else {
                                //if (schema.additionalProperties === true) {
                                    retSchema = getAnyType()
                                //}
                            }
                        }
                    }
                } else if (schema.type == 'array') {
                    //if the schema is array, only match if the expression is
                    //also an array
                    if (expr.isArray) {
                        var item = this.getItemSchema(schema)
                        if (expr.rest) {
                            retSchema = this.findFieldRecursive(expr.rest, item)
                        } else {
                            retSchema = item
                        }
                    }
                } else if (schema.type == 'any') {
                    retSchema = getAnyType()
                } else if (schema.type == 'null') {
                    if (!expr.rest) {
                        retSchema = null
                    }
                } else {
                    //purposely left empty
                }
            } else {
                retSchema = {
                    type: typeof expression
                }
            }
        }
        if (retSchema && retSchema['_id']) {
            var refId = retSchema['_id']
            if (!this.cachedRefs[refId]) {
                this.cachedRefs[refId] = retSchema
            }
        }
        return retSchema
    }

    function getAnyType() {
        return {
            type: "any"
        }
    }

    ExpressionContext.prototype.getItemSchema = function(schema) {
        var items = schema.items || null, item = null
        if (Array.isArray(items)) {
            if (items.length > 0) {
                item = items[0]
            }
        } else {
            item = items
        }
        if (item['$ref']) {
            item = this.cachedRefs[item['$ref']]
        }
        return item
    }

    ExpressionContext.prototype.getUrlSchema = function(expression, schema) {
        var expStr = util.astToStr(expression, true)
        return this.findFieldRecursive(expStr, schema)
    }

    ExpressionContext.prototype.isSchemaMatch = function(expected, actual) {
        return isSchemaMatch(expected, actual)
    }

    var isSchemaMatch = function(expected, actual) {
        var match = false
        expected = util.normalizeSchema(expected)
        actual = util.normalizeSchema(actual)
        //@TODO _id are retained by resolver after resolving $ref
        //a better way to checking $ref would actually be using
        //resolver to resolve schemas and do a check instead of
        //relying on _id
        var expectedRef = expected['$ref'] || expected['_id']
        var actualRef = actual['$ref'] || actual['_id']
        if (expectedRef && expectedRef == actualRef) {
            match = true
        } else if (expected['id'] && expected['id'] == actual['id']) {
            match = true
        } else if (expected && expected.type) {
            if (actual && actual.type) {
                switch (expected.type) {
                    case 'object':
                        match = compareObjectSchema(expected, actual)
                        break
                    case 'array':
                        match = compareArraySchema(expected, actual)
                        break
                    case 'any':
                        match = true
                        break
                    case 'string':
                    case 'boolean':
                    case 'integer':
                    case 'number':
                    case 'null' :
                        match = isCompatibleType(expected.type, actual.type)
                        break
                    default :
                        //console.log("Unrecognized schema type")
                        match = actual && actual.type && expected.type == actual.type
                }
            } else {
                match = false
            }
        }
        return match
    }

    var compareObjectSchema = function(expected, actual) {
        var match = true
        if (!actual || !actual.type ||actual.type != expected.type) {
            match = false
        } else {
            var properties = expected.properties
            var actualPropsSize = util.size(actual.properties)
            var propSize = util.size(properties)
            if (propSize === 0) {
                if (actualPropsSize > 0) {
                    match = false
                }
            } else if (actualPropsSize === 0) {
                match = false
            } else if (propSize != actualPropsSize) {
                match = false
            } else {
                for (var p in properties) {
                    if (actual.properties[p]) {
                        match = isSchemaMatch(properties[p], actual.properties[p])
                    } else {
                        match = false
                        break
                    }
                }
            }
        }
        return match
    }

    var compareArraySchema = function(expected, actual) {
        var match = true
        if (!actual || !actual.type ||actual.type != expected.type) {
            match = false
        } else {
            if (util.size(expected.items) > 0) {
                if (util.size(actual.items) > 0) {
                    var expectedItems = util.extractItemSchema(expected.items)
                    var actualItems = util.extractItemSchema(actual.items)
                    match = isSchemaMatch(actualItems, expectedItems)
                } else {
                    match = false
                }
            } else if (util.size(actual.items) == 0) {
                match = false
            }
        }
        return match
    }

    var isCompatibleType = function(expected, actual) {
        var match = false
        if (expected == actual) {
            match = true
        } else if (expected == 'number' && actual == 'integer') {
            match = true
        } else if (expected == 'integer' && actual == 'number') {
            match = false
        } else if (expected == 'any') {
            match = true
        }
        return match
    }

    ExpressionContext.prototype.getMethods = function() {
        return this.methods
    }

    ExpressionContext.prototype.getDataSchema = function() {
        return this.schema
    }

    ExpressionContext.prototype.setDataSchema = function(schema) {
        schema = schema || {}
        if (!schema.type) {
            schema.type = 'object'
        }
        if (!schema.properties) {
            schema.properties = {}
        }
        schema.properties.global = this.getGlobalSchema()
        this.schema = schema
    }

    ExpressionContext.prototype.getGlobalSchema = function() {
        return {
            type: "object",
            additionalProperties: {
                type: "any"
            },
            _treeData: {
                cssClass: "data_header",
                ui_state: "open"
            }
        }
    }

    ExpressionContext.prototype.setScope = function(schema) {
        this.scopeSchema = schema
    }

    ExpressionContext.prototype.addScopeElement = function(key, schema) {
        this.scopeSchema.properties[key] = schema
    }

    ExpressionContext.prototype.getScope = function() {
        return this.scopeSchema
    }

    ExpressionContext.prototype.addScope = function(name, schema, level) {
        var scopeSchema = this.scopeSchema
        if (!scopeSchema || !scopeSchema.properties) {
            scopeSchema = {
                type: "object",
                properties: {

                }
            }
            this.scopeSchema = scopeSchema
        }
        if (level) {

        }
        scopeSchema.properties[name] = schema
    }

    return ExpressionContext
})
