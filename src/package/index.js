/**
 * Author Alias Eldhose (aliaseldhose@codeflow.co), Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
var _ = require('underscore')
var api = require('./api')
var async = require('async')
var Package = require('../package')
//var Installer = require('./install')

/**
 * Get the details of package with name
 */
var details = function(params, cb) {
    api.request('package/details', {
        namespace: params.pop()
    }, {
        method: 'POST',
        success: function(response) {
            if (response.error) {
                cb(response.error)
            } else {
                if (!response.data) {
                    cb()
                } else {
                    console.log(response.data.name + " " + response.data.version + " [" + response.data.namespace + "]")
                    response.data.modules.forEach(function(_module) {
                        console.log("  -> " + _module.meta.name + " [" + _module.ref + "]")
                    })
                    cb(null, response.data)
                }
            }
        }
    })
}

/**
 * Search for package with name
 */
var _search = function(params, cb) {
    api.request('package/list', {
        query: params.pop()
    }, {
        method: 'POST',
        success: function(response) {
            cb(response.error ? response.error : null, response.packages)
        }
    })
}

/**
 * List installed packages
 */
var _list = function(cb) {
    Package.listInstalled(process.cwd(), function(err, packages) {
        if (err) {
            return cb && cb(err)
        }
        packages.forEach(function(_package) {
            console.log('Package: '+ _package.name)
            console.log('Namespace: '+ _package.namespace)
            console.log('Modules')
            _package.modules.forEach(function(_module) {
                console.log("  -> " + _module.meta.name + " [" + _module.ref + "]")
            })
        })
    })
}

/**
 * Install one or more packages
 */
var _install = function(params, cb) {
    console.log('Getting package information')
    async.eachSeries(params, function(packageNs, next) {
        Installer.install(packageNs, process.cwd(), next)
    }, cb)
}

/**
 * Uninstall a package
 */
var _uninstall = function(params, cb) {
    async.eachSeries(params, function(packageNs, next) {
        Installer.unInstall(packageNs, process.cwd(), function(err) {
            if (err) {
                console.log(packageNs +" - "+ err)
            } else {
                console.log('Uninstalled ' + packageNs)
            }
            next()
        })
    }, cb)
}

/**
 * Show the status of installed packages
 */
var _status = function(params, cb) {
    Package.listInstalled(process.cwd(), function(err, packages) {
        if (err) {
            return cb(err)
        }
        var packagesStatus = {}
        params.map(function(_package) {
            packagesStatus[_package] = 'missing'
        })
        packages.forEach(function(_package) {
            if (params.indexOf(_package.name) < 0) {
                return
            }
            packagesStatus[_package.name] = _package.version
        })
        _.each(packagesStatus, function(_version, _packageStatus) {
            if (_version == 'missing') {
                console.error(_packageStatus + " - missing")
            } else {
                console.log(_packageStatus + "@" + _version)
            }
        })
    })
}

module.exports = {
    details: details
}