/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose (aliaseldhose@codeflow.co)
 *
 * Copyright 2024 Codeflow Private Limited
 *
 * MIT Licensed
 */
var logger = require('../logger')
var Utils = require('../util')

class NpmManager {

    constructor() {
        this.install = this.install.bind(this)
        this.execNpmCmd = this.execNpmCmd.bind(this)
        this.listInstalled = this.listInstalled.bind(this)
    }

    /**
     * Install npm modules in a given location
     * @param {Object} modules
     * @param {String} location
     * @returns
     */
    async install(modules=[], location) {

        let args = ['install']

        if(Array.isArray(modules)) {
            args = [...args, ...modules]
        } else if(typeof modules === 'object') {
            for(let key in modules) {
                let version = modules[key] && modules[key] !== '' ? '@'+ modules[key] : ''
                args.push(`${key}${version}`)
            }
        } else if(typeof modules === 'string') {
            args.push(modules)
        }

        return this.execNpmCmd(args, {cwd: location})
    }

   /**
    * Run npm command inside a directory using child process
    * @param {Array} args
    * @param {Object} options
    * @returns
    */
    async execNpmCmd(args, options={}) {
        let npmInfo
        if(process.env.IDE_CONFIG_NPM_PATH) {
            npmInfo = await Utils.which(process.env.IDE_CONFIG_NPM_PATH, { nothrow: true})
            if(!npmInfo.path) {
                throw new Error("Could not locate NPM. Custom configured npm path is invalid.")
            }
        }
        if(!npmInfo || !npmInfo.path) {
            npmInfo = await Utils.which('npm', { nothrow: true})
        }
        if(process.versions.electron && !npmInfo.path) {
            throw new Error("Could not locate NPM. Codeflow requires Node.js installed in the system.")
        }

        var cmd = npmInfo.path
        logger.info("Executing ", cmd, args, options)

        try {
            options.detached = false
            let resp = await Utils.spawnAsync(cmd, args, options)
            return resp
        } catch(e) {
            throw e
        }
    }

    /**
     * Get list of node modules installed for a given location
     * @param {String} location
     */
    async listInstalled(location) {
        try {
            let resp = await execNpmCmd(['ls', '--json'], {cwd: location})
            return JSON.parse(resp)
        } catch(e) {
            throw e
        }
    }
}

module.exports = new NpmManager()