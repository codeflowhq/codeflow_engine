/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose (aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
const fs = require('fs')
const path = require('path')
const fse = require('fs-extra')

const NpmManager = require('./npmManager')
const Downloader = require('./downloader')

const Package = require('../package')
const logger = require('../logger')

class PackageInstall {

    /**
     * Install all the codeflow packages with in a folder after reading from package.json
     * @param {*} basePath
     * @param {*} cb
     * @returns
     */
    async installAll(basePath) {

        if (!fs.existsSync(basePath)) {
            throw new Error('Unable to locate the project directory')
        }
        let packageJson = await this.readPackageJson(basePath, true)
        if (!packageJson || !packageJson.codeflow) {
            throw new Error("Is it a valid codeflow project? Try the command from inside the project's directory.")
        }

        let result
        if (packageJson.codeflow.dependencies) {
            result = await this.install(packageJson.codeflow.dependencies, null, basePath)
        }

        return result
    }

    /**
     *
     * Install a new package to the path specified
     * @param {*} packageNames
     * @param {*} user
     * @param {*} basePath
     * @param {*} cb
     * @returns
     */
    async install(packageNames, user, basePath) {
        var packages = []
        if (Array.isArray(packageNames)) {
            packages = packageNames
        } else if (typeof packageNames == 'string') {
            packages = [packageNames]
        } else if (typeof packageNames == 'object') {
            packages = Object.keys(packageNames).map(packageName => {
                let version = packageNames[packageName] ? '@' + packageNames[packageName] : ''
                return packageName + version
            })
        } else {
            throw new Error("No package to install")
        }
        if (!fs.existsSync(basePath)) {
            throw new Error('Unable to locate the project directory')
        }
        let errors = []
        const self = this
        await Promise.all(packages.map(pkgName => {
            return new Promise(resolve => {
                let [name, version] = pkgName.split('@')
                logger.info("Installing package: ", name)
                var params = {
                    path: basePath,
                    packageId: name,
                    version: version || '*',
                    onComplete: async packagePath => {
                        try {
                            let result = await self.postInstall(packagePath, basePath)
                            resolve(result)
                        } catch (e) {
                            errors.push(e.message)
                            logger.error(e.message)
                            resolve()
                        }
                    },
                    onError: async e => {
                        errors.push(e.message)
                        logger.error(e.message)
                        resolve()
                    }
                }
                if (user) {
                    params.token = user.token
                }
                let download = new Downloader(params)
                download.start()
            })
        }))
        if (errors.length > 0) {
            throw new Error(errors.length === 1 ? errors[0] : "One or more packages failed to install")
        }

        //reload packages and clear cache
        return Package.loadInstalled(basePath, true)
    }


    async readPackageJson(pckPath, noError=false) {
        try {
            const packageJson = await fse.readJson(path.join(pckPath, 'package.json'))
            return packageJson
        } catch(e) {
            if(!noError) {
                throw e
            }
            return false
        }
    }

    /**
     * Process the dependencies after installing a package
     */
    async postInstall(packagePath, basePath) {
        try {
            let pkgPackageJson = await this.readPackageJson(packagePath, true)
            if (!pkgPackageJson) {
                throw new Error("Not a valid codeflow package.")
            }
            let pkgInfo = pkgPackageJson.codeflow || {}

            //process npm dependencies
            await NpmManager.install(pkgPackageJson.dependencies, packagePath)

            //process codeflow dependencies
            if (pkgInfo.dependencies && Object.keys(pkgInfo.dependencies).length > 0) {
                await this.install(pkgInfo.dependencies, null, packagePath)
            }

            /** Write the codeflow pacakge dependecy to the project's package.json file **/
            let packageJson = await this.readPackageJson(basePath, true)
            if (packageJson) {
                if (!packageJson.codeflow) {
                    packageJson.codeflow = {}
                }
                if (!packageJson.codeflow.dependencies) {
                    packageJson.codeflow.dependencies = {}
                }
                packageJson.codeflow.dependencies[pkgInfo.packageId] = pkgPackageJson.version || '*'
                await fse.writeJson(path.join(basePath, 'package.json'), packageJson, { spaces: 4 })
            }

            return pkgPackageJson

        } catch (e) {
            try {
                await fse.remove(packagePath)
            } catch (eRemove) {
                let removeError = 'Unable to remove the package directory'
                //don't throw an error, continue removing the package from project's package.json
                logger.error(eRemove.message)
            }
            throw e
        }
    }

    async uninstall(packageNS, basePath) {

        try {
            if (!fs.existsSync(basePath)) {
                throw new Error('Unable to locate the project directory')
            }
            let installed = await Package.loadInstalled(basePath)
            Package.removeChildProcess(basePath)

            let pkg = installed.find(item => item.namespace === packageNS)
            if (!pkg) {
                throw new Error("Package does not exist")
            }
            delete (pkg.is_installed)
            let removeError = false
            try {
                await fse.remove(pkg.path)
            } catch (e) {
                removeError = 'Unable to remove the package directory'
                //don't throw an error, continue removing the package from project's package.json
                logger.error(e.message)
            }
            pkg.uninstalled = true

            /** Remove the codeflow pacakge dependecy from the project's package.json file **/
            let packageJson = await this.readPackageJson(basePath, true)
            if (packageJson && packageJson.codeflow && packageJson.codeflow.dependencies) {
                if (packageJson.codeflow.dependencies[packageNS]) {
                    delete packageJson.codeflow.dependencies[packageNS]
                    await fse.writeJson(path.join(basePath, 'package.json'), packageJson, { spaces: 4 })
                }
            }

            //reload packages by clearing cache
            await Package.loadInstalled(basePath, true)
            if (removeError) {
                throw new Error(removeError)
            }
            return true
        } catch (e) {
            throw e
        }
    }
}

const Installer = new PackageInstall()

module.exports = {
    installAll: async (basePath, cb) => {
        try {
            let result = await Installer.installAll(basePath)
            cb(null, result)
        } catch (e) {
            cb(e)
        }
    },

    install: async (packageNames, user, basePath, cb) => {
        try {
            let result = await Installer.install(packageNames, user, basePath)
            cb(null, result)
        } catch (e) {
            cb(e)
        }
    },

    uninstall: async (packageNS, basePath, cb) => {
        try {
            let result = await Installer.uninstall(packageNS, basePath)
            cb(null, result)
        } catch (e) {
            cb(e)
        }
    },

}