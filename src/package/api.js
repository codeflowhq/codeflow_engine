/**
 * Author Alias Eldhose (aliaseldhose@codeflow.co), Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT License
 */
var fs = require('fs')
var http = require('http')
var https = require('https')
var URL = require('url')
var crypto = require('crypto')
var _ = require('underscore')
var progress = require('progress-stream')

var TIMEOUT = 120000 //120 seconds
var CRYPT_ALGORITHM = 'aes256'
var CRYPT_KEY = 'c0d3%10W'

var API_URL = 'https://packages.codeflow.co'

var API_PREFIX = 'api/v1'

var enginePackageJson = require("../../package.json")
var ENGINE_VERSION = enginePackageJson && enginePackageJson.version

var IDE_INFO = {}
/**
 * Format the path to give a http url
 */
var getUrl = function(path) {
    path = path.indexOf('/') === 0 ? path.substr(1) : path
    //if the path is an absolute url just return
    if (path && path.indexOf('http') == 0) {
        return path
    }
    return (API_URL +'/'+ API_PREFIX +'/'+ (!path ? '' : path))
}

/**
 * Prepare the params in string format,
 */
var prepareParams = function(params) {
    if (typeof params !== 'object' || params === null) {
        return ''
    }
    /**
     * Stringify a value and key
     */
    function stringify(key, value) {
        var values = []
        if (typeof value === 'undefined') return values

        if (value === null) {
            value = ''
        } else if (!!(value.constructor && value.constructor.isBuffer && value.constructor.isBuffer(value))) { //is buffer
            value = value.toString()
        } else if (value instanceof Date) {
            value = value.toISOString()
        }
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
            return [encodeURIComponent(key) + '=' + encodeURIComponent(value)]
        }
        _.each(value, function(value, vKey) {
            values = values.concat(stringify(key +'['+ vKey +']', value))
        })
        return values
    }
    var _params = []
    _.each(params, function(value, key) {
        _params = _params.concat(stringify(key, value))
    })
    return _params.join('&')
}

/**
 * HTTP reuquest wrapper
 */
var request = function(url, params, options) {
    url = getUrl(url)
    options = options || {}
    var reqId = 'api_'+ Math.floor(Math.random() * Math.pow(10,10))
    var method = options.method || 'GET'
    var hadCallback = false

    var onSuccess = function(response) {
        if (typeof response != 'object') {
            response = {
                error: response
            }
        }
        if (!hadCallback) {
            hadCallback = true
            return options.success && options.success(response)
        }
    }

    var onError = function(err) {
        if (!hadCallback) {
            hadCallback = true
            return options.error && options.error(err)
        }
    }

    var parts = URL.parse(url)
    //set the default protocol as http
    var protocol = http
    var port = 80

    if (parts.protocol == 'https:') {
        //set the https protocol if URL demands it
        protocol = https
        port = 443
    }

    var reqOptions = {
        host: parts.hostname,
        port: parts.port ? parts.port : port,
        path: parts.pathname,
        method: method,
        agent: false
    }

    if (options.headers) {
        reqOptions.headers  = options.headers
    }
    let d = new Date()
    reqOptions.headers = reqOptions.headers || {}
    reqOptions.headers['x-system-time'] = d.toUTCString()
    if(IDE_INFO.build_id)  {
        reqOptions.headers['x-build-id'] = IDE_INFO.build_id
    }
    if(IDE_INFO.machine_id)  {
        reqOptions.headers['x-machine-id'] = IDE_INFO.machine_id
    }
    if(IDE_INFO.ide_version)  {
        reqOptions.headers['x-ide-version'] = IDE_INFO.ide_version
    }
    if(IDE_INFO.engine_version)  {
        reqOptions.headers['x-engine-version'] = IDE_INFO.engine_version
    }
    if(IDE_INFO.ide_mode)  {
        reqOptions.headers['x-ide-type'] = IDE_INFO.ide_mode
    }
    if (!IDE_INFO.engine_version && ENGINE_VERSION) {
        reqOptions.headers['x-engine-version'] = ENGINE_VERSION
    }

    if (!_.isString(params) && (method == 'GET' || method == 'POST')) {
        params = prepareParams(params)
    }

    if (method == 'POST') {
        if(!reqOptions.headers) {
            reqOptions.headers = {}
        }
        reqOptions.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        reqOptions.headers['Content-Length'] = params.length
    } else if (method == 'PUT') {
        if (!reqOptions.headers) {
            reqOptions.headers = {}
        }
        reqOptions.headers['Content-Type'] = 'application/octect-stream'
        reqOptions.headers['Content-Length'] = fs.statSync(params.body.path).size
    } else {
        reqOptions.path += '?'+ params
    }

    //add the users auth token to the request query
    if (options.auth_token) {
        reqOptions.path +=
            (reqOptions.path.indexOf('?') >= 0 ?
                '&auth_token='+ options.auth_token :
                '?auth_token='+ options.auth_token
            )
    }

    //set the timeout for the request
    var reqTimeout = options.timeout || TIMEOUT
    try {
        var request = protocol.request(reqOptions, function(result) {

            if (options.mode == 'download' && result.statusCode === 200) {
                console.log("result", result.statusCode)
                //create a write stream of the file and pipe it to response
                var outStream = fs.createWriteStream(options.file)
                result.on('error', function(err) {
                    outStream.close()
                    onError(err)
                })
                if (options.progressCb) {
                    var progressStream = progress({
                        time: 300
                    })
                    progressStream.on('progress', function(stats) {
                        options.progressCb(stats)
                    })
                    result.pipe(progressStream).pipe(outStream)
                } else {
                    result.pipe(outStream)
                }
                outStream.on('finish', function() {
                    clearTimeout(timeout)
                    outStream.close(function() {
                        onSuccess({
                            output: options.file
                        })
                    })
                })
            } else {
                result.setEncoding('utf8')
                var body = ''
                result.on('data', function(chunk) {
                    body += chunk
                })
                result.on('end', function() {
                    clearTimeout(timeout)
                    result.emit('close')
                })
                result.on('close', function() {
                    var response
                    try {
                        response = JSON.parse(body)
                    } catch(e) {
                        response = body
                    }
                    onSuccess(response)
                })
            }
        })
        //send the params if its a post request
        if (method == 'POST') {
            request.write(params)
        }
        if (method == 'PUT' && params.body && params.body instanceof Object) {
            params.body.pipe(request)
        } else {
            //end the request
            request.end()
        }
        //set the timeout handler
        var timeout = setTimeout(function() {
            request.abort() //this will also throw the error event, no need to call onError ,
            //onError({error_code : 28, error : {message : 'timeout', type : 'CurlException'}})
        }, reqTimeout)
        //set error handler
        request.on('error', function(err, stack) {
            clearTimeout(timeout)
            onError && onError(err, stack)
        })
    } catch(err) {
        onError && onError(err)
        //@TODO - why try/catch ?
    }
}

/**
 * Encrypt the user information for authenticating with the module manager server.
 */
var getUserEncrypted = function(user) {
    var _user = {
        name: user.display_name,
        email: user.email,
        token: user.token
    }
    var cipher = crypto.createCipher(CRYPT_ALGORITHM, CRYPT_KEY)
    var encrypted  = cipher.update(JSON.stringify(_user), 'utf8', 'hex')
    return encrypted + cipher.final('hex')
}

/**
 * Authenticate a user with the API serer
 */
var authUser = function(user, cb) {
    request('user/auth', {
            auth_data: getUserEncrypted(user)
        },{
        success: function(response) {
            if (response && !response.error) {
                return cb(null, response)
            } else if(response.error) {
                return cb(response.error)
            }
            return cb(new Error('Unable to authenticate the user'))
        },
        error: function(err) {
            return cb(new Error('Unable to authenticate the user'))
        }
    })
}

module.exports = {
    authUser: authUser,
    request: request,

    setIdeInfo: function(info) {
        IDE_INFO = info
    }
}