/**
 * Author Alias Eldhose (aliaseldhose@codeflow.co), Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
var fs = require('fs')
var path = require('path')
var zlib = require('zlib')
var crypto = require('crypto')

var _ = require('underscore')
var fse = require('fs-extra')

var api = require('./api')
var fsUtil = require('../fsUtil')
var util = require('../util')
//var validator = require('validate-npm-package-name')

//var PREFIX = '@codeflow/'
var PREFIX = ''

var validator = {
    validate: function(fullname) {
        var blacklist = [
            //add blacklisted names if any here
        ]
        var reg = /^[a-z][a-z\d_-]*$/
        var error = null
        var name
        if (fullname.indexOf(PREFIX) === 0) {
            name = fullname.substring(PREFIX.length)
        } else {
            error = "Package name should prefix " + PREFIX
        }
        if (error)  {
            return {
                error: error
            }
        }
        if (!name) {
            error = "Package name should not be empty"
        } else if (name.length <3) {
            error = "Package name should be at least 3 characters long."
        } else if (name.length > 50) {
            error = "Package name should be at most 50 characters long."
        } else if (!reg.test(name)) {
            error = "Package name should start with an alphabet and can contain only lowercase alphanumeric characters, - (hyphen) and _ (underscore)"
        } else if (_.find(blacklist, function(item){return name == item})) {
            error = "Package name is reserved for system use and is not allowed"
        }
        return {
            error: error
        }
    }
}

/**
 * Package json file which will be maintained for an already published package
 */
var PACKAGE_CONFIG_FILE_NAME = 'package.json'

var getPackageJsonPath = function(packagePath) {
    return path.join(packagePath, PACKAGE_CONFIG_FILE_NAME)
}

var initPackageJson = function(data, cb) {
    var project = data.project
    fse.readJson(getPackageJsonPath(project.path), function(err, json) {
        if (err || !json) {
            return createPackageJson(data, cb)
        }
        cb(null, json)
    })
}

var createPackageJson = function(data, cb) {
    var packageJson = {
        name: PREFIX + data.project.name,
        version: '0.0.1'
    }
    if (data.user) {
        packageJson.author = {
            name: data.user.name,
            email: data.user.email
        }
    }
    fse.writeJson(getPackageJsonPath(data.project.path), packageJson, {spaces: 4}, function(err) {
        if (err) {
            return cb(err)
        }
        cb(null, packageJson)
    })
}

/**
 * Update package.json with user supplied data.
 */
var updatePackageJson = function(packagePath, packageData, cb) {
    fse.readJson(packagePath, function(err, packageJson) {
        if (packageData.version) {
            packageJson.version = packageData.version
        }
        if (packageData.name) {
            packageJson.name = packageData.name
        }
        if (packageData.description) {
            packageJson.description = packageData.description
        }
        //merge(packageJson, 'keywords', packageData.keywords)
        if (packageData.website) {
            packageJson.website = packageData.website
        }
        if (packageData.license) {
            packageJson.license = packageData.license
        }
        if (packageData.codeflow) {
            packageJson.codeflow = packageData.codeflow
        }
        fse.writeJson(packagePath, packageJson, {spaces: 4}, cb)
    })
}

/**
 * Walk through the package path and check for
 * dependencies (flows and modules)
 */
var loadPackageDetails = function(packagePath, cb) {
    var packageInfo = {
        modules: [],
        dependencies: {
            node: {},
            packages: {}
        }
    }
    fsUtil.listFileTree(packagePath, function(err, files, tree) {
        if (err) {
            return cb(err)
        }
        util.iterate(files, function(file, next) {
            var ext = path.extname(file)
            if(ext === '.js' && path.basename(file, ext) === 'modules') {
                packageInfo.dynamicModules = true
                return next()
            }
            if (ext != '.flw' && ext != '.act' && ext != '.cm') {
                return next()
            }
            if (err) {
                return next()
            }
            fse.readJson(file, function(err, data) {
                if (err) {
                    return next()
                }

                var module = {
                    file: path.relative(packagePath, file),
                    meta: {
                        name: path.basename(file, ext).replace(/\W+/g, "_"),
                        description: data.meta && data.meta.description ? data.meta.description : null
                    }
                }
                packageInfo.modules.push(module)
                return next()
            })
        }, function() {
            cb(null, packageInfo)
        })
    }, {level: 6, excludedPaths: '.packages'})
}

/**
* Validate and save package information.
*/
var validatePackage = function(data, cb) {
    var project = data.project
    var packageData = data.packageData
    var validate = validatePackageInfo(packageData)
    if (validate.error) {
        return cb(new Error(validate.error))
    }
    validateUnique({name: packageData.label, namespace: packageData.namespace, version: packageData.version}, data.token, function(err) {
        if (err) {
            return cb(err)
        }
        updatePackageJson(getPackageJsonPath(project.path), packageData, cb)
    })
}

/**
 * Validate the package information
 */
var validatePackageInfo = function(packageInfo) {
    var error = null
    if (!packageInfo.description) {
        error = 'Package description should be provided.'
    } else if (packageInfo.description.length > 1024) {
        error = 'description can be at most 1024 characters'
    }
    if (!packageInfo.version) {
        error = 'Package version must be specified to publish'
    } else if (packageInfo.version.trim().length < 5) {
        error = 'Package version should follow semantic versioning.'
    } else {
        var resp = validator.validate(packageInfo.name)
        if (resp.error) {
            error = resp.error
        }
    }
    return {
        error: error
    }
}

/**
 * Check the paramters before publishing the file
 */
var prePublish = function(packageInfo, token, cb) {
    api.request('package/pre_publish', {
        data: packageInfo
    }, {
        method: 'POST',
        auth_token: token,
        success: function(response) {
            response.publish = response.publish || {}
            return cb(response.error, response.publish)
        }
    })
}

/**
 * Publish the package with the selected modules
 */
var publish = function(packagePath, packageInfo, token, cb) {
    console.log([packageInfo])
    preparePublishTarball(packagePath, packageInfo, function(err, tarFile) {
        if (err) {
            return cb(new Error('Error publishing the package. ' + err.toString()))
        }
        //set the body of the request
        var params = {
            body: fs.createReadStream(tarFile)
        }
        api.request('package/publish', params, {
            method: 'PUT',
            auth_token: token,
            timeout: 1200000,
            success: function(response) {
                fs.unlink(tarFile, function(error) {
                    //ignoring
                })
                if (response.error) {
                    return cb(response.error)
                }
                //handle one more error situation here
                if(!response.package_json && response.code) {
                    return cb(response.message)
                }
                response.package_json = response.package_json || {}
                return cb(response.error, response.package_json)
            },
            error: function(response) {
                fs.unlink(tarFile, function(error){
                    //ignoring
                })
                return cb(new Error('Unable to complete the request. Please try again.'))
            }
        })
    })
}

var preparePublishTarball = function(packagePath, packageInfo, cb) {
    var tmpDir = path.join(__dirname, '../../tmp', '_' + 'publish') + new Date().getTime()
    var excludeNames = [
        '.packages',
        '.DS_Store',
        '.codeflow',
        '.git',
        '.gitignore',
        '.history',
        '.idea',
        '.vscode',
        'package-lock.json',
        'node_modules'
    ]
    var excludeExtensions = [
        'zip',
        'tar',
        'tar.gz',
        'tgz',
        '.rar'
    ]
    //copy the package files into tmp directory
    fse.copy(packagePath, tmpDir, {
        preserveTimestamps: true,
        filter: function(file) {
            let fileName = path.basename(file)
            if(excludeNames.find(exclude=>fileName.indexOf(exclude) === 0)) {
                return false
            }
            if(excludeExtensions.find(exclude=>path.extname(file).toLowerCase() === '.'+ exclude)) {
                return false
            }
            //exclude all the hidden files?
            if (fileName.indexOf('.') === 0) {
                return false
            }
            return true
        }
    }, function(err) {
        if (err) {
            return cb(err)
        }
        fse.ensureDir(tmpDir, function(err) {
            if (err) {
                return cb(err)
            }
            var confFile = path.join(tmpDir, 'package.conf')
            fse.writeJson(confFile, packageInfo, {spaces: 4}, function(err) {
                if (err) {
                    return cb(err)
                }
                fsUtil.compress(tmpDir, function(err, compressedFile) {
                    if (err) {
                        return cb(err)
                    }
                    fse.remove(tmpDir, function() {
                        //return the tarball file path which is the last entry in the results array
                        return cb(null, compressedFile)
                    })
                })
            })
        })
    })
}

var validateUnique = function(data, token, cb) {
    api.request('package/check_unique', {package:data}, {
        method: 'POST',
        auth_token: token,
        success: function(response) {
            if (response && response.error) {
                var error = response.error
                return cb(response.error)
            }
            cb(null, response)
        }
    })
}

var initPublish =  function(data, cb) {
    var token = data.token
    var project = data.project
    var user = data.user
    if (!user || !project || !token) {
        //
    }
    //load the config if its an already published package
    initPackageJson({
        project: project,
        user: user
    }, function(err, packageConfig) {
        if (err) {
           return cb(err)
        }
        loadPackageDetails(project.path, function(err, packageInfo) {

            //return if there are no flow files in the root of the package path
            if (!packageInfo || !packageInfo.modules || (packageInfo.modules.length < 1 && !packageInfo.dynamicModules)) {
                return cb(new Error('Unable to find publishable files'))
            }

            if (packageConfig.codeflow && packageConfig.codeflow.modules) {
                _.each(packageConfig.codeflow.modules, function(publishedModule) {
                    var module = _.find(packageInfo.modules, function(module) {
                        return module.file == publishedModule
                    })
                    if (module) {
                        module.enabled = true
                    }
                })
            }
            var output = {
                packageName: packageConfig.name,
                modules: packageInfo.modules,
                dependencies: packageInfo.dependencies,
                dynamicModules: packageInfo.dynamicModules
            }
            var packageDependencies = packageConfig.codeflow ? packageConfig.codeflow.dependencies : {}
            output.dependencies.packages = packageDependencies
            output.dependencies.node = packageConfig.dependencies
            output.packageJson = packageConfig

            //get the pacakge details if already published
            api.request('package/details', {
                name: packageConfig.name,
                namespace: packageConfig.codeflow ? packageConfig.codeflow.packageId : null
            }, {
                method: 'POST',
                timeout: 11000,
                auth_token: token,
                error: function(error) {
                    return cb(error)
                },
                success: function(response) {
                    if(response.error) {
                        return cb(response.error)
                    }
                    if (response.data) {
                        output.publishedData = response.data
                    }
                    return cb(null, output)
                }
            })
        })
    })
}

var publishAsPackage = function(packagePath, packageInfo, token, cb) {
    prePublish(packageInfo, token, function(err, response) {
        if (err) {
            return cb(err)
        }
        publish(packagePath, packageInfo, token, function(err, response) {
            if (err) {
                return cb(err)
            }
            if (!response) {
                return cb(new Error('Error in retrieving the published information.'))
            } else {
                updatePackageJson(path.join(packagePath, PACKAGE_CONFIG_FILE_NAME), response, function(err) {
                    if (err) {
                        return cb(err)
                    }
                    return cb(null, response)
                })
            }
        })
    })
}

module.exports = {
    initPublish: initPublish,
    validatePackage: validatePackage,
    publishAsPackage: publishAsPackage
}