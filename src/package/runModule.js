const path = require('path')
const runner = require('../runner')
const loader = require('../loader')
const { loadModuleSource } = require('../util')

let processErrored = false
let isPersistent = false
process.on('message', async data => {
    if(typeof data.isPersistent !== 'undefined' && data.isPersistent) {
        isPersistent = true
        return
    }

    let {method, input, options, messageId} = data
    try {
        let result = null
        if(method) { //run a specific method in the module main file
            result = await runMethod(method, input, options)
        } else { //execute the step with the ref
            result = await runStep(options.ref, input, options)
        }
        if(processErrored && !isPersistent) {
            return false
        }
        process.send({data: result, messageId})
    } catch(e) {

        if(processErrored && !isPersistent) {
            return false
        }
        if(typeof e === 'object' && e.message) {
            return process.send({error: e.message, messageId})
        }
        process.send({error: e.toString(), messageId})
    }
})
process.on('uncaughtException', function(error) {
    processErrored = true
    console.error('Unhandled Exception')
    process.send({error: error.message})
})
process.on('unhandledRejection', function(reason, p){
    processErrored = true
    console.error('Unhandled Rejection')
    process.send({error: reason})
})

async function evalInput(input, rootDir) {
    try {
        await runner.loadProperties(rootDir)
        var evaluated = loader.evaluateInputData(input, {}, 'execute-dynamic-'+ Date.now())
        return evaluated || input
    } catch(e) {
        return input
    }
}

function runMethod(method, input, options) {
    return new Promise(async (resolve, reject)=>{

        let step = options.step

        let moduleMain = await loadModuleSource(path.join(step.path, step.source))
        if(!moduleMain || !moduleMain[method]) {
            return reject('Module or method not defined')
        }
        moduleMain[method](input, (error, data)=>{
            if(error) {
                //in case of error return the default schema for appltInputFiler method
                if(method === 'applyInputFilter') {
                    return resolve({inputSchema: step['input-schema'], outputSchema: step['output-schema']})
                }
                return reject(error)
            }
            //remove the circular references
            if(typeof data === 'object') {
                data = JSON.parse(stringify(data))
            }
            resolve(data)
        }, options)
    })
}

function runStep(ref, input, options) {
    return new Promise((resolve, reject)=>{
        if(options.step_id) {
            ref = `${options.step_id}:${ref}`
        }

        evalInput(input, options.project_path).then(input=>{
            runner.run('.', {workingDir: options.project_path, runStep: ref, input: JSON.stringify(input), debug: false}, (error, result)=>{
                let output = null

                if(result) {
                    error = result.error || error
                    //remove any circular references
                    output = typeof result.output === 'object' ? JSON.parse(stringify(result.output)) : JSON.parse(stringify(result))
                }
                if(error) {
                    return reject(error)
                }

                resolve(output)
            })
        })
    })
}

//Json-stringify-safe to remove circular references
//https://github.com/moll/json-stringify-safe
function stringify(obj, replacer, spaces, cycleReplacer) {
    return JSON.stringify(obj, serializer(replacer, cycleReplacer), spaces)
}

function serializer(replacer, cycleReplacer) {
    var stack = [], keys = []

    if (cycleReplacer == null) cycleReplacer = function(key, value) {
        if (stack[0] === value) return "[Circular ~]"
        return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]"
    }

    return function(key, value) {
        if (stack.length > 0) {
            var thisPos = stack.indexOf(this)
            ~thisPos ? stack.splice(thisPos + 1) : stack.push(this)
            ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key)
            if (~stack.indexOf(value)) {
                value = cycleReplacer.call(this, key, value)
            }
        } else {
            stack.push(value)
        }

        return replacer == null ? value : replacer.call(this, key, value)
    }
}
