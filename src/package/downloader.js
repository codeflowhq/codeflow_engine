/**
 * Author Alias Eldhose (aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
var fse = require('fs-extra')
var path = require('path')
var _ = require('underscore')
var api = require('./api')
var fsUtil = require('../fsUtil')

var STATUS_STARTING = 0
var STATUS_DOWNLOADING = 1
var STATUS_PAUSED = 2
var STATUS_DOWNLOADED = 3
var STATUS_ERROR = 4
var STATUS_COMPLETE = 5

/**
 * Common directory to contain the packages
 */
var PACKAGES_DIR = '.packages'

/**
 * The retry count for download, in case of a network error or package error
 */
var MAX_DOWNLOAD_RETRY = 3

function Downloader(params) {
    this.packageId = params.packageId //name of package to download
    this.version = params.version
    this.basePath = params.path
    this.userToken = params.token || null
    this.onComplete = params.onComplete
    this.onError = params.onError
    this.retryCount = 0
    this.status = STATUS_STARTING
    this.lastError = null
}

/**
 * Start a download by and add it to the global queue
 */
Downloader.prototype.start = function() {
    //return if already in downloading state
    if (this.status == STATUS_DOWNLOADING || this.status == STATUS_COMPLETE || this.status == STATUS_ERROR) {
        return cb(new Error('Already downloading'))
    }
    var self = this
    self.download()
}

/**
 * Download a package from the repository and extract its contents in to the specified path
 */
Downloader.prototype.download = function() {
    var self = this
    var packageId = this.packageId
    var version = this.version
    var packageRoot = path.join(this.basePath, PACKAGES_DIR)
    var packagePath = path.join(packageRoot, packageId)
    fse.ensureDir(packageRoot, function(err) {
        if (err) {
            return self.onError(new Error('Invalid project path'))
        }
        fse.ensureDir(packagePath, function(err) {
            if (err) {
                return self.onError(new Error('Unable to create packages path'))
            }
            //get a random filename for the package tarball
            var tarballName = 'tmp_'+ new Date().getTime() +'.tar.gz'
            var filePath = path.join(packagePath, tarballName)
            //set the package item parameters before starting the download
            self.lastError = null
            self.retryCount += 1
            self.status = STATUS_DOWNLOADING

            api.request('package/download', {
                auth_token: self.userToken,
                id: packageId,
                version: version
            }, {
                file: filePath,
                mode: 'download',
                method: 'POST',
                timeout: 60000,
                success: function(response) {
                    self.status = STATUS_DOWNLOADED
                    if ((!response || !response.output) && !self.canRetryDownload()) {
                        self.status = STATUS_ERROR
                        var errorMsg = response.message
                        if (response.error &&  typeof response.error === 'object') {
                            errorMsg = response.error.message
                        }
                        fse.rmdir(packagePath)
                        return self.onError(new Error(errorMsg))
                    }
                    return self.afterDownload(packagePath, filePath, function(err) {
                        //check if there was an error in processing the file and retry if necessary
                        if (err) {
                            self.lastError = err
                            if (self.canRetryDownload()) {
                                return self.download()
                            } else {
                                return self.onError(err)
                            }
                        }
                        return self.onComplete(packagePath)
                    })
                },
                error: function(error) { //error in downloading
                    console.error(error)
                    self.lastError = error
                    self.status = STATUS_ERROR
                    if (!self.canRetryDownload()) {
                        fse.rmdir(packagePath)
                        return self.onError(new Error('the package server is not reachable'))
                    }
                },
                progressCb: function(stats) {
                    //console.log("Download progress", stats)
                }
            })
        })
    })
}

/**
 * Check if the download process can be tried again and initiate a download process
 */
Downloader.prototype.canRetryDownload = function() {
    if (this.status != STATUS_DOWNLOADED || this.status != STATUS_ERROR) {
        return false
    }
    if (this.retryCount >= MAX_DOWNLOAD_RETRY) {
        return false
    }
    return true
}

/**
 * Process the file after download
 */
Downloader.prototype.afterDownload = function(packagePath, filePath, cb) {
    fsUtil.uncompress(filePath, packagePath, true, function(err){
        //delete the temporary file, its currently removed while decompressing
        /*if (fse.existsSync(filePath)) {
            fse.unlink(filePath)
        }*/
        return cb(err)
    })
}

module.exports = Downloader