/**
 * Author Alias Eldhose (aliaseldhose@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var fs = require('fs')
var fse = require('fs-extra')
var path = require('path')

var async = require('async')
var _ = require('underscore')
//var requireAnalyzer = require('require-analyzer')
//var semver = require('semver')
//var detective = require('detective')

var Package = require('../package')
var manager = require('./npmManager')
var fsUtil = require('../fsUtil')

var analyzer = new function() {
    var _dependencyCache = {}
    /*########## Analyse the NPM dependences in a file level ##############*/
    /**
     * Process NPM dependencies for a given path
     *
     * @param path
     * @param cb
     * @private
     */
    var _processPathForDependency = function(folderPath,cb) {
        fsUtil.listFileTree(folderPath, function(err, files, tree) {
            if (err) {
                return cb(err)
            }
            var dependencies = {}
            async.each(files, function(file, next) {
                //get the dependencies for the file
                // The dependencies once identified will be merged (without matching the version since all the files
                // of the current operation belongs to the same root directory
                _processFileForNodeDependecies(file, function(err, nodeModules) {
                    if (nodeModules) {
                        _.each(nodeModules, function (version, module) {
                            dependencies[module] = version
                        })
                    }
                    next(err)
                })

            }, function(err) {
                _getMissingNpmModules(dependencies, projectPath, cb)
            })
        }, {list_option:'show'})
    }

    /**
     * Parse a step or flow file and get its dependencies
     * 
     * @param filePath
     * @param basePath
     * @params callback
     */
    var _processFileForDependency = function(filePath, cb) {     
        var basePath = path.dirname(filePath)        
        if (_dependencyCache[filePath]) {
            //return cb(false, _dependencyCache[filePath])
        }
        var dependency = {sub_flows: [], modules: {}, node: {}}
        var ext = path.extname(filePath)
        if (ext == '.flw') {
            _processFlowForDependency(filePath, basePath, function(err, dependency) {
                if(dependency) {
                    _dependencyCache[filePath] = dependency
                }
                return cb(null, dependency)
            })
        } else if (ext == '.act' || ext == '.cm') {
            var fileData = fse.readJsonSync(filePath, {
                throws: false
            })
            if (!fileData) {
                return cb('invalid file' + filePath)
            }
            _processJsFileForNpmDependecies(path.join(path.dirname(filePath), fileData.source), function (err, nodeDependency) {
                if (nodeDependency) {
                    _.extend(dependency.node, nodeDependency)
                    _dependencyCache[filePath] = dependency
                }
                return cb(null, dependency)
            })
        } else if(ext == '.js') {
            _processJsFileForNpmDependecies(filePath, function(err, nodeDependency) {
                if (nodeDependency) {
                    _.extend(dependency.node, nodeDependency)
                    _dependencyCache[filePath] = dependency
                }
                return cb(null, dependency)
            })
        }
    }

    /**
     * Process a javascript file and find the dependencies
     *
     * @param file
     * @param cb
     * @private
     */
    var _processJsFileForNpmDependecies = function(file, cb) {
        var dependency = {}
        var dAnalyzer = requireAnalyzer.file({
            target: file, 
            reduce: true
        }, function (err, nodeModules) {
            if (err && !nodeModules) {
                return cb('Error processing npm dependency for ' + path.basename(file))
            }
            _findRequiredNodeModulesVersion(nodeModules, file, cb)
        })
        ///dAnalyzer.on('dependency', function(deps) {
        //    console.log(deps);
        //})

        //dAnalyzer.on('reduce', function(deps) {
        //    console.log(deps)
        //})
    }

    /**
     * Process a flow file to find the dependecies, (node, packages and sub flows)
     * The method will even go through the inline scripts also to identify the node dependencies
     *
     * @param filepath
     * @param basePath
     * @param cb
     * @returns {*}
     * @private
     */
    var _processFlowForDependency = function(filePath, basePath, cb) {
        if (typeof basePath == 'function') {
            cb = basePath
            basePath = path.dirname(filePath)
        }
        var fileData = fse.readJsonSync(filePath, {throws: false})
        if (!fileData) {
            return cb('invalid file' + filePath)
        }
        var dependency = {
            sub_flows: [], 
            modules: {}, 
            node: {}
        }
        async.each(fileData['steps'], function (step, next) {
            if (['core/main/start', 'core/main/end'].indexOf(step.ref) != -1) {
                next()
                return
            }
            dependency.modules[step.ref] = '*'
            //process subflows
            if (step['input-binding'] && step['input-binding'].flowfile) {
                //file name will be  enclosed in escaped double quotes
                var subFlow = path.join(basePath, eval(step['input-binding'].flowfile))
                _.union(dependency.sub_flows, [subFlow])
                _processFlowForDependency(subFlow, basePath, function (err, subflowDependency) {
                    if (!err) {
                        _.extend(dependency.modules, subflowDependency.modules)
                    }
                    next()
                })
            }
            //process inline script step
            else if (step['input-binding'] && step['input-binding'].script && step['input-binding'].script.length > 6) {
                //analyze only if its a valid javascript with requireed module
                var script = step['input-binding'].script.slice(1,-1)
                _processScriptForRequiredNodeModule(filePath, script, function(err, nodeDeps) {
                    if(nodeDeps) {
                        _.extend(dependency.node, nodeDeps)
                    }
                    next()
                })
            } else {
                next()
            }
        }, function (err) {
            cb(err, dependency)
        })
    }

    /**
     * Process a sscript in string format for node modules and analyze file tree to get the exact module and version
     * First script will be analyzed to find the requires, and then each of the node module will be processed for version info
     *
     * @param filePath
     * @param cb
     * @private
     */
    var _processScriptForRequiredNodeModule = function(filePath, script, cb) {
        var _reqAnalyzerOptions = {
            reduce: true, 
            target: filePath, 
            errors: []
        }
        try {
            //get the require module names
            var requireModules = detective.find(script)
        } catch(e) {
            console.log(e)
        }
        if (!requireModules || !requireModules.strings) {
            return cb()
        }
        _findRequiredNodeModulesVersion(requireModules.strings, filePath, cb)
    }

    /**
     * find the instaled version and the version that is requested (in package.json) if requested version not found
     * installed version will be taken as the module version required
     * @param module
     * @param filePath
     * @param cb
     * @returns {*}
     * @private
     */
    var _findRequiredNodeModulesVersion = function(modules, filePath, cb) {
        var _modules = {}
        async.each(modules, function(module, next) {
            if (requireAnalyzer.isNative(module)) {
                return next()
            }
            //resolve the absolute path of the module in the current folder on folder in the upper hierarchy
            var _moduleAbsolutePath = requireAnalyzer.resolve(module, filePath)
            if (!_moduleAbsolutePath) {
                _modules[module] = '*'
                return next()
            } else {
                //find the module directory inside the node_modules folder ex: [basePath]/node_modules/mysql
                requireAnalyzer.findModulesDir(path.dirname(_moduleAbsolutePath), function(err, moduleDir) {
                    if (!moduleDir) {
                        return next()
                    }
                    //get the base path where the project is running
                    var basePath = path.dirname(path.dirname(moduleDir))
                    //reach the project package.json to see the dependencies configures and then the version specified
                    if (fs.existsSync(path.join(basePath, 'package.json'))) {
                        var basePkgJson = fse.readJsonSync(path.join(basePath, 'package.json'), {throws: false})
                        if (basePkgJson && basePkgJson.dependencies && basePkgJson.dependencies[module]) {
                            _modules[module] = basePkgJson.dependencies[module]
                        }
                    }
                    //else get the installed module version
                    if (!_modules[module] && fs.existsSync(path.join(moduleDir, 'package.json'))) {
                        var modulePkgJson = fse.readJsonSync(path.join(moduleDir, 'package.json'), {throws: false})
                        if (modulePkgJson && modulePkgJson.version) {
                            _modules[module] = modulePkgJson.version
                        }
                    }
                    _modules[module] = _modules[module] || '*'
                    return next()
                })
            }
        }, function(err) {
            cb(err, _modules)
        })
    }

    /*########## Analyse the NPM dependences in a file level ##############*/
    /**
     * Get the list of missing packages
     *
     * @param dependencies array of node modules
     * @param paths string| path names where to look for the installed modules
     * @param cb
     * @returns {*}
     * @private
     */
    var _findMissingNodeModules = function(deps, checkPath, cb) {
        if (!deps) {
            return cb(null)
        }
        /*var treePath = checkPath.split(path.sep)
        var paths = treePath.map(function(item, idx) {
            if(idx > 0) treePath.pop()
            return treePath.reduce(function(prev, curr) {
                    return path.resolve(path.sep, prev, curr)
                })
        })*/
        var paths = [checkPath]
        var missing = Object.keys(deps)
        var found = {}
        async.each(paths, function(folderPath, next){
            if (missing.length < 1) {
                next()
            } else {
                //check the symbolic link and resolve to the original
                var stats = fs.lstatSync(folderPath)
                if (stats.isSymbolicLink()) {
                    folderPath = fs.realpathSync(folderPath)
                }
                manager.npmList(folderPath, function(err, pkgJson) {
                    if (err) {
                        next(err)
                    } else {
                        var installedModules = pkgJson && pkgJson.dependencies ? pkgJson.dependencies : {}
                        for (var module in deps) {
                            if (typeof installedModules[module] != 'object' || installedModules[module].missing) {
                                continue
                            }
                            if (found[module]) {
                                continue
                            }
                            if (installedModules[module].version == deps[module] || deps[module] == '*') {
                                found[module] = installedModules[module].version
                                missing = _.without(missing, module)
                                continue
                            }
                            //check if the version is satifsfied with the existing one
                            if (semver.satisfies(installedModules[module].version, deps[module])) {
                                missing = _.without(missing, module)
                            }
                        }
                        next()
                    }
                })
            }
        }, function(err) {
            var missingDeps = {}
            _.each(missing, function(dep) {
                missingDeps[dep] = deps[dep]
            })
            cb(err, missingDeps)
        })
    }

    /**
     * Process the package package dependencies
     * 
     * @param dependencies
     * @param basePath
     * @param cb
     * @private
     */
    var _processPackageDependencies = function(dependencies, basePath, cb) {
        var missing = []
        //load the package list to make sure that the cache is there
        Package.list(basePath, function() {
            _.each(dependencies, function(version, dependencyRef) {
                if (!Package.getModuleDefinition(dependencyRef)) {
                    missing.push(dependencyRef)
                }
            })
            return cb(null, missing)
        })
    }

    /**
     * Get the package JSON stack for the package and the project path
     * NOT USED - CAN BE REMOVED
     *
     * @param packagePath
     * @param projectPath
     * @returns {{}}
     * @private
     */
    var _getPackageJsonStack = function(packagePath, projectPath) {
        //set the package json file for the package so that it will be added to the stack when parsing for directory
        var stackPath = path.join(packagePath, 'package.json')
        var packageJsonStack = []
        var foundRoot = false
        while (!foundRoot) {
            stackPath = path.dirname(stackPath)
            if (fs.existsSync(path.join(stackPath, 'package.json'))) {
                packageJsonStack.unshift(path.join(stackPath, 'package.json'))
            }
            if (!stackPath || stackPath == projectPath) {
                foundRoot = true
                break
            }
        }
        //get the package json config for each of the list stack
        var packageJson = {}
        for (var i = 0; i < packageJsonStack.length; i++) {
            var pkgJsonFile = packageJsonStack[i]
            packageJson[pkgJsonFile] = JSON.parse(fs.readFileSync(pkgJsonFile))
        }
        return packageJson
    }

    return {
        /*processPathForDependency: _processPathForDependency,
        processFileForDependency: _processFileForDependency,
        findMissingNodeModules: _findMissingNodeModules,*/
        processPackageDependencies: _processPackageDependencies
    }
}
module.exports = analyzer