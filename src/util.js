/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 *
 * Copyright 2024 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'


const fs = require('fs')
const path = require('path')
const Validator = require("./validator")

const which = require('which')
const process = require('node:process')
const OS = require('node:os')
const { spawn } = require('node:child_process')
const { pathToFileURL } = require('node:url')
const logger = require('./logger')

exports.validateJson = function(schema, data) {
    if (typeof schema == 'undefined' || schema == null) {
        //TODO change this to throw error - as there shouldnt be this case
        return {
            errors: []
        }
    }
    return Validator.validate(data, schema)
}

exports.deepClone = function(object) {
    if (typeof object === 'undefined') {
        return object
    }
    return JSON.parse(JSON.stringify(object))
}

/**
 * Shallow extend an object with properties from another
 */
exports.extendObject = function(destination, object) {
    var keys = Object.keys(object)
    for (var i = 0; i < keys.length; i++) {
        destination[keys[i]] = object[keys[i]]
    }
    return destination
}


/*
* Shallow clone an object. will override
*/
exports.shallowClone = function(source) {
    var keys = Object.keys(source)
    var destination = {}
    for (var i = 0; i < keys.length; i++) {
        destination[keys[i]] = source[keys[i]]
    }
    return destination
}

var localIdx = 0

exports.getUniqueId = function(prefix) {
    return localIdx++
}

exports.printKeys = function(obj) {
    for (var key in obj) {
        console.log(key)
    }
}

exports.mergeObjects = function(into, from) {
    function MergeRecursive(obj1, obj2) {
        var keys = Object.keys(obj2)
        for (var k = 0; k < keys.length; k++) {
            var p = keys[k]
            try {
                // Property in destination object set; update its value.
                if (obj2[p].constructor == Object) {
                    obj1[p] = MergeRecursive(obj1[p], obj2[p])
                } else {
                    obj1[p] = obj2[p]
                }
            } catch (e) {
                // Property in destination object not set; create it and set its value.
                obj1[p] = obj2[p]
            }
        }
        return obj1
    }
    return MergeRecursive(into, from)
}

function isStream(stream) {
    return stream !== null &&
        typeof stream === 'object' &&
        typeof stream.pipe === 'function';
}

/**
 * Extracted from expression builder(util.mergeIfComptible)
 * Extend a schema with another schema.
 */
function extendSchema(dest, source) {
    if (!dest || !dest.type) {
        return source
    }
    if (dest.type == 'object') {
        if (source.type == 'object' && source.properties) {
            dest.properties = dest.properties || {}
            for (var p in source.properties) {
                if (source.properties.hasOwnProperty(p)) {
                    if (dest.properties[p] && dest.properties[p].format !== 'noEval') {
                        dest.properties[p] = extendSchema(dest.properties[p], source.properties[p])
                    }
                }
            }
        }
        else {
            return source //@TODO experimentally added on 3 Nov/2015 by murukesh
        }
    } else if (dest.type == 'array') {
        if (source.type == 'array') {
            if (!dest.items || dest.items.length == 0) {
                if (source.items) {
                    dest.items = source.items
                }
            } else if (source.items && source.items.length > 0) {
                var destItem = dest.items[0]
                var sourceItem = source.items[0]
                destItem = extendSchema(destItem, sourceItem)
            }
        } else {
            return source //@TODO experimentally added on 3 Nov/2015 by murukesh
        }
    } else if (dest.type == 'any') {
        if (source.type != 'null') {
            return source
        }
    }
    return dest
}

exports.extendSchema = extendSchema


//http://stackoverflow.com/a/1203361/574716
exports.getFileExt = function(filename) {
    var a = filename.split(".")
    if (a.length == 1 || (a[0] = "" && a.length == 2)) {
        return ""
    }
    return a.pop()
}

/**
 * Returns the object structure limiting to nesting level passed
 *
 * @param {object} data The Object to be pruned
 * @param {number} [maxLevel] The depth of nesting levels to traverse
 * @param {number} [maxItems] The maximum # items allowed in array
 * @param {number} [maxStringLen] Maximum length of string to be allowed
 * @return  {object} The Object pruned and serialized to the given nesting levels
 */
exports.serialize = function(data, maxLevel, maxItems, maxStringLen) {
    var filterSystemObjects = function(key, level) {
        if (level == 0 && key == 'GLOBAL') {
            return false
        }
        return true
    }
    maxLevel = maxLevel || 10
    maxItems = maxItems || 50
    maxStringLen = maxStringLen || 1000
    var serializeObject = function(obj, level) {
        var output
        var tmp
        if (level <= maxLevel) {
            if (typeof obj == 'object') {
                //array
                if (obj == null) {
                    output = null
                } else if (Array.isArray(obj)) {
                    output = []
                    for (var i = 0; i < obj.length; i++) {
                        if (i < maxItems) {
                            tmp = serializeObject(obj[i], level + 1)
                            //if (typeof ret != 'undefined') {
                            output.push(tmp)
                            //}
                        } else {
                            tmp = '[Array too long. Showing only first ' + maxItems + '. Total ' + obj.length + ' items]'
                            output.push(tmp)
                            break
                        }
                    }
                } else if (obj && obj.constructor && obj.constructor.name == 'Object') {
                    output = {}
                    i = 0
                    var keys = Object.keys(obj)
                    var len = keys.length
                    for (var k = 0; k < len; k++) {
                        if (++i < maxItems) {
                            var key = keys[k]
                            if (filterSystemObjects(key, level)) {
                                tmp = serializeObject(obj[key], level + 1)
                                //if (typeof ret != 'undefined') {
                                output[key] = tmp
                                //}
                            }
                        } else {
                            output[key] = '[Object too big. Showing only first ' + maxItems + '. Total ' + len + ' items]'
                            break
                        }
                    }
                } else {
                    if (Buffer.isBuffer(obj)) {
                        output = '[object.buffer]'
                    } else if (isStream(obj)) {
                        output = '[object.stream]'
                    } else {
                        if (obj instanceof Date) {
                            output = obj.toISOString()
                        } else {
                            output = '[object]'
                        }
                    }
                }
            } else if (typeof obj == 'string') {
                if (obj.length > maxStringLen) {
                    output = obj.slice(0, maxStringLen) + '... [Big string, trimmed at ' + maxStringLen + ' chars]'
                } else {
                    output = obj
                }
            } else if (typeof obj == 'function') {
                output = '[function]'
            }
            else {
                output = obj
            }
        } else {
            output = '[Properties hidden. Nesting too deep]'
        }
        return output

    }
    return serializeObject(data, 0)
}

exports.prettifyHrtime = function(time) {
    return (time[0] * 1000) + time[1] / (1000 * 1000)
}

/**
 * Iterate over an array or object(using keys)
 *
 * @param collection
 * @param fn
 * @param cb
 */
exports.iterate = function(collection, fn, cb) {
    if (!collection || typeof collection != 'object') {
        return cb(new Error('Util.iterate: Invalid inputs, collection should be an object'))
    }
    var keys = Object.keys(collection)
    if (keys.length == 0) {
        return cb()
    }
    function _iterate(indx) {
        if (!(indx in keys)) {
            return cb()
        }
        fn(collection[keys[indx]], function(err, isDone) {
            if (err) {
                return cb(err)
            }
            if (isDone) {
                return cb()
            }
            _iterate(indx + 1)
        }, keys[indx])
    }
    _iterate(0)
}

/**
 * Iterate parellely over an array or object(using keys)
 *
 * @param collection
 * @param fn
 * @param cb
 */
exports.iterateAsync = function(collection, fn, cb) {
    if (!collection || typeof collection != 'object') {
        return cb(new Error('Util.iterateAsync: Invalid inputs, collection should be an object'))
    }
    //if the collection is an object generate the keys array
    var keys = Object.keys(collection)
    if (keys.length == 0) {
        return cb()
    }

    var count = 0
    var error = false
    for (var i = 0; i < keys.length; i++) {
        fn(collection[keys[i]], function(err) {
            if (err) {
                error = err
                return cb(err)
            }
            count++
            if (count == keys.length && !error) {
                return cb()
            }
        }, keys[i])
    }
}

/**
 * Trim characters from left
 *
 * @param str
 * @param chars
 */
exports.ltrim = function(str, chars) {
    chars = chars || ' '
    for (var i = 0; i < chars.length; i++) {
        str = str.indexOf(chars[i]) == 0 ? str.slice(1, str.length) : str
    }
    return str
}
/**
 * Trim characters from right
 *
 * @param str
 * @param chars
 */
exports.rtrim = function(str, chars) {
    chars = chars || ' '
    for (var i = 0; i < chars.length; i++) {
        str = str.lastIndexOf(chars[i]) == str.length - 1 ? str.slice(0, str.length - 1) : str
    }
    return str
}

exports.uuid = function(prefix) {
    return (prefix || '') + (new Date().getTime()) + "_" + Math.round((Math.random() * 1000))
}

const delay = ms => {
    return new Promise(resolve => setTimeout(resolve, ms))
}
/**
 * Get the default schell in the system
 * @returns
 */
const getDefaultShell = () => {
	const {env, platform} = process

	if (platform === 'win32') {
		return env.COMSPEC || 'cmd.exe'
	}

	try {
		const {shell} = OS.userInfo()
		if (shell) {
			return shell
		}
	} catch {}

	if (platform === 'darwin') {
		return env.SHELL || '/bin/zsh'
	}

	return env.SHELL || '/bin/sh'
}

/**
 * Get the PATH variable from the environement
 * @returns
 */
const getEnvPath = async ()=> {
	const {env, platform} = process
    if (platform === 'win32') {
		return env.PATH
	}

	try {
		let _$path = await this.spawnAsync(getDefaultShell(), ['-ilc', 'echo $PATH'], {env: {DISABLE_AUTO_UPDATE: 'true'}})
		//strip ANSI
        const pattern = [
            '[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]+)*|[a-zA-Z\\d]+(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)',
            '(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-nq-uy=><~]))'
        ].join('|')
        _$path = _$path.toString().replace(new RegExp(pattern, 'g'), '')
        return _$path.split('\n').filter(line => Boolean(line)).join(':')
	} catch (error) {
        return env
	}
}
/**
 * Fix path for electron builds
 * @returns
 */
const fixPath = async ()=>{

    let $path = await getEnvPath()
    return [
        process.env.PATH = $path || [
            './node_modules/.bin',
            '/.nodebrew/current/bin',
            '/usr/local/bin',
            process.env.PATH,
        ].join(':')
    ]
}
/**
 * Get a path to an executable
 * @returns
 */
exports.which = async (prgm, round=0)=>{
    let file, version
    try {
        file = await which(prgm, { nothrow: true})
        if(!file) {
            if(round > 8) {
                throw new Error('Unable to identify the executable '+ prgm)
            }
            //fix the PATH variable in env and try to locate the program again
            fixPath()
            await delay(300)
            return this.which(prgm, ++round)
        } else {
            version = await this.spawnAsync(file, ["-v"], { cwd: __dirname, detached: false})
            return {path: file, version: version && version.toString().replace(/[v|\n]/g, '')}
        }
    } catch(e) {
        return {path: file || null}
    }
}

/**
 * Run a command in child process and return response or error
 * Line by line error or data will logged onto the console
 * @param {*} cmd
 * @param {*} args
 * @param {*} opts
 * @returns
 */
exports.spawnAsync = (cmd, args, opts)=>{
    return new Promise((resolve, reject)=> {
        let options = {
            maxBuffer: 1000 * 1000 * 100,
            buffer: true,
            stripFinalNewline: true,
            extendEnv: true,
            preferLocal: false,
            localDir: opts.cwd || process.cwd(),
            execPath: process.execPath,
            encoding: 'utf8',
            reject: true,
            cleanup: true,
            all: false,
            windowsHide: true,
            detached: true,
            shell: true,
            windowsVerbatimArguments: true,
            ...opts
        }

        let child
        try {
            child = spawn('"'+cmd+'"', args, options)
        } catch (error) {
            logger.error(error)
            throw error
        }
        let resp = ''
        let error = ''
        child.stderr.on('data', function(buffer) {
            let data = buffer.toString()
            error += data
            resp += data
            logger.error(data)
        })
        child.stdout.on('data', function (buffer) {
            let data = buffer.toString()
            resp += data
            //logger.debug(data)
        })
        child.on('exit', function(code) {
            if (code !== 0) {
                reject(new Error("Error occurred while executing command " + cmd))
            } else {
                resolve(resp)
            }
        })
    })
}

/**
 * Load a module's source file at run time, this will try to laod the file using require first and if it fails with an error
 * then a dynamic import will be used
 * @param {*} file
 * @returns {Object}
 */
exports.loadModuleSource = async(file)=>{
    try {
        let code = require(file)
        return code
    } catch(e) {
        if(e.code === 'ERR_REQUIRE_ESM') {
            try {
                file = pathToFileURL(file)
                let code = await import(file)
                return code && code.default
            } catch(err) {
                throw err
            }
        }
        throw e
    }
}