/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Giju Eldhose (giju@ceegees.in)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var _ = require('underscore')
var Ice = require('icepick')
var Loader = require('./loader')
var Util = require('./util')
var Flow = require('./flow')
var Path = require('path')
var Eval = require('./eval')
var Status = require('./status')
var Logger = require('./logger')
var _Error = require('./error')
var Constants = require('./constants')

var gId = 0 //global instance id

var trampolineFn = process.nextTick
/**
 * @Class Instance - An instance of a flow
 */
function Instance(engine, flow, params, parent, parentStep) {
    this.id = parent ? (parent.id + '.' + parent.childCount)
        : ("" + gId++)
    this.childCount = 0
    this.seqId = parent ? parent.childCount++ : 0
    // @TODO pass it from command line
    this.engine = engine
    this.flow = flow
    this.params = params

    if (parent) {
        this.vars = parent.vars
        this.rootId = parent.rootId
    } else {
        this.vars = {}
        this.rootId = this.id
    }

    if (this.engine.isDebugMode) {
        trampolineFn = setImmediate
        this.isDebugMode = true
        this.subInstances = {}
        if (parent) {
            this.parent = parent
            this.parentStep = parentStep
            var parentActMap = parent.stepMap ? parent.stepMap[parentStep] : {}
            if (parentActMap.mode == Status.DEBUG_MODE.STEP) {
                this.mode = Status.DEBUG_MODE.STEP
            }
        }
    }
    this.stepMap = {}
    //prepare stack/or stack brothers, as both a hashmap and array both pointing
    //to same objects are used. array is passed for evaluating expressions,
    //while hashmap is used for quickly accessing entries.
    var tmp = flow.getStackArray && flow.getStackArray() || {}
    this.stack = params.stack || tmp.stack
    this.stackArray = tmp.stackArray

    this.transitionError = {}
    this.transitionState = {}

    this.incomingSuccess = {}
    this.incomingFailure = {}
    this.incomingError = {}

    //to check if step is already called
    this.invokeCount = {}
    //for debugging
    this.invokeQueue = {}

    this.isTriggerChild = null

    this.status = Status.INSTANCE_STATUS.READY
    this.errors = new Array()

    this.stepsCompleted = null
}

/* used for setting error */
var evalError = {}

/**
 * Starts execution for a flow with a set of parameters
 */
Instance.prototype.execute = function() {
    var start = this.flow.startStep
    this.status = Status.INSTANCE_STATUS.RUNNING
    var list = new Array(1)
    list[0] = start.id
    this.startExecution(list)
}

/**
 * Add a sub instance
 */
Instance.prototype.addSubInstance = function(instance) {
    this.subInstances[instance.id] = instance
}

/**
 * Remove a sub instance
 */
Instance.prototype.removeSubInstance = function(instance) {
    delete this.subInstances[instance.id]
}

/**
 * Get the state(debug) of a step
 */
Instance.prototype.getStepState = function(id) {
    return this.stepMap[id]
}

/**
 * Get the definition of a step
 */
Instance.prototype.getStep = function(id) {
    return this.flow.steps[id]
}

/**
 * Return which step is to be invoked next.
 */
Instance.prototype.peekQueued = function() {
    var keys = Object.keys(this.stepMap)
    for (var i = 0; i < keys.length; i++) {
        var step = this.stepMap[keys[i]]
        if (step.status == Status.STEP_STATUS.PAUSED_BEFORE) {
            return step
        }
    }
    return null
}

/**
 * Return the full step status
 */
Instance.prototype.getStepMap = function() {
    return this.stepMap
}

/**
 * Update step status
 */
Instance.prototype.updateStepStatus = function(stepId, status, dontNotify) {
    if (this.isDebugMode) {
        var invoker = this.stepMap[stepId]
        if (invoker) {
            invoker.status = status
            if (!dontNotify) {
                this.engine.sendUpdate()
            }
        } else {
            Logger.log("[updateStepStatus] Step is not found in map: ", this.stepMap, stepId)
        }
    }
}

/**
 * Mark a step as erred out
 */
Instance.prototype.setStepError = function(step, error) {
    var id = step.id
    var errorWrap
    // console.log("stack", this.getStackTrace(step))
    if (error instanceof _Error) {
        errorWrap = new _Error(error.message, error.code, error.data, [step.path].concat(error.path), null, error.originalError)
    } else {
        if (error instanceof Error) {
            errorWrap = new _Error(error.message, error.code, error.data, [step.path], null, error.stack)
        } else if (typeof error == 'string') {
            errorWrap = new _Error(error, null, null, [step.path])
        } else {
            console.error("Invalid error type specified by step " + step.path, error)
            errorWrap = new _Error("Invalid error type.", "INVALID_ERROR", error, [step.path], null)
        }
    }
    //this.stack[id] = this.stack[id] || {}
    this.stack[id].error = errorWrap

    if (this.isDebugMode) {
        var invoker = this.stepMap[id]
        if (invoker) {
            invoker.status = Status.STEP_STATUS.ERROR
            invoker.error = errorWrap
            this.isDebugMode && this.engine.sendUpdate()
        } else {
            Logger.log("[setStepError] Invoker not found in map: ", id)
        }
    }
}

/**
 * @TODO Experimental method to get stack trace.
 */
Instance.prototype.getStackTrace = function(currentStep) {
    var instance = this
    var stackTrace = []
    stackTrace.push({
        // flow: this,
        step: currentStep.path
    })
    while (instance) {
        instance = instance.parent
        if (instance) {
            stackTrace.push({
                // flow: instance,
                step: instance.parentStep.path
            })
        }
    }
    return stackTrace
}

/**
 * Formats error output of a step
 *
 * @TODO - this requires a major revamp.
 */
Instance.prototype.formatErrorReport = function(type, report) {
    var list = []
    for (var i = 0; i < report.errors.length; i++) {
        var property = report.errors[i].property
        var message = report.errors[i].message
        list.push(type + " property <b>" + property + "</b> - " + message)
    }
    return list.join("\n")
}

/**
 * Starts Execution of a list of steps on the list provided
 */
Instance.prototype.startExecution = function(stepList, isPaused) {
    for (var i = 0; i < stepList.length; i++) {
        var id = stepList[i]
        this.invokeQueue[id] = true
        if (this.isDebugMode) {
            var stepInfo = this.initStepStatus(id)
            if (isPaused) {
                stepInfo.status = Status.STEP_STATUS.PAUSED_BEFORE
            }
            this.processNext(stepInfo)
        } else {
            this.executeStep(id)
        }
    }
    if (this.isDebugMode) {
        this.processQueued()
    }
}

/**
 * Chose to invoke the step immediately or delay it
 */
Instance.prototype.processNext = function(stepInfo) {
    if (this.isDebugMode) {
        this.stepMap[stepInfo.id] = stepInfo
    } else {
        stepInfo.execute()
    }
}

/**
 * Iterate from the list of queued steps and invoke them(or putback)
 */
Instance.prototype.processQueued = function() {
    var stepIds = Object.keys(this.stepMap)
    //since it's expected to be called only during debug mode, it's ok to iterate
    //the entire list
    for (var k = 0; k < stepIds.length; k++) {
        var id = stepIds[k]
        var stepInfo = this.stepMap[id]
        var status = stepInfo.status
        //stepMap includes completed steps as well.
        //this could be revamped to use a new variable just to hold
        //pending stuff alone.
        if (status == Status.STEP_STATUS.READY ||
            status == Status.STEP_STATUS.READY_OVERRIDE) {
            //this is where to check debug mode, breakpoint, step in, step out etc
            var step = this.flow.steps[id]
            var hasBp = this.getStepBpStatus(step)
            if (status != Status.STEP_STATUS.READY_OVERRIDE &&
                (hasBp || this.mode == Status.DEBUG_MODE.STEP)) {
                this.updateStepStatus(id, Status.STEP_STATUS.PAUSED_BEFORE)
                this.engine.sendSetFocus(this, step)
                if (hasBp) {
                    this.engine.sendBpNotify(this, step)
                }
            } else {
                stepInfo.execute()
            }
        }
    }
}

/**
 * Check if the step has breakpoint enabled.
 */
Instance.prototype.getStepBpStatus = function(step) {
    let stepHasBp = step.meta && step.meta.breakPointBefore
    const file = this.params.scope ? this.params.origFile : this.flow.fileName
    const stepId = this.params.scope ? this.params.scope + Constants.GROUP_ID_SEPARATOR + step.id : step.id
    const status = this.engine.getBreakpointOverride(file, stepId)
    if (status !== null) {
        stepHasBp = status
    }
    return stepHasBp
}

/**
 * Create a function with execute method that wraps engine.execute.
 * Used to pass to steps so that if any steps uses the engine, the
 * parent reference is available.
 */

//For performance, setImmediate/process.nextTick wont be called for every invocation.
//instead a counter is used to allow maximum iteration before calling trampoline
//function.

var _tick = 0
var MAX_TICK = 10

Instance.prototype.getEngineWrapped = function(stepId) {
    var self = this
    return {
        loadFlow: Loader.loadFlow,
        loadResource: Loader.loadResource,
        listen: function(event, fn) {
            self.engine.listen(event, fn)
        },
        emit: function(event, data) {
            self.engine.emit(event, data)
        },
        makeError: function(message, code, data, parent) {
            return new _Error(message, code, data, parent)
        },
        //wrapper to engine.execute
        execute: function(params, callback) {
            //pass the instance and step to the engine
            if (params.file == self.params.file && ++_tick == MAX_TICK) {
                _tick = 0
                trampolineFn(function() {
                    self.engine.execute(params, self, stepId, callback)
                })
            } else {
                self.engine.execute(params, self, stepId, callback)
            }
        },
        executeStep: function(params, callback) {
            // params.file can come empty when running the project
            params.file = params.file || self.params.file || ''
            self.engine.executeStep(params, self, stepId, callback)
        }
    }
}

/**
 * Handles the trigger steps, Takes care of creating a process
 * invocation and starting the steps in the flow
 *
 * @param  {object} step  The Step Object
 * @param  {object} input     input passed to the step to be set on stack
 * @param  {object} output    output of the step to be set on stack
 * @param  {object} vars The hidden/system variables to be pre-populated in the stack
 *
 */
Instance.prototype.handleTriggerStep = function(step, input, output, error, vars) {
    var self = this
    var stepId = step.id

    //Load the flow again (hopefully is cached) to create a clone of the current instance
    Loader.loadFlow(this.params, function(err, flow) {
        //Now clone the current instance for each new trigger instance.
        var instance = new Instance(self.engine, flow, self.params, self, stepId)

        if (self.isDebugMode) {
            self.engine.addInstance(instance)
        }

        // new execution trigger from a starter step
        //- create new flow
        var tmp = flow.getStackArray()
        var stack = tmp.stack
        stack[stepId] = {
            input: input
        }

        instance.vars = vars || {} //vars are instance specific
        instance.stack = stack
        instance.stackArray = tmp.stackArray
        instance.isTriggerChild = true

        var start = flow.startStep

        //Already trigger(start) is executed, so fake a start step
        //for breakpoints and all
        var invoker = {
            id: stepId,
            status: Status.STEP_STATUS.READY,
            execute: function() {
                var self = this
                if (error) {
                    self.setStepError(step, error)
                } else {
                    stack[stepId].output = output
                    self.isDebugMode && self.updateStepStatus(stepId, Status.STEP_STATUS.CALLBACK, false)
                }
                //@TODO - Implement error handling for trigger + how to push a trigger from custom flow?
                var next = self.getNext(start)
                self.startExecution(next.steps)
                self.isDebugMode && self.engine.sendUpdate()
            }.bind(instance)
        }
        instance.processNext(invoker)
        if (instance.isDebugMode) {
            instance.processQueued()
        }
    })
}

/**
 * Manages closure + Stack for a single step invocation
 */
Instance.prototype.initStepStatus = function(id) {
    var self = this
    return {
        id: id,
        status: Status.STEP_STATUS.READY,
        execute: function() {
            self.executeStep(id)
        }
    }
}

/**
* Shallow clone stack. Perhaps it should live in util.
*/
Instance.prototype.cloneStack = function() {
    return Util.shallowClone(this.stack)
}

Instance.prototype.groupExecute = function(step, data, callback) {
    var self = this
    //create a new stack
    var groupSteps = Util.shallowClone(step.data.steps)
    var subStack = this.cloneStack()

    subStack[step.id] = {
        scope: data
    }
    /*groupSteps['Start'] = {
        id: 'Start',
        ref: Constants.START_REF,
        name: 'Start'
    }
    groupSteps['End'] = {
        id: 'End',
        ref: Constants.END_REF,
        name: 'End'
    }*/
    var flowData = {
        meta: { _isGroup: true },
        steps: groupSteps,
        transitions: step.data.transitions
    }
    var scope = this.params.scope ? this.params.scope + Constants.GROUP_ID_SEPARATOR + step.id : step.id
    //we need to generate a unique id for the group name otherwise it can collide with flow filenames in cache loader

    var fileName = this.params.scope ? this.params.file : (this.params.name || this.params.file)
    var uniqFilename = fileName.slice(-4) === ".grp" ? fileName : fileName + '.grp'

    this.engine.execute({
        flow: flowData,
        scope: scope,
        packageName: this.params.packageName,
        origFile: this.params.scope ? this.params.origFile : this.params.name,
        file: uniqFilename,
        name: scope + '@' + uniqFilename,
        stack: subStack,
        callback: function(error, data) {
            trampolineFn(function() {
                callback(error, data)
            })
        }
    }, this, step.id)
}

Instance.prototype.evalObject = function(step, stack) {
    var result
    try {
        result = Eval.evalObject(step.inputBinding, stack, step.path)
    } catch (err) {
        evalError.error = err
        return evalError
    }
    return result
}

Instance.prototype.executeStep = function(stepId) {
    var self = this
    var stack = self.stack
    var step = self.flow.steps[stepId]
    var meta = step.meta
    var input
    var stepError = null
    //flag to see if the step is inside a group
    var isGroup = self.flow.meta && self.flow.meta._isGroup

    //dont do much processing for start and step inside a group
    if (isGroup && step.isStart) {
        this.onStepComplete(step, input)
        return
    }
    stack[stepId] = {}

    self.isDebugMode && self.updateStepStatus(stepId, Status.STEP_STATUS.PROCESS_INPUT)

    if (self.isDebugMode) {
        stack[stepId].startTm = Date.now()
    }
    if (this.incomingError[stepId]) {
        stepError = "one or more steps leading to the join has an unhandled error"
    }
    if ((step.isTrigger || step.isStart) && self.params.input != null) {
        input = self.params.input
    } else {
        if (step.inputBinding) {
            var result = self.evalObject(step, stack)
            if (result === evalError) {
                stepError = evalError.error.toString()
            } else {
                input = result
            }
        }
    }

    if (stepError) {
        self.onStepComplete(step, input, stepError)
    } else {
        stack[stepId].input = input
        if (meta && meta.autoLog) {
            Logger.log("Input:" + step.path, input)
        }
        var options = {
            //wrap the engine in a closure so that if the step calls another flow
            //using the engine again, it will keep the reference to parent
            engine: self.getEngineWrapped(stepId),
            global: global,
            instance: self.id,
            root: self.rootId,
            packageName: self.params.packageName,
            vars: self.vars,
            stack: stack,
            stepname: stepId,
            step: step,
            filename: self.flow.fileName // this should be full path?
        }
        if (step.isGroup) {
            options.execute = function(data, callback) {
                self.groupExecute(step, data, callback)
            }
        }

        if (self.engine.validationEnabled/* || self.isDebugMode*/) {
            var report = Util.validateJson(step.inputSchema, input)
            if (report && report.errors.length != 0) {
                stepError = "Validation Error: Input is not conforming to the schema. " + self.formatErrorReport("Input", report)
            }
        }

        if (stepError) {
            self.onStepComplete(step, input, stepError)
        } else {
            self.invokeCount[stepId] = 0
            if (step.isTrigger) {
                self.isDebugMode && self.updateStepStatus(stepId, Status.STEP_STATUS.EVENT_WAITING)
            } else {
                self.isDebugMode && self.updateStepStatus(stepId, Status.STEP_STATUS.EXECUTING)
            }
            step.execute(input, options, function(error, output, vars) {
                self.onStepComplete(step, input, error, output, vars)
            })
        }
    }
}

/**
 * Wrapping up a step's completion. Many key things (like picking next step(s)) happen here.
 */
Instance.prototype.onStepComplete = function(step, input, error, output, vars) {
    var stepId = step.id
    var stack = this.stack
    var meta = step.meta
    var self = this
    var isGroup = self.flow.meta && self.flow.meta._isGroup //flag to see if the step is inside a group
    var skipStep = isGroup && (step.isStart) //skip start step inside a group - why ?
    delete self.invokeQueue[stepId]
    if (self.isDebugMode && !skipStep) {
        stack[stepId].tm = Date.now() - stack[stepId].startTm
        delete stack[stepId].startTm
    }
    //Freeze the output
    if (typeof output == 'object' && output != null) {
        //@TODO commenting until good...
        //Ice.freeze(output)
    }
    self.invokeCount[stepId]++
    if (step.isTrigger) {
        // If the step is a trigger step that would have got handled
        // already, then no need for further processing
        self.handleTriggerStep(step, input, output, error, vars)
    } else {
        if (vars) {
            _.each(vars, function(val, key) {
                self.vars[key] = val
            })
        }
        //cache current status
        var actStatus
        if (self.isDebugMode) {
            actStatus = self.stepMap[stepId].status
        }
        if (self.invokeCount[stepId] > 1) {
            //If the same step invokes callback/returns more than once, it will be ignored
            Logger.error('Fatal error:' + step.path + ' Non-trigger step attempted to return more than once. Output will be ignored.')
            console.log(new Error())
            if (error) {
                console.error(error)
            }
        } else {
            if (error) {
                self.setStepError(step, error)
            } else {
                //update to callback
                self.isDebugMode && self.updateStepStatus(stepId, Status.STEP_STATUS.CALLBACK, true)
                //Adding output to stack
                if (!skipStep) {
                    stack[stepId].output = output
                }
                if (meta && meta.autoLog) {
                    Logger.log("Output:" + step.path, output)
                }
                if (self.engine.validationEnabled/* || self.isDebugMode*/) {
                    var report = Util.validateJson(step.outputSchema, output)
                    if (report && report.errors.length != 0) {
                        self.setStepError(step, new Error("Validation Error: Output is not conforming to the schema. " + self.formatErrorReport("Output", report)))
                    }
                }
            }

            var next = self.getNext(step)

            error = stack[step.id] ? stack[step.id].error : null
            if (error && step.errorHandlerCount === 0) {
                self.errors.push(error)
            }

            //starting execution if there is scope already
            var isPaused = false
            if (next.steps.length > 0) {
                if (self.isDebugMode) {
                    if (actStatus == Status.STEP_STATUS.PAUSED_AFTER) {
                        isPaused = true
                    }
                }
                self.startExecution(next.steps, isPaused)
            } else {
                var pending = self.getPendingNonAsyncTasks().length > 0
                if (!pending && self.status !== Status.INSTANCE_STATUS.CALLBACK) {
                    //error = new Error("No paths leading to End or Error found")
                }
                if (step.isEnd || next.isEnd || step.isError || (!pending && self.errors.length > 0)) {
                    //returns when end step is encountered
                    if (self.status === Status.INSTANCE_STATUS.CALLBACK) {
                        if (error || (self.errors && self.errors.length > 0)) {
                            console.error("Error occurred after end was invoked, will be ignored.")
                            console.error(error || self.errors)
                        } else {
                            console.error("End cannot be invoked more than once. It will be ignored.")
                            self.setStepError(step, new Error('End cannot be invoked more than once. It will be ignored.'))
                        }
                    } else {
                        self.status = Status.INSTANCE_STATUS.CALLBACK
                        self.isDebugMode && self.checkIfDone()
                        if (typeof self.params.callback == 'function') {
                            if (error || self.errors.length > 0) {
                                if (!self.parent || self.isTriggerChild) {
                                    // console.error(error || self.errors)
                                }
                                self.params.callback(error || self.errors[0])
                            } else {
                                self.params.callback(null, output)
                            }
                        }
                    }
                } else {
                    if (self.status != Status.INSTANCE_STATUS.CALLBACK) {
                        //probably need a check if there are pending steps
                        //if not there is an error
                        //throw new Error("No path")
                    }
                    self.isDebugMode && self.checkIfDone()
                }
            }
        }
    }
}

Instance.prototype.getPendingNonAsyncTasks = function() {
    var self = this
    return _.filter(Object.keys(this.invokeQueue), function(step) {
        return !(self.flow.steps[step].isAsync)
    })
}

Instance.prototype.getPendingTasks = function() {
    return Object.keys(this.invokeQueue)
}

/**
 *
 */
Instance.prototype.checkIfDone = function() {
    this.checkIfComplete()
    this.engine.sendUpdate()
    if (this.mode == Status.DEBUG_MODE.STEP) {
        this.engine.sendSetFocus(this)
    }
}

/**
 * Check if all the  in the stack have received callback as well as
 * if all the child instances have completed as well.
 */
Instance.prototype.checkIfComplete = function() {
    if (!this.isDebugMode) {
        return
    }
    var self = this
    var cnt = 0
    if (!this.stepsCompleted) {
        _.each(self.stepMap, function(step) {
            if (step.status != Status.STEP_STATUS.CALLBACK) {
                cnt++
            }
        })
        this.stepsCompleted = (cnt == 0)
    }
    if (this.stepsCompleted) {
        var childCnt = 0
        _.each(self.subInstances, function(instance) {
            if (instance.status != Status.INSTANCE_STATUS.COMPLETE) {
                childCnt++
            }
        })
        if (childCnt == 0) {
            self.status = Status.INSTANCE_STATUS.COMPLETE
            //self.engine.sendUpdate()
            if (self.parent) {
                //notify the parent
                self.parent.onChildStatusComplete(self)
            }
        }
    }
}

/**
 * Called when a child instance is completed
 */
Instance.prototype.onChildStatusComplete = function() {
    //check if the flow is complete and notify parent if needed
    this.checkIfComplete()
}

/**
 * Mark a whole subtree as inactive, plus add next executable steps thereby
 * freed up to the list.
 *
 * @param step -
 * @param list -
 */
Instance.prototype.markInactiveTree = function(step, list) {
    var transitions = step.outgoing
    for (var i = 0; i < transitions.length; i++) {
        var transition = transitions[i]
        var act = transition.to
        if (typeof this.incomingFailure[act.id] == 'undefined') {
            this.incomingFailure[act.id] = 0
        }
        if (typeof this.incomingSuccess[act.id] == 'undefined') {
            this.incomingSuccess[act.id] = 0
        }
        this.incomingFailure[act.id]++
        this.transitionState[transition.id] = Status.TR_STATUS.NOT_REACHABLE
        this.checkAndAddStepToList(act, list)

        if (this.incomingFailure[act.id] === act.incomingCount) {
            this.markInactiveTree(act, list)
        }
    }
}

/**
 *
 */
Instance.prototype.markSuccess = function(transition) {
    var self = this
    self.transitionState[transition.id] = Status.TR_STATUS.SUCCESS
    self.incomingSuccess[transition.to.id]++
}

/**
 *
 */
Instance.prototype.markFail = function(transition, stepList) {
    var self = this
    self.transitionState[transition.id] = Status.TR_STATUS.FAIL
    self.incomingFailure[transition.to.id]++
    if (self.incomingFailure[transition.to.id] === transition.to.incomingCount) {
        self.markInactiveTree(transition.to, stepList)
    }
}

/**
 * Algorithm that checks if a step should be included in execution list.
 */
Instance.prototype.checkAndAddStepToList = function(step, stepList) {
    //if at least one successful incoming path as well as all the
    //other paths are evaluated, then pick the step
    var id = step.id
    var totalPaths = this.incomingSuccess[id] + this.incomingFailure[id] + this.incomingError[id]
    var isAllPathsEncountered = (totalPaths === step.incomingCount)
    var atleastOnePathEvaluated = this.incomingSuccess[id] > 0
    var atleastOnePathErred = this.incomingError[id] > 0

    if (atleastOnePathEvaluated && !atleastOnePathErred && isAllPathsEncountered) {
        //if ((atleastOnePathEvaluated || (atleastOnePathErred && step.incomingCount > 1)) && isAllPathsEncountered) {
        stepList[id] = true
    }
}

Instance.prototype.initState = function(stepId) {
    if (typeof this.incomingSuccess[stepId] == 'undefined') {
        this.incomingSuccess[stepId] = 0
    }
    if (typeof this.incomingFailure[stepId] == 'undefined') {
        this.incomingFailure[stepId] = 0
    }
    if (typeof this.incomingError[stepId] == 'undefined') {
        this.incomingError[stepId] = 0
    }
}

/* recursively mark a subtree as having unhandled error */
Instance.prototype.markError = function(step) {
    var self = this
    var transitions = step.outgoing
    for (var i = 0; i < transitions.length; i++) {
        var act = transitions[i].to
        self.initState(act.id)
        self.incomingError[act.id]++
        self.transitionState[transitions[i].id] = Status.TR_STATUS.UNHANDLED_ERROR_SPILLOVER
        //self.checkAndAddStepToList(act, stepList)
        if (self.incomingError[act.id] === act.incomingCount) {
            //recurse
            self.markError(act)
        }
    }
}

/**
 * Key logic of flow control. Get the next set of steps that should be executed next.
 */
Instance.prototype.getNext = function(inpStep) {
    var self = this
    var transitions = inpStep.outgoing
    if (transitions.length == 0) {
        return {
            steps: new Array()
        }
    }

    var actStack = this.stack[inpStep.id]
    var error = actStack ? actStack.error : null
    var stepList = {}
    var i, transition, to
    var isEnd = false
    if (error) {
        if (inpStep.errorHandlerCount > 0) {
            for (i = 0; i < transitions.length; i++) {
                transition = transitions[i]
                this.initState(transition.to.id)
                if (transition.type == 'error' || transition.type == 'always') {
                    this.markSuccess(transition)
                } else {
                    this.markFail(transition, stepList)
                }
            }
        } else {
            var self = this
            //@TODO rethink this
            if (this.getPendingTasks().length === 0) {
                isEnd = true
            } else {
                this.markError(inpStep)
            }
        }
    } else {
        var anySucceeded = false
        var otherwiseTransition = null
        var condEval = {}
        //precalculate transition conditions
        //this is useful for checking exceptions and marking
        //another option would be to ignore exceptions
        for (i = 0; i < transitions.length; i++) {
            transition = transitions[i]
            this.initState(transition.to.id)
            if (transition.type == 'condition') {
                condEval[transition.id] = this.evalCondition(transition, this.stack)
                if (condEval[transition.id] === evalError) {
                    this.setStepError(inpStep, evalError.error)
                    return this.getNext(inpStep)
                }
            }
        }
        //second pass.
        for (i = 0; i < transitions.length; i++) {
            transition = transitions[i]
            to = transition.to
            if (transition.type == "always") {
                anySucceeded = true
                this.markSuccess(transition)
            } else {
                if (transition.type == "error") {
                    this.markFail(transition, stepList)
                } else if (transition.type == "otherwise" && !otherwiseTransition) {
                    //only one otherwise is taken others will go to fail
                    otherwiseTransition = transition
                } else { // normal or condition or second otherwise
                    if (typeof transition.type == 'undefined'
                        || transition.type == 'normal'
                        || condEval[transition.id]) {
                        anySucceeded = true
                        this.markSuccess(transition)
                    } else {
                        this.markFail(transition, stepList)
                    }
                }
            }
        }

        if (otherwiseTransition) {
            if (!anySucceeded) {
                this.markSuccess(otherwiseTransition)
            } else {
                this.markFail(otherwiseTransition, stepList)
            }
        }
    }

    for (i = 0; i < transitions.length; i++) {
        transition = transitions[i]
        this.checkAndAddStepToList(transition.to, stepList)
    }
    return {
        isEnd: isEnd,
        steps: Object.keys(stepList)
    }
}


/**
 * Evaluates a condition on the transition.
 */
Instance.prototype.evalCondition = function(transition, stack) {
    var condition = transition.condition
    if (condition == undefined || (typeof condition == 'string' && condition.length == 0)) {
        return true
    } else if (typeof condition == 'boolean') {
        return condition
    }
    try {
        var result = Eval.evalExpression(condition, stack)
    } catch (e) {
        //if there is an error in transition, mark as error in the
        //step and call again
        //this.stack[inpStep.id].error = e
        this.transitionError[transition.id] = e
        this.transitionState[transition.id] = Status.TR_STATUS.ERROR
        var wrappedError = new Error("Error occurred while evaluating condition at transition from ["
            + transition.from.id + "] to ["
            + transition.to.path + "]. Error: [" + e.message + "]")
        evalError.error = wrappedError
        return evalError
    }
    return result
}

module.exports = Instance