"use strict"

var helper = require('../helper')
var _  = require('underscore')
var Constants = require('../../constants')
var Flow = require('../../flow')
/**
 * Compiled static & asynchronous snippets for base modules. This will improve performance
 * by inserting javascript code snippets directly inside a code instead of invoking a wrapper function.
 */

/*
/* Sync modules
 *
 * Every snippet should return a js code that will be executed under a closure. Below variables are
 * available in the closure where the generated code will be executed:
 *
 * self.steps - the stack
 * input      - input for the step
 * callback   - applicable only for core/start
 *
 * Each snippet function is passed with below paramters:
 * step - step
 * params - {id: <>, <packageName: <name of the package, if step is under a package>}
 * input - input
 * callback - optional, if present the caller will expect the generated code to be passed via the callback.
 */

var sync = {
    'core/start': function (step, params, input) {
        return `self.steps["${step.id}"].output = self.params.input`
    },
    'core/console/log': function (step, params, input) {
        return `input && console.log(input.text)`
    },
    'core/build_object': function (step, params, input) {
        return `self.steps["${step.id}"].output = input`
    },
    'core/join': function (step) {
        return ``
    },
    'core/get_variable': function (step, params, input) {
        return `
            if (input && typeof input.key !== 'undefined') {
                self.steps["${step.id}"].output = global[input.key]
            }
        `
    },
    'core/set_variable': function (step, params, input) {
        return `
            if (input && typeof input.key !== 'undefined') {
                global[input.key] = input.value
                self.steps["${step.id}"].output = global[input.key]
            }
        `
    },
    'core/date/currenttime': function (step, params, input) {
        return `self.steps["${step.id}"].output = Date.now()`
    },
    'core/iterator-group': function (step, params, input, callback) {
        if (!callback) {
            throw new Error("No callback provided")
        }
        //@TODO - this is doing a check if the steps are fully synchronous, if so the steps can be optimized to
        //generate synchronous code that can run without worrying about trampolining
        const steps = step.data ? step.data.steps : {}
        let isOptimizable = true
        helper.areAllSync(steps, (error, isSync) => {
            if (!isSync || error) {
                return callback(error)
            }
            _.each(step.data.transitions, function(transition) {
                if (transition.type && transition.type != 'default') {
                    isOptimizable = false
                }
            })
            if (!isOptimizable) {
                return callback()
            }
            let generatedCode = 'var input\n', endCode = ''
            let parent = step
            new Flow().load(parent.id, step.data, function(err, flow) {
                if (err) {
                    return callback(err)
                }
                var seqSteps = []
                var index = {}
                const startStep = _.find(flow.steps, step => {
                    return step.isStart || step.isTrigger
                })
                const queue = []
                queue.push(startStep)
                do {
                    const step = queue.pop()
                    if (!index[step.id]) {
                        seqSteps.push(step)
                        index[step.id] = true
                        if (step.outgoing) {
                            _.each(step.outgoing, function(transition) {
                                queue.push(transition.to)
                            })
                        }
                    }
                } while (queue.length)

                seqSteps.forEach(function (step, index) {
                    const inputCompiled = helper.compileJst(step.inputBinding)
                    if (step.isEnd) {
                        endCode += `input = ${inputCompiled}\n`
                        endCode += `step_${parent.id}.output.push(input)\n`
                    } else if (step.isStart) {
                        //do nothing
                    } else {
                        let snippet = sync[step.ref]
                        if (snippet) {
                            generatedCode += `input = ${inputCompiled}\n`
                            generatedCode += `self.steps["${step.id}"] = {}\n`
                            generatedCode += sync[step.ref](step, params, input) + '\n'
                        }
                        else
                            console.error("No snippet for ", step.ref)
                    }
                })
                const groupCode = generatedCode + endCode
                const code = `
                    self.steps["${step.id}"].output = []
                    var step_${step.id} = self.steps["${step.id}"]
                    //(function() {
                    var data = self.steps
                    var tmp = data["${step.id}"]
                    var collection = input ? input.collection : []
                    if (collection && typeof collection.forEach == 'function') {
                        collection.forEach(function(item, index) {
                            var self = {
                                steps: data
                            }
                            data["${step.id}"] = {
                                scope: {
                                    value: item,
                                    index: index
                                }
                            }
                            ${groupCode}
                        })
                    }
                    data["${step.id}"] = tmp
                    //})()

                `
                return callback(null, code)
            })
        })
    }
}

/**
 * Async modules
 *
 * Every snippet should return a js code that will be executed under a closure and
 * perform an asynchronous action. Below variables are available in the closure where the
 * generated code will be executed:
 *
 * self.steps - the stack
 * input      - input for the step
 * self.postCallbackStmt - javascript that should be made after callback
 *
 * Each snippet function is passed with below paramters:
 * step - step
 * params - {id: <>, callsStmt: <code snippet that will be executed after the module> ,<packageName: <name of the package, if step is under a package>}
 * input - input
 * callback - optional, if present the caller will expect the generated code to be passed via the callback.
 *
 */

var async = {
    'core/subflow': function (step, params, input) {
        let callsStmt = params.callsStmt
        const code =
            `
        var name = input.flow.file
        if (!name) {
            return callback(new Error("An empty flow path was passed."))
        }
        var packageName = "${params.packageName || ''}"
        if (packageName) {
            name = '.packages' + '/' + packageName + '/' + name
        }
        var file = helper.fixPath(name)
        var params = {
            file: file,
            name: name,
            input: input.flow.input,
            callback:function(_error, _data) {
                self.steps["${step.id}"].error = _error
                self.steps["${step.id}"].output = _data
                if (self.steps["${step.id}"].autoLog) {
                    helper.logOutput("${step.path}", output)
                }
                process.nextTick(function() {
                    ${callsStmt}
                })
            }
        }
        //@TODO experimental
        if (params.name == self.flow.fileName) {
            var instance = new self.constructor(self.engine, params, self, "${step.id}")
            process.nextTick(function() {
                instance.start()
            })
        } else {
            self.engine.getCompiled(params, function(_error, Instance) {
                if (_error) {
                    self.steps["${step.id}"].error = _error
                    console.error(_error)
                    ${callsStmt}
                } else {
                    var instance = new Instance(self.engine, params, self, "${step.id}")
                    //process.nextTick(function() {
                        instance.start()
                    //})
                }
            })
        }
        `
        return code
    },
    'core/iterator': function (step, params, input) {
        //this could range from an async call to a sync call based on the subflow.
        const callsStmt = params.callsStmt
        const code =
            `var async = helper.async
        var _ = helper._
        var iteratee = input.iteratee
        var isObj = false
        var collection = input.collection
        if (typeof collection == 'object') {
            if (!_.isArray(collection)) {
                collection = Object.keys(collection)
                isObj = true
            }
        }
        var iterateFn = async.eachSeries
        var config = input.options || {}
        if (config.parallel) {
            if (config.limit) {
                iterateFn = function(arr, iterator, cb) {
                    async.eachLimit(arr, config.limit, iterator, cb)
                }
            } else {
                iterateFn = async.each
            }
        }
        var iteratorKey = input.key
        var inputCompiled = helper.getCachedJstExpression(iteratee.__input, "${step.path}:iterator")
        var output = []
        var errors = []
        var name = iteratee.file
        var packageName = "${params.packageName || ''}"
        if (packageName) {
            name = '.packages' + '/' + packageName + '/' + name
        }
        var file = helper.fixPath(name)
        var index = 0
        //make a shallow copy of stack to newStack
        //this is because new key will be added to newStack
        //and we do not want to pollute the parent's context
        var stack = self.steps
        var newStack = {}
        var curKeys = Object.keys(stack)
        for (var i = 0; i < curKeys.length; i++) {
            newStack[curKeys[i]] = stack[curKeys[i]]
        }
        var options = {
            file : file,
            name: name
        }
        self.engine.getCompiled(options, function(_error, Instance) {
            if (_error) {
                console.error(_error)
                self.steps["${step.id}"].error = _error
                ${callsStmt}
            } else {
                iterateFn(collection, function(elem, next)  {
                    var key = isObj ? elem: index++
                    var value = isObj ? input.collection[elem] : elem
                    newStack[iteratorKey] = {
                        value: value,
                        index: key
                    }
                    var subflowInput = inputCompiled(newStack, __methods)
                    var options = {
                        file : file,
                        name: name,
                        input: subflowInput,
                        callback: function(_error, _data) {
                            if (_error) {
                                if (!config.ignoreError) {
                                    errors.push({
                                        message: _error.message,
                                        path: _error.path,
                                        code: _error.code,
                                        data: _error.data
                                    })
                                }
                            } else {
                                if (!(config.excludeEmpty && helper.isEmpty(_data))) {
                                    output.push(_data)
                                }
                            }
                            process.nextTick(next)
                        }
                    }
                    var instance = new Instance(self.engine, options, self, "${step.id}")
                    process.nextTick(function() {
                        instance.start()
                    })
                }, function(err) {
                    var _error = null
                    if (errors.length > 0) {
                        _error = new Error("Error occurred during iterator")
                        _error.data = errors
                        self.steps["${step.id}"].error = _error
                    }
                    self.steps["${step.id}"].output = output
                    if (self.steps["${step.id}"].autoLog) {
                        helper.logOutput("${step.path}", output)
                    }
                    ${callsStmt}
                })
            }
        })`
        return code
    }
}

module.exports = {
    sync: sync,
    async: async
}