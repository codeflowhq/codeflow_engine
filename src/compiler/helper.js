/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

const Path = require('path')
const async = require('async')
const _ = require('underscore')
const isAbsolute = require('path-is-absolute')
const Util = require('../util')
const _Error = require('../error')
const Constants = require('../constants')
const Context = require('../lib/expressionBuilder/src/context')
const JstUtil = require('../lib/jst/src/util')
const JstCompiler = require('../lib/jst/src/compiler')

const context = new Context()
const __methods = context.getMethodContext()
const executionContext = context.getExecutionContext({})

const MAX_TICK = 10
const trampolineFn = process.nextTick
const cached = {}
const pathCache = {}
const cachedJst = {}

/**
* Util function to determine if a value is empty.
* numbers and booleans are not considered empty even if they are
* evaluated to 0 or false.
*/
const isEmpty = (data) => {
    switch(typeof data) {
        case 'object':
        case 'string':
            return _.isEmpty(data)
        case 'number':
        case 'boolean':
            return false
        case 'undefined':
            return true
        default:
            return _.isEmpty(data)
    }
}

const getModule = async (source) => {
    if (!cached[source]) {
        try {
            cached[source] = await Util.loadModuleSource(source)
        } catch(e) {
            console.error(e)
        }
        //cached[source] = require(Path.join(packagesPath, source))
    }
    return cached[source]
}

const makeClassId = (id, scope) => {
    return 'Flow_$' + makeScopeId(id, scope).replace(Constants.GROUP_ID_SEPARATOR, '$')
}

const makeScopeId = (id, scope) => {
    return (scope ? scope + Constants.GROUP_ID_SEPARATOR : '') + id
}

const getSnippet = (step, inputCompiled, callsStmt, params, type, callback) => {
    const snippets = require('./js/snippets')
    const snippet = snippets[type][step.ref]
    if (!snippet) {
        return callback()
    }
    params = Util.shallowClone(params)
    params.callsStmt = callsStmt

    let result = null
    //if the method expects a callback
    if (typeof snippet === 'function' && snippet.length > 3) {
        snippet(step, params, inputCompiled, (err, code) => {
            if (err) {
                return callback(err)
            }
            if (code) {
                result = {}
                result[type] = code
            }
            return callback(null, result)
        })
    } else {
        let code = snippet(step, params, inputCompiled)
        if (code) {
            result = {}
            result[type] = code
        }
        return callback(null, result)
    }
    return null
}

/**
 * Check if a group of steps is synchronous or not.
 */
const areAllSync = (steps, callback) => {
    let isSync = true
    let snippets = require('./js/snippets')
    let stepsArr = []
    for (let id in steps) {
        stepsArr.push(steps[id])
    }
    if (!stepsArr.length) {
        return callback()
    }
    async.each(stepsArr, (step, next) => {
        if (step.type && step.type === "end") {
            return next()
        }
        if (step.type === "group") {
            return next(new Error("break"))
            /*
            areAllSync(step.data.steps, function(err, isSync) {
                if (!isSync) {
                    return next(new Error("break"))
                }
                return next(err)
            })*/
        } else {
            getSnippet(step, {}, "", {}, 'sync', (err, snippet) => {
                if (!snippet || !snippet.sync) {
                    return next(new Error("break")) //hack to break out immediately
                }
                return next(err)
            })
        }
    }, (error) => {
        if (error) {
            if (error.message === 'break') {
                isSync = false
            } else {
                return callback(error)
            }
        }
        return callback(null, isSync)
    })
}

let gId = 0
let tick = 0

module.exports = {
    getId: (parent) => {
        return parent ? (parent.id + '.'  + parent.childCount) : gId++
    },
    cloneInstance: (flowCls, instance, step) => {
        return new instance.constructor(instance.engine, instance.flow, instance.params, instance, step)
    },
    callJs: async (instance, params, callback) => {
        let id = params.id
        let modulePath = params.modulePath
        let method = params.method
        let input = params.input

        let jsCode = await getModule(modulePath)
        let step =  instance.flow.steps[id]
        //this should be moved to instance level
        let options = {
            vars: instance.vars,
            global: global,
            engine: instance.engine,
            filename: instance.flow.fileName, //
            step: step,
            execute: (data, callback) => {
                if (!params.Cls) {
                    return callback(new Error("Illegal attempt to invoke options.execute. Not a group closure."))
                }
                let gInst = new params.Cls(instance.engine, {callback: (error, data) => {
                    if (++tick >= MAX_TICK) {
                        tick = 0
                        trampolineFn(() => {
                            callback(error, data)
                        })
                    } else {
                        callback(error, data)
                    }
                }, data: data, id: step.id, scope: params.scope}, instance, step)
                gInst.start()
            },
            stepname: id, //
            packageName: instance.params.packageName, // used to identify custom installed packages
            instance: instance.id,
            root: instance.rootId, // used by setInstancevar to set root-Instance level variables
            stack: instance.steps
        }
        try {
            jsCode[method].apply(jsCode, [input, callback, options])
        } catch(error) {
            callback(new _Error(error.message, error.code, error.data || error.stack, step.path, error, error.stack))
        }
    },
    callFlow: (instance, params, callback) => {
        let id = params.id
        let packageName = params.packageName
        let flowfile = params.flowfile
        let input = params.input

        let cwd = global.ROOT_DIR
        let options = {
            vars: instance.vars,
            global: global,
            engine: instance.engine,
            filename: instance.flow.fileName, //
            step: instance.flow.steps[id],
            execute: (data, callback) => {
                return callback()
            },
            stepname: id, //
            packageName: packageName, // used to identify custom installed packages
            instance: instance.id,
            root: instance.rootId, // used by setInstancevar to set root-Instance level variables
            stack: instance.steps
        }
        return instance.engine.executeOptimized({
            file: flowfile,
            packageName: packageName,
            name: Path.relative(cwd, flowfile),
            input: input,
            options: options,
            callback: callback
        }, instance, id)
    },
    logInput: (stepPath, input) => {
        console.log(`Input: ${stepPath}`, input)
    },
    logOutput: (stepPath, output) => {
        console.log(`Output: ${stepPath}`, output)
    },
    compileJst: (jst, options) => {
        let compiler = new JstCompiler(executionContext, options)
        return compiler.compileJst(JstUtil.preProcess(Util.deepClone(jst)))
    },
    getCachedJstExpression: (jst, key, options) => {
        if (!cachedJst[key]) {
            let compiler = new JstCompiler(executionContext, options)
            let data = compiler.compileJst(JstUtil.preProcess(Util.deepClone(jst)))
            cachedJst[key] = new Function("data", "__methods", "return " + data)
        }
        return cachedJst[key]
    },
    compileExpression: (expr, options) => {
        let compiler = new JstCompiler(executionContext, options)
        return compiler.compileElement(expr)
    },
    _getMethodContext: () => {
        return  __methods
    },
    fixPath: (path) => {
        if (!pathCache[path]) {
            pathCache[path] = !isAbsolute(path) ? ROOT_DIR + '/' + path : path
        }
        return pathCache[path]
    },
    wrapError: (error, stepPath) => {
        let errorWrap = null
        if (error) {
            if (error instanceof _Error) {
                errorWrap = new _Error(error.message, error.code, error.data, [stepPath].concat(error.path), null, error.originalError)
            } else {
                if (error instanceof Error) {
                    errorWrap = new _Error(error.message, error.code, error.data, stepPath, null, error.stack)
                } else if (typeof error == 'string') {
                    errorWrap = new _Error(error, null, null, stepPath)
                } else {
                    console.error(`Invalid error type specified by step ${stepPath}`, error)
                    errorWrap = new _Error("Invalid error type.", "INVALID_ERROR", error, stepPath, null)
                }
            }
        }
        return errorWrap
    },
    async: async,
    _: _,
    isAbsolute: isAbsolute,
    isEmpty: isEmpty,
    areAllSync: areAllSync,
    getSnippet: getSnippet,
    makeClassId: makeClassId,
    makeScopeId: makeScopeId
}