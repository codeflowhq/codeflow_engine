/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

function Compiler(settings) {
    settings = settings || {}
    var generator = settings.generator || 'javascript'
    var GeneratorClass = require('./compiler/' + generator + '')
    this.settings = settings
    this.generator = new GeneratorClass({
        
    }) 
}

/**
 * Compiles a Flow to Js.
 */
Compiler.prototype.compile = function(flow, cb) {
    this.generator.generate(flow, cb)
}

Compiler.prototype.execute = function(executable, input) {
    executable.execute(input)
}

function Executable(code) {
    this.code = code
}

Executable.prototype.execute = function(data) {
    this.code(data)
}