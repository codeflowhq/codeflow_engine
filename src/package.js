/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 *
 * package.json custom parameter flags,
 * codeflow_hidden_package - hide a packge from listing
 * codeflow_sort_index - sort the package in designer list panel (a negative value keeps the package on top)
 *
 */
'use strict'

var fs = require('fs')
var path = require('path')
var util = require('util')

var _ = require('underscore')

var crypto = require('crypto')
var semver = require('semver')
var util = require('./util.js')
var fsUtil = require('./fsUtil.js')
var packageLib = require('./package/index')
var Eval = require('./eval')
var Constants = require('./constants')
const {fork} = require('child_process')

var TYPE_CORE  = 'core'
var TYPE_INSTALLED = 'installed'
var EXT_MODULE_OLD = '.act'
var EXT_MODULE = '.cm'
var EXT_FLOW = '.flw'
var PACKAGE_INSTALL_FOLDER = '.packages'
var DYNAMIC_MODULE_FILE = 'modules.js'

var CORE_PACKAGES_PATH = path.join(__dirname, '../')

var lookupPathCache = {}


class Package {

    constructor() {
        this.packageCache = {}
        this.systemCache = {}
        this.moduleCache = {}
        this.childProcessMap = {}
        this.packagePathsLoaded = []
        this.childProcessCb = {}
    }

    getChildProcess(cwd) {
        if (this.childProcessMap[cwd]) {
            return this.childProcessMap[cwd]
            //this.removeChildProcess(cwd)
        }

        const childProcess = fork(path.join(__dirname, 'package', 'runModule.js'), [] , {
            cwd:  cwd,
            silent: true,
            detached: false,
            //execArgv: ["--inspect-brk=5860"]
            //execArgv: ["--inspect=5860"]
        })
        this.childProcessMap[cwd] = childProcess
        childProcess.on('exit', code => {
            delete this.childProcessMap[cwd]
        })
        childProcess.stdout.on('data', message => {
            console.log(message.toString())
        })
        childProcess.stderr.on('data', err=> {
            console.log(err.toString())
            //no need to reject here as the process sends back both error and data in the on message handler
            //reject(err.toString())
        })
        childProcess.on('message', (message)=>{
            message = message || {}
            let {data, error, messageId} = message
            if(!this.childProcessCb[messageId]) { return }

            if(error) {
                console.error(error)
                return this.childProcessCb[messageId].error(error)
            }
            this.childProcessCb[messageId].success(data)
        })

        childProcess.send({isPersistent: true})
        return childProcess
    }

    removeChildProcess(cwd) {
        if(!this.childProcessMap[cwd]) {
            return false
        }
        try {
            this.childProcessMap[cwd].kill('SIGKILL')
        } catch(e) {}
    }

    /**
     * Load all the packges system|installed
     * @param {*} basePath
     * @param {*} forClient
     * [
     *  {
     *      expanded: boolean,
     *      hidden: boolean,
     *      invisible: boolean,
     *      icon: string, ex: data:image/svg+xml;base64,PD94.....
     *      name: string,
     *      namespace: string,
     *      path: string [absolute path to .packages], ex: /Users/ace/Works/server/www/main/codeflow_engine/.packages
     *      root_path: string [absolute path to .packages], ex: /Users/ace/Works/server/www/main/codeflow_engine/.packages
     *      random_id: integer,
     *      sort_index: integer,
     *      type: string {core|installed),
     *      version: string,
     *      modules: [
     *          {
     *             expanded: boolean,
     *             hidden: boolean,
     *             invisible: boolean,
     *             ref: string, ex: core/bootup
     *             path: string [absolute path to .packages], ex: /Users/ace/Works/server/www/main/codeflow_engine/.packages
     *             package: string, ex: core
     *             packageType: string (core|installed), ex: core
     *             packagePath: string [absolute path to package folder], ex: /Users/ace/Works/server/www/main/codeflow_engine/.packages/codeflow/core,
     *
     *             source: string, ex: "codeflow/core/system.js"
     *             type: string {trigger | folder)}, folder will container modules property repeating the modules item schema
     *             method: string,
     *             input-schema-builder: boolean,
     *             input-schema: object,
     *             output-schema: object,
     *             meta: object {
     *                  name: string,
     *                  description: string,
     *                  sort_index: integer,
     *                  icon: {
     *                      type: 'string',
     *                      name: 'string',
     *
     *                  },
     *             },
     *             definitions: object,
     *             groups: object,
     *             input: string, ex: hidden
     *             output: string, ex: hidden
     *             error-builder: boolean
     *          }
     *      ],
     *      codeflow: {
     *          packageId: string,
     *          modules: [
     *              string
     *          ],
     *          dependencies: object
     *      }
     *  }
     * ]
     *
     */
    async loadPackages(basePath, forClient=false) {
        try {
            let core = this.getCachedPackages(CORE_PACKAGES_PATH)
            let installed = this.getCachedPackages(basePath)
            if(!core) {
                core = await this.listPackages(CORE_PACKAGES_PATH, {type: TYPE_CORE,for_client: forClient})
                this.cacheSystemPackages(core)
                this.cachePackages(CORE_PACKAGES_PATH, core)
            }

            //if(!installed) {
                installed = await this.listPackages(basePath, {for_client: forClient})
                let inProject = await this.loadSelf(basePath, forClient)
                if(inProject) {
                    installed.push(inProject)
                }
                this.cachePackages(basePath, installed)
            //}
            //this.logPackages(core, installed)
            //return after cloning the list to prevent from cache getting altered
            return {
                core: JSON.parse(JSON.stringify(core || [])),
                installed: JSON.parse(JSON.stringify(installed || []))
            }
        } catch(e) {
            console.error(e)
        }
    }

    /**
     * Load the current project as a package if the package.json contains codeflow config
     * this is only for assting in pacakge building
     * @param {*} basePath
     * @param {*} forClient
     */
    async loadSelf (basePath, forClient) {
        let pckJson = path.join(basePath, 'package.json')
        try {
            if(!fs.existsSync(pckJson)) { return null }
            let json = JSON.parse(fs.readFileSync(pckJson).toString())
            if(!json || !json.codeflow || ! json.codeflow.packageId ) { return null }

            let packageInfo = await loadPackageInfo(basePath, 'codeflow', {relativePath: '', for_client: forClient})
            if(!packageInfo) { return null }

            packageInfo.root_path = basePath
            packageInfo.path = '.'
            await listModules(packageInfo)
            return packageInfo
        } catch(e) {
            return null
        }
    }

    _hash(str) {
        return crypto.createHash('md5').update(str).digest('hex')
    }

    cacheSystemPackages(packages) {
        packages.forEach(pkg=>{
            pkg.modules.forEach(module=>{
                if (module.type && module.type == "folder") {
                    this.cacheSystemPackages([module])
                } else {
                    this.systemCache[module.ref] = module
                }
            })
        })
    }

    getCachedSystemPackage(ref) {
        return this.systemCache[ref]
    }

    cachePackages(basePath, packages) {
        var key = this._hash(basePath)
        this.packageCache[key] = packages
    }
    getCachedPackages(basePath) {
        var key = this._hash(basePath)
        return this.packageCache[key] || null
    }

    cacheModule(basePath, module) {
        basePath = path.normalize(basePath)
        var key = this._hash(basePath + '@ref://' + module.ref)
        this.moduleCache[key] = module
        return module
    }

    getCachedModule(basePath, ref) {
        basePath = path.normalize(basePath)
        var key = this._hash(basePath + '@ref://' + ref)
        return this.moduleCache[key] || null
    }

    /**
     * Method to use while installing and uninstalling packages for cache handling
     *
     * @param {*} basePath
     * @param {*} useCache
     * @returns
     */
    async loadInstalled(basePath, clearCache) {
        try {
            let installed = this.getCachedPackages(basePath)
            if(clearCache || !installed) {
                //forClient flag is set to true since this will be called from the client
                installed = await this.listPackages(basePath, {for_client: true})
                this.cachePackages(basePath, installed)
            }
            return installed
        } catch(e) {
            throw e
        }
    }

    /**
     * Check for package updates
     * @param {*} basePath
     * @returns
     */
    async checkUpdates(basePath) {
        try {
            let installed = await this.loadInstalled(basePath)
            if(!installed || installed.length < 1) { return null }

            let hasUpdates = []
            let checks = installed.map(pkg=>new Promise(resolve=>{
                packageLib.details([pkg.name], function(err, packageCurr) {
                    if (err) {
                        return resolve()
                    }
                    if (pkg.version != packageCurr.version && semver.lt(pkg.version, packageCurr.version)) {
                        pkg.update = {
                            version: packageCurr.version,
                            installed_version: pkg.version,
                            description: packageCurr.description,
                            updated_on: packageCurr.updated_on
                        }
                        hasUpdates.push(pkg)
                    }
                    return resolve()
                })
            }))

            await Promise.all(checks)
            return hasUpdates
        } catch(e) {
            throw e
        }
    }

    /**
     * List the installed packages and their modules in the basePath folder,
     * which will be an absolute path, there can be an optional relative path to
     * process a single package
     *
     * @param basePath path which will be processed to find modules and packages
     * @param options object {
     *                  relativePath: path which will be relative to the basePath where the modules will be looked for (this is used only for library module lookup)
     *                  type: type of the the packages to be loaded, this can be {TYPE_CORE}||{TYPE_INSTALLED}
     *                }
     */
    async listPackages(basePath, options) {
        options = options || {}
        if(!basePath) {
            return
        }
        let packagesPath = path.join(basePath, PACKAGE_INSTALL_FOLDER)
        if (options.relativePath) {
            packagesPath = path.join(packagesPath, options.relativePath)
        }
        if (!fs.existsSync(packagesPath)) {
            return
        }
        let packages = []
        try {
            let pkgFolders = []

            let {files, tree: fileTree} = await fsUtil.listFileTree(packagesPath, null, {level: 5})

            Object.keys(fileTree).forEach(ns=>{
                if(typeof fileTree[ns] !== 'object') { return }
                Object.keys(fileTree[ns]).forEach(pkg=>{
                    if(typeof fileTree[ns][pkg] !== 'object') { return }
                    pkgFolders.push({
                        path: path.join(packagesPath, ns, pkg),
                        ns: ns,
                        folder: pkg
                    })
                })
            })

            let clonedCore = []
            packages = await Promise.all(pkgFolders.map(pkg=>{
                return new Promise(async resolve=>{
                    let packageInfo = await this.loadPackageInfo(pkg.path, pkg.ns, options)
                    packageInfo.root_path = packagesPath
                    try {
                        await this.listModules(packageInfo)
                    } catch(e) {
                        console.error(e)
                    }
                    /*if (options.type === TYPE_CORE && pkg.folder !== 'core') {
                        let _packageInfo = util.deepClone(packageInfo)
                        _packageInfo.namespace = pkg.folder
                        _packageInfo.hidden = true
                        clonedCore.push(_packageInfo)
                    }*/
                    resolve(packageInfo)
                })
            }))
            clonedCore && clonedCore.map(item=>{
                packages.push(item)
            })
            //cachePackages(packages, packagesPath)
            return this.sortPackages(packages)
        } catch(e) {
            console.error(e)
        }
        return packages
    }

    /**
     * Load package data from the package path
     * @param {*} pckgPath
     * @param {*} parentFolder
     * @param {*} options
     * @returns
     */
    async loadPackageInfo(pckgPath, parentFolder, options) {
        var packageFolder = path.basename(pckgPath)
        var namespacePrefix = (options.namespace || parentFolder) + "/"
        if (options.type === TYPE_CORE) {
            if (packageFolder !== 'core') {
                namespacePrefix = 'core/'
            } else {
                namespacePrefix = ''
            }
        }
        var namespace = namespacePrefix + packageFolder
        let packageInfo = {
            name: path.basename(pckgPath),
            path: pckgPath,
            modules: [],
            namespace: namespace, //for core packages shipped along with engine, namespace shouldn't be added
            version: "*",
            type: options.type || TYPE_INSTALLED
        }

        let pckgJsonFile = path.join(pckgPath, 'package.json')
        if(!fs.existsSync(pckgJsonFile)) {
            return packageInfo
        }

        try {
            let data = await fsUtil.readJsonFile(pckgJsonFile) || {}
            //check if its a published module
            if (data.codeflow) {
                packageInfo.name = data.name
                packageInfo.codeflow = data.codeflow
                packageInfo.version = data.version
            }
            packageInfo.sort_index = typeof data.codeflow_sort_index != 'undefined' ? data.codeflow_sort_index : 10
            packageInfo.hidden = (typeof data.codeflow_hidden_package != 'undefined' && data.codeflow_hidden_package !== false)

            if(!options.for_client) {
                return packageInfo
            }

            if (data.icon) {
                packageInfo.icon = data.icon
                return packageInfo
            }
            //check for the icon the package root folder, checks only for svg and png files with name 'icon'
            let iconFile = path.join(pckgPath, 'icon.svg')
            let iconType = 'data:image/svg+xml;base64'
            if(!fs.existsSync(iconFile)) {
                iconFile = path.join(pckgPath, 'icon.png')
                iconType = 'data:img/png;base64'
            }
            if(fs.existsSync(iconFile)) {
                let iconData = await fs.readFileSync(iconFile)
                packageInfo.icon = `${iconType},${iconData.toString('base64')}`
            }
        } catch(e) {}

        return packageInfo
    }

    sortPackages(packages) {
        if(!packages || packages.length < 1) { return packages }
        packages = _.sortBy(packages, function(pckg) {
            return pckg.sort_index ? pckg.sort_index : 0
        })

        _.each(packages, pckg=> {
            this.sortModules(pckg.modules)
        })
        return packages
    }
    sortModules(modules) {

        return modules.sort((a, b) =>{
            if(a.modules) {
                this.sortModules(a.modules)
            }
            if(b.modules) {
                this.sortModules(b.modules)
            }
            if(!a.meta || !b.meta) {
                return 0
            }
            let aname = a.meta.name
            let bname = b.meta.name
            let aindex = a.meta.sort_index || 0
            let bindex = b.meta.sort_index || 0
            //sort by predefined index and move trigger steps to top
            if(a.type === 'trigger' || b.type === 'trigger') {
                return a.type === 'trigger' ? -1 : 1
            }

            let sortName = aname > bname ? 1 :
                                aname < bname ? -1 : 0

            return aindex > bindex ? 1 :
                                aindex < bindex ? -1  : sortName
        })
    }

    /**
     * List all the modules inside a package path (core/installed)
     * @param {*} packageInfo
     */
    async listModules(packageInfo) {
        let moduleFiles = []
        if (packageInfo.codeflow && packageInfo.codeflow.modules) {
             moduleFiles = packageInfo.codeflow.modules.map(module=>path.join(packageInfo.path, module))
        } else {
            let _files = await fsUtil.listFileTree(packageInfo.path)
            moduleFiles = _files.files
        }

        await Promise.all(moduleFiles.map(async moduleFile=>{
            return new Promise(async resolve=>{
                let ext = path.extname(moduleFile)
                if (ext.toLowerCase() != EXT_MODULE_OLD && ext.toLowerCase() != EXT_MODULE && ext.toLowerCase() != EXT_FLOW) {
                    return resolve()
                }
                try {
                    await this.loadModuleInfo(moduleFile, packageInfo)
                } catch(e) {
                    console.error(e)
                }
                resolve()
            })
        }))
        await this.processDynamicModuleFile(packageInfo)
    }

    /**
     * Process Dynamic modules by calling modules.js file if it exists in the package root
     * @param {*} packageInfo
     * @returns
     */
    async processDynamicModuleFile(packageInfo) {
        try {
            let file
            try {
                file = await fs.promises.stat(path.join(packageInfo.path, DYNAMIC_MODULE_FILE))
                if(!file.isFile()) { return }
            } catch(e) {
                //throw e
                return
            }
            let handle
            try {
                handle = await util.loadModuleSource(path.join(packageInfo.path, DYNAMIC_MODULE_FILE))
            } catch(e) {
                console.error(e)
            }
            let modules = handle
            if(handle && typeof handle === 'function') {
                modules = await handle.call(null)
            }

            Array.isArray(modules) && modules.forEach(module=>{
                this.postProcessModule(module, packageInfo)
                //packageInfo.modules.push(modules)
            })
            packageInfo.modules = modules
        } catch(e) {
            console.error(e)
            return e.message
        }
    }

    /**
     * Load module definition from the module file
     * @param {*} moduleFile
     * @param {*} packageInfo
     * @returns
     */
    async loadModuleInfo(moduleFile, packageInfo) {
        //read the package.json for the project
        let moduleData = await fsUtil.readJsonFile(moduleFile)
        if (!moduleData) {
            return
        }
        //if the module file is a flow file, parse the config to prepare a temporary module info
        if (path.extname(moduleFile) === '.flw') {
            var steps = moduleData['steps'] || moduleData['activities']
            let start, end
            Object.keys(steps).forEach(id=>{
                if(!start && (steps[id].type == Constants.TRIGGER_TYPE || steps[id].ref === Constants.START_REF)) {
                    start = steps[id]
                }
                if(!end &&  steps[id].ref === Constants.END_REF) {
                    end = steps[id]
                }
            })
            if(start.type == Constants.TRIGGER_TYPE) {
                try {
                    let moduleDef = await this.getModuleDefinition(start.ref, moduleFile)
                    if (!start['input-schema'] && moduleDef && moduleDef['input-schema']) {
                        start['input-schema'] = start['input-schema']
                    }
                } catch(e) {}
            }
            //create the module's data from the flow
            var meta = moduleData.meta || {}
            var module = {
                source: moduleFile,
                'input-schema': start['input-schema'],
                'output-schema': end['input-schema'],
                meta: {
                    name: path.basename(moduleFile, path.extname(moduleFile)),
                    version: '0.0.1',
                    inherits: meta.inherits,
                    isInterface: meta.isInterface,
                    description: meta.description || ''
                }
            }
            if (start.type === Constants.TRIGGER_TYPE) {
                module.type = Constants.TRIGGER_TYPE
            }
            //expand annotation schema
            if (meta.annotationSchema) {
                try {
                    let schema = await fsUtil.readJsonFile(path.join(packageInfo.path, meta.annotationSchema))
                    module.meta.annotationSchema = schema
                } catch(e) {}
            }
        } else {
            module = moduleData
            if (!module.source) {
                //throw new Error("Invalid module definition file. Missing source.")
                return null
            }
            module.source = path.join(path.dirname(moduleFile), module.source)
        }
        return this.postProcessModule(module, packageInfo, moduleFile)
    }

    /**
     * Process module to add package data and ref (if missing)
     * @param {*} module
     * @param {*} packageInfo
     * @param {*} moduleFile
     * @returns
     */
    postProcessModule(module, packageInfo, moduleFile) {
        let isDynamic = !moduleFile
        if(module.type !== 'folder') {
            module.package = packageInfo.namespace
            if(module.source) {
                module.source = isDynamic ? path.join(packageInfo.path, module.source) : module.source
                module.source = path.relative(packageInfo.root_path, module.source)
            }
            module.path = packageInfo.root_path
            module.packagePath = packageInfo.path
            module.packageType = packageInfo.type
        }
        //dynamic modules
        if(isDynamic) {
            if(module.type === 'folder') {
                return module.modules.map(subModule=> {
                    this.postProcessModule(subModule, packageInfo)
                })
            }
            // ref_is_final is a temporary flag used to indicate that the module is already having a finalized ref value
            if(module.ref_is_final) {
                delete module.ref_is_final
            } else {
                module.ref =  packageInfo.namespace +'/'+ module.ref
            }

            //remove the .packages dir from cached hash as it will make the lookup easier
            this.cacheModule(path.dirname(module.path), module)
            return module
        }

        let modulePath = path.relative(packageInfo.path, moduleFile)
        if (!module.ref && modulePath) {
            var refs = [packageInfo.namespace]
            if(path.dirname(modulePath) != '.') {
                refs.push(path.dirname(modulePath).replace('\\', '/'))
            }
            refs.push(path.basename(moduleFile, path.extname(moduleFile)).toLowerCase())
            module.ref =  refs.join('/')
        }

        if(!module.ref) {
            return
        }

        //remove the .packages dir from cached hash as it will make the lookup easier
        this.cacheModule(path.dirname(module.path), module)

        if (modulePath && modulePath.indexOf(path.sep) > -1) { //@toCheck is this still needed??
            let modTree = modulePath.split(path.sep).slice(0, -1)
            let moduleList = packageInfo.modules
            //group modules using the folder name
            modTree.map(folderName=>{
                //check if the folder is already defined
                let folder = moduleList.find(item=> item.type == 'folder' && item.meta.name == folderName)
                //create new folder if not exists
                if (!folder) {
                    folder = {type: 'folder', meta: {name: folderName}, modules: []}
                    moduleList.push(folder)
                }
                //set the module list as the last folder's modules stack
                moduleList = folder.modules
            })
            //add module information to the last identified folder
            moduleList.push(module)
        } else {
            packageInfo.modules.push(module)
        }
    }

    /**
     * Get the paths in which to look up for a module
     * @param currPath
     * @returns
     */
    getModuleLookupPaths(currPath) {
        var key = this._hash(currPath)
        var cached = lookupPathCache[key]
        if (cached) {
            return cached
        }
        var parts = currPath.split(path.sep)
        var paths = parts.map( (v, i)=>{
                            return (i===0 ? parts : parts.slice(0, i*-1)).join(path.sep)
                        }).filter(p=>p!=='')

        //add the engine core path for packages
        //paths.unshift(CORE_PACKAGES_PATH)
        lookupPathCache[key] = paths
        return paths
    }

    /**
     * Lookup moduel definition using the ref and project/file current step's path
     * @param {*} ref
     * @param {*} currPath
     * @returns
     */
    async getModuleDefinition(ref, currPath) {
        var cached = null
        //check in system cache
        cached = this.getCachedSystemPackage(ref)
        if (!cached) {
            cached = this.getCachedModule(currPath, ref)
        }
        if(!cached) {
            cached = this.getCachedModule(path.dirname(currPath) || '', ref)
        }
        if (cached) {
            return cached
        }

        return this.lookupModuleDefinition(ref, currPath)
    }

    /**
     * If getModuleDefinition couldn't find module in cache, perform a lookup in folder hirechy
     * @param {*} ref
     * @param {*} currPath
     * @returns
     */
    lookupModuleDefinition(ref, currPath) {
        let moduleLookupPaths = this.getModuleLookupPaths(currPath)
        let cached = moduleLookupPaths.reduce((cached, path)=>{
            return cached || this.getCachedModule(path, ref)
        }, null)
        if (cached) {
            return cached
        }

        return new Promise((resolve, reject)=>{
            var moduleInfo = null
            util.iterate(moduleLookupPaths, (lpath, next) => {
                //check if path is already loaded, if yes check the module cache for the lpath
                if(this.packagePathsLoaded.includes(lpath)) {
                    moduleInfo = this.getCachedModule(lpath, ref)
                    next()
                }
                if (moduleInfo) { return next(null, true) }

                //load packages for the path and check for cache
                this.listPackages(lpath, {}).then(()=> {
                    moduleInfo = this.getCachedModule(lpath, ref)
                    this.packagePathsLoaded.push(lpath)
                    return next(null, !!moduleInfo)
                })
            }, function(e) {
                if (moduleInfo) {
                    return resolve(moduleInfo)
                }
                return reject(e || new Error("Could not find definition for " + ref))
            })
        })
    }

    /**
     * Run a module method in the source js file from within the main process
     * @param {*} method
     * @param {*} input
     * @param {*} options
     * @returns
     */
    runModuleMethodLocal(method, input, options) {
        return new Promise((resolve, reject)=>{

            let def = options.step
            method = method || def.method

            const onError = (e) =>{
                if(method === 'applyInputFilter') {
                    return resolve({inputSchema: def['input-schema'], outputSchema: def['output-schema']})
                }
                return reject(e)
            }
            let jsFile = path.join(def.path, def.source)
            let source
            try {
                source = require(jsFile)
            } catch(e) {
                console.error(e)
                return onError(e)
            }

            if(!source || !source[method]) {
                return onError(new Error(`method ${method} not defined`))
            }

            source[method](input, (err, output)=>{
                if(err) {
                    return onError(err)
                }

                resolve(output)
            }, options)
        })
    }

    /**
     * * Run a module method in the source js file in a child process
     * @param {*} data
     * @returns
     */
    runModuleMethod(data) {
        return new Promise(async (resolve, reject)=>{
            try {
                let def = await this.getModuleDefinition(data.ref, data.project_path)
                if(!def) {
                    throw new Error('Module not found')
                }

                if(data.ref.indexOf('codeflow/') < 0 && data.ref.indexOf('core/') < 0) {
                    throw new Error('Operation not allowed')
                }

                let options = {
                    step: JSON.parse(JSON.stringify(def)),
                    ref: data.ref,
                    packageName: def.package,
                    stepname: data.name || data.step_id,
                    stepId: data.step_id,
                    _stepId: data._step_id,
                    filename: data.filename,
                    project_path: data.project_path,
                    trigger_field: data.trigger_field,
                    input_orig: data.input_orig //un evaluated input, because expressions will be nullified and the corresponsing keys won't be present in data.input
                }

                //run locally instead of child process, for dev mode,
                //if(process.versions.electron && process.env.TERM_PROGRAM && data.method) {
                     //return this.runModuleMethodLocal(data.method, data.input, options).then(resolve, reject)
                //}


                const childProcess = this.getChildProcess(data.project_path)
                let messageId = util.getUniqueId()
                this.childProcessCb[messageId] = {
                    success: resolve,
                    error: (e)=>{
                        if(data.method === 'applyInputFilter') {
                            return resolve({inputSchema: def['input-schema'], outputSchema: def['output-schema']})
                        }
                        reject(e)
                    }
                }
                childProcess.send({input: data.input, method: data.method, options, messageId})

            } catch(e) {
                reject(e)
            }
        })
    }

    /**
    *
    * @param {object} data
        *       filename: {string}
                ref: {string},
                step_id: {string},
                _step_id: {string},
                input: {object},
                input_field: {string}
                project_path: {string}
                skip_evaluation: {boolean}
    */
    async applyInputFilter(data) {
        let input = data.input
        if(!data.skip_evaluation) {
            try {
                data.input = Eval.evalObject(data.input, {}, `${data.step_id}-${Date.now()}` )
                data.input_orig = input
            } catch(err) {
                throw err
            }
        }

        data.method = 'applyInputFilter'
        try {
            let result = await this.runModuleMethod(data)
            if(result.inputSchema && !result.input) {
                try {
                    result.input = this.pruneInputSchema(result.inputSchema, input)
                } catch(e) {}
            }
            return result
        } catch(e) {
            console.error(e.message)
            return null
        }
    }

    /**
     * validate an input against a given schema and remove undefined fields
     * @param {*} schema
     * @param {*} input
     */
    pruneInputSchema(schema, input) {
        if(typeof schema !== 'object' || !input) {
            return input = null
        }
        switch(schema.type) {
            case 'object':
                if(typeof schema.additionalProperties === 'undefined' || schema.additionalProperties) {
                    return input
                }
                if(typeof input !== 'object') {
                    return input
                }
                typeof input === 'object' && schema.properties && Object.keys(input).forEach(key=>{
                    if(!schema.properties[key] && key.indexOf('@') !== 0) {
                        return delete input[key]
                    }
                    this.pruneInputSchema(schema.properties[key], input[key])
                })
            break
            case 'array':
                if(typeof input === 'string') {
                    return input
                }
                if(!Array.isArray(input)) {
                    input = []
                }
                input.forEach(item=>{
                    if(!item['#item']) { return }
                    this.pruneInputSchema(schema.items, item['#item'])
                })
            break
            default:
                return input
        }
        return input
    }

    /**
     *
     * @param {object} data
     *          step_id: {string}
     *          ref: {string}
     *          name: {string} step name
     *          input: {object}
     *          project_path: {string}

     */
    async runModule(data) {
        try {
            let result = await this.runModuleMethod(data)
            return result
        } catch(e) {
            let err = e.message || e.toString()
            if(err.indexOf('TypeError:') > 1) {
                err = err.split('TypeError:')[0]
            }
            let error = err.length > 300 ? err.substring(0, 300) + '...' : err
            throw new Error(error)
        }
    }

    logPackages(system, installed) {

        // derive the package and modules schema data
        let packageFields = []
        let moduleFields = []
        const iterateModules = function (modules) {
            modules.map(item=>{
                Object.keys(item).forEach(k=>{
                    let info = moduleFields.find(i=>i.name=== k)
                    if(!info) {
                        info = {
                            name: k,
                            type: typeof item[k],
                            examples: []
                        }
                        moduleFields.push(info)
                    }
                    if(info.examples.length < 5 && ['input-schema', 'output-schema', 'modules'].indexOf(k) < 0) {
                        if(!info.examples.find(i=>i==item[k])) {
                            info.examples.push(item[k])
                        }
                    }
                })
                if(item.modules) {
                    iterateModules(item.modules)
                }
            })
        }
        const iteratePackages = function(packages){
            packages.map(item=>{
                Object.keys(item).forEach(k=>{
                    let info = packageFields.find(i=>i.name=== k)
                    if(!info) {
                        info = {
                            name: k,
                            type: typeof item[k],
                            examples: []
                        }
                        packageFields.push(info)
                    }
                    if(info.examples.length < 5 && ['modules'].indexOf(k) < 0) {
                        if(!info.examples.find(i=>i==item[k])) {
                            info.examples.push(item[k])
                        }
                    }
                })
                iterateModules(item.modules)
            })
        }

        try {
            iteratePackages(system)
            iteratePackages(installed)
        } catch(e) {
            console.error(e)
        }
        let http = system.find(i=>i.name ==='http')
        let router = http.modules.find(m=>m.meta.name === 'router').modules.find(m2=>m2.meta.name === 'routeInterface')
        console.log(JSON.stringify(router))
        //console.log(system, installed)
        //console.log(packageFields, moduleFields)
    }
}

/*process.on('unhandledRejection', function(reason, p){
    console.log('unhandled rejection')
    console.log(reason)
    console.log(p)
})
*/

let Pkg = new Package()
module.exports = {
    loadPackages: async(basePath, cb, forClient=false)=>{
        try {
            let packages = await Pkg.loadPackages(basePath, forClient)
            cb(null, packages)
        } catch(e) {
            cb(e)
        }
    },
    listInstalled: async(basePath, cb, clearCache)=>{
        try {
            let installed = await Pkg.loadInstalled(basePath, true)
            cb(null, JSON.parse(JSON.stringify(installed)))
        } catch(e) {
            cb(e)
        }
    },
    getModuleDefinition: async (ref, currPath, cb) =>{
        try {
            let def = await Pkg.getModuleDefinition(ref, currPath)
            cb(null, def)
        } catch(e) {
            cb(e)
        }
    },
    applyInputFilter: async(data, cb)=>{
        try {
            let result = await Pkg.applyInputFilter(data)
            cb(null, result)
        } catch(e) {
            cb(e)
        }
    },
    runModule: async(data, cb)=>{
        try {
            let result = await Pkg.runModule(data)
            cb(null, result)
        } catch(e) {
            cb(e)
        }
    },
    loadInstalled: (basePath, clearCache)=>{
        return Pkg.loadInstalled(basePath, clearCache)
    },
    removeChildProcess: (cwd)=>{
        return Pkg.removeChildProcess(cwd)
    }
}