/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

module.exports = {
	DEFAULT_DEBUGGER_PORT: 12340,
	EXT: 'flw',
	GROUP_ID_SEPARATOR: '-',	
	TRIGGER_TYPE : 'trigger',
	GROUP_TYPE : 'group',
	START_REF : 'core/start',
	END_REF : 'core/end',
	ERROR_REF : 'core/error',
	PACKAGES_FOLDER : '.packages'
}