/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var _ = require('underscore')
var util = require('./util')
var Instance = require('./instance')
var Loader = require('./loader')
var Agent = require('./debugger/agent')
var _Error = require('./error')
var Serializer = require('./debugger/serializer')
var Status = require('./status')
var assert = require('assert')
var step = require('./step')

var ROOT_DIR = global.ROOT_DIR || process.cwd()
var MAX_DEBUG_INSTANCES = 50

/**
 * Class Engine
 * @param {*} props to flag the engine during startup
 */
function Engine(props) {
    this.isDebugMode = !!props.debug
    this.debugChannel = props.debug === 'tcp' ? 'tcp' : 'ipc'
    this.rootInstances = {}
    this.validationEnabled = false
    if (this.isDebugMode) {
        this.debugAgent = Agent.getInstance(this)
        //to debounce sendUpdate()
        this.pendingUpdate = 0
        this.lastUpdateTimer = null
    }
    this.bpOverride = {}
    this.listeners = {}
    this.isInitialized = false
    this.id = util.uuid()
}

Engine.prototype.init = function(cb) {
    var self = this
    if (this.debugAgent) {
        this.debugAgent.init(function() {
            //debugging is disabled for cluster mode workers
            if(process.env['NODE_UNIQUE_ID'] || process.env['ENV_CLUSTER_WORKER']) {
                console.error('Cannot activate debugger in worker mode')

                global._CF_DEBUG_ENABLED = false
                self.isDebugMode = false
                self.debugAgent = false
            }
            self._load(cb)
        })
    } else {
        self._load(cb)
    }
}

Engine.prototype._load = function(cb) {
    this.isInitialized = true
    Loader.loadPackages(cb)
}

Engine.prototype.emit = function(event, data) {
    var eventListeners = this.listeners[event] || []
    for (var i = 0, length = eventListeners.length; i < length; i++) {
        var fn = eventListeners[i]
        if (typeof fn == 'function') {
            fn(data)
        }
    }
}

Engine.prototype.listen = function(event, fn) {
    if (!this.listeners[event]) {
        this.listeners[event] = []
    }
    this.listeners[event].push(fn)
}

Engine.prototype.getDebugAgent = function() {
    return this.debugAgent
}

Engine.prototype.sendSetFocus = function(instance, step, sessionId) {
    var data = {
        instance: instance.id,
        step: step ? step.id: null
    }
    //@TODO why sending update?
    this.sendUpdate(data, sessionId)
    this.debugAgent.sendMessage('focus', data, sessionId)
}

Engine.prototype.sendBpNotify = function(instance, step, sessionId) {
    var data = {
        instance: instance.id,
        instanceFile: instance.flow.fileName,
        step: step ? step.id: null,
        stepName: step && step.name ? step.name: step.id
    }
    this.debugAgent.sendMessage('breakPoint', data, sessionId)
}

/**
 * Debounced sendUpdate
 */
Engine.prototype.sendUpdate = function(data, sessionId) {
    var self = this
    if (this.pendingUpdate == 0) {
        self.debugAgent.sendMessage('update')
    } else if (!this.lastUpdateTimer) {
        this.lastUpdateTimer = setTimeout(function() {
            self.pendingUpdate = 0
            self.lastUpdateTimer = null
            self.debugAgent.sendMessage('update')
        }, 100)
    }
    this.pendingUpdate++
}

Engine.prototype.sendStack = function(sessionId) {
    var stackTree = this.serializeInstances()
    this.debugAgent && this.debugAgent.sendMessage('stack', stackTree, sessionId)
}

Engine.prototype.sendData = function(data, sessionId) {
    this.debugAgent && this.debugAgent.sendMessage('data', data, sessionId)
}

Engine.prototype.sendInstances = function(instances, sessionId) {
    this.debugAgent && this.debugAgent.sendMessage('instances', instances, sessionId)
}

Engine.prototype.getId = function() {
    return this.id
}

Engine.prototype.serializeInstances = function() {
    var self = this
    var fileInstances = {}
    _.each(this.rootInstances, function(instance) {
        if (!instance) {
            return
        }
        if (!instance.parent) {
            var file = instance.flow.fileName
            if(process.platform === 'win32') {
                file = require('./fsUtil').toUnixPath(file)
            }
            if (!fileInstances[file]) {
                fileInstances[file] = []
            }
            self.trimSubInstances(instance)
            fileInstances[file].push(Serializer.serializeInstance(instance))
        }
    })
    return fileInstances
}

Engine.prototype.clearInstances = function() {
    this.rootInstances = {}
}

Engine.prototype.getInstances = function() {
    return this.rootInstances
}

/**
 * Resolve a id in the format x.x.x.x to correct instance, traversing
 * from the root.
 */
Engine.prototype.getInstance = function(instanceId) {
    var ids = instanceId ? instanceId.split('.') : []
    var instance
    var map = this.rootInstances
    var prevId
    //dive into subinstances to find the match
    while (ids.length) {
        var id = (prevId ? prevId + '.' : '') + ids.shift()
        instance = map ? map[id] : null
        if (instance) {
           map = instance.subInstances
        } else {
            console.error("Instance with Id " + instanceId + " not found")
            break
        }
        prevId = id
    }
    return instance
}

/**
 * Only required for debugging
 */
Engine.prototype.addInstance = function(instance) {
    var parent = instance.parent
    if (parent != null) {
        parent.addSubInstance(instance)
        this.trimSubInstances(parent)
    } else {
        this.rootInstances[instance.id] = instance
    }
}

Engine.prototype.trimSubInstances = function(instance) {
    var subInstances = instance.subInstances
    var keys = Object.keys(subInstances)
    var len = keys.length
    if (len > MAX_DEBUG_INSTANCES) {
        var toTrim = len - MAX_DEBUG_INSTANCES
        for (var i = 0; i < toTrim; i++) {
            var id = keys[i]
            delete subInstances[id]
        }
    }
}

//in-memory breakpoint status to set/unset breakpoints without reloading flow
//files
Engine.prototype.getBreakpointOverride = function(file, stepId) {
    return this.bpOverride[file] ? this.bpOverride[file][stepId] : null
}

Engine.prototype.setBreakpointOverride = function(file, stepId, status) {
    if (!this.bpOverride[file]) {
        this.bpOverride[file] = {}
    }
    this.bpOverride[file][stepId] = status
}

Engine.prototype.loadFlow = function(options, cb) {
    return Loader.loadFlow(options, cb)
}

Engine.prototype.loadResource = function(path, cb) {
    return Loader.loadResource(path, cb)
}

Engine.prototype.makeError = function(message, code, data, parent) {
    return new _Error(message, code, data, parent)
}


Engine.prototype.execute = function(params, parentInstance, parentStep, cb) {
    var self = this
    assert(this.isInitialized, "Engine is not initialized. Call .init() prior to execute()")
    assert(params.flow || params.file, "Either a flow object or the file name should be passed")
    Loader.loadFlow(params, function(error, flow) {
        if (error) {
            if (params.callback) {
                return params.callback(error)
            } else {
                console.error(error)
            }
        } else {
            var instance = new Instance(self, flow, params, parentInstance, parentStep)
            //for trigger types, sub instances will be added
            if (self.isDebugMode) {
                self.addInstance(instance)
                if (instance.mode === Status.DEBUG_MODE.STEP) {
                    self.sendSetFocus(instance)
                }
            }
            instance.execute()
            return cb && cb({
                instance: instance,
                path: flow.fileName,
                startRef: flow.startStep.ref
            })
        }
    })
}

Engine.prototype.executeStep = function(params, parentInstance, parentStep, callback) {
    assert(this.isInitialized, "Engine is not initialized. Call .init() prior to execute()")


    let stepId = params.id
    let {id, ref, input, file} = params
    if(!id) {
        id = ref.split('/').pop()
    }
    let steps = {}
    steps[id] =  new step(id, {id, ref, name: id}, file)
    //set the step as the start & end
    // if start if not set the input passed in parameter will not be taken insted it will try to evaluate the input-binding in the step data
    steps[id].isStart = true
    steps[id].isEnd = true

    let instance = new Instance(this, {steps}, {callback,stack: {},input},parentInstance,parentStep)

    instance.stepMap[id] = {}
    instance.executeStep(id)
}

Engine.prototype.getCompiled = function(params, cb) {
    var CompiledInstance = Loader.getLoaded(params.name || params.file)
    if (CompiledInstance) {
        return cb(null, CompiledInstance)
    }
    Loader.loadFlow(params, function(error, flow) {
        if (error) {
            return cb(error)
        }
        Loader.loadCompiled(flow, params, function(err, CompiledInstance) {
            return cb(null, CompiledInstance)
        })
    })
}

Engine.prototype.executeOptimized = function(params, parentInstance, parentStep, cb) {
    var self = this
    assert(this.isInitialized,"Engine is not initialized. Call .init() prior to execute()")
    assert(params.flow || params.file, "Either a flow object or the file name should be passed")
    this.getCompiled(params, function(error, CompiledInstance) {
        if (error) {
            if (params.callback) {
                params.callback(error)
            } else {
                console.error(error)
            }
            return
        }
        var instance = new CompiledInstance(self, params, parentInstance, parentStep)
        //for trigger types, sub instances will be added
        if (self.isDebugMode) {
            self.addInstance(instance)
            if (instance.mode === Status.DEBUG_MODE.STEP) {
                self.sendSetFocus(instance)
            }
        }
        instance.start()
        return cb && cb({
            instance: instance,
            path: instance.flow.fileName,
            startRef: instance.flow.startStep.ref
        })
    })
}

module.exports = Engine