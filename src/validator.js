/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var SchemaValidator = require("json-schema")

exports.validate = function(data, schema) {
    return SchemaValidator.validate(data, schema)
}