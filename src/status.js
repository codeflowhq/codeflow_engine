/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

/**
 * Global constants
 */

var INSTANCE_STATUS = {
    READY: 1,  //when the instance is created
    RUNNING: 10,  //when a step starts executing - for triggers it's when the trigger actually happens
    CALLBACK: 20, //when the end is reached (callback is executed)
    CALLBACK_ERROR: 23, //when callback is executed with error
    COMPLETE: 30,  // when execution is over - ie. all steps & child instances have ended
    ERROR: 40
}

var DEBUG_MODE = {
    STEP: 1,
    ACTIVE: 5
}

/** Sequence is important as of now. Will be changed in future */
var STEP_STATUS =  {
    READY: 1,   //ready to execute
    READY_OVERRIDE: 2, //ready to execute, override breakpoints
    PAUSED_BEFORE: 5, //breakpoint before
    PROCESS_INPUT: 6,
    EVENT_WAITING: 7, //trigger step waiting for event
    EXECUTING: 10, //started 
    PAUSED_AFTER: 15, //breakpoint after
    CALLBACK: 20, //callback is executed. Note: The step may not actually complete here as there could be
    ERROR: 25
}

var STEP_SUB_STATUS =  {
    
}

var TR_STATUS = {
    SUCCESS: 2,
    FAIL: 5,
    NOT_REACHABLE: 7,
    UNHANDLED_ERROR_SPILLOVER: 8,
    ERROR: 10
}

exports.INSTANCE_STATUS = INSTANCE_STATUS
exports.STEP_STATUS = STEP_STATUS
exports.DEBUG_MODE = DEBUG_MODE
exports.TR_STATUS = TR_STATUS