/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var fs = require('fs')
var path = require('path')
var Package = require('./package')
var Flow = require('./flow')
var Eval = require('./eval')
var Util = require('./util')

var JsCompiler = null //delegating require for background compatibility with node <4.0

var flowCache  = {}
var compiledCache = {}
var resourceCache = {}
var packagesLoaded = false
var packagesLoading = false
var packagesLoadedCbs = []

var toPosixPath = function(pth) {
    if(!pth) { return null }
    return pth.split(path.sep).join(path.posix.sep)
}

var loadFlow = function(params, cb) {
    var file = toPosixPath(params.file)
    var name = params.name ? toPosixPath(params.name) : file
    var cached = flowCache[name]
    if (cached) {
        return cb(null, cached)
    }
    if (params.flow) {
        //set the cache and return
        return setFlowCache(name, params.flow, cb)
    }
    fs.readFile(file, {
        encoding: 'utf8'
    }, function(err, scriptData) {
        if (err) {
            return cb(new Error("Could not load flow file: " + file))
        }
        return setFlowCache(name, scriptData, cb)
    })
}

var loadResource = async function(path, cb) {
    return new Promise(async(resolve, reject)=>{
        const _return = (err, data)=> {
            if(cb) {
                return cb(err, data)
            }
            if(err) {
                return reject(err)
            }
            resolve(data)
        }

        if(resourceCache[path]) {
            return _return(null, resourceCache[path])
        }
        try {
            let fileData = await fs.promises.readFile(path,{encoding: 'utf8'})
            try {
                let resObj = JSON.parse(fileData)
                //evaluate for JST expressions
                let data = evaluateInputData(resObj, {}, 'resource-'+ path)
                resourceCache[path] = data
                return _return (null, data)
            } catch(e) {
                return _return(new Error("Error parsing resource file: " + path))
            }
        } catch(e) {
            return _return(new Error("Could not load resource file: " + path))
        }
    })
}
/**
 * Workaround method to load the reference in resounrce file and while testing a resource
 * @param {*} resObj
 * @param {*} data
 * @param {*} id
 * @returns
 */
var evaluateInputData = function(resObj, data, id) {
    //evaluate for JST expressions
    let evaluated = Eval.evalObject(resObj, data, id)
    return Util.mergeObjects(resObj, evaluated)
}

var loadPackages = function(callback) {
    if (packagesLoaded) {
        return callback()
    }
    if (packagesLoading) {
        return packagesLoadedCbs.push(callback)
    }
    packagesLoading = true
    Package.loadPackages(ROOT_DIR, function() {
        packagesLoading = false
        packagesLoaded = true
        callback()
        if (packagesLoadedCbs.length > 0) {
            for (var i = 0; i < packagesLoadedCbs.length; i++) {
                packagesLoadedCbs[i]()
            }
        }
    })
}

var loadCompiled = function(flow, params, cb) {
    var name = params.name || params.file
    var Instance = compiledCache[name]
    if (Instance) {
        return cb(null, Instance)
    }
    if (!JsCompiler) {
        JsCompiler = require('./compiler/js')
    }
    var js = new JsCompiler(flow, params)
    js.generate(function(err, Instance) {
        if (err) {
            return cb(err)
        }
        compiledCache[name] = Instance
        return cb(null, Instance)
    })
}

var setFlowCache = function(fileName, data, cb) {
    cb = cb || function() {}
    fileName = toPosixPath(fileName)
    new Flow().load(fileName, data, function(err, flow) {
        if (err) {
            return cb(err)
        }
        flowCache[fileName] = flow
        return cb(null, flow)
    })
}

var clearFlowCache = function(fileName) {
    fileName = toPosixPath(fileName)
    delete flowCache[fileName]
}

var isPackagesLoaded = function() {
    return packagesLoaded
}

var getLoaded = function(name) {
    return compiledCache[name]
}

module.exports = {
   isPackagesLoaded: isPackagesLoaded,
   loadFlow: loadFlow,
   loadResource: loadResource,
   loadPackages: loadPackages,
   getLoaded: getLoaded,
   loadCompiled: loadCompiled,
   setFlowCache: setFlowCache,
   clearFlowCache: clearFlowCache,
   evaluateInputData: evaluateInputData
}