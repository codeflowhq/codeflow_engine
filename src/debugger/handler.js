/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var Serializer = require('./serializer')
var Eval = require('../eval')
var Status = require('../status')
var Loader = require('../loader')
var Util = require('../util')
var _ = require('underscore')
var path = require('path')
var Logger = require('../logger')

var engineHandler = {
    updateFlow: function(engine, params) {
        if (Loader.isPackagesLoaded()) {
            var data = params.flowData
            var file = params.fileName
            if (file && data) {
                Loader.setFlowCache(file, data, function() {
                    Eval.purgeCache()
                })
            }
        }
    },
    updateBreakpoint: function(engine, params) {
        var stepId = params.stepId
        var file = params.fileName
        var status = params.status
        engine.setBreakpointOverride(file, stepId, status)
    },
    getCallStack: function(engine, params, sessionId) {
        engine.sendStack(sessionId)
    },
    getInstances: function(engine, params, sessionId) {
        var ids = params.instances
        var list = []
        _.each(ids, function(id) {
            list.push(Serializer.serializeInstance(engine.getInstance(id)))
        })
        //got the instances
        engine.sendInstances({
            _cId: params._cId,
            data: {
                instances: list
            }
        }, sessionId)
    },
    addFileToContext: function(engine, params, sessionId) {
        let filePath = path.join(process.cwd(), params.file)
        let existingInstance = _.find(engine.rootInstances, instance=>instance.params.file === filePath || instance.params.file === params.file)

        if(existingInstance && !existingInstance.stepsCompleted) {
            //allow the file to be executed if the existing instance is having errors
            if(!existingInstance.errors || existingInstance.errors.length === 0) {
                return Logger.warn(`Another instance of ${params.file} is already running`)
            }
        }
        //Logger.warn("addFileToContext")
        engine.execute({
            file: params.file,
            input: params.input,
            // options: options,
            callback: function(error, data) {
                //what to do?
            }
        })
    }
}

var instanceHandler = {
    getData: function(instance, params, sessionId) {
        var stepId = params.stepId
        if (stepId) {
            var data = instance.stack[stepId]
            var step = instance.getStepState(stepId)
            if (step) {
                instance.engine.sendData({
                    instance: instance.id,
                    step: stepId,
                    data: {
                        status: step.status,
                        data: Util.serialize(data),
                        error: Util.serialize(step.error)
                    }
                }, sessionId)
            }
        } else {
            instance.engine.sendData({
                instance: instance.id,
                data: Serializer.serializeStack(instance)
            }, sessionId)
        }
    },
    stepIn: function(instance, params) {
        var stepId = params.stepId
        var step = instance.getStepState(stepId)
        if (!step || step.status > Status.STEP_STATUS.PAUSED_BEFORE) {
            step = instance.peekQueued()
        }
        if (step) {
            step.mode = Status.DEBUG_MODE.STEP
            instanceHandler.stepOver(instance, params)
        }
    },
    stepOver: function(instance, params) {
        var stepId = params.stepId
        var stepState = instance.getStepState(stepId)
        if (!stepState || stepState.status > Status.STEP_STATUS.PAUSED_BEFORE) {
            stepState = instance.peekQueued()
        }
        if (stepState) {
            instance.mode = Status.DEBUG_MODE.STEP
            if (stepState && stepState.status == Status.STEP_STATUS.PAUSED_BEFORE) {
                var step = instance.getStep(stepId)
                if (!step) {

                }
                if (step && step.isEnd && instance.parent) {
                    instanceHandler.stepReturn(instance, params)
                } else {
                    stepState.execute()
                }
            }
        }
    },
    stepReturn: function(instance, params, sessionId) {
        var parent = instance.parent
        if (parent) {
            var parentStep = parent.getStepState(instance.parentStep)
            if (parentStep && parentStep.status == Status.STEP_STATUS.EXECUTING) {
                parentStep.status = Status.STEP_STATUS.PAUSED_AFTER
            }
        }
        instanceHandler.resumeAll(instance, params, sessionId, true)
        if (parent && parent.id != instance.rootId) {
            instance.engine.sendSetFocus(parent, sessionId)
        }
    },
    resume: function(instance, params, sessionId) {
        var stepId = params.stepId
        var stepState = instance.getStepState(stepId)
        if (!stepState || (stepState && stepState.status != Status.STEP_STATUS.PAUSED_BEFORE)) {
            stepState = instance.peekQueued()
        }
        if (stepState) {
            instance.mode = Status.DEBUG_MODE.ACTIVE
            if (stepState && stepState.status == Status.STEP_STATUS.PAUSED_BEFORE) {
                stepState.execute()
                instance.engine.sendSetFocus(instance, sessionId)
            }
        }
    },
    resumeAll: function(instance, params, sessionId, noFocus) {
        for (var i in instance.subInstances) {
            instanceHandler.resumeAll(instance.subInstances[i], params, sessionId, true)
        }
        var stepMap = instance.getStepMap()
        var numPaused = 0
        for (var a in stepMap) {
            if (stepMap[a].status == Status.STEP_STATUS.PAUSED_BEFORE) {
                stepMap[a].status = Status.STEP_STATUS.READY_OVERRIDE
                numPaused++
            }
        }
        instance.mode = Status.DEBUG_MODE.ACTIVE
        if (!noFocus) {
            //instance.engine.sendSetFocus(instance, sessionId)
        }
        process.nextTick(instance.processQueued.bind(instance))
        return numPaused
    },
    pause: function(instance, params, sessionId) { //unused
        var stepId = params.stepId
        if (stepId) {
            var step = instance.getStepState(stepId)
            if (step) {
                if (step.status < Status.STEP_STATUS.EXECUTING) {
                    step.mode = Status.DEBUG_MODE.PAUSED_BEFORE
                } else if (step.status == Status.STEP_STATUS.EXECUTING) {
                    step.mode = Status.DEBUG_MODE.PAUSED_AFTER
                }
            }
        } else {
            instance.mode = Status.DEBUG_MODE.STEP
        }
        instance.engine.sendSetFocus(instance, sessionId)
    },
    clear: function(instance, params, sessionId) {
        var engine = instance.engine
        var rootInstances = engine.getInstances()
        _.each(rootInstances, function(instance, id) {
            instance.subInstances = {}
        })
        engine.sendStack()
    }
}

module.exports = {
    engineHandler: engineHandler,
    instanceHandler: instanceHandler
}