/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var Path = require('path');

/**
 * Communication channel for debugging
 */
var protocolMap = {
    'ipc': 'ipc',
    /*'socket': 'socket',*/
    'tcp': 'tcp'
}

function Channel(protocol) {
    if (!protocolMap[protocol]) {
        throw new Error("Unsupported protocol : " + protocol)
    }
    var channelPath = './transport/' + protocol
    try {
        var channel = require(channelPath)
        this.channelInstance = new channel()
    } catch(e) {
        throw new Error("Unable to load debug channel protocol : " + protocol +  "; " + e)
    }
} 

Channel.prototype.init = function(cb) {
    this.channelInstance.init(cb)
}

Channel.prototype.send = function(data) {
    this.channelInstance.send(data)
}

Channel.prototype.onMessage = function(cb) {
    this.channelInstance.onMessage(cb)
}

module.exports = Channel