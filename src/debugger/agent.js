/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var Channel = require('./channel')
var Handler = require('./handler')

var DEFAULT_PROTOCOL = 'tcp' //'tcp' //'process'

function Agent(engine) {    
    this.engine = engine
    this.initialized = false
    this.channel = null
}

Agent.prototype.init = function(cb) {    
    var self = this
    if (this.initialized) {
        return cb && cb()
    }
    this.initialized = true
    this.channel = new Channel(this.engine.debugChannel || DEFAULT_PROTOCOL)
    this.channel.init(function(err) {
        if (err) {
            return cb(err)
        }
        self.channel.onMessage(function(msg) {
            if (msg && msg.cmd) {
                var instanceId = msg.params ? msg.params['instanceId'] : null
                if (instanceId != null) {
                    var instance = self.engine.getInstance(instanceId)
                    if (instance && Handler.instanceHandler[msg.cmd]) {
                        Handler.instanceHandler[msg.cmd](instance, msg.params, msg.sessionId)
                    }
                } else {
                    if (Handler.engineHandler[msg.cmd]) {
                        Handler.engineHandler[msg.cmd](self.engine, msg.params, msg.sessionId)
                    }
                }
            }
        })
        cb && cb()
    })    
}


Agent.prototype.sendMessage = function(cmd, data, sessionId) {
    this.channel.send({
        sessionId: sessionId,
        cmd: cmd,
        data: data
    })
}

var getInstance = function(engine) {
    return new Agent(engine)
}

module.exports = {
    getInstance: getInstance
}