/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var Util = require('../util')
var _ = require('underscore')

/**
 * Copy an instance to an object ready for serializing into JSON.
 */
function serializeInstance(instance) {
    var data = {}

    data.id = instance.id
    data.seqId = instance.seqId
    data.status = instance.status
    data.scope = instance.params.scope
    if (!instance.parent) {
        data.label = 'instance_0' //root instance
    }
    if (instance.params.scope) {
        data.label = instance.parentStep + "_" + instance.seqId
        data.file = instance.params.origFile || instance.params.file
    } else {
        data.file = instance.flow.fileName
    }
    if(data.file.indexOf('.grp') > -1) {
        data.file = data.file.replace('.grp', '')
    }
    
    if (process.platform === 'win32') {
        data.file = require('../fsUtil').toUnixPath(data.file)
    }
    //set trigger type if its root instance is a trigger type
    data.isTriggerType = instance.parent && instance.parent.flow.isTriggerType

    var subInstances = {}, cnt = 0, instanceMap = {}

    _.each(instance.subInstances, function(subInstance) {
        var serialized = serializeInstance(subInstance)
        //serialized.label = 'instance_' + cnt
        subInstances[subInstance.id] = serialized
        cnt++
        //store a map of subinstances to parent step
        var parentStep = subInstance.parentStep
        if (parentStep) {
            var arr = instanceMap[parentStep]
            if (!arr) {
                arr = instanceMap[parentStep] = []
            }
            arr.push(subInstance.id)
        } else {
            //something is wrong
        }
    })
    if (cnt > 0) {
        data.subInstances = subInstances
    }
    var item
    data.steps = {}
    _.each(instance.stepMap, function(step, id) {
        item = {
            id: step.id,
            status: step.status,
            tm: instance.stack[step.id] ? instance.stack[step.id].tm : null
        }
        if (step.error) {
            item.error = serializeError(step.error) //Util.serialize(step.error)
        }
        data.steps[id] = item
        //Map sub-instances that originated from the step here
        var subInstances = instanceMap[step.id]
        if (subInstances) {
            item.subInstances = subInstances
        }
    })

    var transitions = instance.flow.transitions
    data.transitions = []
    _.each(transitions, function(transition, idx) {
        var state = instance.transitionState[idx]
        if (state) {
            var trItem = {
                state: state,
                from: transition.from.id,
                to: transition.to.id
            }
            if (instance.transitionError[idx]) {
                trItem.error = instance.transitionError[idx]
            }
            data.transitions.push(trItem)
        }
    })

    if (instance.parent) {
        data.parent = {
            id: instance.parent.id,
            step: instance.parentStep
        }
        if (instance.params.scope) {
            //traverse until root and find all the parents, useful for rendering in client
            data.parents = traverseParents(instance)
        }
        data.isTriggerChild = instance.isTriggerChild
    }
    return data
}

function traverseParents(instance) {
    var tmp = instance.parent
    var list = []
    while (tmp) {
        list.push({id: tmp.id, scope: tmp.params.scope})
        tmp = tmp.parent
    }
    return list
}

/**
 * Serialize error
 */
function serializeError(error) {
    var errorObj = {
        message: error.message,
        code: error.code,
        path: error.path
    }
    if (error.data) {
        errorObj.data = Util.serialize(error.data)
    }
    return errorObj
}

/**
 * Serialize the stack data.
 */
function serializeStack(instance) {
    var stack = instance.stack
    var data = {}
    for (var k in stack) {
        if (instance.stepMap[k]) {
            var item = stack[k]
            data[k] = {
                status: instance.stepMap[k].status,
                data: Util.serialize(item)
            }
        }
    }
    return data
}

module.exports = {
    serializeStack: serializeStack,
    serializeInstance: serializeInstance
}