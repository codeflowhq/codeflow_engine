/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

/**
 * Debugger channel on top of the TCP protocol.
 * @TODO - Currently only a single client is supported. 
 */

var net = require('net')
var JsonSocket = require('json-socket')

var Logger = require('../../logger')
var util = require('../../util')
var Constants = require('../../constants')

var HOST = '127.0.0.1'
    
function Tcp(port) {
    this.socket = null
    this.port = port || Constants.DEFAULT_DEBUGGER_PORT
}

Tcp.prototype.init = function(cb, options) {
    var self = this
    options = options || {}

    var onData = options.onData 
    var onConnect = options.onConnect
    var onInit = options.onInit

    var server = net.createServer()

    server.on('listening', function() {
        Logger.error("Codeflow debugger listening on port ", self.port)
    })

    server.on('connection', function(socket) {
        socket = new JsonSocket(socket)
        if (!self.socket) {
            self.socket = socket
            socket.on('close', function() {
                self.socket = null
            })
            cb()
        } else {
            cb(new Error("Error: Multiple client connections are not yet supported."))
        }
    })
    server.on('error', function(e) {
        cb(e)
    })
    server.listen(self.port)
}

Tcp.prototype.onMessage = function(cb) {
    if (this.socket) {
        this.socket.on('message', function(data) {
            cb && cb(data)
        })
    }
}

Tcp.prototype.send = function(data) {
    if (this.socket) {
        this.socket.sendMessage(data)
    } else {
        Logger.log("Send error: no connection available.")
    }
}
  
module.exports = Tcp