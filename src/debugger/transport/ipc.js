/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

/**
 * Debug protocol based on simple message passing to parent process.
 * Can be used only when engine is invoked by a parent process.
 */
function Ipc() {
    
}

Ipc.prototype.init = function(cb) {
    cb()
}

Ipc.prototype.send = function(data) {
    process.send && process.send(data)
}

Ipc.prototype.onMessage = function(cb) {
    process.on('message', cb)
}
  
module.exports = Ipc