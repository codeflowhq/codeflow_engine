/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose(aliaseldhose@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

function Transition(id, props) {
    this.id = id
    this.condition = props.condition
    this.from = null
    this.to = null
    this.type = null
}

module.exports = Transition