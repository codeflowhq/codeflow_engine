/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var Context = require('./lib/expressionBuilder/src/context')
var Evaluator = require('./lib/expressionBuilder/src/evaluator')
var JstUtil = require('./lib/jst/src/util')
var JstEvaluator = require('./lib/jst/src/evaluator')
var JstCompiler = require('./lib/jst/src/compiler')
var Util = require('./util')

var _expressionCache = {}
var _jstCache = {}

var context = new Context()
var methodContext = context.getMethodContext()


/**
 * Eval a complete JST template object.
 *
 * @id To uniquely identify the JST template. If present,
 * the compiled template should be taken from cache rather
 * than interpreting at realtime.
 */
var evalObject = function(expression, data, id) {
    var result = null
    if (!id) {
        console.error(new Error("Warning: id not found in the arguments. Could not cache compiled script."))
    }
    if (expression) {
        data.CONFIG = global.CONFIG || {}
        data.global = global
        data.ENV = data.global.ENV = process.env
        //data.GLOBAL = global
        result = evalObjectWithContext(expression, data, id)
    }
    return result
}

/**
 * Evaluate a single expression. Used by transitions etc where complete object
 * (JST) evaluation is not needed.
 */
var evalExpression = function(expression, data) {
    var result
    if (expression) {
        //TODO
        data.CONFIG = global.CONFIG || {}
        data.global = global
        data.ENV = data.global.ENV = process.env

        if (typeof expression == 'object') {
            expression = JSON.stringify(expression)
        }
        if (!_expressionCache[expression]) {
            _expressionCache[expression] = Evaluator(expression)
        }
        result = _expressionCache[expression].call(null, methodContext, context.getExecutionContext(data))
    }
    return result
}

/**
 * Evaluate an expression
 */
var evalObjectWithContext = function (expression, data, id) {
    var object = null
    //cache preprocessed template.
    var jst = id ? _jstCache[id] : null
    if (!jst) {
        object = Util.deepClone(expression)
        jst = JstUtil.preProcess(object)
        if (id) {
            _jstCache[id] = jst
        }
    }
    var evaluator = new JstEvaluator(context.getExecutionContext(data), methodContext)
    return evaluator.evaluate(jst)
}

/**
 * Experimental compiled eval. 10x performance improvement over plain eval
 */
var compiledEvalObjectWithContext = function (expression, data, id) {
    var f = id ? _jstCache[id] : null
    //cache compiled template
    if (!f) {
        var executionContext = context.getExecutionContext(data)
        var compiler = new JstCompiler(executionContext)
        var compiled = compiler.compileJst(JstUtil.preProcess(Util.deepClone(expression)))
        f = new Function("data", "__methods", 'return ' + compiled + ";")
        if (id) {
            _jstCache[id] = f
        }
    }
    return f(data, methodContext)
}

/**
* Reset caches. Used during debugging.
*/
var purgeCache = function() {
    _jstCache = {}
    _expressionCache = {}
    JstUtil.clearExpressionCache()
}

module.exports = {
    evalObject: evalObject,
    evalExpression: evalExpression,
    purgeCache: purgeCache
}