/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

function Error(message, code, data, path, cause, originalError) {
    this.message = message
    if (code) {
        this.code = code
    }
    if (data) {
        this.data = data
    }
    if (path) {
        this.path = path
    }
    if (cause) {
        this.cause = cause
    }
    if (originalError) {
        this.originalError = originalError
    }
}

Error.prototype.getMessage = function() {
    return this.message
}

Error.prototype.getData = function() {
    return this.data
}

Error.prototype.getCode = function() {
    return this.code
}

module.exports = Error