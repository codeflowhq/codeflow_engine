/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co)
 * 
 * Copyright 2016 Codeflow Private Limited 
 * 
 * MIT Licensed
 */
'use strict'

var Step = require('./step')
var Transition = require('./transition')
var Eval = require('./eval')
var Util = require('./util')
var async = require('async')
var Constants = require('./constants')

function Flow() {
    this.steps = {}
    this.transitions = []
    this.meta = {}
    this.fileName = null
    this.loaded = false
    this.startStep = null
    this.isTriggerType = false
    this.endStep = null
    return this
}

Flow.prototype.load = function(fileName, data, callback) {
    var self = this
    this.fileName = fileName
    //below will parse and create a map of Step and Transition objects
    this.buildGraph(data, function(err) {
        if (err) {
            return callback(err)
        } 
        //self.loadSteps(function() {            
        //below will find and cache fields start, end and if the start is a trigger
        //for faster lookup
        self.findStepTypes(function(err) {
            if (err) {
                return callback(err)
            }
            self.markIsAsync()
            return callback(null, self)
        })
        //})
    })
}

Flow.prototype.parseFlowData = function(flowData) {
    var graph
    if (typeof flowData != 'undefined') {
        if (typeof flowData == 'object' && flowData && (flowData.activities || flowData.steps)) {
            graph = flowData
        } else if (typeof flowData == 'string') {
            // convert scripts passed as string to JSON obj
            try {
                graph = JSON.parse(flowData)
            } catch(err) {
                //
            }
        }
    }
    //for backward compatibility
    if (graph && graph.activities) {
        graph.steps = graph.activities
        delete graph.activities
    }
    return graph
}

Flow.prototype.buildGraph = function(flowData, callback) {
    var self = this
    var fileName = this.fileName
    var graph = this.parseFlowData(flowData)
    if (!graph) {
        return callback(new Error("Could not parse flow file " + fileName))
    }
    var i, id
    var steps = Object.keys(graph.steps)
    for (i = 0; i < steps.length; i++) {
        id = steps[i]
        var props = graph.steps[id]
        var step = new Step(id, props, self.fileName)
        self.steps[id] = step
    }
    var keys = Object.keys(graph.transitions)
    for (i = 0; i < keys.length; i++) {
        var transProp = graph.transitions[keys[i]]
        id = keys[i]
        if (transProp.to !== transProp.from) { // prevent self reference
            var to = self.steps[transProp.to]
            var from = self.steps[transProp.from]
            if (from && to) {
                var transition = new Transition(id, transProp)
                transition.from = from
                transition.to = to
                transition.type = transProp.type
                to.addIncoming(transition)
                from.addOutgoing(transition)
                self.transitions[id] = transition
            }
        }
    }
    self.meta = graph.meta
    return callback(null, self)
}


/** 
 * method to fully load steps 
 */
Flow.prototype.loadSteps = function(cb) {
    var self = this
    if (this.loaded) {
        return cb()
    }
    var steps = Object.keys(this.steps)
    async.eachSeries(steps, function(id, next) {
        self.steps[id].load(function(error) {
            next(error)    
        })
    }, function(err) {
        self.loaded = true
        return cb(err)
    })
}

Flow.prototype.findStepTypes = function(callback) {
    var steps = this.steps
    var step, keys = Object.keys(steps)
    for (var i = 0; i < keys.length; i++) {
        step = steps[keys[i]]
        if (step.isStart || step.isTrigger) {
            this.startStep = step
            if (step.isTrigger) {
                this.isTriggerType = true
            }
        } else if (step.isEnd) {
            this.endStep = step
        }
        if (this.startStep && this.endStep) {
            break
        }
    }
    if (!this.startStep) {
        return callback(new Error("Invalid flow format. Start not found."))
    }
    return callback(null)
}

/**
 * Mark if each step has a link to the end or error step.
 * This is done only once per flow and will speed up calculations
 * during instance level calcluations.
 */
Flow.prototype.markIsAsync = function() {
    var flow = this
    var steps = this.steps
    var step, keys = Object.keys(steps)
    for (var i = 0; i < keys.length; i++) {
        step = steps[keys[i]]
        if (step.isStart || step.isTrigger
            || step.isEnd || step.isError) {
            step.isAsync = false
        } else {
            step.isAsync = this.checkIsAsync(step)
        }
    }
}

Flow.prototype.isEndStep = function(step) {
    return (step.isError || step.isEnd)
}

//trace path till end or error. 
Flow.prototype.checkIsAsync = function(step) {
    var stack = []
    var stepMap = {}
    var tmp = step
    while(tmp) {
        for (var i = 0; i < tmp.outgoing.length; i++) {
            var trans = tmp.outgoing[i]
            if (this.isEndStep(trans.to)) {
                return false
            } 
            if (!stepMap[trans.to.id]) {
                stack.push(trans.to)
            }            
        }
        tmp = stack.pop()
        if (tmp) {
            stepMap[tmp.id] = tmp
        }
    }
    return true
}

//@deprecated - not used anymore
//find list of steps leading to current step
Flow.prototype.traceIncomingPath = function(step) {
    var	stack = []
    var stepMap = {}
    var tmp = step
    while(tmp) {
        for (var i = 0; i < tmp.incoming.length; i++) {
            var trans = tmp.incoming[i]
            if (!stepMap[trans.from.id]) {
                stack.push(trans.from)
            }
        }
        tmp = stack.pop()
        if (tmp) {
            stepMap[tmp.id] = tmp
        }
    } 
    return stepMap
}

/**
 * This is a very critical method. Here we are creating a new stack object and
 * it's corresponding array. This is required to be passed to function expressions.
 * As function expressions would only take array - we have to create object stack and 
 * corresponding array. 
 */
Flow.prototype.getStackArray = function() {
    var steps = this.steps
    var keys = Object.keys(steps)
    var stack = {}
    var isGroup = this.meta && this.meta._isGroup
    var stackArray = []
    for (var i = 0; i < keys.length; i++) {
        var id = keys[i]
        var step = steps[id]        
        if (isGroup && (step.ref == Constants.START_REF || step.ref == Constants.END_REF)) {
            continue
        }
        stackArray[i] = stack[id] = {}         
    }
    return {
        stack : stack,
        stackArray: stackArray
    }
}

module.exports = Flow