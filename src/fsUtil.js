/**
 * Author Murukesh Sadasivan (murukesh@codeflow.co), Alias Eldhose (aliaseldhose@codeflow.co)
 *
 * Copyright 2016 Codeflow Private Limited
 *
 * MIT Licensed
 */
'use strict'

var fs = require('fs')
var path = require('path')
var zlib = require('zlib')
var crypto = require('crypto')
var tar = require('tar-fs')
var util = require('./util')

var EXCLUDED_PATHS = [
    '.',
    '..',
    '.DS_Store',
    '.codeflow',
    '.git',
    'node_modules'
]

/**
 * From https://github.com/anodynos/upath
 * @param p
 * @returns {*}
 */
exports.toUnixPath = function(p) {
    p = p.replace(/\\/g, '/')
    var double = /\/\//
    while(p.match(double)) {
        p = p.replace(double, '/') //node on windows doesn't replace doubles
    }
    return p
}
/**
 * List all the files in the directory and its sub directory
 *
 * @param directory
 * @param cb
 * @param [options]
 * @param [level]
 */
exports.listFileTree = function(directory, cb, options, level) {
    var files = []
    var tree = {}
    level = level || 0
    options = options || {}
    var excludedPaths = EXCLUDED_PATHS.concat(options.excludedPaths)
    //read the directory files
    return new Promise(async (resolve, reject)=>{
        try {
            let fileList = await fs.promises.readdir(directory, { withFileTypes: true })
            util.iterate(fileList, async(file, next) => {
                if (!file || excludedPaths.indexOf(file.name) > -1 || file.name[0] == '_') {
                    return next()
                }
                var filePath = path.join(directory, file.name)
                let isDir = file.isDirectory()
                let isLink = file.isSymbolicLink()
                if(!isDir && isLink) {
                    try {
                        isDir = fs.statSync(filePath).isDirectory()
                    } catch(e) {
                        return next()
                    }
                }
                if (!isDir) {
                    files.push(filePath)
                    tree[file.name] = filePath
                    return next()
                }

                tree[file.name] = []
                if (typeof options.level !== 'undefined' && level >= options.level) {
                    return next() //restrict listing only to the level specified
                }
                let {files: dirFiles, tree: dirTree} = await this.listFileTree(filePath, null, options, level + 1)
                files = files.concat(dirFiles)
                tree[file.name] = dirTree
                next()
            }, function() {
                return cb ? cb(false, files, tree) : resolve({files, tree})
            })
        } catch (e) {
            cb ? cb(e.message) : reject(e)
        }
    })
}

/**
 * Read from file into JSON object
 *
 * @param fileName
 * @param cb
 */
exports.readJsonFile = function(fileName, cb) {
    return new Promise(async(resolve, reject)=>{
        try {
            let stats = await fs.promises.stat(fileName)
            if(!stats || !stats.isFile()) {
                throw new Error(`Invalid File ${fileName}`)
            }

            let data = await fs.promises.readFile(fileName, 'utf8')
                try {
                    let jsonData = JSON.parse(data)
                    return cb ? cb(false, data) : resolve(jsonData)
                } catch(e) {
                    throw new Error(`Error reading file ${fileName}, Error: ${e.toString()}`)
                }
        } catch(e) {
            cb ? cb(e.message) : reject(e)
        }
    })
}

/**
 * Get the MD5 checksum for a file
 * https://github.com/roryrjb/md5-file
 *
 * @param filePath
 * @param cb
 * @returns {*}
 */
exports.md5Checksum = function (filePath, cb) {
    var sum = crypto.createHash('md5')

    if (typeof cb === 'function') {
        var fileStream = fs.createReadStream(filePath)

        fileStream.on('data', function (chunk) {
            try {
                sum.update(chunk)
            } catch (ex) {
                return cb(ex.toString(), null)
            }
        })
        fileStream.on('end', function () {
            return cb(null, sum.digest('hex'))
        })
        fileStream.on('error', function (err) {
            return cb(err)
        })
    } else {
        sum.update(fs.readFileSync(filePath))
        return sum.digest('hex')
    }
}

/**
 * Compress a directory
 *
 * @params dirPath
 * @param cb
 * @private
 */
exports.compress = async(dirPath, cb) => {
    let target = path.join(path.dirname(dirPath), dirPath.split(path.sep).pop() +'.tar.gz')
    try {
        let stats = await fs.promises.stat(dirPath)
        if(!stats.isDirectory()) {
            return cb('Error, the path supplied for compression is not a directory')
        }
        const exists = !!(await fs.promises.stat(target).catch(() => null))
        if (exists) {
            return cb(new Error('File already exists'), target)
        }

        let compressed = tar.pack(dirPath).pipe(zlib.createGzip()).pipe(fs.createWriteStream(target))
        compressed.on('finish', function() {
            cb(false, target)
        })
    } catch(e) {
        return cb('Error accessing the directory for compression')
    }
}

exports.uncompress = async(filePath, destinationPath, deleteSource, cb) => {
    if (typeof deleteSource == 'function') {
        cb = deleteSource
        deleteSource = false
    }
    if (typeof destinationPath == 'function') {
        cb = destinationPath
        destinationPath = false
    }
    if (!destinationPath) {
        destinationPath = path.join(path.dirname(filePath), path.basename(filePath).split('.').shift())
    }

    try {
        await extract(filePath, destinationPath)
        cb(null, destinationPath)
    } catch(e) {
        return cb(e.message)
    }
}

const extract = (filePath, dir) => {

    return new Promise(async (resolve, reject)=> {
        let fileStream = fs.createReadStream(filePath)
        let ext = path.extname(filePath).split('.').pop()
        if(!ext === 'gz' && ext !== 'tar') {
            throw new Error('Unsupported file format. Ony tar and gz are supported for now.')
        }

        let pipe
        if(ext === 'gz') {
            pipe = fileStream.pipe(zlib.createGunzip()).pipe(tar.extract(dir))
        } else {
            pipe = fileStream.pipe(tar.extract(dir))
        }

        pipe.on('finish', resolve)
        pipe.on('error', ()=>{
            reject('Error extracting file' + e.message)
        })
    })
}