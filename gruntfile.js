module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            all: ['build/',  'release/'],
            build:['build/']            
        },  
        copy: {
            main: {
                files: [{
                    expand: true, 
                    src: [
                        'bin/**/*',
                        'Readme.md',
                        'LICENSE',
                        'command.js',
                        'package-lock.json',
                        'package.json',
                        'index.js'
                    ], 
                    dest: 'build/'
                }]
            },
            src: {
				files: [{
			 	    expand: true,
				    src: ['src/**/**'],
				    dest: 'build/'
				}]
            },
            packages: {
                files: [{
                    expand: true,
                    src: [
                        '.packages/**/*', 
                        '!.packages/**/node_modules/**', 
                        '!.packages/**/.packages/**'
                    ],
                    dest: 'build/'
                }]
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'tar',
                    archive: 'release/codeflow-<%=pkg.version%>.tar'
                },
                files: [{
                    expand: true,
                    cwd:'build',
                    src: '**',
                    dest: ''
                }]
            }
        }  
    })
    grunt.loadNpmTasks('grunt-contrib-clean')
    grunt.loadNpmTasks('grunt-contrib-copy')
    grunt.loadNpmTasks('grunt-contrib-compress')
    grunt.registerTask('default', ['copy:main', 'copy:packages', 'copy:src', 'compress'])
    grunt.registerTask('clean', ['clean:all'])
}
