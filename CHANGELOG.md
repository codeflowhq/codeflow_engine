# Change log

## 0.7.6

- Removing uncommitted files from build
- Minor cleanup

## 0.7.2

- Fixing install command

## 0.7.1

- Added streams package
- Added stream modules in file package
- Added middleware(s)

## 0.7.0

- Renamed dictionary to objectStore
- Compiled code generator fixes and optimization
- Other minor udpates

## 0.4.7

- Fixing compiler bugs
- Fixing assert module reference
- Using template literal for string escaping
- Added test cases for checking subflow input and output

## 0.4.6

- Start of change log