/**
 * Performance comparison testing of various ways to execute a javascript 
 * expression inside Node.js
 **/
var vm = require('vm');

var data = {
    name: "murukesh",
    age: {obj: 30}, 
    email: function() {
        return  'murukesh@gmail.com'
    }
};

var context = vm.createContext(data)
var runInProxy = function(code) {
    return new Function(["data"], 
    "with(data) { return (function(){return " 
        + code 
     + "})()}"
    )
}

var runInProxyWithoutWith = function(args, code) {
    return new Function([args], "return " + code )
}

var template = '"Hello @"+ name + ", you are " + (age.obj+age) + " " + email() + "!"'

var proxyFn = runInProxy(template)

var keys = []
var values = []
for (var key in data) {
    if (data.hasOwnProperty(key)) {
        keys.push(key)
        values.push(data[key])
    }
}
var withoutWith = runInProxyWithoutWith(keys, template)

var functionPlain = function(data) {
    return "Hello @" + data.name + ', you are ' + data.age + ' ' + data.email()+ '!';
}

var functionProxyWithoutWith = function(data) {
    //return "Hello @" + data.name + ', you are ' + data.age + ' ' + data.email()+ '!';
    //console.log(runInProxyWithoutWith(keys, template).toString())
    return withoutWith.apply(null, values)
}

var functionProxy = function(data) {
    return proxyFn(data)
}

var functionVm = function(context) {
    
    return vm.runInContext(template, context)
}

var functionEval = function(data) { 
    return eval('"Hello @"+ data.name + ", you are " + data.age + " " + data.email() + " !"')    
}

var limit = 10000, t1, t2, output;

t1 = Date.now();		
for (i = 0; i < limit; i++) {
    output = functionPlain(data)
}	
t2 = Date.now();
console.log("\nfunctionPlain took " + (t2 - t1) + ' mils to give ' + output);

t1 = Date.now();		
for (i = 0; i < limit; i++) {
    output = functionProxy(data)
}	
t2 = Date.now();
console.log("\nfunctionProxy took " + (t2 - t1) + ' mils to give ' + output);


t1 = Date.now();		
for (i = 0; i < limit; i++) {
    output = functionProxyWithoutWith(data)
}	
t2 = Date.now();
console.log("\nfunctionProxyWithoutWith took " + (t2 - t1) + ' mils to give ' + output);

data.age.obj = 33
t1 = Date.now();		
for (i = 0; i < limit; i++) {
    output = functionProxyWithoutWith(data)
}	
t2 = Date.now();
console.log("\nfunctionProxyWithoutWith took " + (t2 - t1) + ' mils to give ' + output);

t1 = Date.now();
for (i = 0; i < limit; i++) {
    output = functionEval(data)
}	
t2 = Date.now();
console.log("\neval took " + (t2 - t1) + ' mils to give ' +  output);

t1 = Date.now();
for (i = 0; i < limit; i++) {
    output = functionVm(context)
}	
t2 = Date.now();
console.log("\nvm.run took " + (t2 - t1) + ' mils to give ' +  output);
