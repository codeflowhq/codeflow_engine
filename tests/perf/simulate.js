/**
 * Simulating a mini engine execution to capture 
 * performance to do bare minimal plumbing.
 * 
 * Running this code on a 2012 Macbook pro for 100,000 runs is taking 1.4-2.5 
 * seconds.
 * Running a flow with a simple iterator on the other hand is taking around 
 * 4 seconds. Almost 2x difference between bare metal code vs a flow code.
 * 
 * This code can show how much improvement can be made by compiling flow to 
 * native javascript vs interpreting it. Currently its not too much, as the
 * actual slowing down is caused by the asynchronous loop and the trampolining
 * that is required (setImmediate or process.nextTick).
 * 
 */
 
var t = Date.now()
var fs = require('fs')
var flow = fs.readFileSync('iter.flw')
var async = require('async')
var _ = require('underscore')
var EE = require('../../src/eval.js')
var Flow = require('../../src/flow.js')
var Instance = require('../../src/instance.js')
var Loader = require('../../src/loader.js')
var Status = require('../../src/status.js')
var Path = require('path')

console.log(Date.now() - t)
var Engine = require('../../src/engine.js')
var engine = new Engine({
    debug: false
})

var fileName = 'iter.flw'
var params = {
    file: Path.join(process.cwd(), fileName),
    name: fileName
}
global.ROOT_DIR = process.cwd()

var collection = []
var length = 100000

if (typeof length === "number" && length > 0) {
    collection = Array(length+1)
    for (var i = 0, k = 0; k <= length; k++, i++) {
        collection[i] = k
    }
}

var isObj = false
if (typeof collection == 'object') {
    if (!_.isArray(collection)) {
        collection = Object.keys(collection)
        isObj = true
    }
}
var index = 0
var options = {stack: {}}
console.log("processing ", collection.length)
async.eachSeries(collection, function(elem, cb)  {
    setImmediate(function() {
        someFunction(true, function() {
            var key = isObj ? elem: index++
            var value = isObj ? collection[elem] : elem
            options.stack["key"] = {
                value: value,
                index: key
            }
            var subFlowInput = 0
            try {
            subFlowInput = EE.evalObject("add(10, key.index)", options.stack, "this")
            } catch(e) {
                
            }
            //console.log(subFlowInput)
            cb(null, subFlowInput)
        })
    })
}, function(err, data) {
    console.log("done", Date.now() - t)
})

Instance.prototype.getModuleInvoker = function(id) {
    return {
        id: id,
        status: Status.STEP_STATUS.READY,
        execute: function(cb) {
            fakeExecute(cb)
        }
    }
}
var isPaused = false

var someFunction = function(data, cb) {
    Loader.loadPackages(function() {
        var a = {}
        Loader.loadFlow(params, function(flow) {
            load(flow, data, cb)
        })
    })
}

var load = function(flow, data, cb) {
    var stepList = []
    var instance = new Instance(engine, flow, params , this, 'test')
    var someArr = new Array()
    if (data) {
        cb ()
    }
}

var fakeExecute = function(cb) {
    //console.log("I am called")
    cb && cb()
}

function someFn() {
    this.data = []
}