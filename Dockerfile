FROM mhart/alpine-node:14.16.1
LABEL maintainer="Codeflow team"

RUN npm i codeflow@0.7.10 -g

RUN mkdir /app
WORKDIR /app

ENTRYPOINT ["codeflow"]

CMD ["-h"]
